# Template #

Contém um template para desenvolvimento de módulos Java

## Features

* Executa standalone e no Tomcat
* Monta imagem Docker
* Testes automatizados
* Micrometer
* spring actuator com Health Check, Prometheus
* Log com cronometro e msisdn
* Rest Template com pool de conexões e timeout
* Cache
* DB versionada com Fly Way
* Spring Security - estático
* Spring Data
* Suporte a configuração com arquivo externo

### Utilização
Para utilizar, clone o repositório para um novo e faça as seguintes alterações:

* Renomeie os packages de br.com.supportcomm.template para br.com.supportcomm.modulo (ramos main e test)
* Altere as referencia ao template no POM, em artifactId, name, description, start-class e docker.image.name
* Altere a configuração do logback, nas classes dos encoders (ramos main e test)
* Altere o application.properties, acerte o package no nível do log e crie a variável LOG_FILE
* Altere as constantes na classe AppConfig

### Docker
Para gerar o conteiner docker, deve-se ter o docker rodando e as variáveis de ambiente necessárias ao Docker (docker-machine.exe env) devem estar configuradas corretamente. 
