package br.com.supportcomm.template.h2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class HistoricoDTOJdbcRepository {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public boolean findIfFileIsInTable(String file) {
		HistoricoDTO obj = jdbcTemplate.queryForObject("select * from historico where arquivo=?", new Object[] {
				file
				},
				new BeanPropertyRowMapper<HistoricoDTO>(HistoricoDTO.class));
		
		if ( obj != null)
			return true;
		else
			return false;
	}
	
	public void insertScenario1() {
		String query = "INSERT INTO historico " +
				   "(historicoid, datadeprocessamento, datadereferencia, cargafull, numeroderegistrosprocessados, arquivo, processado) " +
			       "values " +
			       "(?,?,?,?,?,?,?)";
				   //"(%d,%s,%s,%d,%d,%s,%d)";
		

		
		jdbcTemplate.update(query,
				new Object[] {
						1,"2020-08-13 16:39:41.179","2020-08-06 11:50:50",0,29, "BDT20200211115050.txt",1
				});
		
		jdbcTemplate.update(query,
				new Object[] {
						1,"2020-08-13 13:34:42.179","2020-08-06 11:50:50",0,33, "BDT20200343115050.txt",1
				});
		
		jdbcTemplate.update(query,
				new Object[] {
						1,"2020-08-13 10:19:31.179","2020-08-06 11:50:50",0,48, "BDT20200411145050.txt",1
				});
		
		jdbcTemplate.update(query,
				new Object[] {
						1,"2020-08-13 11:38:51.179","2020-08-06 11:50:50",0,69, "BDT20200721111650.txt",1
				});
		
	}
}
