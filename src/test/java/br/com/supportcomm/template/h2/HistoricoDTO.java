package br.com.supportcomm.template.h2;

import java.sql.Timestamp;

import javax.persistence.Entity;



@Entity
public class HistoricoDTO {


	@Override
	public String toString() {
		return "HistoricoDTO [historicoId=" + historicoId + ", version=" + version + ", processado=" + processado
				+ ", arquivo=" + arquivo + ", dataDeReferencia=" + dataDeReferencia + ", dataDeProcessamento="
				+ dataDeProcessamento + ", cargaFull=" + cargaFull + ", numeroDeRegistrosProcessados="
				+ numeroDeRegistrosProcessados + "]";
	}


	public Long getHistoricoId() {
		return historicoId;
	}


	public void setHistoricoId(Long historicoId) {
		this.historicoId = historicoId;
	}


	public int getVersion() {
		return version;
	}


	public void setVersion(int version) {
		this.version = version;
	}


	public int getProcessado() {
		return processado;
	}


	public void setProcessado(int processado) {
		this.processado = processado;
	}


	public String getArquivo() {
		return arquivo;
	}


	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}


	public Timestamp getDataDeReferencia() {
		return dataDeReferencia;
	}


	public void setDataDeReferencia(Timestamp dataDeReferencia) {
		this.dataDeReferencia = dataDeReferencia;
	}


	public Timestamp getDataDeProcessamento() {
		return dataDeProcessamento;
	}


	public void setDataDeProcessamento(Timestamp dataDeProcessamento) {
		this.dataDeProcessamento = dataDeProcessamento;
	}


	public int getCargaFull() {
		return cargaFull;
	}


	public void setCargaFull(int cargaFull) {
		this.cargaFull = cargaFull;
	}


	public int getNumeroDeRegistrosProcessados() {
		return numeroDeRegistrosProcessados;
	}


	public void setNumeroDeRegistrosProcessados(int numeroDeRegistrosProcessados) {
		this.numeroDeRegistrosProcessados = numeroDeRegistrosProcessados;
	}


	private Long historicoId;
	
	
	private int version;
	
	
	private int processado;
	
	
	private String arquivo;

	
	private Timestamp dataDeReferencia;
	

	private Timestamp dataDeProcessamento;
	

	private int cargaFull;
	

	private int numeroDeRegistrosProcessados;
}
