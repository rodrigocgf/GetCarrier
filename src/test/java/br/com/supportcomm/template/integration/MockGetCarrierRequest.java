package br.com.supportcomm.template.integration;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.com.supportcomm.business.ProcessRequest;

import br.com.supportcomm.controllers.GetCarrierController;
import br.com.supportcomm.controllers.NoAuthenticationController;

@RunWith(SpringRunner.class)
public class MockGetCarrierRequest {
	
	private MockMvc mockMvc;	
	
	@Mock
	private ProcessRequest processRequest;
	
	@InjectMocks
	private GetCarrierController getCarrier;
	
	@InjectMocks
	private NoAuthenticationController noAuth;
	
	@Test
	public void testNoAuthentication() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(noAuth).build();
		
		mockMvc.perform(
				MockMvcRequestBuilders.get("/noauthentication")
				.param("name","you")
		)		
				.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void testEmptyParameters() throws Exception {
		
		mockMvc = MockMvcBuilders.standaloneSetup(getCarrier).build();
		
		ResultMatcher err400 = MockMvcResultMatchers.status().is4xxClientError();

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/getcarrierJunit");
		
		mockMvc.perform(builder).andExpect(err400);
		
	}
	
	@Test
	public void testEmptyParametersCheckResponse() throws Exception {
		
		mockMvc = MockMvcBuilders.standaloneSetup(getCarrier).build();
				
		ResultMatcher err400 = MockMvcResultMatchers.status().is4xxClientError();
		ResultMatcher expectedContent = MockMvcResultMatchers.content().string("<html>\n" + 
				"<head>\n" + 
				"	<title>Erro</title>\n" + 
				"</head>\n" + 
				"<body>\n" + 
				"<H1>Mensagem</H1>\n" + 
				"<H2>Operacao nao especificada.</H2>\n" + 
				"</body>\n" + 
				"</html>\n");

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/getcarrierJunit");
		
		
		
		MvcResult result = mockMvc.perform(builder)
			.andExpect(err400)
			.andExpect(MockMvcResultMatchers.content().contentType("text/html;charset=UTF-8"))
			.andExpect(expectedContent)
			.andReturn();
		
		String content = result.getResponse().getContentAsString();
		
		System.out.println(content);
	
		
	}
	
	@Test
	public void testMissingFormatoParameterCheckResponse() throws Exception {
		
		mockMvc = MockMvcBuilders.standaloneSetup(getCarrier).build();
				
		ResultMatcher err400 = MockMvcResultMatchers.status().is4xxClientError();		
		
		ResultMatcher expectedContent = MockMvcResultMatchers.content().string("<html>\n" + 
				"<head>\n" + 
				"	<title>Erro</title>\n" + 
				"</head>\n" + 
				"<body>\n" + 
				"<H1>Mensagem</H1>\n" + 
				"<H2>Formato nao especificado.</H2>\n" + 
				"</body>\n" + 
				"</html>\n");

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/getcarrierJunit")
				.param("operacao", "buscar")
				.param("msisdn", "5511999999999");
				
		
		mockMvc.perform(builder)
			.andExpect(err400)
			.andExpect(MockMvcResultMatchers.content().contentType("text/html;charset=UTF-8"))
			.andExpect(expectedContent);	
		
	}
	
	@Test
	public void testOnlyOperacaoParameterCheckResponse() throws Exception {
		
		mockMvc = MockMvcBuilders.standaloneSetup(getCarrier).build();
				
		ResultMatcher err400 = MockMvcResultMatchers.status().is4xxClientError();
		ResultMatcher ok = MockMvcResultMatchers.status().isOk();
		
		ResultMatcher expectedContent = MockMvcResultMatchers.content().string("<html>\n" + 
				"<head>\n" + 
				"	<title>Erro</title>\n" + 
				"</head>\n" + 
				"<body>\n" + 
				"<H1>Mensagem</H1>\n" + 
				"<H2>Operacao qqq desconhecida.</H2>\n" + 
				"</body>\n" + 
				"</html>\n");

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/getcarrierJunit")
				.param("operacao", "qqq")
				.param("msisdn", "5511999999999");
				
		
//		mockMvc.perform(builder)
//			.andExpect(err400)
//			.andExpect(MockMvcResultMatchers.content().contentType("text/html;charset=UTF-8"))
//			.andExpect(expectedContent);	
		
		MvcResult result = mockMvc.perform(builder)
			.andExpect(err400)
			.andExpect(MockMvcResultMatchers.content().contentType("text/html;charset=UTF-8"))
			.andExpect(expectedContent)
			.andReturn();
		
		String content = result.getResponse().getContentAsString();
		
		System.out.println(content);
		
	}
	
	@Test
	public void testMissingOperacaoParameterCheckResponse() throws Exception {
		
		mockMvc = MockMvcBuilders.standaloneSetup(getCarrier).build();
				
		ResultMatcher err400 = MockMvcResultMatchers.status().is4xxClientError();		
		
		ResultMatcher expectedContent = MockMvcResultMatchers.content().string("<html>\n" + 
				"<head>\n" + 
				"	<title>Erro</title>\n" + 
				"</head>\n" + 
				"<body>\n" + 
				"<H1>Mensagem</H1>\n" + 
				"<H2>Operacao nao especificada.</H2>\n" + 
				"</body>\n" + 
				"</html>\n");

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/getcarrierJunit")
				.param("formato", "html")
				.param("msisdn", "5511999999999");
				
		
		mockMvc.perform(builder)
			.andExpect(err400)
			.andExpect(MockMvcResultMatchers.content().contentType("text/html;charset=UTF-8"))
			.andExpect(expectedContent);	
		
	}
	
	
	@Test
	public void testOnlyFormatoParameterCheckResponse() throws Exception {
		
		mockMvc = MockMvcBuilders.standaloneSetup(getCarrier).build();
				
		ResultMatcher err400 = MockMvcResultMatchers.status().is4xxClientError();
		ResultMatcher ok = MockMvcResultMatchers.status().isOk();
		
		ResultMatcher expectedContent = MockMvcResultMatchers.content().string("<html>\n" + 
				"<head>\n" + 
				"	<title>Erro</title>\n" + 
				"</head>\n" + 
				"<body>\n" + 
				"<H1>Mensagem</H1>\n" + 
				"<H2>Formato qqq desconhecido.</H2>\n" + 
				"</body>\n" + 
				"</html>\n");

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/getcarrierJunit")
				.param("operacao", "buscar")
				.param("formato", "qqq")
				.param("msisdn", "5511999999999");
				
		
		mockMvc.perform(builder)
			.andExpect(err400)
			.andExpect(MockMvcResultMatchers.content().contentType("text/html;charset=UTF-8"))
			.andExpect(expectedContent);	
		
	}
	
	@Test
	public void testNoOperadoraFoundCheckResponse() throws Exception {
		
		mockMvc = MockMvcBuilders.standaloneSetup(getCarrier).build();
				
		ResultMatcher err400 = MockMvcResultMatchers.status().is4xxClientError();
		ResultMatcher ok = MockMvcResultMatchers.status().isOk();
		
		ResultMatcher expectedContent = MockMvcResultMatchers.content().string("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2//EN\">\n" + 
				"<html>\n" + 
				"<head>\n" + 
				"<title>Portabilidade</title>\n" + 
				"</head>\n" + 
				"<body>\n" + 
				"<table>\n" + 
				"<tbody>\n" + 
				"<H1>Mensagem</H1><H2>Nao foi encontrado operadora para o MSISDN 55119999999999</H2></tbody></table></body>\n" + 
				"</html>\n");

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/getcarrierJunit")
				.param("operacao", "buscar")
				.param("formato", "html")
				.param("msisdn", "55119999999999");
				
		
		mockMvc.perform(builder)
			.andExpect(ok)			
			.andExpect(expectedContent);	
		
	}
	
}
