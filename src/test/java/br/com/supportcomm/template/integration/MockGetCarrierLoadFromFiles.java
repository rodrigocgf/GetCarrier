package br.com.supportcomm.template.integration;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.function.BiConsumer;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.com.supportcomm.GetCarrier;
import br.com.supportcomm.business.Msisdn;
import br.com.supportcomm.business.MsisdnExt;
import br.com.supportcomm.business.ProcessRequest;
import br.com.supportcomm.config.AppConfig;
import br.com.supportcomm.controllers.GetCarrierController;
import br.com.supportcomm.controllers.NoAuthenticationController;
import br.com.supportcomm.jparepositories.HistoricoRepository;
import br.com.supportcomm.jparepositories.AreaRepoImpl;
import br.com.supportcomm.jparepositories.HistoricoRepoImpl;
import br.com.supportcomm.jparepositories.NumeroPortadoRepoImpl;
import br.com.supportcomm.jparepositories.OperadoraRepoImpl;
import br.com.supportcomm.jparepositories.PaisRepoImpl;
import br.com.supportcomm.jparepositories.SerieRepoImpl;
import br.com.supportcomm.log.LogstashEncoderPattern;
import br.com.supportcomm.models.db.Historico;
import br.com.supportcomm.util.ArquivoAbr;
import br.com.supportcomm.util.FiltroDeArquivosAbrDelta;
import br.com.supportcomm.util.FiltroDeArquivosAbrFull;
import br.com.supportcomm.util.ProcessAbrLineBlock;
import br.com.supportcomm.util.SystemUtils;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
public class MockGetCarrierLoadFromFiles {
	
	public static Logger logger = LoggerFactory.getLogger(GetCarrier.class);
	
	private MockMvc mockMvc;
	
	
	@InjectMocks
	private GetCarrierController getCarrier;
	
	@InjectMocks
	private NoAuthenticationController noAuth;
	
	@Test
	public void testNoAuthentication() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(noAuth).build();
		
		mockMvc.perform(
				MockMvcRequestBuilders.get("/noauthentication")
				.param("name","you")
		)		
				.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	
	private static void startupProperties(BiConsumer<String,String> propertyHandler) {
		AppConfig.printSystemInfo();
		Properties propFile = null;
		
		String propertyFile = SystemUtils.getPropertyOrEnvironment(AppConfig.CONFIG_EXTERNAL_FILE, AppConfig.NAME_EXTERNAL_FILE);
		
    	if (propertyFile != null && !"OFF".equalsIgnoreCase(propertyFile) && SystemUtils.fileExists(propertyFile)) {
		
    		propertyHandler.accept("spring.config.location", "classpath:application.properties,file:" + propertyFile);
    		propFile = SystemUtils.loadFile(propertyFile);
    	}
		Properties props = SystemUtils.loadResource("application.properties");
		
		if (props == null) {
			props = propFile;
		} else if (propFile != null) {
			props.putAll(propFile);
		}
		
		if (props != null) {
			setSystemProperties(props, "sc.basedir");
			setSystemProperties(props, "sc.carga.DiretorioEntradaDeCarga");
			setSystemProperties(props, "sc.carga.DiretorioSaidaDeCarga");
			setSystemProperties(props, "sc.carga.DiretorioIgnorados");
			setSystemProperties(props, "sc.carga.DiretorioRejeitados");
		}		
		
	}
	
	private static void setSystemProperties(Properties prop, String name) {
		setSystemProperties(prop, name, (String)null);
	}
	
	private static void setSystemProperties(Properties prop, String name, String valDef) {
		String value = Objects.toString(prop.get(name), valDef);
		if (value != null) {
			if (SystemUtils.getPropertyOrEnvironment(name, null) == null) {
				System.out.println(AppConfig.APP_NAME + ": " + name + "=" + value);
				System.setProperty(name, value);
			}
			else {
				System.out.println(AppConfig.APP_NAME + ": **Ignoring " + name + "=" + value + ". Using " + name + "=" + SystemUtils.getPropertyOrEnvironment(name, null));
			}
		}
	}
	
	@Test
	public void testValidateLoadFromFilesDirectories() throws Exception {
		
		startupProperties((key, value) -> {
			System.out.println("CONFIG MAIN: " + key + "=" + value); 
			System.setProperty(key, value);
		});
		
		String diretorioBase			  	= System.getProperty("sc.basedir");
		String diretorioEntradaDeCarga 	= System.getProperty("sc.carga.DiretorioEntradaDeCarga").replace("${sc.basedir}", diretorioBase);
		String diretorioSaidaDeCarga 	= System.getProperty("sc.carga.DiretorioSaidaDeCarga").replace("${sc.basedir}", diretorioBase);
		String diretorioIgnorados 		= System.getProperty("sc.carga.DiretorioIgnorados").replace("${sc.basedir}", diretorioBase);
		String diretorioRejeitados 		= System.getProperty("sc.carga.DiretorioRejeitados").replace("${sc.basedir}", diretorioBase);
				
		Path pathEntrada 		= Paths.get(diretorioEntradaDeCarga);
		Path pathSaida 		= Paths.get(diretorioSaidaDeCarga);
		Path pathIgnorados 	= Paths.get(diretorioIgnorados);
		Path pathRejeitados 	= Paths.get(diretorioRejeitados);
		
		
		File dirIn = pathEntrada.toFile();
		File dirOut = pathSaida.toFile();
		File dirIgnorados = pathIgnorados.toFile();
		File dirRejeitados = pathRejeitados.toFile();
		
		assertEquals(true , dirIn.isDirectory() );
		
		assertEquals(true , dirOut.isDirectory() );
		
		assertEquals(true , dirIgnorados.isDirectory() );
		
		assertEquals(true , dirRejeitados.isDirectory() );		
		
	}	

}
