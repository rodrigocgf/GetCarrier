package br.com.supportcomm.template.integration;

import static org.junit.Assert.assertEquals;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.supportcomm.business.ProcessFileAbr;
import br.com.supportcomm.business.ProcessRequest;
import br.com.supportcomm.models.db.TelefoniaEnum;

@RunWith(SpringRunner.class)
public class MockParseFileLinesCargaDeltaAbr  {
	
	@InjectMocks
	private ProcessFileAbr processFileAbr;
	
	private Date dateOfReference = null;
	
	private final String first_Line_OK_AsInFile = "20191210135050;10072";
	
	private final String first_Line_Invalid_Date_Of_Generation_AsInFile = "20001210135050;10072";
	
	private final String firstLine_Invalid_Number_Of_Fields_AsInFile = "20191210135050;";
	
	private final String firstLine_Invalid_Date_AsInFile = "x*k:1515135050;10072";
	
	private final String firstLine_Invalid_Number_Of_Lines_AsInFile = "20191515135050;-105";
	
	private final String firstLine_Invalid_Number_Of_Lines_1_AsInFile = "20191515135050;abcd";
	
	private final String otherThanFirstLine_OK_AsInFile1 =   "76017464;0;11930763577;0;0;0351;823;20191210120000;20191210120000";															

	private final String otherThanFirstLine_OK_AsInFile2 =   "74824094;1;1126945687;;;;;;20191210120000";
	
	private final String otherThanFirstLine_Too_Short_AsInFile =   "74824094;1";
	
	private final String otherThanFirstLine_Invalid_Action_AsInFile =   "74824094;5;44";
	
	private final String otherThanFirstLine_Invalid_Telephone_Number_AsInFile =   "74824094;1;551199998888125";
	
	private final String otherThanFirstLine_Small_Number_Of_Fields_AsInFile =   "76017464;0;11930763577;5;";
	
	private final String otherThanFirstLine_Invalid_Line_Type_AsInFile =   "76017464;0;11930763577;5; ;0351;";
	
	private final String otherThanFirstLine_Invalid_Spid_AsInFile =   "76017464;0;11930763577;0; ;-135;";
	
		
	
	private final String linesAsInFile = 	  "20200211145050;5\r\n"
											+ "76017105;0;11930085885;0;0;0351;823;20191210120000;20191210120000\r\n"
											+ "76016711;0;1144881642;0;0;0123;203;20191210120000;20191210120000\r\n"
											+ "70464198;1;1146541128;;;;;;20191210120000\r\n"
											+ "78d08a0b41eef832d06dd3c1a6f89eca";
	
	private final String lineRemoveInFile = "46510880;1;11983620492;;;;;;20191210080703";
	
	public MockParseFileLinesCargaDeltaAbr() {		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		
		try {
			dateOfReference = formatter.parse("31/03/2010");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void MockRemoveCargaDeltaAbr() {
		ProcessFileAbr.LineEnum enumRet;
		String line = lineRemoveInFile;
		enumRet = processFileAbr.checkOthersThanFirstLineLoadDeltaAbr(line);
		
		assertEquals(enumRet, ProcessFileAbr.LineEnum.LINE_OK);
	}
	
	@Test
	public void MockCheckSumCalculation() {
		String [] lines = linesAsInFile.split("\r\n");		
		String[] fields;
		MessageDigest l_digest = null;
		int numberOfLines = 0;
		String checkSum = "0";
		int lineNumber = 1;
		
		try {
			l_digest = MessageDigest.getInstance("MD5");
			 
			l_digest.reset();
			
			for ( String line : lines ) {
				if ( lineNumber == 1) {
					
					fields = line.split(";");
					if ( fields.length == 2)
						numberOfLines 		= Integer.parseInt(fields[1].trim());
					else
						break;
				}
				
				if ( lineNumber != numberOfLines) {
					line += "\n";
					l_digest.update((line).getBytes());
				} else {
					checkSum = line;
				}			
				
				lineNumber += 1;
				
			}
		}
		catch (NoSuchAlgorithmException e) {			
			assert(false);
		}
		
		assertEquals( true , processFileAbr.executeCheckSum(checkSum,l_digest) );
	}
	
	@Test
	public void MockFirstLineOKCargaDeltaAbr() {
		ProcessFileAbr.LineEnum enumRet;
		String line = first_Line_OK_AsInFile;
		
		enumRet = processFileAbr.checkFirstLineLoadDeltaAbr(line , dateOfReference);
		assertEquals(enumRet, ProcessFileAbr.LineEnum.LINE_OK);
	}
	
	@Test
	public void MockFirstLineInvalidDateCargaDeltaAbr() {
		ProcessFileAbr.LineEnum enumRet;
		String line = firstLine_Invalid_Date_AsInFile;
		
		enumRet = processFileAbr.checkFirstLineLoadDeltaAbr(line , dateOfReference);
		assertEquals(enumRet, ProcessFileAbr.LineEnum.INVALID_DATE_FORMAT);
	}
	
	@Test
	public void MockFirstLineInvalidDateOfGenerationCargaDeltaAbr() {
		ProcessFileAbr.LineEnum enumRet;
		String line = first_Line_Invalid_Date_Of_Generation_AsInFile;
		
		enumRet = processFileAbr.checkFirstLineLoadDeltaAbr(line , dateOfReference);
		assertEquals(enumRet, ProcessFileAbr.LineEnum.INVALID_DATE_OF_GENERATION);
	}
	
	@Test
	public void MockFirstLineInvalidNumberOfFieldsCargaDeltaAbr() {
		ProcessFileAbr.LineEnum enumRet;
		String line = firstLine_Invalid_Number_Of_Fields_AsInFile;
		
		enumRet = processFileAbr.checkFirstLineLoadDeltaAbr(line , dateOfReference);
		assertEquals(enumRet, ProcessFileAbr.LineEnum.FISRT_LINE_OUT_OF_FORMAT);
	}
	
	@Test
	public void MockFirstLineInvalidNumberOfLinesCargaDeltaAbr() {
		ProcessFileAbr.LineEnum enumRet;
		String line = firstLine_Invalid_Number_Of_Lines_AsInFile;
		
		enumRet = processFileAbr.checkFirstLineLoadDeltaAbr(line , dateOfReference);
		assertEquals(enumRet, ProcessFileAbr.LineEnum.INVALID_NUMBER_OF_LINES);
	}
	
	@Test
	public void MockFirstLineInvalidNumberOfLines1CargaDeltaAbr() {
		ProcessFileAbr.LineEnum enumRet;
		String line = firstLine_Invalid_Number_Of_Lines_1_AsInFile;
		
		enumRet = processFileAbr.checkFirstLineLoadDeltaAbr(line , dateOfReference);
		assertEquals(enumRet, ProcessFileAbr.LineEnum.INVALID_NUMBER_OF_LINES);
	}
	
	
	@Test
	public void MockOtherThanFirstLineOKCargaDeltaAbr() {
		ProcessFileAbr.LineEnum enumRet;
		String line = otherThanFirstLine_OK_AsInFile1;
		enumRet = processFileAbr.checkOthersThanFirstLineLoadDeltaAbr(line);
		
		assertEquals(enumRet, ProcessFileAbr.LineEnum.LINE_OK);
	}
	
	@Test
	public void MockOtherThanFirstLineOKCargaDeltaAbr1() {
		ProcessFileAbr.LineEnum enumRet;
		String line = otherThanFirstLine_OK_AsInFile2;
		enumRet = processFileAbr.checkOthersThanFirstLineLoadDeltaAbr(line);		
		
		assertEquals(enumRet, ProcessFileAbr.LineEnum.LINE_OK);
	}
	
	
	@Test
	public void MockOtherThanFirstLineTooShortCargaDeltaAbr1() {
		ProcessFileAbr.LineEnum enumRet;
		String line = otherThanFirstLine_Too_Short_AsInFile;
		
		enumRet = processFileAbr.checkOthersThanFirstLineLoadDeltaAbr(line);		
		
		assertEquals(enumRet, ProcessFileAbr.LineEnum.TOO_SHORT);
	}
	
	@Test
	public void MockOtherThanFirstLineInvalidActionCargaDeltaAbr1() {
		ProcessFileAbr.LineEnum enumRet;
		String line = otherThanFirstLine_Invalid_Action_AsInFile;
		
		enumRet = processFileAbr.checkOthersThanFirstLineLoadDeltaAbr(line);		
		
		assertEquals(enumRet, ProcessFileAbr.LineEnum.INVALID_ACTION);
	}
	
	
	@Test
	public void MockOtherThanFirstLineInvalidTelephoneNumberCargaDeltaAbr1() {
		ProcessFileAbr.LineEnum enumRet;
		String line = otherThanFirstLine_Invalid_Telephone_Number_AsInFile;
		
		enumRet = processFileAbr.checkOthersThanFirstLineLoadDeltaAbr(line);		
		
		assertEquals(enumRet, ProcessFileAbr.LineEnum.TELEF_NUM_INVALID_FORMAT);
	}
	
	@Test
	public void MockOtherThanFirstLineSmallNumberOfFieldsCargaDeltaAbr1() {
		ProcessFileAbr.LineEnum enumRet;
		String line = otherThanFirstLine_Small_Number_Of_Fields_AsInFile;
		
		enumRet = processFileAbr.checkOthersThanFirstLineLoadDeltaAbr(line);		
		
		assertEquals(enumRet, ProcessFileAbr.LineEnum.FIELDS_SMALLER_THAN_6);
	}
	
	
	
	@Test
	public void MockOtherThanFirstLineInvalidLineTypeCargaDeltaAbr1() {
		ProcessFileAbr.LineEnum enumRet;
		String line = otherThanFirstLine_Invalid_Line_Type_AsInFile;
		
		enumRet = processFileAbr.checkOthersThanFirstLineLoadDeltaAbr(line);		
		
		assertEquals(enumRet, ProcessFileAbr.LineEnum.INVALID_LINE_TYPE);
	}
	
	@Test
	public void MockOtherThanFirstLineInvalidSpidCargaDeltaAbr1() {
		ProcessFileAbr.LineEnum enumRet;
		String line = otherThanFirstLine_Invalid_Spid_AsInFile;
		
		enumRet = processFileAbr.checkOthersThanFirstLineLoadDeltaAbr(line);		
		
		assertEquals(enumRet, ProcessFileAbr.LineEnum.SPID_INVALID);
	}
	
	@Deprecated
	public LineEnum checkFirstLineCargaDeltaAbr(String line, Date dateOfReference) {		
		Date dateOfGeneration = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		int numberOfLines = 0;
		
		String[] fields = line.split(";");
		
		if (fields.length != 2) {
			return LineEnum.FISRT_LINE_OUT_OF_FORMAT;			
		}
		
		try {
			dateOfGeneration = sdf.parse(fields[0]);
		} catch (ParseException ex) {
			return LineEnum.INVALID_DATE_FORMAT;			
		}
		
		try {
			numberOfLines = Integer.parseInt(fields[1]);
		} catch (NumberFormatException nfe) {
			return LineEnum.INVALID_NUMBER_OF_LINES;			
		}
		
		if (numberOfLines < 0) {
			return LineEnum.INVALID_NUMBER_OF_LINES;			
		}
		
		if ( dateOfGeneration.getTime() <= dateOfReference.getTime() ) {		
			return LineEnum.INVALID_DATE_OF_GENERATION;
		}
		
		return LineEnum.LINE_OK;
	}
		
	@Deprecated
	public LineEnum checkOthersThanFirstLineCargaDeltaAbr(String line) {
		long ticketNumber;
		int action;
		String telefoneNumber = "";		
		short spid;		
		int lineType;		
		
		String[] fields = line.split(";");
		
		if (fields.length < 3) {
			return LineEnum.TOO_SHORT;		
		}
		
		
		ticketNumber = Long.parseLong(fields[0]);
		action = Integer.parseInt(fields[1]);
		telefoneNumber = fields[2];	
		
		
		
		if (action != 0 && action != 1) {
			return LineEnum.INVALID_ACTION;			
		}
		
		if (telefoneNumber.length() >= 12) {
			return LineEnum.TELEF_NUM_INVALID_FORMAT;			
		}
		
		if (action == 0) {
			if (fields.length < 6) {
				return LineEnum.FIELDS_SMALLER_THAN_6;				
			}
			
			try {
				lineType = Integer.parseInt(fields[3]);
			} catch (NumberFormatException nfe) {
				lineType = 0;
			}
			
			if (lineType != 0 && lineType != 2) {
				return LineEnum.INVALID_LINE_TYPE;				
			}
			try {
				spid = Short.parseShort(fields[5]);
			} catch (NumberFormatException nfe) {
				spid = 0;
			}
			if (spid < 0) {
				return LineEnum.SPID_INVALID;				
			}
		} 
		
		return LineEnum.LINE_OK;
		
	}
	
	@Deprecated
	enum LineEnum {
		FISRT_LINE_OUT_OF_FORMAT,
		INVALID_DATE_FORMAT,
		INVALID_NUMBER_OF_LINES,
		INVALID_DATE_OF_GENERATION,
		TOO_SHORT,
		INVALID_ACTION,		
		TELEF_NUM_INVALID_FORMAT,
		FIELDS_SMALLER_THAN_6,
		INVALID_LINE_TYPE,
		SPID_INVALID,
		LINE_OK
	}
}
