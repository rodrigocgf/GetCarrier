package br.com.supportcomm;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.function.BiConsumer;
import java.util.function.Function;

import javax.net.ssl.SSLContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;

import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.config.RequestConfig.Builder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.context.annotation.Bean;

import br.com.supportcomm.config.AppConfig;
import br.com.supportcomm.http.HttpInterceptor;
import br.com.supportcomm.log.LogstashEncoderPattern;
import br.com.supportcomm.util.SystemUtils;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import org.apache.log4j.PropertyConfigurator;
//import org.apache.log4j.Logger;


@SpringBootApplication
@EnableScheduling
@EnableCaching
public class GetCarrier extends SpringBootServletInitializer {
	
	@Autowired 
	ApplicationControlEvents appControlEvents;
	
	@Autowired 
	private AppConfig appConfig;
	
	public static Logger logger = LoggerFactory.getLogger(GetCarrier.class);
	
	private static final AppServletContextListener listener = new AppServletContextListener();
	
	public static void main(String[] args) {		
		
		
		startupProperties((key, value) -> {
			System.out.println("CONFIG MAIN: " + key + "=" + value); 
			System.setProperty(key, value);
		});
		SpringApplication app = new SpringApplication(GetCarrier.class);
        
		//PropertyConfigurator.configure("src/main/resources/log4j.properties");			
		
		logger.debug("***********************************************");
		logger.debug("***********************************************");
		logger.debug("**                                           **");
		logger.debug("**               INICIANDO                   **");
		logger.debug("**                                           **");
		logger.debug("***********************************************");
		logger.debug("***********************************************");
		
		app.addListeners(listener);
		app.run(args);
    }
	
	
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
    	builder = builder.listeners(listener).sources(GetCarrier.class);
		Properties prop = new Properties();
		startupProperties((key, value) -> {
			System.out.println("CONFIG: " + key + "=" + value); 
			prop.put(key, value);
		});
		if (!prop.isEmpty()) {
			builder.properties(prop);
		}
		return builder.sources(GetCarrier.class);
    	//return builder;
	}
	
	private static void setSystemProperties(Properties prop, String name) {
		setSystemProperties(prop, name, (String)null);
	}
	
	private static void setSystemProperties(Properties prop, String name, String valDef) {
		String value = Objects.toString(prop.get(name), valDef);
		if (value != null) {
			if (SystemUtils.getPropertyOrEnvironment(name, null) == null) {
				System.out.println(AppConfig.APP_NAME + ": " + name + "=" + value);
				System.setProperty(name, value);
			}
			else {
				System.out.println(AppConfig.APP_NAME + ": **Ignoring " + name + "=" + value + ". Using " + name + "=" + SystemUtils.getPropertyOrEnvironment(name, null));
			}
		}
	}

	private static void setSystemProperties(Properties prop, String nameGet, String nameSet, Function<String, String> funcVal) {
		String value = funcVal.apply(Objects.toString(prop.get(nameGet)));
		if (value != null) {
			if (SystemUtils.getPropertyOrEnvironment(nameSet, null) == null) {
				System.out.println(AppConfig.APP_NAME + ": " + nameSet + "=" + value);
				System.setProperty(nameSet, value);
			}
			else {
				System.out.println(AppConfig.APP_NAME + ": **Ignoring " + nameSet + "=" + value + ". Using " + nameSet + "=" + SystemUtils.getPropertyOrEnvironment(nameSet, null));
			}
		}
	}

	private static void startupProperties(BiConsumer<String,String> propertyHandler) {
		AppConfig.printSystemInfo();
		
		
		String propertyFile = SystemUtils.getPropertyOrEnvironment(AppConfig.CONFIG_EXTERNAL_FILE, AppConfig.NAME_EXTERNAL_FILE);
		
		Properties propFile = null;
    	if (propertyFile != null && !"OFF".equalsIgnoreCase(propertyFile) && SystemUtils.fileExists(propertyFile)) {
    		propertyHandler.accept("spring.config.location", "classpath:application.properties,file:" + propertyFile);
    		propFile = SystemUtils.loadFile(propertyFile);
    	}
		Properties props = SystemUtils.loadResource("application.properties");
		
		if (props == null) {
			props = propFile;
		}
		else if (propFile != null) {
			props.putAll(propFile);
		}
		if (props != null) {
			setSystemProperties(props, "logging.appender.file.level");
			setSystemProperties(props, "logging.appender.console.level");
			setSystemProperties(props, "logging.appender.error.level");
			setSystemProperties(props, AppConfig.CONFIG_LOG_JSON, "logging.encoder.class", v -> "true".equalsIgnoreCase(v)? LogstashEncoderPattern.class.getName() : PatternLayoutEncoder.class.getName());
		}
		
	}

    
    @Bean
    protected ServletListenerRegistrationBean<ServletContextListener> servletListener() {
        ServletListenerRegistrationBean<ServletContextListener> srb = new ServletListenerRegistrationBean<>();
        srb.setListener(listener);
        return srb;
    }
    
    @Bean
    public PoolingHttpClientConnectionManager poolingHttpClientConnectionManager(AppConfig appConfig) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
    	PoolingHttpClientConnectionManager result;
    	if (appConfig.isNoSslValidation()) {
    		Registry<ConnectionSocketFactory> socketFactoryIgnoreSslValidation = createSocketFactoryIgnoreSslValidation();
    		result = new PoolingHttpClientConnectionManager(socketFactoryIgnoreSslValidation);
    	}
    	else {
    		result = new PoolingHttpClientConnectionManager();
    	}
		if (appConfig.getHttpMaxConnections() != null) {
			result.setMaxTotal(appConfig.getHttpMaxConnections());
		}
		if (appConfig.getHttpMaxPerRoute() != null) {
			result.setDefaultMaxPerRoute(appConfig.getHttpMaxPerRoute());
		}

		logger.debug(String.format("******* poolingHttpClientConnectionManager criado: {}", result) );
		return result;
    }

    private Registry<ConnectionSocketFactory> createSocketFactoryIgnoreSslValidation() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
        HttpClientBuilder b = HttpClientBuilder.create();

        // setup a Trust Strategy that allows all certificates.
        SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, (X509Certificate[] chain, String authType) -> true).build();
        b.setSSLContext(sslContext);

        // don't check Hostnames, either.
        SSLConnectionSocketFactory sslConnectionFactory = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

        // here's the special part:
        //      -- need to create an SSL Socket Factory, to use our weakened "trust strategy";
        //      -- and create a Registry, to register it.
        //
        //Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory> create().register("http", PlainConnectionSocketFactory.getSocketFactory()).register("https", sslSocketFactory).build();
    	Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
    			.register("https", sslConnectionFactory)
    			.register("http", PlainConnectionSocketFactory.getSocketFactory())
    	        .build();
    	return socketFactoryRegistry;
    }

    @Bean
    public RequestConfig requestConfig(AppConfig appConfig) {
    	Builder builder = RequestConfig.custom()
    			.setConnectionRequestTimeout(appConfig.getHttpPoolTimeout())
    			.setConnectTimeout(appConfig.getConnectTimeout())
    			.setSocketTimeout(appConfig.getReadTimeout());
    	if (appConfig.getHttpProxyHost() != null) {
    		logger.debug(String.format("******* requestConfig Proxy configurado {}:{}", appConfig.getHttpProxyHost(), appConfig.getHttpProxyPort()));
    		HttpHost proxy = new HttpHost(appConfig.getHttpProxyHost(), appConfig.getHttpProxyPort()); 
    		logger.debug(String.format("******* requestConfig Proxy inserido: {}", proxy) );
    		builder.setProxy(proxy);
    	}
    	RequestConfig result = builder.build();
		logger.debug(String.format("******* requestConfig criado: {}", result));
      return result;
    }
    
    @Bean
    public CloseableHttpClient httpClient(PoolingHttpClientConnectionManager poolingHttpClientConnectionManager, RequestConfig requestConfig, AppConfig appConfig) {
    	HttpClientBuilder builder = HttpClientBuilder.create()
			.setConnectionManager(poolingHttpClientConnectionManager)
			.setDefaultRequestConfig(requestConfig);
    	if (appConfig.getHttpSecondsKeepAlive() != null) {
    		final int seconds = Math.max(0, appConfig.getHttpSecondsKeepAlive());
    		builder.setKeepAliveStrategy(new ConnectionKeepAliveStrategy() {
        	    @Override
        	    public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
        	        HeaderElementIterator it = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
        	        while (it.hasNext()) {
        	            HeaderElement he = it.nextElement();
        	            String param = he.getName();
        	            String value = he.getValue();
        	            if (value != null && param.equalsIgnoreCase("timeout")) {
        	                return Long.parseLong(value) * 1000;
        	            }
        	        }
        	        return seconds * 1000;
        	    }
        	});
    	}
		CloseableHttpClient result = builder.build();
		logger.debug(String.format("******* httpClient criado: {}", result));
		return result;
    }
    
    @Bean
    public RestTemplate restTemplate(HttpClient httpClient, HttpInterceptor interceptor) {
    	HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
    	requestFactory.setHttpClient(httpClient);
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        List<ClientHttpRequestInterceptor> interceptors = restTemplate.getInterceptors();
        if (CollectionUtils.isEmpty(interceptors)) {
        	interceptors = new ArrayList<>();
        }
        interceptors.add(interceptor);
        restTemplate.setInterceptors(interceptors);
		logger.debug(String.format("******* RestTemplate criado: {}", restTemplate));
		
		return restTemplate;
	}

	@Bean
	public CommonsRequestLoggingFilter requestLoggingFilter() {
	    CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
	    loggingFilter.setIncludeClientInfo(true);
	    loggingFilter.setIncludeQueryString(true);
	    loggingFilter.setIncludePayload(true);
	    loggingFilter.setIncludeHeaders(false);
	    return loggingFilter;
	}
	
}
