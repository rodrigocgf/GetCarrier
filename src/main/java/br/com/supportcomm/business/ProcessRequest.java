package br.com.supportcomm.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Estado;
import br.com.supportcomm.models.db.NumeroPortado;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;
import br.com.supportcomm.models.db.TelefoniaEnum;
//import br.com.supportcomm.crudrepositories.NumeroPortadoRepositoryFormer;
//import br.com.supportcomm.crudrepositories.OperadoraRepositoryFormer;
import br.com.supportcomm.exception.CodigoRetornoEnum;
import br.com.supportcomm.exception.PortabilidadeException;
import br.com.supportcomm.jparepositories.AreaRepoImpl;
import br.com.supportcomm.jparepositories.EstadoRepoImpl;
import br.com.supportcomm.jparepositories.NumeroPortadoRepoImpl;
import br.com.supportcomm.jparepositories.OperadoraRepoImpl;
import br.com.supportcomm.jparepositories.PaisRepoImpl;
import br.com.supportcomm.jparepositories.SerieRepoImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class ProcessRequest {
	public static Logger logger = LoggerFactory.getLogger(ProcessRequest.class);
	
	@Autowired
	private OperadoraRepoImpl operadoraRepoImpl;
	
	@Autowired
	private NumeroPortadoRepoImpl numeroPortadoRepoImpl;	
	
	@Autowired
	public PaisRepoImpl  paisRepoImpl;
	
	@Autowired
	private AreaRepoImpl areaRepoImpl;
	
	@Autowired
	private EstadoRepoImpl estadoRepoImpl;
	
	@Autowired
	private SerieRepoImpl serieRepoImpl;
	
	private static HashMap<Short, Long> paises = new HashMap<Short, Long>();
	private static HashMap<String, Long> areas = new HashMap<String, Long>();
	
	public InfoMsisdn execute(String msisdn) {
		InfoMsisdn infoMsisdn = new InfoMsisdn();
		
		Pais pais = null;
		Area area = null;		
		
		int tamanhoDoDdi;
		String nomeDoPais = "";
		short ddd = -1;
		short ddi = -1;
		long numero = -1;
		String nomeDoEstado = "";
		String siglaDoEstado = "";
		String nomeDaOperadora = "";
		String nomeDaOperadoraNormalizado = "";
		short codigoDaOperadora = -1;
		short spid = -1;
		
		infoMsisdn.setMsisdn(msisdn.trim());
		
		
		for (tamanhoDoDdi = 1; tamanhoDoDdi < 5; tamanhoDoDdi++) {
			pais = procuraPais(msisdn, tamanhoDoDdi);
			if (pais != null) {
				break;
			} else {
				logger.info("Procurando pelo proximo DDI...");
			}
		}
		
		infoMsisdn.setPais(pais);
		
		nomeDoPais = pais.getNome();
		infoMsisdn.setNomeDoPais(nomeDoPais);		
		logger.info(String.format("[inicia] Nome do pais : %s", nomeDoPais));		
		
		
		ddi = pais.getDdi();
		infoMsisdn.setDdi(ddi);		
		logger.info(String.format("[inicia] DDI  do pais : %d", ddi));
		
		
		if (pais.getTamanhoDoDdd() == 0) {
			ddd = 0;
			numero = Long.parseLong(msisdn.substring(tamanhoDoDdi));
		} else {
			ddd = Short.parseShort(msisdn.substring(tamanhoDoDdi, tamanhoDoDdi + pais.getTamanhoDoDdd()));
			numero = Long.parseLong(msisdn.substring(tamanhoDoDdi + pais.getTamanhoDoDdd()));
		}
		infoMsisdn.setDdd(ddd);
		infoMsisdn.setNumero(numero);
		
		area = procuraArea(pais, ddd);
		if (area != null) {
			Estado estado = estadoRepoImpl.searchById(area.getEstadoid());
			if ( estado != null) {
				nomeDoEstado = estado.getNome();
				siglaDoEstado = estado.getSigla();
				
				infoMsisdn.setArea(area);
				infoMsisdn.setNomeDoEstado(nomeDoEstado);
				infoMsisdn.setSiglaDoEstado(siglaDoEstado);
				
				logger.info(String.format("[inicia] Nome do Estado: %s", nomeDoEstado));
				logger.info(String.format("[inicia] Sigla do Estado: %s", siglaDoEstado));
			}
		}
		
		procuraOperadoraGenerica(infoMsisdn);
		
		if ( infoMsisdn.getOperadora() != null) {
			nomeDaOperadora = infoMsisdn.getOperadora().getNome();
			nomeDaOperadoraNormalizado = infoMsisdn.getOperadora().getNomeNormalizado();
			codigoDaOperadora = infoMsisdn.getOperadora().getCodigo();
			spid = infoMsisdn.getOperadora().getSpid();
			
			infoMsisdn.setNomeDaOperadora(nomeDaOperadora);
			infoMsisdn.setNomeDaOperadoraNormalizado(nomeDaOperadoraNormalizado);
			infoMsisdn.setCodigoDaOperadora(codigoDaOperadora);
			
			
			logger.info("[start] Operadora : ");
			logger.info(String.format("........nome da operadora              : %s", nomeDaOperadora));
			logger.info(String.format("........nome da operadora normalizado  : %s", nomeDaOperadoraNormalizado));
			logger.info(String.format("........codigo da operadora            : %d", codigoDaOperadora));
			logger.info(String.format("........spid                           : %d", spid ));
		} else {
			infoMsisdn = null;
			logger.info("[start] Operadora Generica NAO ENCONTRADA.");
			
		}
		
		return infoMsisdn;
	}
	
	
	
	protected void procuraOperadoraGenerica(InfoMsisdn infoMsisdn)  {
		logger.info("[procuraOperadoraGenerica]");		
		
	
		String prefixoStr = "";
		String sufixoStr = "";
		Operadora ras = null;
		String numeroStr = "";
		int tamanhoPrefixo = 0;
		long prefixo = -1;
		long sufixo = -1;
		
		procuraOperadoraPorNumeroArea(infoMsisdn );
		
		if ( infoMsisdn.getOperadora() != null) {		
			infoMsisdn.setPrecisao(0);			
			return;
		} else {
		
	        numeroStr = Long.toString(infoMsisdn.getNumero() );
	        tamanhoPrefixo = numeroStr.length() - 4;
	        
	        logger.info(String.format("Numero  ................ : %s", numeroStr));
	        logger.info(String.format("Tamanho do prefixo ..... : %d", tamanhoPrefixo));
	        
	        if (tamanhoPrefixo >= 3 && tamanhoPrefixo <= 5)
	        {
	        	sufixoStr = numeroStr.substring(numeroStr.length() - 4);
	            
	            if (infoMsisdn.getDdd() > 0)
	            {
	                prefixoStr = Short.toString( infoMsisdn.getDdd() ) + numeroStr.substring(0, tamanhoPrefixo);
	            }
	            else
	            {
	                prefixoStr = numeroStr.substring(0, tamanhoPrefixo);
	            }
	            
	            prefixo = Long.parseLong(prefixoStr);	
	            sufixo = Long.parseLong(sufixoStr);
	            
	            logger.info(String.format("Prefixo ................ : %s", prefixoStr));
	            logger.info(String.format("Sufixo  ................ : %s", sufixoStr));
	            
	            procuraOperadoraPorAreaSerie (infoMsisdn, prefixo, sufixo, true);
	            if ( infoMsisdn.getOperadora() != null) {
	            	infoMsisdn.setPrecisao(1);          
	            }
	        }
	        
		}
		
	}
	
	private void procuraOperadoraPorNumeroArea(InfoMsisdn infoMsisdn)  {
		logger.info("=============================================================");
		logger.info("[procuraOperadoraPorNumeroArea]");
		
		TelefoniaEnum telefonia = TelefoniaEnum.UNDEFINED;
		NumeroPortado numeroPortado;
		Operadora operadora = null;
		long operadoraid = 0;
		
		logger.info("Search at NumeroPortado table where : ");
		logger.info(String.format("........area.areaid   : %d", infoMsisdn.getArea().getAreaid()));
		logger.info(String.format("........area.ddd      : %d", infoMsisdn.getArea().getDdd()));
		logger.info(String.format("........area.estadoid : %d", infoMsisdn.getArea().getEstadoid()));
		logger.info(String.format("........area.paisid   : %d", infoMsisdn.getArea().getPais()));
		logger.info(String.format("........numero        : %d", infoMsisdn.getNumero() ));
					
		
		try {		
			numeroPortado = numeroPortadoRepoImpl.procuraPor_Area_Numero( infoMsisdn.getArea().getAreaid() , infoMsisdn.getNumero() );
			if ( numeroPortado != null ) {
				infoMsisdn.setPortado(1);
				
				telefonia = numeroPortado.getTelefonia();
				infoMsisdn.setTelefonia(telefonia);
				
				operadoraid = numeroPortado.getOperadoraid();
				operadora = operadoraRepoImpl.selectOperadoraById(operadoraid);
				
				infoMsisdn.setOperadora(operadora);
				
				logger.info("FOUND : " );
				logger.info(String.format("........OPERADORA : %s", operadora.getNome() ));
			}
						
		} catch (NoResultException nre) {
			
		}
		catch(SQLGrammarException ex) {
			logger.debug("[procuraOperadora] Exception : %s", ex.fillInStackTrace());
			
		}
		logger.info("=============================================================");		
	}
	
	private void procuraOperadoraPorAreaSerie(InfoMsisdn infoMsisdn, long prefixo, long sufixo, boolean especial)  {
		logger.info("=============================================================");
		logger.info("[procuraOperadoraPorAreaSerie]");
		List<Serie> series;
		TelefoniaEnum telefonia = TelefoniaEnum.UNDEFINED;
		Serie serie;
		
		if ( infoMsisdn.getArea() == null) {
			infoMsisdn.setOperadora(null);
			return;
		}
		
		logger.info("Search at SERIE table where :");
		logger.info(String.format("........areadId          : %d", infoMsisdn.getArea().getAreaid() ));
		logger.info(String.format("........prefixo          : %d", prefixo ));
		logger.info(String.format("........sufixo           : %04d", sufixo ));
		logger.info(String.format("........especial         : %d", (int) (especial ? 1 : 0) ));
		
		series = serieRepoImpl.procuraPor_Area_Especial_Serie_Sufixo(infoMsisdn.getArea().getAreaid(), (short) (especial ? 1 : 0), (int)prefixo, (int) sufixo);
				
		try {
			if ( series == null ) {				
				infoMsisdn.setOperadora(null);
				return;
			}
			if ( series.size() == 0 ) {				
				infoMsisdn.setOperadora(null);
				return;
			}
			serie = series.get(0);
			telefonia = serie.getTelefonia();
			
			Operadora operadora = operadoraRepoImpl.selectOperadoraById(serie.getOperadoraid());
			
			infoMsisdn.setOperadora(operadora);
			infoMsisdn.setSerie(serie);
			infoMsisdn.setNomeDaOperadora(operadora.getNome());
			infoMsisdn.setNomeDaOperadoraNormalizado(operadora.getNomeNormalizado());
			infoMsisdn.setTelefonia(telefonia);
			infoMsisdn.setCodigoDaOperadora(operadora.getCodigo());
			
		} catch (NoResultException nre) {
			
		}
		logger.info("=============================================================");
	}
	
	private void procuraOperadoraPorAreaSerie(InfoMsisdn infoMsisdn, long serieIn, boolean especial)  {
		logger.info("=============================================================");
		logger.info("[procuraOperadoraPorAreaSerie]");
		List<Serie> series;
		TelefoniaEnum telefonia = TelefoniaEnum.UNDEFINED;
		Serie serie;
		
		if ( infoMsisdn.getArea() == null) {
			infoMsisdn.setOperadora(null);
			return;
		}
		
		logger.info("Search at SERIE table where :");
		logger.info(String.format("........areadId          : %d", infoMsisdn.getArea().getAreaid() ));
		logger.info(String.format("........serie            : %d", serieIn ));
		logger.info(String.format("........especial         : %d", (int) (especial ? 1 : 0) ));
		
		series = serieRepoImpl.procuraPor_Area_Especial_Serie(infoMsisdn.getArea().getAreaid(), (short) (especial ? 1 : 0), (int)serieIn);
				
		try {
			if ( series == null ) {				
				infoMsisdn.setOperadora(null);
				return;
			}
			if ( series.size() == 0 ) {				
				infoMsisdn.setOperadora(null);
				return;
			}
			serie = series.get(0);
			telefonia = serie.getTelefonia();
			
			infoMsisdn.setTelefonia(telefonia);
			
			Operadora operadora = operadoraRepoImpl.selectOperadoraById(serie.getOperadoraid());
			
			infoMsisdn.setOperadora(operadora);
			
		} catch (NoResultException nre) {
			
		}
		logger.info("=============================================================");
	}

	
	private Area procuraArea(Pais paisIn, short dddIn )  {
		logger.info("=============================================================");
		logger.info("[procuraArea]");
		
		Area areaRas = null;
		StringBuffer sb = new StringBuffer();
		String key;
		sb.append(paisIn.getPaisId().toString());
		sb.append("_");
		sb.append(dddIn);
		key = sb.toString();
		
		if (areas.containsKey(key)) {
			Long id;
			id = areas.get(key);
			if (id.longValue() < 0) {
				return (null);
			}
			return areaRepoImpl.selectAreaById(id);
		}
				
		try {
			logger.info("Search at AREA table where :");
			logger.info(String.format("........pais.paisid         : %d", paisIn.getPaisId() ));
			logger.info(String.format("........pais.ddi            : %d", paisIn.getDdi()));
			logger.info(String.format("........pais.nome           : %s", paisIn.getNome()));
			logger.info(String.format("........pais.tamanho do ddd : %d", paisIn.getTamanhoDoDdd() ));
			logger.info(String.format("........dddIn               : %d", dddIn ));
			
			areaRas = areaRepoImpl.procuraPor_Pais_DDD(paisIn.getPaisId(), dddIn);
			
		} catch (NoResultException nre) {
			areas.put(key, new Long(-1));
			return (null);
		} catch (IllegalStateException ise) {
			//throw new PortabilidadeException(CodigoRetornoEnum.FALHA, "Erro na leitura do DDD |" + dddIn + "| para o pais |" + paisIn.toString() + "|");
		}
		if ( areaRas != null) {
			logger.info(String.format("FOUND AreaId : %d", areaRas.getAreaid()));
			areas.put(key, areaRas.getAreaid());
		}
		logger.info("=============================================================");
		return areaRas;
	}
	
	
	private Pais procuraPais(String msisdnIn, int tamanhoDoDdiIn) {
		logger.info("=============================================================");
		logger.info(String.format("[procuraPais] msisdnIn : %s ; tamanhoDoDdiIn : %d", msisdnIn, tamanhoDoDdiIn));
		
		List<Pais> lista = null;
		Pais pais = null;
		short ras;
		
		if (msisdnIn.length() < tamanhoDoDdiIn) {
			return null;
			//throw new PortabilidadeException(CodigoRetornoEnum.MSISDN_INVALIDO, "MSISDN com tamanho invalido |" + msisdn + "|");
		}
		
		ras = Short.parseShort(msisdnIn.substring(0, tamanhoDoDdiIn));
						
		if (paises.containsKey(ras)) {
			Long id = paises.get(ras);
			
			if (id.longValue() < 0) {
				return (null);
			}
			return paisRepoImpl.selectPaisById(id);			
		}
		
		logger.info(String.format("DDI to be searched : %d", ras) );
		
		try {						
			lista = paisRepoImpl.procuraPor_DDI(ras);
						
			if (lista == null || lista.size() == 0) {
				paises.put(ras, new Long(-1));
				return (null);
			}
			pais = (Pais) lista.get(0);
		} catch (NoResultException nre) {
			paises.put(ras, new Long(-1));
			return (null);
		} catch (IllegalStateException ise) {
			//throw new PortabilidadeException(CodigoRetornoEnum.FALHA, "Erro na leitura do DDI |" + ras + "|");
		}
		if ( pais != null )
			paises.put(ras, pais.getPaisId());
		
		logger.info("=============================================================");
		return pais;
	}
	
	
}
