package br.com.supportcomm.business;

import br.com.supportcomm.jparepositories.AreaRepoImpl;
import br.com.supportcomm.jparepositories.HistoricoRepoImpl;
import br.com.supportcomm.jparepositories.OperadoraNormalizadaRepoImpl;
import br.com.supportcomm.jparepositories.OperadoraRepoImpl;
import br.com.supportcomm.jparepositories.PaisRepoImpl;
import br.com.supportcomm.jparepositories.SerieRepoImpl;
import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Historico;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.OperadoraNormalizada;
import br.com.supportcomm.models.db.OperadoraOracle;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;
import br.com.supportcomm.models.db.TelefoniaEnum;
import br.com.supportcomm.util.FileAnatelRecord;
import br.com.supportcomm.util.FiltroDeArquivosAnatel;
import br.com.supportcomm.util.KeyArea;
import br.com.supportcomm.util.KeyOperadoraAnatel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.*;


import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.com.supportcomm.util.KeySerieAnatel;
import br.com.supportcomm.util.ValueOperadoraAnatel;
import br.com.supportcomm.util.ValueSerieAnatel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


class MultiMap<K, V>
{
	private Map<K, Collection<V>> map = new HashMap<>();

	/**
	* Add the specified value with the specified key in this multimap.
	*/
	public void put(K key, V value) {
		if (map.get(key) == null)
			map.put(key, new ArrayList<V>());

		map.get(key).add(value);
	}

	/**
	* Associate the specified key with the given value if not
	* already associated with a value
	*/
	public void putIfAbsent(K key, V value) {
		if (map.get(key) == null)
			map.put(key, new ArrayList<>());

		// if value is absent, insert it
		if (!map.get(key).contains(value)) {
			map.get(key).add(value);
		}
	}

	/**
	* Returns the Collection of values to which the specified key is mapped,
	* or null if this multimap contains no mapping for the key.
	*/
	public Collection<V> get(Object key) {
		return map.get(key);
	}

	/**
	* Returns a Set view of the keys contained in this multimap.
	*/
	public Set<K> keySet() {
		return map.keySet();
	}

	/**
	* Returns a Set view of the mappings contained in this multimap.
	*/
	public Set<Map.Entry<K, Collection<V>>> entrySet() {
		return map.entrySet();
	}

	/**
	* Returns a Collection view of Collection of the values present in 
	* this multimap.
	*/
	public Collection<Collection<V>> values() {
		return map.values();
	}

	/**
	* Returns true if this multimap contains a mapping for the specified key.
	*/
	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	/**
	* Removes the mapping for the specified key from this multimap if present
	* and returns the Collection of previous values associated with key, or
	* null if there was no mapping for key.
	*/
	public Collection<V> remove(Object key) {
		return map.remove(key);
	}

	/**
	* Returns the number of key-value mappings in this multimap.
	*/
	public int size() {
		int size = 0;
		for (Collection<V> value: map.values()) {
			size += value.size();
		}
		return size;
	}

	/**
	* Returns true if this multimap contains no key-value mappings.
	*/
	public boolean isEmpty() {
		return map.isEmpty();
	}

	/**
	* Removes all of the mappings from this multimap.
	*/
	public void clear() {
		map.clear();
	}

	/**
	* Removes the entry for the specified key only if it is currently
	* mapped to the specified value and return true if removed
	*/
	public boolean remove(K key, V value) {
		if (map.get(key) != null) // key exists
			return map.get(key).remove(value);

		return false;
	}

	/**
	* Replaces the entry for the specified key only if currently
	* mapped to the specified value and return true if replaced
	*/
	public boolean replace(K key, V oldValue, V newValue) {

		if (map.get(key) != null) {
			if (map.get(key).remove(oldValue))
				return map.get(key).add(newValue);
		}
		return false;
	}
}

//# Nome da Prestadora;CNPJ da Prestadora;Código Nacional;Prefixo;Faixa Inicial;Faixa Final;Status
// ALGAR TELECOM S/A;71208516000174;16;99659;7000;7999;1

@Service
public class ProcessFileAnatel {
	private static final Logger logger = LoggerFactory.getLogger(ProcessFileAnatel.class);
	private EntityManagerFactory factory;
	private File dirIn;
	private File dirOut;
	private HashMap<String, Long> areas = new HashMap<String, Long>();
	private HashMap<String, Long> operadoras = new HashMap<String, Long>();
	
	private HashMap<String, String> mapOperadorasOracle = null;
	private HashMap<String, OperadoraNormalizada> mapOperadoraNormalizada = null;
	
	private static final int POS_OPERADORA = 0;
	private static final int POS_CNPJ = 1;
	private static final int POS_DDD = 2;
	private static final int POS_SERIE_COMMON = 3;
	private static final int POS_SERIE_START = 4;
	private static final int POS_SERIE_END = 5;
	private static final int POS_STATUS = 6;
	
	private static final int SERIE_VERSION = 6;
	
	@Autowired
	private PaisRepoImpl paisRepoImpl;

	@Autowired
	private SerieRepoImpl serieRepoImpl;
	
	@Autowired
	private OperadoraRepoImpl operadoraRepoImpl;
	
	@Autowired
	private AreaRepoImpl areaRepoImpl;
	
	@Autowired
	private HistoricoRepoImpl histRepoImpl;
	
	private FiltroDeArquivosAnatel filtroAnatel;
	
	//@Autowired
	//OperadoraOracle operadoraOracle;
	
	@Autowired
	private OperadoraNormalizadaRepoImpl operadoraNormalizadaRepoImpl;
	

	
	public ProcessFileAnatel() {
		
	}
	
	public void process(File dirIn, File dirOut) {
		logger.info("======== PROCESSAMENTO ANATEL ========");
		
		this.dirIn = dirIn;
		this.dirOut = dirOut;
		MultiMap<KeySerieAnatel,ValueSerieAnatel> mmapFileAnatel = null;
		
		filtroAnatel = new FiltroDeArquivosAnatel();
		//mapOperadorasOracle = new HashMap<String, String>();
		mapOperadoraNormalizada = new HashMap<String, OperadoraNormalizada>();
		
		List<OperadoraNormalizada> listOn = operadoraNormalizadaRepoImpl.selectAll();
		logger.info("--------------------------------------------------------");
		logger.info("Nomes normalizados : CNPJ ---> NOME NORMALIZADO ");
		
		for(OperadoraNormalizada on : listOn ){
		   String sCnpj  = on.getCnpj();
		   
		   
		   if ( !mapOperadoraNormalizada.containsKey(sCnpj)) {
			   mapOperadoraNormalizada.put(sCnpj, on);
			   logger.info( sCnpj + "   -   " + on.getNomenormalizado());
		   }
		  
		}
		logger.info("--------------------------------------------------------");
		//mapOperadorasOracle = operadoraOracle.loadMapOperadorasByCnpj();

		String [] namesAnatel;
		
		namesAnatel = getFilenames(filtroAnatel);
		
		if ( namesAnatel != null ) {			
			for (int i = 0; i < namesAnatel.length; i++) {		
				
				mmapFileAnatel = processAnatelFile(namesAnatel[i]);
				
				logger.info(String.format("MAP FILE ANATEL size : %d", mmapFileAnatel.size()));
				
				analizeMultimapFileAnatel(namesAnatel[i] , mmapFileAnatel);
				
				logger.info(String.format(" ************** ANATEL file %s processed. *********************", namesAnatel[i] ) );
			}
			
			
		}
	}
	
	String [] getFilenames(FileFilter fileFilter) {
		File[] lista;
		String[] names;
		
		lista = dirIn.listFiles(fileFilter);
		
		if (lista == null || lista.length == 0) {
			return null;
		}
		
		names = new String[lista.length];
		for (int i = 0; i < lista.length; i++) {
			names[i] = lista[i].getName();
		}
		
		Arrays.sort(names);
		
		return names;
	}
	
	private MultiMap<KeySerieAnatel,ValueSerieAnatel> processAnatelFile(String arquivoStr) {
		BufferedReader arquivo;
		String linha = "";
		String strSerie = "";
		String strOperadora = "";	
		String strOperadoraNormalizada = "";
		OperadoraNormalizada operNorm;
		String strCnpj = "";
		String ddd = "";
		String prefixo = "";
		String prefixo_start = "";
		String prefixo_end = "";
		Pais pais;
		List<Pais> paises;
		List<FileAnatelRecord> listFileAnatelRecord;
		MultiMap<KeySerieAnatel,ValueSerieAnatel> mmapFileAnatel = new MultiMap<KeySerieAnatel,ValueSerieAnatel>();
		MultiMap<KeyOperadoraAnatel, ValueOperadoraAnatel> mmapOperadora = new MultiMap<KeyOperadoraAnatel, ValueOperadoraAnatel>();
		MultiMap<KeyArea, Area> mmapArea = new MultiMap<KeyArea, Area>();
		int lineNumber = 0;
		
		try {
			listFileAnatelRecord = new ArrayList<FileAnatelRecord>();
			
			try {
				paises = paisRepoImpl.procuraPor_Nome("BRASIL");
				pais = paises.get(0);				
				
			} catch (NoResultException nre) {
				logger.info("Nao foi encontrado o BRASIL.");
				return null;
			}
			
			logger.info(String.format("Recuperando series do pais: %s." , pais.getNome() ) );
		
			arquivo = new BufferedReader(new InputStreamReader(new FileInputStream(new File(dirIn, arquivoStr)), "UTF-8"));
			
			while((linha = arquivo.readLine()) != null) {
				if ( linha.length() > 36 ) {
					
					String fields[] = linha.split(";");
					
					if ( fields[POS_STATUS].trim().equals("1")) {
						
						ddd = fields[POS_DDD].trim();
						strCnpj = fields[POS_CNPJ].trim();
						
						operNorm = null;
						if ( mapOperadoraNormalizada != null && mapOperadoraNormalizada.containsKey(strCnpj) ) {
							operNorm = mapOperadoraNormalizada.get(strCnpj);
							
							strOperadora = operNorm.getNome();
							strOperadoraNormalizada = operNorm.getNomenormalizado();
						} else {
							//strOperadoraNormalizada = fields[POS_OPERADORA].trim().split(" ")[0];
							strOperadoraNormalizada = "";
							strOperadora = fields[POS_OPERADORA].trim();
						}
						
						prefixo = fields[POS_SERIE_COMMON].trim();
						prefixo_start = fields[POS_SERIE_START].trim();
						prefixo_end = fields[POS_SERIE_END].trim();
						
						TelefoniaEnum telefonia = null;
						telefonia = TelefoniaEnum.SMP;
						
						strOperadora = strOperadora.replaceAll("[áàãâä]","a");
						strOperadora = strOperadora.replaceAll("[óõ]","o");
						strOperadora = strOperadora.replaceAll("[é]","e");

						strOperadora = strOperadora.replaceAll("[ÁÀÃÂÄ]","A");
						strOperadora = strOperadora.replaceAll("[ÓÕ]","O");
						strOperadora = strOperadora.replaceAll("[É]","E");

						strOperadora = strOperadora.replace("Ç", "C");
						strOperadora = strOperadora.replace("ç", "c");
						
						
						strOperadora = strOperadora.toUpperCase();
						
						strSerie = ddd + prefixo;
						
						FileAnatelRecord record = new FileAnatelRecord();
						
						record.setDdd(ddd);
						record.setStrOperadora(strOperadora);
						record.setStrOperadoraNormalizada(strOperadoraNormalizada);
						record.setCnpj(strCnpj);
						record.setPrefixo(Integer.parseInt(strSerie));
						record.setPrefixo_start(Integer.parseInt(prefixo_start)); 
						record.setPrefixo_end(Integer.parseInt(prefixo_end));
						record.setTelefonia(telefonia);
						
						listFileAnatelRecord.add(record);
						
					}
				}
				lineNumber++;
			}
			
			logger.info(".....................................................");
			logger.info(String.format("LINHAS PROCESSADAS : %d", lineNumber));
			logger.info(".....................................................");
			
			
			logger.info(".....................................................");
			logger.info(String.format("LIST FileAnatel RECORD size : %d", listFileAnatelRecord.size()));
			logger.info(".....................................................");
			
			if ( listFileAnatelRecord != null) {
				
				for ( FileAnatelRecord record : listFileAnatelRecord ) {
					KeyOperadoraAnatel key = new KeyOperadoraAnatel( pais , record.getCnpj());
					ValueOperadoraAnatel value = new ValueOperadoraAnatel ( record.getStrOperadora() , record.getCnpj(), record.getStrOperadoraNormalizada() , record.getDdd());
					mmapOperadora.put(key, value);
					
					KeyArea keyArea = new KeyArea(record.getDdd(), pais);
					if ( !mmapArea.containsKey(keyArea)) {
						Area currentArea = procuraArea(record.getDdd(), pais);
						mmapArea.put(keyArea, currentArea);
					}
					
				}
				
				logger.info(".....................................................");
				logger.info(String.format("OPERADORAS no arquivo ANATEL : %d", mmapOperadora.keySet().size()));
				logger.info(".....................................................");
				
				
				logger.info(".....................................................");
				logger.info(String.format("AREAS no arquivo ANATEL : %d", mmapArea.keySet().size()));
				logger.info(".....................................................");
				
				
				for ( KeyOperadoraAnatel key : mmapOperadora.keySet()) {
					Collection<ValueOperadoraAnatel> values = mmapOperadora.get(key);
					
					ValueOperadoraAnatel val = values.iterator().next();
					
					logger.info("===============================================================================");
					logger.info(String.format("Cnpj da operadora : %s", key.getCnpj()));
					
					
					logger.info(String.format("Nome da operadora : %s", val.getNomeOperadora()));
					logger.info(String.format("Nome normalizado da operadora : %s", val.getNomeOperadoraNormalizado()));
						
					
					logger.info("===============================================================================");
					
					// Pick up operadora currently at Database
					Operadora operadora = null;
					operadora = procuraOperadoraPorPaisCnpj(key.getPais().getPaisId() , key.getCnpj() );
					
					if ( operadora != null) {
					
						// Setup nomenormalizado according to value with the same CNPJ at Oracle database
						operadora.setNomeNormalizado(val.getNomeOperadoraNormalizado());
						
					} else {
						operadora = new Operadora();
						//Pais lPais = new Pais();
						//lPais.setNome("BRASIL");
						//lPais.setDdi((short)55);
						//lPais.setTamanhoDoDdd(2);
						//lPais.setUsaPortabilidade(1);
						
						operadora.setNome(val.getNomeOperadora());
						operadora.setCNPJ(val.getCnpj());
						operadora.setNomeNormalizado(val.getNomeOperadoraNormalizado());
						operadora.setCodigo((short)0);
						operadora.setSpid((short)0);
						//operadora.setPaisid(lPais);
					}
					
					updateAtDbOperadoraNomeNormalizado( operadora );
					
					for ( ValueOperadoraAnatel valOper : values) {
						valOper.setOperadora(operadora);
					}
				}
					
				for ( FileAnatelRecord record : listFileAnatelRecord ) {
					
					ValueOperadoraAnatel valueOper = null;
					KeyOperadoraAnatel keyOper = new KeyOperadoraAnatel(pais, record.getCnpj());
					
					KeyArea keyArea = new KeyArea(record.getDdd(), pais);
					Area currentArea = null;
					if ( mmapArea.containsKey(keyArea)) {
						Collection<Area> areas = mmapArea.get(keyArea);
						currentArea = areas.iterator().next();
					}
					
					if ( mmapOperadora.containsKey(keyOper) && (currentArea != null)) {
						
						Collection<ValueOperadoraAnatel> valuesOper = mmapOperadora.get(keyOper);
						valueOper = valuesOper.iterator().next();
						
						if ( valueOper.getOperadora() != null ) {
					
							
							KeySerieAnatel keySerie = new KeySerieAnatel(valueOper.getOperadora(), currentArea, String.valueOf(record.getPrefixo() ) );
							
							ValueSerieAnatel valueSerie = new ValueSerieAnatel(	String.valueOf(record.getPrefixo_start()) , 
																				String.valueOf(record.getPrefixo_end()),
																				record.getTelefonia());
							
							mmapFileAnatel.put(keySerie, valueSerie);
						} else {
							if ( valueOper.getOperadora() == null )
							{
								logger.info("*******************************************************************************");
								logger.info("                OPERADORA NAO ENCONTRADA NA TABELA Operadora                   ");
								logger.info(String.format("Cnpj da operadora : %s", keyOper.getCnpj()));
								
								
								logger.info(String.format("Nome da operadora : %s", valueOper.getNomeOperadora()));
								logger.info(String.format("Nome normalizado da operadora : %s", valueOper.getNomeOperadoraNormalizado()));
									
								
								logger.info("*******************************************************************************");
							}
							
						}
					} else {
						if ( currentArea == null ) {
							logger.info("*******************************************************************************");
							logger.info("                AREA NAO ENCONTRADA NA TABELA Area                   ");
							logger.info(String.format("DDD da area : %s", record.getDdd()));
							logger.info(String.format("pais da area : %s", pais.getNome()));
							logger.info("*******************************************************************************");
						}
					}
					
				}
				
			}
			
			arquivo.close();
			
			return mmapFileAnatel;
			
		}
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		
	}
	
	@SuppressWarnings("unchecked")
	private void analizeMultimapFileAnatel(String arquivoStr , MultiMap<KeySerieAnatel,ValueSerieAnatel> mmapFileAnatel) {
		
		List<Serie> seriesFromDb = null;
		int countDb = 0;
		Historico hist = null;
		
		try {
			
			for ( KeySerieAnatel key : mmapFileAnatel.keySet() ) {
				
				Collection<ValueSerieAnatel> values = mmapFileAnatel.get(key);
				
				seriesFromDb = procuraAtDbPorOperadoraSerie(key.getArea(), key.getOperadora().getOperadoraId() , key.getSerie());
				
				int prefixoFileMax = 0, prefixoFileMin = 0;
				int arrSerieStartFile[];
				int arrSerieEndFile[];
				int cntFile = 0;
				TelefoniaEnum telefonia;
				
				arrSerieStartFile = new int[values.size()];
				arrSerieEndFile = new int[values.size()];
				for ( ValueSerieAnatel lValue : values) {
					arrSerieStartFile[cntFile] = Integer.parseInt(lValue.getPrefixo_start());
					arrSerieEndFile[cntFile] = Integer.parseInt(lValue.getPrefixo_end());
					cntFile++;
				}
				
				telefonia = values.iterator().next().getTelefonia();
					
				prefixoFileMin = getMin(arrSerieStartFile);
				prefixoFileMax = getMax(arrSerieEndFile);
				
				if ( seriesFromDb != null && (seriesFromDb.size() > 0 )) {
					
					int prefixoDbMax = 0 , prefixoDbMin = 0;
					
					int arrSerieStart[];
					int arrSerieEnd[];
					int cnt = 0;
					arrSerieStart = new int[seriesFromDb.size()];
					arrSerieEnd = new int[seriesFromDb.size()];
					
					for ( Serie lSerie : seriesFromDb ) {
						arrSerieStart[cnt] = lSerie.getSerieStart();
						arrSerieEnd[cnt] = lSerie.getSerieEnd();
						cnt++;
					}
					prefixoDbMin = getMin(arrSerieStart);
					prefixoDbMax = getMax(arrSerieEnd);
					
					// DEL BANCO	: [prefixoDbMin, prefixoDbMax]
					removeAtDbSerieBetweenStartEnd(key.getArea(), key.getOperadora(), Integer.parseInt(key.getSerie()), prefixoDbMin , prefixoDbMax);
					countDb++;
					
					// INS ARQUIVO	: [prefixoFileMin, prefixoFileMax]
					insertAtDbSerie(key.getArea(), key.getOperadora(), Integer.parseInt(key.getSerie()), prefixoFileMin , prefixoFileMax , telefonia);
					countDb++;
					
				} else {
					insertAtDbSerie(key.getArea(), key.getOperadora(), Integer.parseInt(key.getSerie()), prefixoFileMin , prefixoFileMax , telefonia);
					countDb++;
				}
			}
			
			hist = new Historico();
			hist.setArquivo(arquivoStr);
			hist.setDataDeReferencia(new Timestamp(new Date().getTime()));
			hist.setDataDeProcessamento(new Timestamp(new Date().getTime()));
			hist.setCargaFull(0);
			hist.setNumeroDeRegistrosProcessados(countDb);
			
			histRepoImpl.update(hist);
			
			moveFileToOutputDirectory(arquivoStr);
			
		} catch( Exception ex) {
			logger.info("Exception ex : " + ex.toString());
		}
	}
	
	private List<Serie> procuraAtDbPorSerieBetweenStartEnd(Area area, long operadoraid, int prefixo, int prefixo_start , int prefixo_end) {
		
		int especial = 1;
		List<Serie> lSeries;
		
		lSeries = serieRepoImpl.procuraPor_Area_Operadora_Especial_Serie_Between_Start_End(	area.getAreaid(), operadoraid, especial, 
																							prefixo, 
																							prefixo_start, prefixo_end);
		return lSeries; 
	}
	
	
	private List<Serie> procuraAtDbPorOperadoraSerie(Area area, long operadoraid  ,String prefixo) {
		int especial = 1;
		List<Serie> lSeries;
		
		lSeries = serieRepoImpl.procuraPor_Area_Especial_Operadora_Serie( 	area.getAreaid(),  especial, operadoraid , 
																			Integer.parseInt(prefixo) );
		
		return lSeries; 
	}
	
	
	private void insertAtDbSerie(Area currentArea, Operadora operadora, int prefixo, int prefixo_start , int prefixo_end , TelefoniaEnum telefonia) {
		
		try {
			Serie s = new Serie();
			
			s.setAreaid(currentArea.getAreaid());
			s.setOperadoraid(operadora.getOperadoraId());
			s.setSerie(prefixo);
			s.setSerieStart(prefixo_start);
			s.setSerieEnd(prefixo_end);
			s.setTelefonia(telefonia);
			//s.setSerieVersion(SERIE_VERSION);
			s.setEspecial(1);
			
			serieRepoImpl.insert(s);
		} 
		catch( Exception ex) {
			logger.info("[insertAtDbSerie] Exception : " + ex.toString());
		}
	}
	
	private void removeAtDbSerieBetweenStartEnd(Area area, Operadora operadora,  int prefixo, int prefixo_start, int prefixo_end) {
		try {
			int especial = 1;
			List<Serie> seriesFromDb = null;
			
			seriesFromDb = procuraAtDbPorSerieBetweenStartEnd(area, operadora.getOperadoraId(), prefixo, prefixo_start, prefixo_end);
			
			for ( Serie lSerie : seriesFromDb) {
				serieRepoImpl.deleteBy_Serieid(lSerie.getSerieid());
			}
		} 
		catch ( Exception ex) {
			logger.info("Exception ex : " + ex.toString());
		}
	}
	

	
	boolean compareOperadora(Operadora operadoraFromDb, String nome, String nomeNormalizado, String cnpj) {
		if ( 	operadoraFromDb.getNome().equals(nome) && 
				operadoraFromDb.getNomeNormalizado().equals(nomeNormalizado) &&
				operadoraFromDb.getCNPJ().equals(cnpj) )
			return true;
		else
			return false;
	}
	
	private void updateAtDbOperadoraNomeNormalizado( Operadora operadora ) {
		operadoraRepoImpl.update(operadora);
	}
	

	
	private Operadora procuraOperadoraPorPaisCnpj( Long paisid , String cnpj) {
		Operadora operadora = null;
		
		try {
			List<Operadora> operadoras = operadoraRepoImpl.procuraPor_Pais_Cnpj(paisid, cnpj);
			if ( operadoras != null && (operadoras.size() > 0) ) {
				operadora = operadoras.get(0);
			
			}
			
			return (operadora);		
		} catch (Exception ex ) {
			
			logger.debug("Exception : " + ex.toString());
			return null;
		}
		
	}
	
	private Area procuraArea( String ddd, Pais pais)  {
		Area areaRas;
		StringBuffer sb = new StringBuffer();
		sb.append(pais.getPaisId().toString());
		sb.append("_");
		sb.append(ddd);
		
		String key = sb.toString();
		if (areas.containsKey(key)) {
			Long id = areas.get(key);
			if (id.longValue() < 0) {
				return (null);
			}
			return areaRepoImpl.selectAreaById(id);		
		}		
		
		areaRas = areaRepoImpl.procuraPor_Pais_DDD(pais.getPaisId(), new Integer(ddd.trim()));
		if ( areaRas != null )
			areas.put(key, areaRas.getAreaid());
		
		return (areaRas);
	}
	
	private void moveFileToOutputDirectory(String fileName) {
		
		try {
			logger.info(String.format("Moving FILE %s FROM directory %s to directory %s", fileName, dirIn.toString(), dirOut.toString() ) );
			
			File file = new File(dirIn, fileName);
			logger.info("Moving the file " + fileName + " to the OUTPUT DIRECTORY.");
			file.renameTo(new File(dirOut, fileName));
		}
		catch(Exception  ex) {
			logger.info(String.format("ERROR at moving FILE %s FROM directory %s to directory %s"), fileName, dirIn.toString(), dirOut.toString() );
		}
	}
	
	
	private int getMax(int[] inputArray) { 
		int maxValue = inputArray[0]; 
	    for(int i=1;i < inputArray.length;i++) { 
	    	if(inputArray[i] > maxValue){ 
	    		maxValue = inputArray[i]; 
	    	} 
	    } 
	    return maxValue; 
	}
	 
	
	private  int getMin(int[] inputArray) { 
		int minValue = inputArray[0]; 
		for(int i=1;i<inputArray.length;i++) { 
			if(inputArray[i] < minValue){ 
				minValue = inputArray[i]; 
			} 
	    } 
	    return minValue; 
	} 

}
