package br.com.supportcomm.business;

import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;
import br.com.supportcomm.models.db.TelefoniaEnum;

public class InfoMsisdn {

	private Pais pais = null;
	private Area area = null;
	protected Operadora operadora = null;
	protected String msisdn = "";
	private short ddi = -1;
	private short ddd = -1;
	private long numero = -1;
	private String nomeDoPais = "";
	private String nomeDoEstado = "";
	private String siglaDoEstado = "";
	protected String nomeDaOperadora = "";
	protected String nomeDaOperadoraNormalizado = "";
	protected short codigoDaOperadora = -1;
	private int precisao = -1;
	private int portado = 0;
	private Serie serie = null;
	private TelefoniaEnum telefonia = TelefoniaEnum.UNDEFINED;
	private short spid;
	
	
	
	public short getSpid() {
		return spid;
	}
	public void setSpid(short spid) {
		this.spid = spid;
	}
	public Serie getSerie() {
		return serie;
	}
	public void setSerie(Serie serie) {
		this.serie = serie;
	}
	
	public Pais getPais() {
		return pais;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	public Area getArea() {
		return area;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	public Operadora getOperadora() {
		return operadora;
	}
	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public short getDdi() {
		return ddi;
	}
	public void setDdi(short ddi) {
		this.ddi = ddi;
	}
	public short getDdd() {
		return ddd;
	}
	public void setDdd(short ddd) {
		this.ddd = ddd;
	}
	public long getNumero() {
		return numero;
	}
	public void setNumero(long numero) {
		this.numero = numero;
	}
	public String getNomeDoPais() {
		return nomeDoPais;
	}
	public void setNomeDoPais(String nomeDoPais) {
		this.nomeDoPais = nomeDoPais;
	}
	public String getNomeDoEstado() {
		return nomeDoEstado;
	}
	public void setNomeDoEstado(String nomeDoEstado) {
		this.nomeDoEstado = nomeDoEstado;
	}
	public String getSiglaDoEstado() {
		return siglaDoEstado;
	}
	public void setSiglaDoEstado(String siglaDoEstado) {
		this.siglaDoEstado = siglaDoEstado;
	}
	public String getNomeDaOperadora() {
		return nomeDaOperadora;
	}
	public void setNomeDaOperadora(String nomeDaOperadora) {
		this.nomeDaOperadora = nomeDaOperadora;
	}
	public String getNomeDaOperadoraNormalizado() {
		return nomeDaOperadoraNormalizado;
	}
	public void setNomeDaOperadoraNormalizado(String nomeDaOperadoraNormalizado) {
		this.nomeDaOperadoraNormalizado = nomeDaOperadoraNormalizado;
	}
	public short getCodigoDaOperadora() {
		return codigoDaOperadora;
	}
	public void setCodigoDaOperadora(short codigoDaOperadora) {
		this.codigoDaOperadora = codigoDaOperadora;
	}
	public int getPrecisao() {
		return precisao;
	}
	public void setPrecisao(int precisao) {
		this.precisao = precisao;
	}
	public int getPortado() {
		return portado;
	}
	public void setPortado(int portado) {
		this.portado = portado;
	}
	public TelefoniaEnum getTelefonia() {
		return telefonia;
	}
	public void setTelefonia(TelefoniaEnum telefonia) {
		this.telefonia = telefonia;
	}
	
	
}
