package br.com.supportcomm.business;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.ServletException;

import org.hibernate.exception.SQLGrammarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.supportcomm.exception.CodigoRetornoEnum;
import br.com.supportcomm.exception.PortabilidadeException;
import br.com.supportcomm.jparepositories.AreaRepoImpl;
import br.com.supportcomm.jparepositories.EstadoRepoImpl;
import br.com.supportcomm.jparepositories.HistoricoRepoImpl;
import br.com.supportcomm.jparepositories.NumeroPortadoRepoImpl;
import br.com.supportcomm.jparepositories.OperadoraRepoImpl;
import br.com.supportcomm.jparepositories.PaisRepoImpl;
import br.com.supportcomm.jparepositories.SerieRepoImpl;
import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Estado;
import br.com.supportcomm.models.db.Historico;
import br.com.supportcomm.models.db.NumeroPortado;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;
import br.com.supportcomm.models.db.TelefoniaEnum;
import br.com.supportcomm.util.ArquivoAbr;
import br.com.supportcomm.util.FiltroDeArquivosAbrDelta;
import br.com.supportcomm.util.FiltroDeArquivosAbrFull;





@Service
public class ProcessFileAbr {
	
	public final static int NUMBER_OF_PARALLEL_THREADS_ABR_DELTA = 30;
	public final static int NUMBER_OF_PARALLEL_THREADS_ABR_FULL = 40;
	
	public class RecordRunnableAbrDelta implements Callable<Integer> {
		private int lineNumber;
		private int numberOfLines;
		private int action;
		private long ticketNumber; 
		private String telephoneNumber;
		private short spid;
		
		public RecordRunnableAbrDelta(	int _lineNumber , 
										int _numberOfLines, 
										int _action, 
										long _ticketNumber , 
										String _telephoneNumber , 
										short _spid) {
		    this.lineNumber = _lineNumber;
		    this.numberOfLines = _numberOfLines;
		    this.action = _action;
		    this.ticketNumber = _ticketNumber;
		    this.telephoneNumber = _telephoneNumber;
		    this.spid = _spid;
		}

		  
		@Override
	    public Integer call() {
	    	return treatMsisdn(lineNumber , numberOfLines, action,ticketNumber, telephoneNumber, spid);
	    }
	};
	

	public class RecordRunnableAbrFull implements Callable<Integer> {
		
		
		long areaid;
		long telephoneNumber;
		Date today;
		long ticketNumber;
		long operadoraid;
		TelefoniaEnum telefonia;
		
		public RecordRunnableAbrFull() {
			
		}
		
		public RecordRunnableAbrFull( long areaid,
				 long telephoneNumber,
				 long ticketNumber,
				 long operadoraid,
				 TelefoniaEnum telefonia) {
			this.areaid = areaid;
			this.telephoneNumber = telephoneNumber;
			this.ticketNumber = ticketNumber;
			this.operadoraid = operadoraid;
			this.telefonia = telefonia;
			
		}
		
		@Override
	    public Integer call() {
			try {
				NumeroPortado np = new NumeroPortado();
				np.setAreaid(areaid);
				np.setNumero(telephoneNumber);
				np.setDataDeModificacao(new Date());
				np.setDataDeInclusao(new Date());
				np.setNumeroDeModificacoes(0L);
				np.setNumerodobilhete(ticketNumber);
				np.setOperadoraid(operadoraid);
				np.setTelefonia(telefonia);
				numeroPortadoRepoImpl.Insert(np);	
				
				return 1;
			} catch(Exception ex) {
				logger.info(String.format("[call] Exception : ", ex.toString()));
				return 0;
			}
		}
		
	};
	
	private List<NumeroPortado> listAbrFull;
	
	
	private static final Logger logger = LoggerFactory.getLogger(ProcessFileAbr.class);
	
	private File dirIn;
	private File dirOut;
	private File dirIgnorados;
	private File dirRejeitados;
	private File fileRejeitados;
	private BufferedWriter bufWout;
	
	@Autowired
	private PaisRepoImpl paisRepoImpl;
	
	@Autowired
	private EstadoRepoImpl estadoRepoImpl;
	
	@Autowired
	private SerieRepoImpl serieRepoImpl;
	
	@Autowired
	private OperadoraRepoImpl operadoraRepoImpl;
	
	@Autowired
	private AreaRepoImpl areaRepoImpl;
	
	@Autowired
	NumeroPortadoRepoImpl numeroPortadoRepoImpl;
	
	@Autowired
	private HistoricoRepoImpl historicoRepoImpl;
	
	ExecutorService executorServiceAbrFull;
	ExecutorService executorServiceAbrDelta;

	
	List<Future<Integer>> resultListAbrFull;
	List<Future<Integer>> resultListAbrDelta;
	
	
	
	private static HashMap<Short, Long> paises = new HashMap<Short, Long>();
	private static HashMap<String, Long> areas = new HashMap<String, Long>();
	private static HashMap<String, Long> operadoras = new HashMap<String, Long>();
	
	private HashMap<String, Serie> 			seriesMap = null;
	private HashMap<String, Area> 				areaMap = null;
	private HashMap<Short, Operadora> 			operadoraMap = null;
	private HashMap<Short, Long>			spid2operadoraidMap = null;
	
	private static List<Area> lstNotInAreaList = new ArrayList<Area>();
	private static List<Long> lstOperadoraidFromSpidNottInList = new ArrayList<Long>();
	private static List<String> lstSpidNotInteger = new ArrayList<String>();
	private static List<LineEnum> lstInvalidFormat = new ArrayList<LineEnum>();
	
	private FiltroDeArquivosAbrFull filtroAbrFull;	
	
	private FiltroDeArquivosAbrDelta filtroAbrDelta;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	
	
	
	public void processDelta( File dirIn, File dirOut, File dirIgnorados, File dirRejeitados)  {
		logger.info(" ************** Starting ABR file processing ... *********************");
		
		seriesMap = new HashMap<String , Serie>();
		
		this.dirIn = dirIn;
		this.dirOut = dirOut;
		this.dirIgnorados = dirIgnorados;
		this.dirRejeitados = dirRejeitados;
		
		fileRejeitados = new File( this.dirRejeitados, "LinhasRejeitadas.txt");
		
		
		try {
			if(!fileRejeitados.exists()){
				fileRejeitados.createNewFile();
	    	}
			
			filtroAbrDelta = new FiltroDeArquivosAbrDelta();			
			String [] namesAbrDelta;
			
			namesAbrDelta = getFilenames(filtroAbrDelta);
			
			resultListAbrDelta = new ArrayList<>();
			if ( namesAbrDelta != null ) {			
				for (int i = 0; i < namesAbrDelta.length; i++) {	
					executorServiceAbrDelta =  Executors.newFixedThreadPool(NUMBER_OF_PARALLEL_THREADS_ABR_DELTA);
					
					processFileAbrDeltaInMemory(namesAbrDelta[i]);
					
					executorServiceAbrDelta.shutdown();
					resultListAbrDelta.clear();;
					
					logger.info(String.format(" ************** ABR DELTA file %s processed. *********************", namesAbrDelta[i] ));
				}
			}
			
			
			
		} catch (IOException e) {			
			e.printStackTrace();
		}		
	}
	
	public void process( File dirIn, File dirOut, File dirIgnorados, File dirRejeitados)  {

		logger.info(" ************** Starting ABR file processing ... *********************");
		
		seriesMap = new HashMap<String , Serie>();
		
		listAbrFull = new ArrayList<NumeroPortado>();
		
		this.dirIn = dirIn;
		this.dirOut = dirOut;
		this.dirIgnorados = dirIgnorados;
		this.dirRejeitados = dirRejeitados;
		
		fileRejeitados = new File( this.dirRejeitados, "LinhasRejeitadas.txt");
		
		
		try {
			if(!fileRejeitados.exists()){
				fileRejeitados.createNewFile();
	    	}
			
			filtroAbrFull = new FiltroDeArquivosAbrFull();
			//filtroAbrDelta = new FiltroDeArquivosAbrDelta();

			String [] namesAbrFull;
			//String [] namesAbrDelta;
			
			namesAbrFull = getFilenames(filtroAbrFull);
			//namesAbrDelta = getFilenames(filtroAbrDelta);
			
			
			resultListAbrFull = new ArrayList<>();
			if ( namesAbrFull != null ) {			
				for (int i = 0; i < namesAbrFull.length; i++) {		
					
					executorServiceAbrFull =  Executors.newFixedThreadPool(NUMBER_OF_PARALLEL_THREADS_ABR_FULL);
					
					processFileAbrFull(namesAbrFull[i]);
					
					executorServiceAbrFull.shutdown();
					resultListAbrFull.clear();
					
					logger.info(String.format(" ************** ABR FULL file %s processed. *********************", namesAbrFull[i] ));
				}
			}
			
			
//			if ( namesAbrDelta != null ) {			
//				for (int i = 0; i < namesAbrDelta.length; i++) {					
//					processFileAbrDeltaInMemory(namesAbrDelta[i] );
//					
//					logger.info(String.format(" ************** ABR DELTA file %s processed. *********************", namesAbrDelta[i] ));
//				}
//			}
			
		} catch (IOException e) {			
			e.printStackTrace();
		}		
	}
	
	
	
	/*
	 * 
	 * 		PROCESS FILE ABR FULL
	 * 
	 */
	
	private void processFileAbrFull(String fileName) {
		
		
		try { 
			logger.info("STARTING FULL LOADING OF FILE: " + fileName);			
			
			BufferedReader brFile = new BufferedReader(new FileReader(new File(dirIn, fileName)));
			
			Pais pais = paisRepoImpl.procuraPor_Nome("BRASIL").get(0);
			if ( pais != null ) {
				
				loadMaps(pais.getPaisId());
				
				if ( verifyFileCheckSum(fileName)) {
					
					parseFileAbrFull(fileName, brFile, pais);
					
				} else {
					logger.info(String.format("File %s has an INVALID CHECKSUM.", fileName));
					moveFileToRejectedDirectory(fileName);
					
					brFile.close();
					
					logger.info("................................................................");
					logger.info("\r\n");
					return;
				}
				
			} else {
				logger.info("LOADING NOT PERFORMED. CAUSE : BRASIL NOT FOUND.");
			}
			
			brFile.close();
			
			moveFileToOutputDirectory(fileName);
			
			logger.info("................................................................");
			logger.info("\r\n");
		} catch ( FileNotFoundException ex ) {
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}	
	
	private void loadMaps(Long paisid) {
		logger.info("LOADING SERIES ...");			
		seriesMap = getSeriesMap();
		
		logger.info("LOADING AREAS ...");
		areaMap = getAreaMap();
		
		logger.info("LOADING OPERADORAS ...");
		operadoraMap = getOperadoraMap(paisid);
		

	}
	
	
	private void parseFileAbrFull( String fileName, BufferedReader brFile, Pais pais ) {
		
		int numberOfLines = 0;
		int lineNumber = 0;
				
		String line = "";
		Date dateOfGeneration = new Date();
		LineEnum enumRet;

		
		String[] fields;
		long initTimeMs = System.currentTimeMillis();
		
		try {

			if( (seriesMap != null) && (areaMap != null) && (operadoraMap != null) ) {
				
				for (lineNumber = 1; (line = brFile.readLine()) != null; lineNumber++) {
					
					if (  lineNumber % 10000 == 0 ) {
					logger.info("-------------------------------------------------------------------------");
					logger.info(String.format("LINE NUMBER : %d of %d ", lineNumber, numberOfLines ));
					logger.info("-------------------------------------------------------------------------");
					}
					
					if ( lineNumber == 1) {
						
						enumRet = checkFirstLineLoadFullAbr(line);
						if ( enumRet != LineEnum.LINE_OK) {
							logger.info("LOADING TERMINATED DUE TO BAD FORMAT OF FIRST LINE.");
							printLineEnum(enumRet);
							
							moveFileToIgnoredDirectory(fileName);
							brFile.close();
							
							logger.info("................................................................");
							logger.info("\r\n");
							return;
						}
						
						fields = line.split(";");
						
						dateOfGeneration 	= sdf.parse(fields[0]);
						numberOfLines = Integer.parseInt(fields[1]);
						
						logger.info("-----------------------------------------------------------------------------------------------------------------------");
						logger.info("Processing " + fileName + " with " + numberOfLines + " LINES (mode=Full),  generated in " + dateOfGeneration.toString());
						logger.info("-----------------------------------------------------------------------------------------------------------------------");
						
						continue;
					}
					
					if (lineNumber < numberOfLines) {
						
				
						enumRet = checkOthersThanFirstLineLoadFullAbr(line);
						if ( enumRet != LineEnum.LINE_OK) {
							logger.info("LINE " + line + " HAS AN INVALID FORMAT AND CAN'T BE PARSED.");
							printLineEnum(enumRet);
							lstInvalidFormat.add(enumRet);
							continue;
						}
						
						fields = line.split(";");			
						Long ticketNumber = Long.parseLong(fields[0]);
						
						Long telephoneNumber = Long.parseLong(fields[2].substring(pais.getTamanhoDoDdd()));
						String ddd = 	fields[2].substring(0, pais.getTamanhoDoDdd());
											
						Short spid = 0;
						
						try {
							// Portabilidade antiga -> arquivo ProcessAbrLineBlock.java line 58
							//spid = Short.parseShort(fields[6]); 
							spid = Short.parseShort(fields[5]);
						} catch(Exception ex) {
							logger.info("Spid is not an integer (field[6]) : " + line);
							lstSpidNotInteger.add(fields[6]);
							continue;
						}
						
						Long operadoraid = getOperadoraidFromMap(spid);
						
						Area area = areaMap.get(ddd);
						TelefoniaEnum telefonia = getTelefonia(ddd, telephoneNumber, seriesMap);
						
						
						if(area != null) {
							
							if(operadoraid >= 0) {
								 
						        
					        	RecordRunnableAbrFull rrAbrFull = new RecordRunnableAbrFull( 	area.getAreaid(),
																								 telephoneNumber,
																								 ticketNumber,
																								 operadoraid,
																								 telefonia);
					            Future<Integer> result = executorServiceAbrFull.submit(rrAbrFull);
					            resultListAbrFull.add(result);
						        
					            
					            if ( lineNumber % 1000 == 0 )
					            	executorServiceAbrFull.awaitTermination(5, TimeUnit.SECONDS);
					            
								
							} else {
								if (!lstOperadoraidFromSpidNottInList.contains(operadoraid) )
									lstOperadoraidFromSpidNottInList.add(operadoraid);
								logger.info("Operadoraid from Spid NOT FOUND at " + line);
							}
							
							
						} else {
							if ( !lstNotInAreaList.contains(area) )
								lstNotInAreaList.add(area);
							logger.info("NO AREA FOUND at " + line);
						}
						
					}
					
				} 
				
				executorServiceAbrFull.awaitTermination(5, TimeUnit.SECONDS);
				
				logger.info("-------------------------------------------------------------------------");
				logger.info(String.format("- LINE NUMBER : %d of %d ", lineNumber, numberOfLines ));
				logger.info("-------------------------------------------------------------------------");
				
			} else {
				logger.info("NO SERIE FOUND");
				return;
			}

			
		} catch (IOException e) {
			logger.info("ERROR ON READ LINE FROM FILE [" + fileName + "]", e);			
		} catch (ParseException e) {		
			logger.info("ParseException : " + e.toString());
			e.printStackTrace();
		} catch (InterruptedException e) {
			logger.info("InterruptedException : " + e.toString());
		} 
		finally {
			int count = 0;
			for (int i = 0; i < resultListAbrFull.size(); i++) 
	        {
	            Future<Integer> result = resultListAbrFull.get(i);
	            Integer number = null;
	            try {
	                number = result.get();
	                if (number == 1)
	                	count++;
	            } catch (InterruptedException e) {
	                e.printStackTrace();
	            } catch (ExecutionException e) {
	                e.printStackTrace();
	            }
	            System.out.printf("Main: Task %d: %d\n", i, number);
	        }
			
			logger.info("-------------------------------------------------------------------------");
			logger.info(String.format("EXECUTION TIME : %d ms",(System.currentTimeMillis() - initTimeMs) ) );
			logger.info(String.format("LINHAS FORA DA AreaMap : %d", lstNotInAreaList.size()));
			logger.info(String.format("LINHAS COM Operadoraid NAO ENCONTRADAS A PARTIR DE Spid : %d", lstOperadoraidFromSpidNottInList.size()));
			logger.info(String.format("SPID NOT INTEGER : %d ", lstSpidNotInteger.size()));
			logger.info(String.format("RECORDS INSERTED IN DATABASE : %d", count));
			logger.info(String.format("INVALID LINES : %d", lstInvalidFormat.size()));
			logger.info(String.format("LINES PROCESSED : %d", lineNumber));
			logger.info("-------------------------------------------------------------------------");
		}
	
	}
				
		
	private TelefoniaEnum getTelefonia(String ddd, Long tn, HashMap<String, Serie> p_seriesMap) {
		String prefix = "";
		if(tn.toString().length() == 9) {
			prefix = tn.toString().substring(0, 5);
		} else if (tn.toString().length() == 8) {
			prefix = tn.toString().substring(0, 4);
		} else {
			return TelefoniaEnum.UNDEFINED;
		}
		TelefoniaEnum telefonia = TelefoniaEnum.UNDEFINED;
		if(p_seriesMap.containsKey(ddd + prefix)) {
			telefonia = p_seriesMap.get(ddd + prefix).getTelefonia();
		}
		return telefonia;
	}
	
	private NumeroPortado findNumeroPortadoList( Area area, Long tn) {
		
		NumeroPortado portado = null;
		
		portado = numeroPortadoRepoImpl.procuraPor_Area_Numero(area.getAreaid(), tn);
		
		return portado;
	}
	
	
	
	String [] getFilenames(FileFilter fileFilter) {
		File[] lista;
		String[] names;
		
		lista = dirIn.listFiles(fileFilter);
		
		if (lista == null || lista.length == 0) {
			return null;
		}
		
		names = new String[lista.length];
		for (int i = 0; i < lista.length; i++) {
			names[i] = lista[i].getName();
		}
		
		Arrays.sort(names);
		
		return names;
	}
	
	private HashMap<String, Serie> getSeriesMap() {
		HashMap<String, Serie> l_seriesMap = new HashMap<String, Serie>();
	
		List<Serie> series = serieRepoImpl.selectAll();

		for ( Serie serie : series) {
			l_seriesMap.put(serie.getSerieid().toString(), serie);
		}		
		 
		return l_seriesMap;
	}
	
	private HashMap<Short, Operadora> getOperadoraMap(Long paisid) {
		HashMap<Short, Operadora> result = new HashMap<Short, Operadora>();
		spid2operadoraidMap = new HashMap<Short,Long>();
		
		try {
		
			List<Operadora> operadoras = operadoraRepoImpl.procuraPor_Pais(paisid);
			for ( Operadora operadora : operadoras ) {
				result.put(operadora.getSpid(), operadora);
				
				if ( !spid2operadoraidMap.containsKey(operadora.getSpid()) ) {
					spid2operadoraidMap.put(operadora.getSpid(), operadora.getOperadoraId() );
				}
			}
		} catch( Exception ex ) {
			logger.info("[getOperadoraMap] Exception : " + ex.toString());
		}
		
		return result;
	}

	private HashMap<String, Area> getAreaMap() {
		HashMap<String, Area> areaMap = new HashMap<String, Area>();
		
		List<Area> areas = areaRepoImpl.selectAll();
		
		for ( Area area : areas) {
			areaMap.put(String.valueOf(area.getDdd()), area);
		}
		
		return areaMap;
	}
	
	private Long getOperadoraidFromMap(Short spid) {
		Long operadoraid = -1L;
		try {
			if ( spid2operadoraidMap.containsKey(spid) )
				operadoraid = spid2operadoraidMap.get(spid);
		} catch(Exception ex) {
			logger.info("[getOperadoraidFromMap] Exception : " + ex.toString());
		}
	
		return operadoraid;
	}
				
	
	public LineEnum checkFirstLineLoadDeltaAbr(String line, Date dateOfReference) {		
		Date dateOfGeneration = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		int numberOfLines = 0;
		
		String[] fields = line.split(";");
		
		if (fields.length != 2) {
			return LineEnum.FISRT_LINE_OUT_OF_FORMAT;			
		}
		
		try {
			dateOfGeneration = sdf.parse(fields[0]);
		} catch (ParseException ex) {
			return LineEnum.INVALID_DATE_FORMAT;			
		}
		
		try {
			numberOfLines = Integer.parseInt(fields[1]);
		} catch (NumberFormatException nfe) {
			return LineEnum.INVALID_NUMBER_OF_LINES;			
		}
		
		if (numberOfLines < 0) {
			return LineEnum.INVALID_NUMBER_OF_LINES;			
		}
		
		//if ( dateOfGeneration.getTime() <= dateOfReference.getTime() ) {
		//	logger.info(String.format("Date of Generation : %s <= Date of Reference : %s", dateOfGeneration.toString(), dateOfReference.toString()));
		//	return LineEnum.INVALID_DATE_OF_GENERATION;
		//}
		
		return LineEnum.LINE_OK;
	}
	
	public LineEnum checkFirstLineLoadFullAbr(String line) {		
		Date dateOfGeneration = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		int numberOfLines = 0;
		
		String[] fields = line.split(";");
		
		if (fields.length != 2) {
			return LineEnum.FISRT_LINE_OUT_OF_FORMAT;			
		}
		
		try {
			dateOfGeneration = sdf.parse(fields[0]);
		} catch (ParseException ex) {
			return LineEnum.INVALID_DATE_FORMAT;			
		}
		
		try {
			numberOfLines = Integer.parseInt(fields[1]);
		} catch (NumberFormatException nfe) {
			return LineEnum.INVALID_NUMBER_OF_LINES;			
		}
		
		if (numberOfLines < 0) {
			return LineEnum.INVALID_NUMBER_OF_LINES;			
		}
		
		return LineEnum.LINE_OK;
	}
	
	public LineEnum checkOthersThanFirstLineLoadDeltaAbr(String line) {
		long ticketNumber;
		int action;
		String telefoneNumber = "";		
		short spid;		
		int lineType;		
		
		String[] fields = line.split(";");
		
		if (fields.length < 3) {
			return LineEnum.TOO_SHORT;		
		}
		
		
		ticketNumber = Long.parseLong(fields[0]);
		action = Integer.parseInt(fields[1]);
		telefoneNumber = fields[2];	
		
		
		
		if (action != 0 && action != 1) {
			return LineEnum.INVALID_ACTION;			
		}
		
		if (telefoneNumber.length() >= 12) {
			return LineEnum.TELEF_NUM_INVALID_FORMAT;			
		}
		
		if (action == 0) {
			if (fields.length < 6) {
				return LineEnum.FIELDS_SMALLER_THAN_6;				
			}
			
			try {
				lineType = Integer.parseInt(fields[3]);
			} catch (NumberFormatException nfe) {
				lineType = 0;
			}
			
			
			if (lineType != 0 && lineType != 2) {
				return LineEnum.INVALID_LINE_TYPE;				
			}
			
			try {
				spid = Short.parseShort(fields[5]);
			} catch (NumberFormatException nfe) {
				spid = 0;
			}
			if (spid < 0) {
				return LineEnum.SPID_INVALID;				
			}
		} 
		
		return LineEnum.LINE_OK;
		
	}
	
	public LineEnum checkOthersThanFirstLineLoadFullAbr(String line) {
		long ticketNumber;
		int action;
		String telefoneNumber = "";		
		short spid;		
		int lineType;		
		int carrierNumber;
		
		String[] fields = line.split(";");
		
		if (fields.length < 6) {
			return LineEnum.TOO_SHORT;		
		}
		
		
		ticketNumber = Long.parseLong(fields[0]);
		action = Integer.parseInt(fields[1]);
		telefoneNumber = fields[2];
		carrierNumber = Integer.parseInt(fields[5]);
		
		
		if (action != 0 && action != 1) {
			return LineEnum.INVALID_ACTION;			
		}
		
		if (telefoneNumber.length() >= 12) {
			return LineEnum.TELEF_NUM_INVALID_FORMAT;			
		}
		
		/*
		if (action == 0) {
			if (fields.length < 6) {
				return LineEnum.FIELDS_SMALLER_THAN_6;				
			}
			
			try {
				lineType = Integer.parseInt(fields[3]);
			} catch (NumberFormatException nfe) {
				lineType = 0;
			}
			
			if (lineType != 0 && lineType != 2) {
				return LineEnum.INVALID_LINE_TYPE;				
			}
			try {
				spid = Short.parseShort(fields[5]);
			} catch (NumberFormatException nfe) {
				spid = 0;
			}
			if (spid < 0) {
				return LineEnum.SPID_INVALID;				
			}
		} 
		*/
		
		return LineEnum.LINE_OK;
		
	}
	
	/*
	 * 
	 * 		PROCESS FILE ABR DELTA
	 * 
	 */
	
	private void processFileAbrDeltaInMemory(String fileName ) {
		logger.info("STARTING DELTA LOADING OF FILE: " + fileName);	
		
		List<Historico> lista = null;
		try {
			lista = historicoRepoImpl.RetornoOrdernado();
		} catch( Exception ex ) {
			logger.info("Erro em historicoRepoImpl.RetornoOrdernado()  : " + ex.toString());
		}
		
		if (lista == null || lista.size() == 0) {
			logger.info("LOADING NOT PERFORMED : THERE WAS NO FORMER FULL LOADING PRIOR THIS DELTA LOADING.");			
			return;
		}
		
		Historico hist = (Historico) lista.get(0);
		Date dataDeReferencia = hist.getDataDeReferencia();
		
		if ( checkIfFileWasAlreadyProcessed(fileName) ) {
			logger.info("****************************************************");
			logger.info("");
			logger.info(String.format("FILE %s ALREADY PROCESSED.", fileName));
			logger.info("");
			logger.info("****************************************************");
			moveFileToOutputDirectory(fileName);
			
			return;
		}
		
		if ( verifyFileCheckSum(fileName)) {
			logger.info(String.format("Going to Load file %s", fileName));
			loadDeltaAbrFileToList(fileName, dataDeReferencia);
			
		} else {
			logger.info(String.format("File %s has an INVALID CHECKSUM.", fileName));
			moveFileToRejectedDirectory(fileName);
		}
	}
	
	
	private boolean checkIfFileWasAlreadyProcessed(String fileName) {
		boolean bFound = false;
		List<Historico> hist = null;
		hist = historicoRepoImpl.ProcuraPorArquivo(fileName);
		
		if ( hist.size() == 0 ) {
			
			Historico histUpdate = new Historico();
			histUpdate.setHistoricoId(null);
			histUpdate.setDataDeProcessamento( new Timestamp(new Date().getTime() ) );
			histUpdate.setDataDeReferencia( new Timestamp(new Date().getTime() ) );
			histUpdate.setCargaFull(0);
			histUpdate.setProcessado(0);
			histUpdate.setNumeroDeRegistrosProcessados(0);
			histUpdate.setArquivo(fileName);
			histUpdate.setVersion(1);
			
			historicoRepoImpl.insert(histUpdate);
			
			return false;
		} else {
			hist = null;
			hist = historicoRepoImpl.ProcuraPorProcessadoAcima24horas(fileName);
			
			if ( hist.size() > 0 ) {
				logger.info(String.format("Arquivo %s com processamento incompleto.", fileName));
				logger.info(String.format("Data da última tentativa de processamento : %s", hist.get(0).getDataDeProcessamento().toString()));
				
				return false;
			}
			
			return true;
		}
		
	}
	
	
	private void loadDeltaAbrFileToList(String fileName, Date dataDeReferencia) {
		
		List<String> fileLinesList = new ArrayList<String>();
		String line;
		
		logger.info("STARTING DELTA LOADING OF FILE: " + fileName);
		BufferedReader brFile;
		try {
			brFile = new BufferedReader(new FileReader(new File(dirIn, fileName)));
		
			line = "";
			while ( (line = brFile.readLine()) != null) {
				fileLinesList.add(line);
				line = "";
			}
			brFile.close();
			
			logger.info(String.format("FILE %s LOADED TO MEMORY LIST.",fileName) );
			processDeltaAbrFileFromList( fileLinesList, fileName, dataDeReferencia );
			
		
		} catch (FileNotFoundException e) {
			logger.info(String.format("Exception : %s", e.toString()));
			e.printStackTrace();
		}
		 catch (IOException e) {
			 logger.info(String.format("Exception : %s", e.toString()));
			e.printStackTrace();
		}
		
	}
	
	
	private void processDeltaAbrFileFromList(	List<String> fileLinesList, String fileName, 
												Date dataDeReferencia ) {
		Pais brasil;
		int lineNumber = 0;
		
		Date dateOfGeneration = new Date();
		int numberOfLines = 0;
		LineEnum enumRet;
		String[] fields;
		
		String checkSum = "";
		long ticketNumber;
		int action;
		String telephoneNumber = "";		
		short spid = 0;		
		int lineType;

		
		try {
			brasil = paisRepoImpl.procuraPor_Nome("BRASIL").get(0);
		} catch (NoResultException nre) {
			logger.info("Country BRASIL was NOT FOUND.");
			return;
			
		}

		List<Serie> series = null;
		try {
			series = serieRepoImpl.buscaPorPais(brasil.getPaisId());
		} catch (Exception nre) {
			logger.info("Could not get SERIES for BRASIL.");			
		}

		if ( series != null ) {
			for (Serie aSerie : series) {
				String key = aSerie.getEspecial() + ":" + aSerie.getSerie();
				seriesMap.put(key, aSerie);
			}
		}
		
		try {
			long tempo1 = System.currentTimeMillis();
			long tempo2;
			
			lineNumber = 1;
			for(String line: fileLinesList) {
				if ( lineNumber == 1) {					
					
					
					enumRet = checkFirstLineLoadDeltaAbr(line, dataDeReferencia);
					if ( enumRet != LineEnum.LINE_OK) {
						logger.info("LOADING TERMINATED DUE TO BAD FORMAT OF FIRST LINE.");
						printLineEnum(enumRet);
						
						moveFileToIgnoredDirectory(fileName);
						
						
						logger.info("................................................................");
						logger.info("\r\n");
						return;
					}
					
					fields = line.split(";");
					
					
					dateOfGeneration 	= sdf.parse(fields[0]);
					
					numberOfLines 		= Integer.parseInt(fields[1]);
					
					logger.info("----------------------------------------------------------------------------------------------------------------");
					logger.info("Processing " + fileName + " with " + numberOfLines + " LINES (mode=Delta),  generated in " + dateOfGeneration.toString());
					logger.info("----------------------------------------------------------------------------------------------------------------");
					
					lineNumber++;
					continue;
				}
				
				if (lineNumber % 200 == 0) {
					tempo2 = System.currentTimeMillis();
					logger.info("Analyzing line " + lineNumber + " of " + numberOfLines + " in " + (tempo2 - tempo1) + " ms.");
					tempo1 = tempo2;
				}
				
				if (lineNumber < numberOfLines) {					
					
					enumRet = checkOthersThanFirstLineLoadDeltaAbr(line);
					if ( enumRet != LineEnum.LINE_OK) {
						logger.info("LINE " + line + " HAS AN INVALID FORMAT AND CAN'T BE PARSED.");
						printLineEnum(enumRet);
						
						lineNumber++;
						continue;
					}
					
					fields = line.split(";");
					
					ticketNumber 	= Long.parseLong(fields[0]);
					action 			= Integer.parseInt(fields[1]);
					telephoneNumber = fields[2];
	
					if ( action == 0 ) {
						try {
							spid = Short.parseShort(fields[5]);
						} catch (NumberFormatException nfe) {
							logger.info("LINE " + line + " HAS AN INVALID SPID CAN'T BE PARSED.");
							lineNumber++;
							continue;
						}
					}
					
					RecordRunnableAbrDelta rrAbrDelta = new RecordRunnableAbrDelta(	lineNumber,numberOfLines, action,
																					ticketNumber, telephoneNumber, spid);
					Future<Integer> result = executorServiceAbrDelta.submit(rrAbrDelta);
					resultListAbrDelta.add(result);
					
		            if ( lineNumber % 1000 == 0 )
		            	executorServiceAbrDelta.awaitTermination(5, TimeUnit.SECONDS);
								
				}
				
				if ( lineNumber == numberOfLines) 				
					checkSum = line;				
	
				
				lineNumber++;
			}
			
			long histId = historicoRepoImpl.RetornaHistoricoIdPorArquivo(fileName);
			
			List<Historico> listHist = historicoRepoImpl.searchHistoricoById(histId); 
			Historico histUpdate = listHist.get(0);
			if ( histUpdate != null) {
				
			
				histUpdate.setDataDeProcessamento( new Timestamp(new Date().getTime() ) );
				histUpdate.setDataDeReferencia( new Timestamp(dateOfGeneration.getTime() ) );
				histUpdate.setCargaFull(0);
				histUpdate.setProcessado(1);
				histUpdate.setNumeroDeRegistrosProcessados(numberOfLines - 2);
				histUpdate.setArquivo(fileName);
				histUpdate.setVersion(1);
				
				historicoRepoImpl.update(histUpdate);
			}
			
			//historicoRepoImpl.UpdatePorAquivoNaoProcessado(new Timestamp(new Date().getTime() ), numberOfLines - 2, fileName);
			
			
			moveFileToOutputDirectory(fileName);
			
			logger.info("................................................................");
			logger.info("\r\n");
		}	catch (ParseException e) {
			e.printStackTrace();
			logger.info(String.format("ParseException : %s", e.toString()) );
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info(String.format("InterruptedException : %s", e.toString()));
		}
		finally {
			int count = 0;
			for (int i = 0; i < resultListAbrDelta.size(); i++) 
	        {
	            Future<Integer> result = resultListAbrDelta.get(i);
	            Integer number = null;
	            try {
	                number = result.get();
	                if (number == 1)
	                	count++;
	            } catch (InterruptedException e) {
	                e.printStackTrace();
	            } catch (ExecutionException e) {
	                e.printStackTrace();
	            }
	            System.out.printf("Main: Task %d: %d\n", i, number);
	        }
			
			logger.info("-------------------------------------------------------------------------");
			logger.info(String.format("ACTIONS TAKEN(I/U/D) : %d", count));
			logger.info(String.format("LINES PROCESSED : %d", lineNumber));
			logger.info("-------------------------------------------------------------------------");
		}
			
	}
	
	
	
	private void loadDeltaAbrFile(String fileName , Historico hist, ExecutorService executorServiceAbrDelta) {
		Pais brasil;
		String line = "";
		
		Date dateOfGeneration = new Date();
		int numberOfLines = 0;
		LineEnum enumRet;
		String[] fields;
		
		String checkSum = "";
		long ticketNumber;
		int action;
		String telephoneNumber = "";		
		short spid;		
		int lineType;
		
		logger.info("STARTING DELTA LOADING OF FILE: " + fileName);		

		try {
			brasil = paisRepoImpl.procuraPor_Nome("BRASIL").get(0);
		} catch (NoResultException nre) {
			logger.info("Country BRASIL was NOT FOUND.");
			return;
			
		}

		List<Serie> series = null;
		try {
			//series = serieRepoImpl.buscaPorPais(brasil);
			series = serieRepoImpl.buscaPorPais(brasil.getPaisId());
		} catch (Exception nre) {
			logger.info("Could not get SERIES for BRASIL.");			
		}

		if ( series != null ) {
			for (Serie aSerie : series) {
				String key = aSerie.getEspecial() + ":" + aSerie.getSerie();
				seriesMap.put(key, aSerie);
			}
		}
		
		
		try {
			BufferedReader brFile = new BufferedReader(new FileReader(new File(dirIn, fileName)));
			
			long tempo1 = System.currentTimeMillis();
			long tempo2;

			
			for (int lineNumber = 1; (line = brFile.readLine()) != null; lineNumber++) {
				
				if ( lineNumber == 1) {					
					
					enumRet = checkFirstLineLoadDeltaAbr(line, hist.getDataDeReferencia());
					if ( enumRet != LineEnum.LINE_OK) {
						logger.info("LOADING TERMINATED DUE TO BAD FORMAT OF FIRST LINE.");
						printLineEnum(enumRet);
						
						moveFileToIgnoredDirectory(fileName);
						brFile.close();
						
						logger.info("................................................................");
						logger.info("\r\n");
						return;
					}
					
					fields = line.split(";");
					
					dateOfGeneration 	= sdf.parse(fields[0]);
					numberOfLines 		= Integer.parseInt(fields[1]);
					
					logger.info("----------------------------------------------------------------------------------------------------------------");
					logger.info("Processing " + fileName + " with " + numberOfLines + " LINES (mode=Delta),  generated in " + dateOfGeneration.toString());
					logger.info("----------------------------------------------------------------------------------------------------------------");
					
					continue;
				}
				
				if (lineNumber % 200 == 0) {
					tempo2 = System.currentTimeMillis();
					logger.info("Analyzing line " + lineNumber + " of " + numberOfLines + " in " + (tempo2 - tempo1) + " ms.");
					tempo1 = tempo2;
				}
				
				if (lineNumber < numberOfLines) {					
					
					enumRet = checkOthersThanFirstLineLoadDeltaAbr(line);
					if ( enumRet != LineEnum.LINE_OK) {
						logger.info("LINE " + line + " HAS AN INVALID FORMAT AND CAN'T BE PARSED.");
						printLineEnum(enumRet);
						
						continue;
					}
					
					fields = line.split(";");
					
					ticketNumber 	= Long.parseLong(fields[0]);
					action 			= Integer.parseInt(fields[1]);
					telephoneNumber = fields[2];

					try {
						lineType = Integer.parseInt(fields[3]);
					} catch (NumberFormatException nfe) {
						logger.info("LINE " + line + " HAS AN INVALID LINETYPE CAN'T BE PARSED.");
						continue;
					}					
					
					if (lineType != 0 && lineType != 2) {
						logger.info("SKIP CNG [" + lineType + "]");
						continue;
					}
					
					try {
						spid = Short.parseShort(fields[5]);
					} catch (NumberFormatException nfe) {
						logger.info("LINE " + line + " HAS AN INVALID SPID CAN'T BE PARSED.");
						continue;
					}					
					
					treatMsisdn(lineNumber , numberOfLines, action,ticketNumber, telephoneNumber, spid);					
				}
				
				if ( lineNumber == numberOfLines) 				
					checkSum = line;				
				
			}
			
			brFile.close();
			
			
			Historico histUpdate = new Historico();
			histUpdate.setHistoricoId(0L);
			histUpdate.setDataDeProcessamento( new Timestamp(new Date().getTime() ) );
			histUpdate.setDataDeReferencia( new Timestamp(dateOfGeneration.getTime() ) );
			histUpdate.setCargaFull(0);
			histUpdate.setProcessado(1);
			histUpdate.setNumeroDeRegistrosProcessados(numberOfLines - 2);
			histUpdate.setArquivo(fileName);
			histUpdate.setVersion(1);
			
			historicoRepoImpl.insert(histUpdate);
			
			moveFileToOutputDirectory(fileName);
			
			logger.info("................................................................");
			logger.info("\r\n");
		} catch (IOException ex) {
			logger.info(String.format("IOException Error at FILE %s DELTA LOAD PROCESSING", fileName) );
		} catch (ParseException e) {
			e.printStackTrace();
			logger.info(String.format("ParseException Error at FILE %s DELTA LOAD PROCESSING", fileName) );
		}
	}
	
	private void printLineInformation(int lineNumber, int numberOfLines) {
		StringBuilder sb = new StringBuilder();
		sb.append("\r\n");
		sb.append("---------------------------------------------------------------------------------------------------------------------").append("\r\n");
		sb.append("|                                                                                                                   |").append("\r\n");
		sb.append(String.format("|                        LINE NUMBER : %43s     of %25s |", String.valueOf(lineNumber) , String.valueOf(numberOfLines) ) ).append("\r\n");
		sb.append("|                                                                                                                   |").append("\r\n");
		sb.append("---------------------------------------------------------------------------------------------------------------------").append("\r\n");
		
		logger.info(sb.toString());
	}
	
	private int treatMsisdn(int lineNumber , int numberOfLines, int action, long ticketNumber , 
								String telephoneNumber , short spid ) {
		String msisdn = "";
		int tamanhoDoDdi;
		Pais pais = null;
		String nomeDoPais = "";
		short ddi = -1;
		short ddd = -1;
		long numero = -1;
		Area area = null;
		String nomeDoEstado = "";
		String siglaDoEstado = "";
		Operadora operadora = null;
		InfoMsisdn infoMsisdn = null;
		boolean bFoundCarrier = false;
		
		
		printLineInformation(lineNumber, numberOfLines);		
		
		infoMsisdn = new InfoMsisdn();			
			
		msisdn = "55" + telephoneNumber;
		
		logger.debug("[treatMsisdn]");
		logger.debug(String.format("msisdnIn : %s", msisdn));			
		
		msisdn = msisdn.trim();
		infoMsisdn.setMsisdn(msisdn);
		
		if (msisdn.matches(".*[^0-9].*")) {
			logger.info("Non numeric msisdn " + msisdn);
			return 0;
		}
		
		
		for (tamanhoDoDdi = 1; tamanhoDoDdi < 5; tamanhoDoDdi++) {
			pais = procuraPais(msisdn, tamanhoDoDdi);
			if (pais != null) {
				break;
			} else {
				logger.info("Procurando pelo proximo DDI...");
			}
		}
		
		if (pais == null) {
			logger.info(String.format("Erro na leitura do pais do msisdn %s", msisdn));
			return 0;				
		}
		infoMsisdn.setPais(pais);
		
		if (msisdn.length() <= (tamanhoDoDdi + pais.getTamanhoDoDdd())) {
			logger.info(String.format("MSISDN %s com tamanho invalido", msisdn));
			return 0;				
		}
		
		nomeDoPais = pais.getNome();
		infoMsisdn.setNomeDoPais(nomeDoPais);
		logger.info(String.format("[treatMsisdn] Nome do pais : %s", nomeDoPais));
		
		ddi = pais.getDdi();
		infoMsisdn.setDdi(ddi);
		logger.info(String.format("[treatMsisdn] DDI  do pais : %d", ddi));
		
		if (pais.getTamanhoDoDdd() == 0) {
			ddd = 0;
			numero = Long.parseLong(msisdn.substring(tamanhoDoDdi));
		} else {
			ddd = Short.parseShort(msisdn.substring(tamanhoDoDdi, tamanhoDoDdi + pais.getTamanhoDoDdd()));
			numero = Long.parseLong(msisdn.substring(tamanhoDoDdi + pais.getTamanhoDoDdd()));
		}
		infoMsisdn.setDdd(ddd);
		infoMsisdn.setNumero(numero);
		infoMsisdn.setSpid(spid);
		
		area = procuraArea(pais, ddd);
		if (area != null) {
			Estado estado = estadoRepoImpl.searchById(area.getEstadoid());
			
			nomeDoEstado = estado.getNome();
			siglaDoEstado = estado.getSigla();
			
			logger.info(String.format("[inicia] Nome do Estado: %s", nomeDoEstado));
			logger.info(String.format("[inicia] Sigla do Estado: %s", siglaDoEstado));
			
			infoMsisdn.setArea(area);
			infoMsisdn.setNomeDoEstado(nomeDoEstado);
			infoMsisdn.setSiglaDoEstado(siglaDoEstado);
		} else {
			logger.info(String.format("Area NOT FOUND for Numero %d.", numero) );
			logger.info("................................................................");
			logger.info("\r\n");
			return 0;
		}
			

		
		if ( action == 0 ) {
			logger.info("ACTION TYPE : INSERT or UPDATE");
			
			
			if ( searchGenericCarrier(infoMsisdn) ) {
				logger.info("Generic carrier found.");
				
				if ( infoMsisdn.getOperadora() != null ) {
					infoMsisdn.setNomeDaOperadora(infoMsisdn.getOperadora().getNome());
					infoMsisdn.setNomeDaOperadoraNormalizado(infoMsisdn.getOperadora().getNomeNormalizado());
					infoMsisdn.setCodigoDaOperadora(infoMsisdn.getOperadora().getCodigo());
					
					logger.info("         Operadora :                                    ");
					logger.info(String.format("........nome da operadora              : %s", infoMsisdn.getOperadora().getNome()));
					logger.info(String.format("........nome da operadora normalizado  : %s", infoMsisdn.getOperadora().getNomeNormalizado() ) );
					logger.info(String.format("........codigo da operadora            : %d", infoMsisdn.getOperadora().getCodigo() ) );
					logger.info(String.format("........spid                           : %d", infoMsisdn.getOperadora().getSpid() ) );
					
				} else {
					logger.info("infoMsisdn : Operadora NOT set.");
				}
				
			} else {
				logger.info("Generic carrier NOT found : ");
				logger.info("    1 ) Operadora NAO ENCONTRADA na tabela NumeroPortado.");	
				logger.info("    or   ");
				logger.info("    2 ) Prefixo nao encontrado para a areaid na tabela serie");
				return 0;
			}
			
			
			
			//SELECT o FROM Operadora o WHERE o.paisid = %d AND o.spid = %d
			if ( !searchCarrierByCountryAndSpid(infoMsisdn) ) {
				logger.info(String.format("Carrier NOT FOUND for country %s and Spid %d", infoMsisdn.getPais().getNome() , spid ) );
				return 0;
			} else {
				infoMsisdn.setNomeDaOperadora(infoMsisdn.getOperadora().getNome());
				infoMsisdn.setNomeDaOperadoraNormalizado(infoMsisdn.getOperadora().getNomeNormalizado());
				infoMsisdn.setCodigoDaOperadora(infoMsisdn.getOperadora().getCodigo());
				infoMsisdn.setPrecisao(0);
				
				//logger.info("         Operadora :                                      ");
				//logger.info(String.format("........nome da operadora              : %s", infoMsisdn.getOperadora().getNome()));
				//logger.info(String.format("........nome da operadora normalizado  : %s", infoMsisdn.getOperadora().getNomeNormalizado() ) );
				//logger.info(String.format("........codigo da operadora            : %d", infoMsisdn.getOperadora().getCodigo() ) );
			}
			
			if ( infoMsisdn.getOperadora() != null)
				updatePortedNumber( infoMsisdn , ticketNumber, seriesMap );				
			
						
			
		} else {
			logger.info("ACTION TYPE : DELETE");
			apagarMsisdn(infoMsisdn);
		}
		
		logger.info("................................................................");
		logger.info("\r\n");
		
		return 1;

	}
	
	private void processFileAbrDelta(String fileName , ExecutorService executorServiceAbrDelta) {
		logger.info("STARTING DELTA LOADING OF FILE: " + fileName);	
		
		List<Historico> lista;
		lista = historicoRepoImpl.RetornoOrdernado();
		
		if (lista == null || lista.size() == 0) {
			logger.info("LOADING NOT PERFORMED : THERE WAS NO FORMER FULL LOADING PRIOR THIS DELTA LOADING.");
			return;
		}
		
		Historico hist = (Historico) lista.get(0);
		
		if ( checkIfFileWasAlreadyProcessed(fileName) ) {
			logger.info("***********************************************************");
			logger.info(String.format("FILE %s WAS ALREADY PROCESSED.", fileName));
			logger.info("***********************************************************");
			moveFileToOutputDirectory(fileName);
			
			return;
		}
		
		if ( verifyFileCheckSum(fileName))
			loadDeltaAbrFile(fileName, hist, executorServiceAbrDelta);
		else {
			logger.info(String.format("File %s has an INVALID CHECKSUM.", fileName));
			moveFileToRejectedDirectory(fileName);
		}
	}
	
	private boolean searchGenericCarrier(InfoMsisdn infoMsisdn) {
		logger.info("[searchGenericCarrier]");
		
		String prefixoStr = "";
		String numeroStr = "";
		Operadora operadora = null;
		int tamanhoPrefixo;
		int prefixo;
		boolean bFoundCarrier = false;
		
		//SELECT np FROM NumeroPortado np WHERE np.areaid = %d AND np.numero = %d
		if ( searchCarrierAtNumeroPortadoByAreaAndNumber(infoMsisdn) ) {
			infoMsisdn.setPrecisao(0);
			
			logger.info("CARRIER FOUND At NumeroPortado table by Area and Number.");
			return true;
		}
		
		numeroStr = Long.toString(infoMsisdn.getNumero());
        tamanhoPrefixo = numeroStr.length() - 4;
        
        logger.info(String.format("Numero : %s", numeroStr));
        logger.info(String.format("Tamanho do prefixo : %d", tamanhoPrefixo));
        
        if (tamanhoPrefixo >= 3 && tamanhoPrefixo <= 5)
        {
            
            if (infoMsisdn.getDdd() > 0)
            {
                prefixoStr = Short.toString(infoMsisdn.getDdd()) + numeroStr.substring(0, tamanhoPrefixo);
            }
            else
            {
                prefixoStr = numeroStr.substring(0, tamanhoPrefixo);
            }
            
            prefixo = Integer.parseInt(prefixoStr);            
            
            //SELECT s FROM Serie s WHERE s.areaid = %d AND s.especial = %d AND s.serie = %d
            bFoundCarrier = searchCarrierBySerie (infoMsisdn , prefixo);
            if ( bFoundCarrier )
            {
            	infoMsisdn.setPrecisao(1);            	
                return true;
            }
        }
		
        
        return false;		
		
	}
	
	
	private boolean searchCarrierByCountryAndSpid(InfoMsisdn infoMsisdn) {		
				
		logger.info("[searchCarrierByCountryAndSpid]");
		StringBuffer sb = new StringBuffer();
		
		
		try {
			//SELECT o FROM Operadora o WHERE o.paisid = %d AND o.spid = %d
			List<Operadora> query = operadoraRepoImpl.procuraPor_Pais_Spid(infoMsisdn.getPais().getPaisId(), infoMsisdn.getSpid());
			
			if(!query.isEmpty()) {
				infoMsisdn.setOperadora(query.get(0));
				return true;
			} else {
				infoMsisdn.setOperadora(null);  
				
				logger.info("Operadora not found for pais = %d and spid = %d", infoMsisdn.getPais().getPaisId(), infoMsisdn.getSpid());				
				return false;
			}
		} catch (NoResultException nre) {
			infoMsisdn.setOperadora(null);
			return false;
		}		

	}
	
	public void apagarMsisdn (InfoMsisdn infoMsisdn)  {
		
		logger.info("[apagarMsisdn]");
				
		NumeroPortado numeroPortado = null;
		
		try {
			if ( infoMsisdn.getArea() == null ) {
				logger.info("[apagarMsisdn] Area NOT FOUND for msisdn " + infoMsisdn.getNumero());
				return;				
			}		
			
			numeroPortado = numeroPortadoRepoImpl.procuraPor_Area_Numero(infoMsisdn.getArea().getAreaid(), infoMsisdn.getNumero());			
		} catch (NoResultException nre) {
			return;
		}
		
		if ( numeroPortado != null) {
			try {			
				numeroPortadoRepoImpl.Delete(numeroPortado);	
				logger.info(String.format("[apagarMsisdn] Numero portado %d DELETED.",infoMsisdn.getNumero() ) );
			} catch (Exception e) {
				logger.info(String.format("[apagarMsisdn] Deletion of MSISDN %s FAILED.", infoMsisdn.getMsisdn()));
			}		
		} else {
			logger.info("[apagarMsisdn] NumeroPortado NOT FOUND.");
		}
		
		
	}

	public void updatePortedNumber( InfoMsisdn infoMsisdn , long numeroDoBilhete, HashMap<String, Serie> seriesList) {

		NumeroPortado numeroPortado;
		
		//SELECT np FROM NumeroPortado np WHERE np.areaid = %d AND np.numero = %d
		numeroPortado = numeroPortadoRepoImpl.procuraPor_Area_Numero(infoMsisdn.getArea().getAreaid(), infoMsisdn.getNumero());		
		
		TelefoniaEnum telefonia = getTelefonia(String.valueOf(infoMsisdn.getArea().getDdd()), infoMsisdn.getNumero(), seriesList);
		
		logger.info("Changing MSISDN  " + infoMsisdn.getMsisdn() + " to the Carrier " + infoMsisdn.getNomeDaOperadora() );
		
		Timestamp tsNow = new Timestamp(new Date().getTime());
		
		try {
			if ( numeroPortado != null ) {
				numeroPortado.setOperadoraid(infoMsisdn.getOperadora().getOperadoraId());
				numeroPortado.setDataDeModificacao(tsNow);
				numeroPortado.setNumeroDeModificacoes(numeroPortado.getNumeroDeModificacoes() + 1);
				numeroPortado.setNumerodobilhete(new Long(numeroDoBilhete));
				numeroPortado.setTelefonia(telefonia);
				
				numeroPortadoRepoImpl.Update(numeroPortado);
				logger.info(String.format("Numero portado %d UPDATED.",infoMsisdn.getNumero() ) );
			} else {
				logger.info(String.format("Numero portado %d not found.",infoMsisdn.getNumero() ) );
				
				numeroPortado = new NumeroPortado();
				numeroPortado.setNumeroPortadoId(0L);
				numeroPortado.setAreaid(infoMsisdn.getArea().getAreaid());
				numeroPortado.setDataDeInclusao(tsNow);
				numeroPortado.setDataDeModificacao(tsNow);
				numeroPortado.setNumero(infoMsisdn.getNumero());
				numeroPortado.setNumeroDeModificacoes((long) 0);
				numeroPortado.setOperadoraid(infoMsisdn.getOperadora().getOperadoraId());
				numeroPortado.setNumerodobilhete(new Long(numeroDoBilhete));
				numeroPortado.setTelefonia(telefonia);
				
				numeroPortadoRepoImpl.Insert(numeroPortado);
				logger.info(String.format("Numero %d INSERTED at NumeroPortado.",infoMsisdn.getNumero() ) );
			}	
			
		}
		catch (Exception e) {
			logger.info(String.format("Could NOT Update PortedNumber %d", infoMsisdn.getNumero() ) );
			
		}
		
	}
	
	

	private boolean searchCarrierAtNumeroPortadoByAreaAndNumber(InfoMsisdn infoMsisdn) {
		logger.info(String.format("[searchCarrierByAreaAndNumber] numero : %d", infoMsisdn.getNumero()));

		NumeroPortado numeroPortado;		
		
		logger.info("Search at NumeroPortado table where : ");
		logger.info(String.format("........area.areaid   : %d", infoMsisdn.getArea().getAreaid()));
		logger.info(String.format("........area.ddd      : %d", infoMsisdn.getArea().getDdd()));
		logger.info(String.format("........area.estadoid : %d", infoMsisdn.getArea().getEstadoid()));
		logger.info(String.format("........area.paisid   : %d", infoMsisdn.getArea().getPais()));
		logger.info(String.format("........numero        : %d", infoMsisdn.getNumero() ));
					
		
		try {		
			//SELECT np FROM NumeroPortado np WHERE np.areaid = %d AND np.numero = %d
			numeroPortado = numeroPortadoRepoImpl.procuraPor_Area_Numero(infoMsisdn.getArea().getAreaid(), infoMsisdn.getNumero());
			if ( numeroPortado != null ) {								
				
				Operadora operadora = operadoraRepoImpl.selectOperadoraById(numeroPortado.getOperadoraid());
				infoMsisdn.setOperadora( operadora );
				infoMsisdn.setTelefonia( numeroPortado.getTelefonia() );
				infoMsisdn.setPortado(1);
				
				logger.info("[searchCarrierByAreaAndNumber] Operadora FOUND.");				
				
			} else {
				logger.info("[searchCarrierByAreaAndNumber] Operadora NOT FOUND.");
				return false;
			}
			
		} catch (NoResultException nre) {
			return false;
		}
		catch(SQLGrammarException ex) {
			logger.debug("[procuraOperadora] Exception : %s", ex.fillInStackTrace());
			return false;
		}		
		
		return true;		
	}
	
	
	
	
	private boolean searchCarrierBySerie( InfoMsisdn infoMsisdn, int prefixo) {
		logger.info("[searchCarrierBySerie]");
		List<Serie> series;
		Query query;
		Serie serie;

		TelefoniaEnum telefonia;
		
		if ( infoMsisdn.getArea() == null) {
			return false;
		}

		series = serieRepoImpl.procuraPor_Area_Especial_Serie(infoMsisdn.getArea().getAreaid(), (short) (1), (int)prefixo);
				
		try {
			if ( series == null ) {				
				return false;
			}
			if ( series.size() == 0 ) {				
				return false;
			}
			serie = series.get(0);
			telefonia = serie.getTelefonia();
			
			Operadora operadora = operadoraRepoImpl.selectOperadoraById(serie.getOperadoraid());
			infoMsisdn.setTelefonia(telefonia);
			infoMsisdn.setOperadora(operadora);
			infoMsisdn.setNomeDaOperadoraNormalizado(operadora.getNomeNormalizado());
			infoMsisdn.setCodigoDaOperadora(operadora.getCodigo());
			
			logger.info("[searchCarrierBySerie] Operadora FOUND.");
			
		} catch (NoResultException nre) {
			return false;
		}		

		return true;
	}
	
	private Pais procuraPais(String msisdnIn, int tamanhoDoDdiIn) {
		logger.info(String.format("[procuraPais] msisdnIn : %s ; tamanhoDoDdiIn : %d", msisdnIn, tamanhoDoDdiIn));
		
		List<Pais> lista = null;
		Pais paisInterno;
		short ras;
		
		if (msisdnIn.length() < tamanhoDoDdiIn) {
			return null;
			//throw new PortabilidadeException(CodigoRetornoEnum.MSISDN_INVALIDO, "MSISDN com tamanho invalido |" + msisdn + "|");
		}
		
		ras = Short.parseShort(msisdnIn.substring(0, tamanhoDoDdiIn));
						
		if (paises.containsKey(ras)) {
			Long id = paises.get(ras);
			
			if (id.longValue() < 0) {
				return (null);
			}
			return paisRepoImpl.selectPaisById(id);			
		}
		
		logger.info(String.format("DDI to be searched : %d", ras) );
		
		try {						
			lista = paisRepoImpl.procuraPor_DDI(ras);
						
			if (lista == null || lista.size() == 0) {
				paises.put(ras, new Long(-1));
				return (null);
			}
			paisInterno = (Pais) lista.get(0);
		} catch (NoResultException nre) {
			paises.put(ras, new Long(-1));
			return (null);
		} catch (IllegalStateException ise) {
			return null;
			//throw new PortabilidadeException(CodigoRetornoEnum.FALHA, "Erro na leitura do DDI |" + ras + "|");
		}
		paises.put(ras, paisInterno.getPaisId());
		return (paisInterno);
	}
	
	private Area procuraArea(Pais paisIn, short dddIn )  {
		logger.debug("[procuraArea]");
		
		Area areaRas;
				
		try {
			logger.debug("Search at AREA table where :");
			logger.debug(String.format("........pais.paisid         : %d", paisIn.getPaisId() ));
			logger.debug(String.format("........pais.ddi            : %d", paisIn.getDdi()));
			logger.debug(String.format("........pais.nome           : %s", paisIn.getNome()));
			logger.debug(String.format("........pais.tamanho do ddd : %d", paisIn.getTamanhoDoDdd() ));
			logger.debug(String.format("........dddIn               : %d", dddIn ));
			
			areaRas = areaRepoImpl.procuraPor_Pais_DDD(paisIn.getPaisId(), dddIn);			
		} catch (NoResultException nre) {
			logger.info("NoResultException : " + nre.toString());
			return (null);
		} catch (IllegalStateException ise) {
			logger.info("IllegalStateException : " + ise.toString());
			return (null);
		} catch( Exception ex ) {
			logger.info("Exception : " + ex.toString());
			return (null);
		}

		return (areaRas);
	}

	private boolean verifyFileCheckSum(String fileName) {
		String line = "";
		String checkSum = "0";
		int numberOfLines = 0;
		String[] fields;
		MessageDigest l_digest = null;
		boolean bCheckSum = false;
		
		try {
			l_digest = MessageDigest.getInstance("MD5");
		 
			l_digest.reset();
			
			BufferedReader brFile = new BufferedReader(new FileReader(new File(dirIn, fileName)));
			
			for (int lineNumber = 1; (line = brFile.readLine()) != null; lineNumber++) {
				
				if ( lineNumber == 1) {
					
					fields = line.split(";");
					if ( fields.length == 2)
						numberOfLines 		= Integer.parseInt(fields[1]);
					else
						break;
				}
				if ( lineNumber != numberOfLines) {
					line += "\n";
					l_digest.update((line).getBytes());
				} else {
					checkSum = line;
				}
			}
			
			brFile.close();
		}
		catch (NoSuchAlgorithmException e) {			
			e.printStackTrace();
			return false;
		} catch (FileNotFoundException e) {			
			e.printStackTrace();
			return false;
		} catch (NumberFormatException e) {			
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		bCheckSum = executeCheckSum(checkSum, l_digest, fileName );
		//bCheckSum = executeCheckSum(checkSum, l_digest );
		if ( !bCheckSum )
			logger.info("WRONG CHECKSUM ON FILE [" + fileName + "].");
		
		return bCheckSum;
	}
	
	//@Deprecated
	private boolean executeCheckSum(String checksum, MessageDigest p_digest, String fileName) {
		
		BigInteger bigInt;
		byte[] md5sum;
		String output = "";
		
		md5sum = p_digest.digest();
		
		bigInt = new BigInteger(1, md5sum);
		output = bigInt.toString(16);
		
		while (output.length() < 32) {
			output = "0" + output;
		}
		
		if (!output.equals(checksum)) {
			logger.info("WRONG CHECKSUM ON FILE [" + fileName + "] : READ[" + checksum + "] CALCULATED[" + output + "]");
			return false;
		} else {
			logger.info("CHECKSUM OK ON FILE [" + fileName + "] : READ[" + checksum + "] CALCULATED[" + output + "]");
			return true;
		}
	}
	
	public boolean executeCheckSum(String checksum, MessageDigest p_digest) {
		
		BigInteger bigInt;
		byte[] md5sum;
		String output = "";
		
		md5sum = p_digest.digest();
		
		bigInt = new BigInteger(1, md5sum);
		output = bigInt.toString(16);
		
		while (output.length() < 32) {
			output = "0" + output;
		}
		
		if (!output.equals(checksum)) 			
			return false;
		else 			
			return true;
		
	}

	private void moveFileToOutputDirectory(String fileName) {
		
		try {
			logger.info(String.format("Moving FILE %s FROM directory %s to directory %s", fileName, dirIn.toString(), dirOut.toString() ) );
			
			File file = new File(dirIn, fileName);
			logger.info("Moving the file " + fileName + " to the OUTPUT DIRECTORY.");
			file.renameTo(new File(dirOut, fileName));
		}
		catch(Exception  ex) {
			logger.info(String.format("ERROR at moving FILE %s FROM directory %s to directory %s"), fileName, dirIn.toString(), dirOut.toString() );
		}
	}
	
	private void moveFileToIgnoredDirectory(String fileName) {
		File file = new File(dirIn, fileName);
		logger.info("Moving the file " + fileName + " to the IGNORED DIRECTORY.");
		file.renameTo(new File(dirIgnorados, fileName));
	}
	
	private void moveFileToRejectedDirectory(String fileName) {
		File file = new File(dirIn, fileName);
		logger.info("Moving the file " + fileName + " to the REJECTED DIRECTORY.");
		file.renameTo(new File(dirRejeitados, fileName));
	}

	public enum LineEnum {
		FISRT_LINE_OUT_OF_FORMAT,
		INVALID_DATE_FORMAT,
		INVALID_NUMBER_OF_LINES,
		INVALID_DATE_OF_GENERATION,
		TOO_SHORT,
		INVALID_ACTION,		
		TELEF_NUM_INVALID_FORMAT,
		FIELDS_SMALLER_THAN_6,
		INVALID_LINE_TYPE,
		SPID_INVALID,
		LINE_OK
	}
	
	void printLineEnum(LineEnum lineEnum) {
		switch (lineEnum) {
			case FISRT_LINE_OUT_OF_FORMAT: logger.info("Error : FISRT_LINE_OUT_OF_FORMAT"); break;
			case INVALID_DATE_FORMAT: logger.info("Error : INVALID_DATE_FORMAT"); break;
			case INVALID_NUMBER_OF_LINES: logger.info("Error : INVALID_NUMBER_OF_LINES"); break;
			case INVALID_DATE_OF_GENERATION: logger.info("Error : INVALID_DATE_OF_GENERATION"); break;
			case TOO_SHORT: logger.info("Error : TOO_SHORT"); break;
			case INVALID_ACTION: logger.info("Error : INVALID_ACTION"); break;		
			case TELEF_NUM_INVALID_FORMAT: logger.info("Error : TELEF_NUM_INVALID_FORMAT"); break;
			case FIELDS_SMALLER_THAN_6: logger.info("Error : FIELDS_SMALLER_THAN_6"); break;
			case INVALID_LINE_TYPE: logger.info("Error : INVALID_LINE_TYPE"); break;
			case SPID_INVALID: logger.info("Error : SPID_INVALID"); break;
			case LINE_OK: logger.info("Error : LINE_OK"); break;
		}
	}
}
