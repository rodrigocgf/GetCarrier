package br.com.supportcomm.business;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.supportcomm.models.db.NumeroPortado;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Serie;
import br.com.supportcomm.models.db.TelefoniaEnum;
//import br.com.supportcomm.crudrepositories.NumeroPortadoRepositoryFormer;
//import br.com.supportcomm.crudrepositories.OperadoraRepositoryFormer;
import br.com.supportcomm.exception.CodigoRetornoEnum;
import br.com.supportcomm.exception.PortabilidadeException;
import br.com.supportcomm.jparepositories.NumeroPortadoRepoImpl;
import br.com.supportcomm.jparepositories.OperadoraRepoImpl;

//import org.apache.log4j.PropertyConfigurator;
//import org.apache.log4j.Logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service 
public class MsisdnExt extends Msisdn {

	public static Logger logger = LoggerFactory.getLogger(Msisdn.class);
	
	@Autowired
	private OperadoraRepoImpl operadoraRepoImpl;
	
	@Autowired
	private NumeroPortadoRepoImpl numeroPortadoRepoImpl;
	
	/**
	 * Dummy
	 */
	
	public MsisdnExt () {
		super();
		logger.debug("MsisdnExt ctor.");
				
	}
	

	/**
	 *
	 *  Busca simples
	 *
	 * @param msisdn
	 * @param em_portabilidade
	 * @throws IOException
	 */
	
	public MsisdnExt (String msisdn) throws PortabilidadeException {
		super (msisdn);
	}
	
	public void start() {		
		
		try {
			if ( !getMsisdn().isEmpty() ) {
				inicia(getMsisdn());
				
				operadora = procuraOperadoraGenerica(getArea());
				if ( operadora != null ) {
					nomeDaOperadora = operadora.getNome();
					nomeDaOperadoraNormalizado = operadora.getNomeNormalizado();
					codigoDaOperadora = operadora.getCodigo();
					spid = operadora.getSpid();
					
					logger.info("[start] Operadora : ");
					logger.info(String.format("........nome da operadora              : %s", nomeDaOperadora));
					logger.info(String.format("........nome da operadora normalizado  : %s", nomeDaOperadoraNormalizado));
					logger.info(String.format("........codigo da operadora            : %d", codigoDaOperadora));
					logger.info(String.format("........spid                           : %d", spid ));
				} else {
					logger.info("[start] Operadora Generica NAO ENCONTRADA.");
				
				}
			}
		}
		catch(PortabilidadeException ex ) {
			
		}		
	}
	
	/**
	 * Muda de operadora por nome
	 *
	 * @param msisdnIn
	 * @param nomeDaOperadoraIn
	 * @param numeroDoBilhete
	 * @param em_portabilidade
	 * @throws java.io.IOException
	 */

	public void Execute(String msisdnIn, String nomeDaOperadoraIn, long numeroDoBilhete) throws PortabilidadeException {		
		
		logger.debug("Execute(String msisdnIn, String nomeDaOperadoraIn, long numeroDoBilhete)");
		Query query;
		NumeroPortado numeroPortado;
		
		
		logger.debug("[MsisdnExt] Going to call operadoraRepoImpl.procuraPor_Pais_Nome.");
		query = (Query) operadoraRepoImpl.procuraPor_Pais_Nome( getPais().getPaisId() , nomeDaOperadoraIn );
		
		try {
			setOperadora ((Operadora) query.getSingleResult ());
		} catch (NoResultException nre) {
			throw new PortabilidadeException(CodigoRetornoEnum.OPERADORA_NAO_ENCONTRADA, "Nao foi encontrado a operadora |" + nomeDaOperadoraIn + "| para o pais |" + getNomeDoPais () + "|");
		}
		
		query = (Query) numeroPortadoRepoImpl.procuraPor_Area_Numero(getArea().getAreaid(), getNumero());
				
		
		Date ref = new Date ();
		try {
			numeroPortado = (NumeroPortado) query.getSingleResult ();
			logger.debug ("Mudando o MSISDN  " + msisdnIn + " para a operadora " + nomeDaOperadoraIn);
			//numeroPortado.setOperadora (getOperadora ());
			numeroPortado.setDataDeModificacao (ref);
			numeroPortado.setNumeroDeModificacoes (numeroPortado.getNumeroDeModificacoes () + 1);
			numeroPortado.setNumerodobilhete (new Long (numeroDoBilhete));

		} catch (NoResultException nre) {
			numeroPortado = new NumeroPortado ();
			//numeroPortado.setArea (getArea ());
			numeroPortado.setDataDeInclusao (ref);
			numeroPortado.setDataDeModificacao (ref);
			numeroPortado.setNumero (getNumero ());
			numeroPortado.setNumeroDeModificacoes ((long) 0);
			//numeroPortado.setOperadora (getOperadora ());
			numeroPortado.setNumerodobilhete (new Long (numeroDoBilhete));

		} catch (Exception e) {
			throw new PortabilidadeException(CodigoRetornoEnum.FALHA, "Falha no criacao do numero portado " + msisdnIn + ", " + e.getMessage ());
		}
	}

	/**
	 * Muda de operadora por SPID
	 *
	 * @param msisdnIn
	 * @param spidIn
	 * @param numeroDoBilhete
	 * @param em_portabilidade
	 * @throws java.io.IOException
	 */
	
	public void Execute(String msisdnIn, short spidIn, long numeroDoBilhete, HashMap<String, Serie> seriesList) throws PortabilidadeException {
		
		NumeroPortado numeroPortado;
		
		try {
			Operadora operadora = procuraOperadora(getPais(), spidIn); 
			setOperadora(operadora);
		} catch (NoResultException nre) {
			throw new PortabilidadeException(	CodigoRetornoEnum.OPERADORA_NAO_ENCONTRADA, 
												"Nao foi encontrado a operadora |" + spidIn + "| para o pais |" + getNomeDoPais() + "|");
		}
		
		numeroPortado = numeroPortadoRepoImpl.procuraPor_Area_Numero(getArea().getAreaid(), getNumero());		
		
		TelefoniaEnum telefonia = getTelefonia(String.valueOf(getArea().getDdd()), getNumero(), seriesList);
		
		logger.info("Mudando o MSISDN  " + msisdnIn + " para a operadora " + getNomeDaOperadora());
		
		Timestamp tsNow = new Timestamp(new Date().getTime());
		
		if ( numeroPortado != null ) {
			//numeroPortado.setOperadora(getOperadora());
			numeroPortado.setDataDeModificacao(tsNow);
			numeroPortado.setNumeroDeModificacoes(numeroPortado.getNumeroDeModificacoes() + 1);
			numeroPortado.setNumerodobilhete(new Long(numeroDoBilhete));
			numeroPortado.setTelefonia(telefonia);
		} else {
			logger.info(String.format("Numero portado %d not found.",getNumero() ) );
			
			numeroPortado = new NumeroPortado();
			//numeroPortado.setArea(getArea());
			numeroPortado.setDataDeInclusao(tsNow);
			numeroPortado.setDataDeModificacao(tsNow);
			numeroPortado.setNumero(getNumero());
			numeroPortado.setNumeroDeModificacoes((long) 0);
			//numeroPortado.setOperadora(getOperadora());
			numeroPortado.setNumerodobilhete(new Long(numeroDoBilhete));
			numeroPortado.setTelefonia(telefonia);
		}
		try {
			numeroPortadoRepoImpl.Update(numeroPortado);
			logger.info(String.format("Numero portado %d UPDATED.",getNumero() ) );
			
		}
		catch (Exception e) {
			throw new PortabilidadeException(CodigoRetornoEnum.FALHA, "Falha no criacao do numero portado " + msisdnIn + ", " + e.getMessage ());
		}
		logger.info("................................................................");
	}

	/**
	 *
	 * @param msisdn
	 * @param em_portabilidade
	 * @throws java.io.IOException
	 */
	
	public void apagarMsisdn (String msisdn) throws PortabilidadeException {
		Query query;
		NumeroPortado numeroPortado;
		Msisdn m = new Msisdn (msisdn);
		
		query = (Query) numeroPortadoRepoImpl.procuraPor_Area_Numero(m.getArea().getAreaid(), m.getNumero());		
		
		try {
			numeroPortado = (NumeroPortado) query.getSingleResult ();
		} catch (NoResultException nre) {
			return;
		}
		
		try {			
			numeroPortadoRepoImpl.DeletePor_NumeroPortadoId(numeroPortado.getNumero());			
		} catch (Exception e) {
			throw new PortabilidadeException(CodigoRetornoEnum.FALHA, "Falha na remocao do MSISDN " + msisdn + ", " + e.getMessage ());
		}
	}

	public void setAllMsisdnForUpdate() {
		
		numeroPortadoRepoImpl.updateStatusUpdate();		
	}
	
	private TelefoniaEnum getTelefonia(String ddd, Long tn, HashMap<String, Serie> seriesList) {
		String prefix = "";
		if(tn.toString().length() == 9) {
			prefix = tn.toString().substring(0, 5);
		} else if (tn.toString().length() == 8) {
			prefix = tn.toString().substring(0, 4);
		} else {
			return TelefoniaEnum.UNDEFINED;
		}
		TelefoniaEnum telefonia = TelefoniaEnum.UNDEFINED;
		if(seriesList.containsKey(ddd + prefix)) {
			telefonia = seriesList.get(ddd + prefix).getTelefonia();
		}
		return telefonia;
	}
	
}
