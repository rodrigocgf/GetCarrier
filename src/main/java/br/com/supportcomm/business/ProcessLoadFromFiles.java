package br.com.supportcomm.business;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.supportcomm.util.ArquivoAbr;
import br.com.supportcomm.util.ArquivoAnatel;

@Service
public class ProcessLoadFromFiles {
	
	public static Logger logger = LoggerFactory.getLogger(ProcessLoadFromFiles.class);

	@Autowired
	ArquivoAnatel arquivoAnatel;
		
	@Autowired
	ArquivoAbr arquivoAbr;
	
	public void execute(File dirIn , File dirOut , File dirIgnorados , File dirRejeitados) {
		
		// O PROCESSAMENTO ANATEL FOI DESCONTINUADO !
		// A Anatel não está mais disponibilizando os arquivos CE_M* das centrais móveis.
		// Via site, eles apenas disponibilizam arquivos do tipo CE_F* das centrais fixas.
		
		arquivoAnatel.processa(dirIn, dirOut);
		
		arquivoAbr.processa(dirIn, dirOut, dirIgnorados, dirRejeitados );
	}
}
