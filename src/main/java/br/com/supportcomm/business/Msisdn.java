package br.com.supportcomm.business;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;


import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Estado;
import br.com.supportcomm.models.db.NumeroPortado;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;
import br.com.supportcomm.models.db.TelefoniaEnum;
//import br.com.supportcomm.crudrepositories.NumeroPortadoRepositoryFormer;
import br.com.supportcomm.exception.CodigoRetornoEnum;
import br.com.supportcomm.exception.PortabilidadeException;
import br.com.supportcomm.jparepositories.AreaRepoImpl;
import br.com.supportcomm.jparepositories.EstadoRepoImpl;
import br.com.supportcomm.jparepositories.NumeroPortadoRepoImpl;
import br.com.supportcomm.jparepositories.OperadoraRepoImpl;
import br.com.supportcomm.jparepositories.PaisRepoImpl;
import br.com.supportcomm.jparepositories.SerieRepoImpl;

//import org.apache.log4j.Logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Msisdn {
	private Pais pais = null;
	private Area area = null;
	protected Operadora operadora = null;
	protected String msisdn = "";
	private short ddi = -1;
	private short ddd = -1;
	private long numero = -1;
	private String nomeDoPais = "";
	private String nomeDoEstado = "";
	private String siglaDoEstado = "";
	protected String nomeDaOperadora = "";
	protected String nomeDaOperadoraNormalizado = "";
	protected short codigoDaOperadora = -1;
	private int precisao = -1;
	private int portado = 0;
	private TelefoniaEnum telefonia = TelefoniaEnum.UNDEFINED;
	private static HashMap<Short, Long> paises = new HashMap<Short, Long>();
	private static HashMap<String, Long> areas = new HashMap<String, Long>();
	private static HashMap<String, Long> operadoras = new HashMap<String, Long>();
	protected short spid;
	
	public static Logger logger = LoggerFactory.getLogger(Msisdn.class);
	
	@Autowired
	private NumeroPortadoRepoImpl numeroPortadoRepoImpl;
	
	@Autowired
	private PaisRepoImpl  paisRepoImpl;
	
	@Autowired
	private EstadoRepoImpl estadoRepoImpl;
	
	@Autowired
	private AreaRepoImpl areaRepoImpl;
	
	@Autowired
	private SerieRepoImpl serieRepoImpl;
	
	@Autowired
	private OperadoraRepoImpl operadoraRepoImpl;
	
	public Msisdn() {
		
	}

	public Msisdn(String msisdnIn) throws PortabilidadeException {
		
		logger.info("¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨");

		paisRepoImpl = new PaisRepoImpl();
		
		inicia(msisdnIn);
		
		operadora = procuraOperadoraGenerica(area);
		if ( operadora != null ) {
			nomeDaOperadora = operadora.getNome();
			nomeDaOperadoraNormalizado = operadora.getNomeNormalizado();
			codigoDaOperadora = operadora.getCodigo();
			spid = operadora.getSpid();
		} else {
			logger.info("Operadora generica NAO ENCONTRADA.");
		}
		
		logger.info("¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨");
	}
	
	protected void inicia(String msisdnIn) throws PortabilidadeException {
		logger.debug("[inicia]");
		logger.debug(String.format("msisdnIn : %s", msisdnIn));
		
		if (msisdnIn == null) {
			throw new PortabilidadeException(CodigoRetornoEnum.MSISDN_INVALIDO, "MSISDN nulo");
		}
		msisdn = msisdnIn.trim();
		
		if (msisdn.matches(".*[^0-9].*")) {
			throw new PortabilidadeException(CodigoRetornoEnum.MSISDN_INVALIDO, "MSISDN com caracter nao numerico |" + msisdn + "|");
		}
		
		int tamanhoDoDdi;
		for (tamanhoDoDdi = 1; tamanhoDoDdi < 5; tamanhoDoDdi++) {
			pais = procuraPais(msisdn, tamanhoDoDdi);
			if (pais != null) {
				break;
			} else {
				logger.info("Procurando pelo proximo DDI...");
			}
		}
		
		if (pais == null) {
			throw new PortabilidadeException(CodigoRetornoEnum.FALHA, "Erro na leitura do DDI |" + msisdn + "|");
		}
		if (msisdn.length() <= (tamanhoDoDdi + pais.getTamanhoDoDdd())) {
			throw new PortabilidadeException(CodigoRetornoEnum.MSISDN_INVALIDO, "MSISDN com tamanho invalido |" + msisdn + "|");
		}
		
		nomeDoPais = pais.getNome();
		logger.info(String.format("[inicia] Nome do pais : %s", nomeDoPais));
		
		ddi = pais.getDdi();
		logger.info(String.format("[inicia] DDI  do pais : %d", ddi));
		if (pais.getTamanhoDoDdd() == 0) {
			ddd = 0;
			numero = Long.parseLong(msisdn.substring(tamanhoDoDdi));
		} else {
			ddd = Short.parseShort(msisdn.substring(tamanhoDoDdi, tamanhoDoDdi + pais.getTamanhoDoDdd()));
			numero = Long.parseLong(msisdn.substring(tamanhoDoDdi + pais.getTamanhoDoDdd()));
		}
		area = procuraArea(pais, ddd);
		if (area != null) {
			Estado estado = estadoRepoImpl.searchById(area.getEstadoid());
			nomeDoEstado = estado.getNome();
			siglaDoEstado = estado.getSigla();
			
			logger.info(String.format("[inicia] Nome do Estado: %s", nomeDoEstado));
			logger.info(String.format("[inicia] Sigla do Estado: %s", siglaDoEstado));
		}
	}
	
	@SuppressWarnings("unchecked")
	private Pais procuraPais(String msisdnIn, int tamanhoDoDdiIn) throws PortabilidadeException {
		logger.info(String.format("[procuraPais] msisdnIn : %s ; tamanhoDoDdiIn : %d", msisdnIn, tamanhoDoDdiIn));
		
		List<Pais> lista = null;
		Pais paisInterno;
		short ras;
		
		if (msisdnIn.length() < tamanhoDoDdiIn) {
			throw new PortabilidadeException(CodigoRetornoEnum.MSISDN_INVALIDO, "MSISDN com tamanho invalido |" + msisdn + "|");
		}
		
		ras = Short.parseShort(msisdnIn.substring(0, tamanhoDoDdiIn));
						
		if (paises.containsKey(ras)) {
			Long id = paises.get(ras);
			
			if (id.longValue() < 0) {
				return (null);
			}
			return paisRepoImpl.selectPaisById(id);			
		}
		
		logger.info(String.format("DDI to be searched : %d", ras) );
		
		try {						
			lista = paisRepoImpl.procuraPor_DDI(ras);
						
			if (lista == null || lista.size() == 0) {
				paises.put(ras, new Long(-1));
				return (null);
			}
			paisInterno = (Pais) lista.get(0);
		} catch (NoResultException nre) {
			paises.put(ras, new Long(-1));
			return (null);
		} catch (IllegalStateException ise) {
			throw new PortabilidadeException(CodigoRetornoEnum.FALHA, "Erro na leitura do DDI |" + ras + "|");
		}
		paises.put(ras, paisInterno.getPaisId());
		return (paisInterno);
	}

	private Area procuraArea(Pais paisIn, short dddIn ) throws PortabilidadeException {
		logger.debug("[procuraArea]");
		Query query;
		Area areaRas;
		StringBuffer sb = new StringBuffer();
		String key;
		sb.append(paisIn.getPaisId().toString());
		sb.append("_");
		sb.append(dddIn);
		key = sb.toString();
		if (areas.containsKey(key)) {
			Long id;
			id = areas.get(key);
			if (id.longValue() < 0) {
				return (null);
			}
			return areaRepoImpl.selectAreaById(id);
		}
				
		try {
			logger.debug("Search at AREA table where :");
			logger.debug(String.format("........pais.paisid         : %d", paisIn.getPaisId() ));
			logger.debug(String.format("........pais.ddi            : %d", paisIn.getDdi()));
			logger.debug(String.format("........pais.nome           : %s", paisIn.getNome()));
			logger.debug(String.format("........pais.tamanho do ddd : %d", paisIn.getTamanhoDoDdd() ));
			logger.debug(String.format("........dddIn               : %d", dddIn ));
			
			areaRas = areaRepoImpl.procuraPor_Pais_DDD(paisIn.getPaisId(), dddIn);
			if ( areaRas != null )
				areas.put(key, areaRas.getAreaid());
		} catch (NoResultException nre) {
			areas.put(key, new Long(-1));
			return (null);
		} catch (IllegalStateException ise) {
			throw new PortabilidadeException(CodigoRetornoEnum.FALHA, "Erro na leitura do DDD |" + dddIn + "| para o pais |" + paisIn.toString() + "|");
		}
		
		return (areaRas);
	}

	protected Operadora procuraOperadoraGenerica(Area areaIn ) throws PortabilidadeException {
		logger.info("[procuraOperadoraGenerica]");
		
		Operadora ras = null;
		
		ras = procuraOperadora(numero, areaIn );
		
		if (ras != null) {
			precisao = 0;
			return (ras);
		}
		
        String numeroStr = Long.toString(numero);
        int tamanhoPrefixo = numeroStr.length() - 4;
        
        if (tamanhoPrefixo >= 3 && tamanhoPrefixo <= 5)
        {
            String prefixoStr;
            if (ddd > 0)
            {
                prefixoStr = Short.toString(ddd) + numeroStr.substring(0, tamanhoPrefixo);
            }
            else
            {
                prefixoStr = numeroStr.substring(0, tamanhoPrefixo);
            }
            long prefixo = Long.parseLong(prefixoStr);
            ras = procuraOperadora (areaIn, prefixo, true);
            if (ras != null)
            {
                precisao = 1;
                return ras;
            }
        }
        
        return ras;
		//throw new PortabilidadeException(CodigoRetornoEnum.OPERADORA_NAO_ENCONTRADA, "Nao foi encontrado operadora para o MSISDN " + msisdn);
	}

	private Operadora procuraOperadora(long numeroIn, Area areaIn ) throws PortabilidadeException {
		logger.debug(String.format("[procuraOperadora] numero : %d", numeroIn));

		NumeroPortado numeroPortado;
		Operadora retOperadora = null;
		long operadoraid = 0;
		
		logger.info("Search at NumeroPortado table where : ");
		logger.info(String.format("........area.areaid   : %d", getArea().getAreaid()));
		logger.info(String.format("........area.ddd      : %d", getArea().getDdd()));
		logger.info(String.format("........area.estadoid : %d", getArea().getEstadoid()));
		logger.info(String.format("........area.paisid   : %d", getArea().getPais()));
		logger.info(String.format("........numero        : %d", getNumero() ));
					
		
		try {		
			numeroPortado = numeroPortadoRepoImpl.procuraPor_Area_Numero(getArea().getAreaid(), getNumero());
			if ( numeroPortado != null ) {
				portado = 1;
				telefonia = numeroPortado.getTelefonia();
				operadoraid = numeroPortado.getOperadoraid();
				retOperadora = operadoraRepoImpl.selectOperadoraById(operadoraid);
				
			}
						
		} catch (NoResultException nre) {
			return (null);
		}
		catch(SQLGrammarException ex) {
			logger.debug("[procuraOperadora] Exception : %s", ex.fillInStackTrace());
			return (null);
		}		
		
		return retOperadora;		
	}

	private Operadora procuraOperadora(Area areaIn, long serieIn, boolean especial) throws PortabilidadeException {
		List<Serie> series;
		Query query;
		Serie serie;
		StringBuffer sb = new StringBuffer();
		String key;
		Operadora operadora =null;
		
		if (areaIn == null) {
			return (null);
		}
		sb.append(areaIn.getAreaid().toString());
		sb.append("_");
		sb.append(serieIn);
		sb.append("_");
		sb.append(especial);
		key = sb.toString();
		if (operadoras.containsKey(key)) {
			Long id = operadoras.get(key);
			if (id.longValue() < 0) {
				return (null);
			}
			//return (em_portabilidadeIn.getReference(Operadora.class, id));
		}		
		
		series = serieRepoImpl.procuraPor_Area_Especial_Serie(areaIn.getAreaid(), (short) (especial ? 1 : 0), (int)serieIn);
				
		try {
			if ( series == null ) {				
				return null;
			}
			if ( series.size() == 0 ) {				
				return null;
			}
			serie = series.get(0);
			telefonia = serie.getTelefonia();
			
			operadora = operadoraRepoImpl.selectOperadoraById(serie.getOperadoraid());
		} catch (NoResultException nre) {
			return (null);
		}
		
		
		return operadora;
	}

	@SuppressWarnings("unchecked")
	protected Operadora procuraOperadora(Pais paisIn, short spidIn) throws PortabilidadeException {
		List<Operadora> operadoraRas;
		StringBuffer sb = new StringBuffer();
		String key;
		sb.append(paisIn.getPaisId().toString());
		sb.append("_");
		sb.append(spidIn);
		key = sb.toString();
		if (operadoras.containsKey(key)) {
			Long id;
			id = operadoras.get(key);
			if (id.longValue() < 0) {
				return (null);
			}
			return operadoraRepoImpl.selectOperadoraById(id);
		}
		
		try {			
			List<Operadora> query = operadoraRepoImpl.procuraPor_Pais_Spid(paisIn.getPaisId(), spidIn);
			
			if(!query.isEmpty()) {
				return query.get(0);
			} else {
				logger.info("Operadora not found for pais = %d and spid = %d", paisIn.getPaisId(), spidIn);				
				return null;
			}
		} catch (NoResultException nre) {
			throw new PortabilidadeException(CodigoRetornoEnum.OPERADORA_NAO_ENCONTRADA, "Nao foi encontrado a operadora |" + spidIn 
					+ "| para o pais |" + paisIn.getNome() + "|");
		}
		
//		Desabilitando o cache fixo para fazer o cache ser reiniciado a cada certo tempo
//		operadoras.put(key, operadoraRas.getOperadoraId());
	}

	/**
	 * @return the msisdn
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * @return the msisdn
	 */
	public short getDdi() {
		if (getPais() == null) {
			return (0);
		}

		return (getPais().getDdi().shortValue());
	}

	/**
	 * @return the msisdn
	 */
	public short getDdd() {
		if (getPais() == null) {
			return (0);
		}

		if (getPais().getTamanhoDoDdd() == 0) {
			return (0);
		}

		return ddd;
	}

	/**
	 * @return the msisdn
	 */
	public long getNumero() {
		return numero;
	}

	/**
	 * @return the nomeDoPais
	 */
	public String getNomeDoPais() {
		return nomeDoPais;
	}

	/**
	 * @return the nomeDoEstado
	 */
	public String getNomeDoEstado() {
		return nomeDoEstado;
	}

	/**
	 * @return the siglaDoEstado
	 */
	public String getSiglaDoEstado() {
		return siglaDoEstado;
	}

	/**
	 * @return the nomeDaOperadora
	 */
	public String getNomeDaOperadora() {
		return nomeDaOperadora;
	}

	/**
	 * @return the nomeDaOperadoraNormalizado
	 */
	public String getNomeDaOperadoraNormalizado() {
		return nomeDaOperadoraNormalizado;
	}

	/**
	 * @return the codigoDaOperadora
	 */
	public short getCodigoDaOperadora() {
		return codigoDaOperadora;
	}

	/**
	 * @return the precisao
	 */
	public int getPrecisao() {
		return precisao;
	}

	/**
	 * @return portado
	 */
	public int getPortado() {
		return portado;
	}

	public TelefoniaEnum getTelefonia() {
		return telefonia;
	}

	public void setTelefonia(TelefoniaEnum telefonia) {
		this.telefonia = telefonia;
	}

	/**
	 * @return the pais
	 */
	protected Pais getPais() {
		return pais;
	}

	/**
	 * @param pais
	 *            the pais to set
	 */
	protected void setPais(Pais pais) {
		this.pais = pais;
	}

	/**
	 * @return the area
	 */
	protected Area getArea() {
		return area;
	}

	/**
	 * @param area
	 *            the area to set
	 */
	protected void setArea(Area area) {
		this.area = area;
	}

	/**
	 * @return the operadora
	 */
	protected Operadora getOperadora() {
		return operadora;
	}

	/**
	 * @param operadora
	 *            the operadora to set
	 */
	protected void setOperadora(Operadora operadora) {
		logger.info("[setOperadora] inicio");
		StringBuilder sb = new StringBuilder();
		
		this.operadora = operadora;
		this.nomeDaOperadora = operadora.getNome();
		this.nomeDaOperadoraNormalizado = operadora.getNomeNormalizado();
		this.codigoDaOperadora = operadora.getCodigo();
		this.precisao = 0;
		
		sb.append("\r\n");
		sb.append("=======================================================").append("\r\n");
		sb.append("                         OPERADORA                      ").append("\r\n");
		sb.append("\r\n");
		sb.append(" Nome                : ").append(this.nomeDaOperadora).append("\r\n");
		sb.append(" Nome normalizado    : ").append(this.nomeDaOperadoraNormalizado).append("\r\n");
		sb.append(" Codigo da operadora : ").append(String.valueOf(this.codigoDaOperadora)).append("\r\n");
		sb.append("=======================================================").append("\r\n");
		
		logger.info(sb.toString());
		
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("msisdn=");
		sb.append(msisdn);
		sb.append(", ddi=");
		sb.append(ddi);
		sb.append(", ddd=");
		sb.append(ddd);
		sb.append(", numero=");
		sb.append(numero);
		sb.append(", pais=");
		sb.append(nomeDoPais);
		sb.append(", estado=");
		sb.append(nomeDoEstado);
		sb.append(", sigla do estado=");
		sb.append(siglaDoEstado);
		sb.append(", operadora=");
		sb.append(nomeDaOperadora);
		sb.append(", operadoraNormalizado=");
		sb.append(nomeDaOperadoraNormalizado);
		sb.append(", codigo da operadora=");
		sb.append(codigoDaOperadora);
		sb.append(", precisao=");
		sb.append(precisao);
		sb.append(", portado=");
		sb.append(portado);

		return (sb.toString());
	}

	public String toLine() {
		StringBuffer sb = new StringBuffer();
		sb.append("msisdn=");
		sb.append(msisdn);
		sb.append(", ddi=");
		sb.append(ddi);
		sb.append(", ddd=");
		sb.append(ddd);
		sb.append(", numero=");
		sb.append(numero);
		sb.append(", pais=");
		sb.append(nomeDoPais);
		sb.append(", estado=");
		sb.append(nomeDoEstado);
		sb.append(", sigla do estado=");
		sb.append(siglaDoEstado);
		sb.append(", operadora=");
		sb.append(nomeDaOperadora);
		sb.append(", operadoraNormalizado=");
		sb.append(nomeDaOperadoraNormalizado);
		sb.append(", codigo da operadora=");
		sb.append(codigoDaOperadora);
		sb.append(", precisao=");
		sb.append(precisao);
		sb.append(", portado=");
		sb.append(portado);

		return (sb.toString());
	}	

	public short getSpid() {
		return spid;
	}

	public void setSpid(short spid) {
		this.spid = spid;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public void setDdi(short ddi) {
		this.ddi = ddi;
	}

	public void setDdd(short ddd) {
		this.ddd = ddd;
	}

	public void setNumero(long numero) {
		this.numero = numero;
	}

	public void setNomeDoPais(String nomeDoPais) {
		this.nomeDoPais = nomeDoPais;
	}

	public void setNomeDoEstado(String nomeDoEstado) {
		this.nomeDoEstado = nomeDoEstado;
	}

	public void setSiglaDoEstado(String siglaDoEstado) {
		this.siglaDoEstado = siglaDoEstado;
	}

	public void setNomeDaOperadora(String nomeDaOperadora) {
		this.nomeDaOperadora = nomeDaOperadora;
	}

	public void setNomeDaOperadoraNormalizado(String nomeDaOperadoraNormalizado) {
		this.nomeDaOperadoraNormalizado = nomeDaOperadoraNormalizado;
	}

	public void setCodigoDaOperadora(short codigoDaOperadora) {
		this.codigoDaOperadora = codigoDaOperadora;
	}

	public void setPrecisao(int precisao) {
		this.precisao = precisao;
	}

	public void setPortado(int portado) {
		this.portado = portado;
	}

	public static void setPaises(HashMap<Short, Long> paises) {
		Msisdn.paises = paises;
	}

	public static void setAreas(HashMap<String, Long> areas) {
		Msisdn.areas = areas;
	}

	public static void setOperadoras(HashMap<String, Long> operadoras) {
		Msisdn.operadoras = operadoras;
	}
	


}
