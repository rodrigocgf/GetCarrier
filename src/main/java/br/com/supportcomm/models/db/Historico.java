package br.com.supportcomm.models.db;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Version;


@Entity
@Table(name = "Historico")
public class Historico implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "HistoricoSeq")
	@SequenceGenerator(name = "HistoricoSeq", sequenceName = "historico_historicoid_seq", allocationSize = 1)
	@Column(name = "historicoid", nullable = false)
	private Long historicoid;
	
	@Version
	private int version;
	
	@Column(name = "processado", nullable = true)
	private int processado;
	
	@Column(name = "Arquivo", nullable = false)
	private String arquivo;

	@Column (name = "DataDeReferencia", nullable = false)
	private Timestamp dataDeReferencia;
	
	@Column (name = "DataDeProcessamento", nullable = false)
	private Timestamp dataDeProcessamento;
	
	@Column(name = "CargaFull", nullable = false)
	private int cargaFull;
	
	@Column(name = "NumeroDeRegistrosProcessados", nullable = false)
	private int numeroDeRegistrosProcessados;

	public Long getHistoricoId ()
	{
		return historicoid;
	}

	public void setHistoricoId (Long id)
	{
		this.historicoid = id;
	}

	
	public int getProcessado() {
		return processado;
	}

	public void setProcessado(int processado) {
		this.processado = processado;
	}
	
	/**
	 * @return the version
	 */
	public int getVersion ()
	{
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion (int version)
	{
		this.version = version;
	}

	/**
	 * @return the nome
	 */
	public String getArquivo ()
	{
		return arquivo;
	}

	/**
	 * @param arquivo
	 */
	public void setArquivo (String arquivo)
	{
		this.arquivo = arquivo;
	}

	/**
	 * @return the dataDeReferencia
	 */
	public Date getDataDeReferencia ()
	{
		return dataDeReferencia;
	}

	/**
	 * @param dataDeReferencia the dataDeReferencia to set
	 */
	public void setDataDeReferencia (Timestamp dataDeReferencia)
	{
		this.dataDeReferencia = dataDeReferencia;
	}

	/**
	 * @return the dataDeProcessamento
	 */
	public Date getDataDeProcessamento ()
	{
		return dataDeProcessamento;
	}

	/**
	 * @param dataDeProcessamento the dataDeProcessamento to set
	 */
	public void setDataDeProcessamento (Timestamp dataDeProcessamento)
	{
		this.dataDeProcessamento = dataDeProcessamento;
	}

	/**
	 * @return the cargaFull
	 */
	public int getCargaFull ()
	{
		return cargaFull;
	}

	/**
	 * @param cargaFull the cargaFull to set
	 */
	public void setCargaFull (int cargaFull)
	{
		this.cargaFull = cargaFull;
	}

	/**
	 * @return the numeroDeRegistrosProcessados
	 */
	public int getNumeroDeRegistrosProcessados ()
	{
		return numeroDeRegistrosProcessados;
	}

	/**
	 * @param numeroDeRegistrosProcessados the numeroDeRegistrosProcessados to set
	 */
	public void setNumeroDeRegistrosProcessados (int numeroDeRegistrosProcessados)
	{
		this.numeroDeRegistrosProcessados = numeroDeRegistrosProcessados;
	}

	@Override
	public int hashCode ()
	{
		int hash = 0;
		hash += (historicoid != null ? historicoid.hashCode () : 0);
		return hash;
	}

	@Override
	public boolean equals (Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Historico))
		{
			return false;
		}
		Historico other = (Historico) object;
		if ((this.historicoid == null && other.historicoid != null) || (this.historicoid != null && !this.historicoid.equals (other.historicoid)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString ()
	{
		StringBuffer sb = new StringBuffer ();

		sb.append (getClass ().getCanonicalName ());
		sb.append (" [historicoId=");
		sb.append (Long.valueOf(historicoid) );
		sb.append (", arquivo=");
		sb.append (arquivo);
		sb.append (", dataDeReferencia=");
		sb.append (dataDeReferencia.toString());
		sb.append (", dataDeProcessamento=");
		sb.append (dataDeProcessamento.toString());
		sb.append (", cargaFull=");
		sb.append (cargaFull);
		sb.append (", numeroDeRegistrosProcessados=");
		sb.append (Integer.valueOf(numeroDeRegistrosProcessados) );
		sb.append ("]");

		return (sb.toString ());
	}

}

