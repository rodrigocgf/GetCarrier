/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.supportcomm.models.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;


/*
 * FORMER QUERY - DOESN'T WORK :
 * 

*/

@Entity
@Table(name = "Operadora")
public class Operadora implements Serializable
{

	private static final long serialVersionUID = 1710300851432696258L;
	@Id
	@GeneratedValue(generator = "OperadoraSeq")
	@SequenceGenerator(name = "OperadoraSeq", sequenceName = "operadora_operadoraid_seq", allocationSize = 1)
	@Column(name = "operadoraid", nullable = false)
	private Long operadoraid;
	
	@Column(name = "version", nullable = false)
	private int version;
	
	@Column(name = "nome", nullable = false)
	private String nome;
	
	@Column(name = "cnpj", nullable = false)
	private String cnpj;
	
	@Column(name = "codigo", nullable = false)
	private short codigo;
	
	@Column(name = "spid", nullable = false)
	private short spid;
	
	@Column(name = "nomenormalizado", nullable = false)
	private String nomeNormalizado;
	
	@Column(name = "paisid", nullable = false)
	private Long paisid;	
	

	public Operadora ()
	{
		
	}

	public Long getOperadoraId ()
	{
		return operadoraid;
	}

	public void setOperadoraId (Long id)
	{
		this.operadoraid = id;
	}

	

	public Long getPaisid() {
		return paisid;
	}

	public void setPaisid(Long paisid) {
		this.paisid = paisid;
	}

	/**
	 * @return the nome
	 */
	public String getNome ()
	{
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome (String nome)
	{
		this.nome = nome;
	}

	/**
	 * @return the nomeNormalizado
	 */
	public String getNomeNormalizado ()
	{
		return nomeNormalizado;
	}

	/**
	 * @param nomeNormalizado the nomeNormalizado to set
	 */
	public void setNomeNormalizado (String nomeNormalizado)
	{
		this.nomeNormalizado = nomeNormalizado;
	}

	/**
	 * @return the Codigo
	 */
	public short getCodigo ()
	{
		return codigo;
	}

	/**
	 * @param Codigo the Codigo to set
	 */
	public void setCodigo (short Codigo)
	{
		this.codigo = Codigo;
	}

	

	/**
	 * @return the spid
	 */
	public short getSpid ()
	{
		return spid;
	}

	/**
	 * @param spid the spid to set
	 */
	public void setSpid (short spid)
	{
		this.spid = spid;
	}

	/**
	 * @return the version
	 */
	public int getVersion ()
	{
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion (int version)
	{
		this.version = version;
	}

	/**
	 * @return the razaoSocial
	 */
	public String getCNPJ ()
	{
		return cnpj;
	}

	/**
	 * @param cnpj
	 */
	public void setCNPJ (String cnpj)
	{
		this.cnpj = cnpj;
	}

	@Override
	public int hashCode ()
	{
		int hash = 0;
		hash += (operadoraid != null ? operadoraid.hashCode () : 0);
		return hash;
	}

	@Override
	public boolean equals (Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Operadora))
		{
			return false;
		}
		Operadora other = (Operadora) object;
		if ((this.operadoraid == null && other.operadoraid != null) || (this.operadoraid != null && !this.operadoraid.equals (other.operadoraid)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString ()
	{
		StringBuffer sb = new StringBuffer ();

		sb.append (getClass ().getCanonicalName ());
		sb.append (" [operadoraId=");
		sb.append (operadoraid);
		sb.append (", nome=");
		sb.append (nome);
		sb.append (", paisid=");
		sb.append (paisid);
		sb.append (", nomeNormalizado=");
		sb.append (nomeNormalizado);
		sb.append (", razaoSocial=");
		sb.append (cnpj);
		sb.append (", codigo=");
		sb.append (codigo);
		sb.append (", spid=");
		sb.append (spid);
		sb.append ("]");

		return (sb.toString ());
	}
}
