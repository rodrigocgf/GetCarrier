package br.com.supportcomm.models.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import br.com.supportcomm.models.db.Estado;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;


@Entity
@Table(name = "Area")
public class Area implements Serializable
{
	private static final long serialVersionUID = 2795910616375322284L;
	@Id
	@GeneratedValue(generator = "AreaSeq")
	@SequenceGenerator(name = "AreaSeq", sequenceName = "area_areaid_seq", allocationSize = 1)
	@Column(name = "areaid", nullable = false)
	private Long areaid;
	
	@Column(name = "DDD", nullable = false)
	private int ddd;
	
	@Column(name = "version", nullable = false)
	private int version;
	
	@Column(name = "estadoid", nullable = false)
	private long estadoid;
	

	@Column(name = "paisid", nullable = false)
	private long paisid;

	
	

	public Area ()
	{
		//serie = new ArrayList<Serie> ();
	}

	

	

	public Long getAreaid() {
		return areaid;
	}





	public void setAreaid(Long areaid) {
		this.areaid = areaid;
	}





	/**
	 * @return the ddd
	 */
	public int getDdd ()
	{
		return ddd;
	}

	/**
	 * @param ddd the ddd to set
	 */
	public void setDdd (int ddd)
	{
		this.ddd = ddd;
	}
	
	
	/**
	 * @return the pais
	 */
	public long getPais ()
	{
		return paisid;
	}

	/**
	 * @param pais the pais to set
	 */
	public void setPais (long paisid)
	{
		this.paisid = paisid;
	}
	
	/**
	 * @return the estado
	 */
	public long getEstadoid ()
	{
		return estadoid;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstadoid (long estadoid)
	{
		this.estadoid = estadoid;
	}
	
		
	@Override
	public int hashCode ()
	{
		int hash = 0;
		hash += (getAreaid () != null ? getAreaid ().hashCode () : 0);
		return hash;
	}

	@Override
	public boolean equals (Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Area))
		{
			return false;
		}
		Area other = (Area) object;
		if ((this.getAreaid () == null && other.getAreaid () != null) || (this.getAreaid () != null && !this.areaid.equals (other.areaid)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString ()
	{
		StringBuffer sb = new StringBuffer ();

		sb.append (getClass ().getCanonicalName ());
		sb.append (" [areaid=");
		sb.append (areaid);
		sb.append (", ddd=");
		sb.append (ddd);
		sb.append ("]");

		return (sb.toString ());
	}
	
}

