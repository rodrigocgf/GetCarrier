package br.com.supportcomm.models.db;

import java.sql.*;
import java.sql.Connection;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.supportcomm.config.AppConfig;
import oracle.jdbc.pool.OracleDataSource;

@Service
public class OperadoraOracle {
	
	@Autowired
	private AppConfig appConfig;
	
	
   String JDBC_DRIVER;  
   String DB_URL;

   //  Database credentials
   String USER;
   String PASS;
   
   Connection conn = null;
   Statement stmt = null;
   private HashMap<String, String> mapOperadorasOracle = null;
   
   
   public OperadoraOracle() {
   		
   		try {
   			//loadConfig();
   			
   			//OracleDataSource ds;
   			//mapOperadorasOracle = new HashMap<String, String>();
   			
   			//ds = new OracleDataSource();
   			//ds.setURL(DB_URL);
   			
   			//conn = ds.getConnection(USER,PASS); 

   		}
   		catch(Exception ex) {
   			
   		}
   		finally {
   		}
   	}
   	
   public void loadConfig() {
	   
/*	   JDBC_DRIVER = appConfig.getOracleDriverClassName();
	   DB_URL = appConfig.getOracleDsURL();
	
	   USER = appConfig.getOracleUser();
	   PASS = appConfig.getOraclePwd();*/
	   
   }
   	
   	public HashMap<String, String> loadMapOperadorasByCnpj() {
   		try {
   			OracleDataSource ds;
   			mapOperadorasOracle = new HashMap<String, String>();
   			
   			loadConfig();
   			
   			ds = new OracleDataSource();
   			ds.setURL(DB_URL);
   			
   			conn = ds.getConnection(USER,PASS); 
   			
   			
   			
			stmt = conn.createStatement();
			String sql;
			sql = "select CNPJ, NOMENORMALIZADO from PORTABILIDADE.Operadora where CNPJ != 0 ORDER BY CNPJ";
			ResultSet rs = stmt.executeQuery(sql);
			
			System.out.println(" CNPJ           -         NOMENORMALIZADO   ");
			while(rs.next()){
			   String sCnpj  = rs.getString("CNPJ");
			   String sNomeNormalizado = rs.getString("NOMENORMALIZADO");
			   if ( !mapOperadorasOracle.containsKey(sCnpj)) {
				   mapOperadorasOracle.put(sCnpj, sNomeNormalizado);
				   System.out.println( sCnpj + "   -   " + sNomeNormalizado);
			   }
			  
			}
			
			rs.close();
			stmt.close();
			conn.close();
   		} catch(SQLException se){
   			System.out.println(se.toString());
   		}
   		
   		return mapOperadorasOracle;
   	}
   
   
}
