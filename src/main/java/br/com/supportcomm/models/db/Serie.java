package br.com.supportcomm.models.db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;


import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.TelefoniaEnum;


@Entity
@Table(name = "Serie")
public class Serie implements Serializable
{
	private static final long serialVersionUID = 7999000923581185577L;
	@Id
	@GeneratedValue(generator = "SerieSeq")
	@SequenceGenerator(name = "SerieSeq", sequenceName = "serie_serieid_seq", allocationSize = 1)
	@Column(name = "serieid", nullable = false)
	private Long serieid;

	@Column(name = "serie", nullable = false)
	private int serie;
	
	@Column(name = "serie_start", nullable = false)
	private int serie_start;
	
	@Column(name = "serie_end", nullable = false)
	private int serie_end;
	
	@Column(name = "especial", nullable = false)
	private int especial;
	
	@JoinColumn(name = "operadoraid", referencedColumnName = "operadoraid", nullable = false)
	private Long operadoraid;
	
	@Column(name = "areaid", nullable = false)
	private Long areaid;

	private TelefoniaEnum telefonia;

	public TelefoniaEnum getTelefonia() {
		return telefonia;
	}

	public void setTelefonia(TelefoniaEnum telefonia) {
		this.telefonia = telefonia;
	}

	public Serie ()
	{

	}

	

	@Override
	public int hashCode ()
	{
		int hash = 0;
		hash += (serieid != null ? serieid.hashCode () : 0);
		return hash;
	}

	@Override
	public boolean equals (Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Serie))
		{
			return false;
		}
		Serie other = (Serie) object;
		if ((this.serieid == null && other.serieid != null) || (this.serieid != null && !this.serieid.equals (other.serieid)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString ()
	{
		StringBuffer sb = new StringBuffer ();

		sb.append (getClass ().getCanonicalName ());
		sb.append (" [serieid=");
		sb.append (serieid);
		sb.append (", serie=");
		sb.append (serie);
		sb.append (", serie_start=");
		sb.append (serie_start);
		sb.append (", serie_end=");
		sb.append (serie_end);
		sb.append (", especial=");
		sb.append (especial);
		sb.append ("]");

		return (sb.toString ());
	}

	public Long getSerieid() {
		return serieid;
	}

	public void setSerieid(Long serieid) {
		this.serieid = serieid;
	}

	public int getSerie() {
		return serie;
	}

	public void setSerie(int serie) {
		this.serie = serie;
	}
	
	public int getSerieStart() {
		return serie_start;
	}

	public void setSerieStart(int serie_start) {
		this.serie_start = serie_start;
	}
	
	public int getSerieEnd() {
		return serie_end;
	}

	public void setSerieEnd(int serie_end) {
		this.serie_end = serie_end;
	}
	

	public int getEspecial() {
		return especial;
	}

	public void setEspecial(int especial) {
		this.especial = especial;
	}

	/**
	 * @return the operadora
	 */
	public Long getOperadoraid ()
	{
		return operadoraid;
	}

	/**
	 * @param operadora the operadora to set
	 */
	public void setOperadoraid (Long operadoraid)
	{
		this.operadoraid = operadoraid;
	}
	
	/**
	 * @return the area
	 */
	public Long getAreaid ()
	{
		return areaid;
	}

	/**
	 * @param area the area to set
	 */
	public void setAreaid (Long areaid)
	{
		this.areaid = areaid;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

