package br.com.supportcomm.models.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "OperadoraNormalizada")
public class OperadoraNormalizada  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1710300851432696231L;

	@Id
	@GeneratedValue(generator = "OperadoraSeq")
	@SequenceGenerator(name = "OperadoraSeq", sequenceName = "operadoranormalizada_operadoranormid_seq", allocationSize = 1)
	@Column(name = "operadoranormid", nullable = false)
	private Long operadoranormid;
	
	
	@Column(name = "nome", nullable = false)
	String nome;
	
	@Column(name = "cnpj", nullable = false)
	String cnpj;
	
	@Column(name = "nomenormalizado", nullable = false)
	String nomenormalizado;
	
	public Long getOperadoranormId ()
	{
		return operadoranormid;
	}

	public void setOperadoranormId (Long id)
	{
		this.operadoranormid = id;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCnpj() {
		return cnpj;
	}
	
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getNomenormalizado() {
		return nomenormalizado;
	}
	public void setNomenormalizado(String nomenormalizado) {
		this.nomenormalizado = nomenormalizado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((nomenormalizado == null) ? 0 : nomenormalizado.hashCode());
		result = prime * result + ((operadoranormid == null) ? 0 : operadoranormid.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "OperadoraNormalizada [operadoranormid=" + operadoranormid + ", nome=" + nome + ", cnpj=" + cnpj
				+ ", nomenormalizado=" + nomenormalizado + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OperadoraNormalizada other = (OperadoraNormalizada) obj;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (nomenormalizado == null) {
			if (other.nomenormalizado != null)
				return false;
		} else if (!nomenormalizado.equals(other.nomenormalizado))
			return false;
		if (operadoranormid == null) {
			if (other.operadoranormid != null)
				return false;
		} else if (!operadoranormid.equals(other.operadoranormid))
			return false;
		return true;
	}


}
