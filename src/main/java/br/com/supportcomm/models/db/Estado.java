package br.com.supportcomm.models.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "Estado")
public class Estado implements Serializable
{
	private static final long serialVersionUID = 2823696724984297289L;
	@Id
	@GeneratedValue(generator = "EstadoSeq")
	@SequenceGenerator(name = "EstadoSeq", sequenceName = "estado_estadoid_seq", allocationSize = 1)
	@Column(name = "estadoid", nullable = false)
	private Long estadoid;
	//@Version
	//private int version;
	@Column(name = "nome", nullable = false)
	private String nome;
	@Column(name = "sigla", nullable = false)
	private String sigla;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "paisid", referencedColumnName = "paisid", nullable = false)
	private Pais paisid;
	

	public Estado ()
	{
		
	}

	public Long getEstadoId ()
	{
		return estadoid;
	}

	public Pais getPaisid() {
		return paisid;
	}

	public void setPaisid(Pais paisid) {
		this.paisid = paisid;
	}

	public void setEstadoId (Long id)
	{
		this.estadoid = id;
	}



	/**
	 * @return the nome
	 */
	public String getNome ()
	{
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome (String nome)
	{
		this.nome = nome;
	}

	/**
	 * @return the nome
	 */
	public String getSigla ()
	{
		return sigla;
	}

	/**
	 *
	 * @param sigla
	 */
	public void setSigla (String sigla)
	{
		this.sigla = sigla;
	}



	@Override
	public int hashCode ()
	{
		int hash = 0;
		hash += (estadoid != null ? estadoid.hashCode () : 0);
		return hash;
	}

	@Override
	public boolean equals (Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Estado))
		{
			return false;
		}
		Estado other = (Estado) object;
		if ((this.estadoid == null && other.estadoid != null) || (this.estadoid != null && !this.estadoid.equals (other.estadoid)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString ()
	{
		StringBuffer sb = new StringBuffer ();

		sb.append (getClass ().getCanonicalName ());
		sb.append (" [estadoid=");
		sb.append (estadoid);
		sb.append (", nome=");
		sb.append (nome);
		sb.append (", sigla=");
		sb.append (sigla);
		sb.append ("]");

		return (sb.toString ());
	}
	
}
