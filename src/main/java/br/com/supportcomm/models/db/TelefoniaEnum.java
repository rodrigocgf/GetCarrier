package br.com.supportcomm.models.db;

import javax.persistence.Entity;


public enum TelefoniaEnum {
	UNDEFINED(0),
	STFC(1),
	SMP(2),
	SME(3);
	
	private int value;

	TelefoniaEnum(int value) {
		this.value = value;
	}
	
	public int getValue(){
		return this.value;
	}
	
	public static TelefoniaEnum fromValue(int value){
		for (TelefoniaEnum element : values()) {
			if (element.value == value) {
				return element;
			}
		}
		return null;
	}
}