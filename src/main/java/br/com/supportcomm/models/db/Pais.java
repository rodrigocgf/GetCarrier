package br.com.supportcomm.models.db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



@Entity
@Table(name = "Pais")
public class Pais implements Serializable
{
	private static final long serialVersionUID = -241480634435535847L;
	@Id
	@GeneratedValue(generator = "PaisSeq")
	@SequenceGenerator(name = "PaisSeq", sequenceName = "pais_paisid_seq", allocationSize = 1)
	@Column(name = "paisid", nullable = false)
	private Long paisid;
	
	@Column(name = "version")
	private int version;
	
	@Column(name = "nome", nullable = false, unique = true)
	private String nome;
	
	@Column(name = "ddi", nullable = false)
	private Short ddi;
	
	@Column(name = "prioridade", nullable = false)
	private int prioridade;
	
	@Column(name = "tamanhododdd", nullable = false)
	private int tamanhoDoDdd;
	
	@Column(name = "usaportabilidade", nullable = false)
	private int usaPortabilidade;
	
		
	

	public Pais ()
	{
		
	}
	
	public Long getPaisId ()
	{
		return paisid;
	}

	public void setPaisId (Long paisId)
	{
		this.paisid = paisId;
	}

	/**
	 * @return the nome
	 */
	public String getNome ()
	{
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome (String nome)
	{
		this.nome = nome;
	}


	/**
	 * @return the ddi
	 */
	public Short getDdi ()
	{
		return ddi;
	}

	/**
	 * @param ddi the ddi to set
	 */
	public void setDdi (Short ddi)
	{
		this.ddi = ddi;
	}	

	/**
	 * @return the prioridade
	 */
	public int getPrioridade ()
	{
		return prioridade;
	}

	/**
	 * @param prioridade the prioridade to set
	 */
	public void setPrioridade (int prioridade)
	{
		this.prioridade = prioridade;
	}

	/**
	 * @return the tamanhoDoDdd
	 */
	public int getTamanhoDoDdd ()
	{
		return tamanhoDoDdd;
	}

	/**
	 * @param tamanhoDoDdd the tamanhoDoDdd to set
	 */
	public void setTamanhoDoDdd (int tamanhoDoDdd)
	{
		this.tamanhoDoDdd = tamanhoDoDdd;
	}

	/**
	 * @return the usaPortabilidade
	 */
	public int getUsaPortabilidade ()
	{
		return usaPortabilidade;
	}

	/**
	 * @param usaPortabilidade the usaPortabilidade to set
	 */
	public void setUsaPortabilidade (int usaPortabilidade)
	{
		this.usaPortabilidade = usaPortabilidade;
	}

	@Override
	public int hashCode ()
	{
		int hash = 0;
		hash += (paisid != null ? paisid.hashCode () : 0);
		return hash;
	}

	@Override
	public boolean equals (Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Pais))
		{
			return false;
		}
		Pais other = (Pais) object;

		if ((this.paisid == null && other.paisid != null) || (this.paisid != null && !this.paisid.equals (other.paisid)))
		{
			return false;
		}

		return true;
	}

	@Override
	public String toString ()
	{
		StringBuffer sb = new StringBuffer ();

		sb.append (getClass ().getCanonicalName ());
		sb.append (" [paisId=");
		sb.append (String.valueOf(paisid) );
		sb.append (", nome=");
		sb.append (nome);
		sb.append (", ddi=");
		sb.append (String.valueOf(ddi));
		sb.append (", prioridade="); 
		sb.append (String.valueOf(prioridade) ); 
		sb.append (", tamanhoDoDdd="); 
		sb.append (String.valueOf(tamanhoDoDdd) ); 
		sb.append (", usaPortabilidade="); 
		sb.append (String.valueOf(usaPortabilidade) ); 
		sb.append ("]");

		return (sb.toString ());
	}

	
}
