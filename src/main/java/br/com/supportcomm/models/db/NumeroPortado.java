package br.com.supportcomm.models.db;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import br.com.supportcomm.models.db.TelefoniaEnum;

@Entity
@Table(name = "NumeroPortado")
public class NumeroPortado implements Serializable
{
	private static final long serialVersionUID = -7748678372448544631L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "numeroportadoid", nullable = false)
	private Long numeroportadoid;
	
	
	@Column (name = "datadeinclusao", nullable = false)
	@Temporal (javax.persistence.TemporalType.TIMESTAMP)
	private Date datadeinclusao;
	
	@Column (name = "datademodificacao", nullable = false)
	@Temporal (javax.persistence.TemporalType.TIMESTAMP)
	private Date datademodificacao;
	
	@Column(name = "numerodemodificacoes", nullable = false)
	private Long numerodemodificacoes;
	
	@Column(name = "numerodobilhete", nullable = false)
	private Long numerodobilhete;
	
	@Column(name = "numero", nullable = false)
	private Long numero;
	
	@Column(name = "version")
	private int version;	
	
	@JoinColumn(name = "operadoraid", referencedColumnName = "operadoraid", nullable = false)
	private Long operadoraid;
	
	@JoinColumn(name = "areaid", referencedColumnName = "areaid", nullable = false)
	private Long areaid;
	
	@Column(name = "statusupdate", nullable = false)	
	private int statusupdate;
	

	@Column(name = "telefonia", nullable = false)
	private TelefoniaEnum telefonia;
	

	public TelefoniaEnum getTelefonia() {
		return telefonia;
	}

	public void setTelefonia(TelefoniaEnum telefonia) {
		this.telefonia = telefonia;
	}	

	public Long getNumeroPortadoId ()
	{
		return numeroportadoid;
	}

	public void setNumeroPortadoId (Long numeroPortadoId)
	{
		this.numeroportadoid = numeroPortadoId;
	}


	public int getStatusupdate() {
		return statusupdate;
	}

	public void setStatusupdate(int statusupdate) {
		this.statusupdate = statusupdate;
	}


	/**
	 * @return the operadora
	 */
	public Long getOperadoraid ()
	{
		return operadoraid;
	}

	/**
	 * @param operadora the operadora to set
	 */
	public void setOperadoraid (Long operadoraid)
	{
		this.operadoraid = operadoraid;
	}

	/**
	 * @return the area
	 */
	public Long getAreaid ()
	{
		return areaid;
	}

	/**
	 * @param area the area to set
	 */
	public void setAreaid ( Long areaid )
	{
		this.areaid = areaid;
	}

	/**
	 * @return the numero
	 */
	public Long getNumero ()
	{
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero (Long numero)
	{
		this.numero = numero;
	}

	/**
	 * @return the dataDeInclusao
	 */
	public Date getDataDeInclusao ()
	{
		return datadeinclusao;
	}

	/**
	 * @param dataDeInclusao the dataDeInclusao to set
	 */
	public void setDataDeInclusao (Date dataDeInclusao)
	{
		this.datadeinclusao = dataDeInclusao;
	}

	/**
	 * @return the dataDeModificacao
	 */
	public Date getDataDeModificacao ()
	{
		return datademodificacao;
	}

	/**
	 * @param dataDeModificacao the dataDeModificacao to set
	 */
	public void setDataDeModificacao (Date dataDeModificacao)
	{
		this.datademodificacao = dataDeModificacao;
	}

	/**
	 * @return the numeroDeModificacoes
	 */
	public Long getNumeroDeModificacoes ()
	{
		return numerodemodificacoes;
	}

	/**
	 * @param numeroDeModificacoes the numeroDeModificacoes to set
	 */
	public void setNumeroDeModificacoes (Long numeroDeModificacoes)
	{
		this.numerodemodificacoes = numeroDeModificacoes;
	}

	/**
	 * @return the numerodobilhete
	 */
	public Long getNumerodobilhete ()
	{
		return numerodobilhete;
	}

	/**
	 * @param numeroDoBilhete the numeroDoBilhete to set
	 */
	public void setNumerodobilhete (Long numeroDoBilhete)
	{
		this.numerodobilhete = numeroDoBilhete;
	}

	@Override
	public int hashCode ()
	{
		int hash = 0;
		hash += (numeroportadoid != null ? numeroportadoid.hashCode () : 0);
		return hash;
	}

	@Override
	public boolean equals (Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof NumeroPortado))
		{
			return false;
		}
		NumeroPortado other = (NumeroPortado) object;
		if ((this.numeroportadoid == null && other.numeroportadoid != null) || (this.numeroportadoid != null && !this.numeroportadoid.equals (other.numeroportadoid)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString ()
	{
		StringBuffer sb = new StringBuffer ();

		sb.append (getClass ().getCanonicalName ());
		sb.append (" [numeroportadoid=");
		sb.append (numeroportadoid);
		sb.append (", numero=");
		sb.append (numero.toString());
		sb.append (", datadeinclusao=");
		sb.append (datadeinclusao.toString());
		sb.append (", datademodificacao=");
		sb.append (datademodificacao.toString());
		sb.append (", numerodemodificacoes=");
		sb.append (numerodemodificacoes.toString());
		sb.append (", numerodobilhete=");
		sb.append (numerodobilhete.toString());
		sb.append ("]");

		return (sb.toString ());
	}

	/**
	 * @return the version
	 */
	public int getVersion ()
	{
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion (int version)
	{
		this.version = version;
	}
}
