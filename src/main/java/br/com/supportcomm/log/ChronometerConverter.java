package br.com.supportcomm.log;

import java.util.Map;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

public class ChronometerConverter extends ClassicConverter {

	public static long getTimerValue(String ticks) {
		try {
			long tick = Long.parseLong(ticks);
			return System.currentTimeMillis() - tick;
		}
		catch (NumberFormatException e) {
			System.out.println("Error in ChronometerConverter. timer start not mumeric: " + ticks);
			return -1;
		}
	}

	@Override
	public String convert(ILoggingEvent event) {
		Map<String,String> mdc = event.getMDCPropertyMap();
		String start = mdc.get("timer_start_sub");
		String prefix = mdc.get("timer_name_sub");
		Long tick = null;
		if (start == null) {
			start = mdc.get("timer_start");
			prefix = mdc.get("timer_name");
		}
		if (start != null) {
			tick = getTimerValue(start);
		}
		if (tick != null && tick >= 0) {
			if (prefix != null) {
				return " " + prefix + ":" + tick;
			}
			else {
				return " " + tick;
			}
		}
		else {
			return "";
		}
	}
}
