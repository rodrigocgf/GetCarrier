package br.com.supportcomm.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import static net.logstash.logback.argument.StructuredArguments.kv;
import static net.logstash.logback.argument.StructuredArguments.v;

public class LogExtension {

	private static Logger logger = LoggerFactory.getLogger(LogExtension.class);
	
	public static void clearAll() {
		MDC.clear();
	}
	
	public static void setMsisdn(String msisdn) {
		MDC.put("msisdn", msisdn);
	}

	public static void setVariable(String name, String value) {
		MDC.put(name, value);
	}

	public static void removeVariable(String name) {
		MDC.remove(name);
	}

	public static void startTimer(String prefix) {
		MDC.put("timer_name", prefix);
		MDC.put("timer_start", String.valueOf(System.currentTimeMillis()));
	}

	public static void startSubTimer(String prefix) {
		MDC.put("timer_name_sub", prefix);
		MDC.put("timer_start_sub", String.valueOf(System.currentTimeMillis()));
	}

	public static void clearMsisdn() {
		MDC.remove("msisdn");
	}

	public static void stopTimer() {
		if (MDC.get("timer_start_sub") != null) {
			logger.debug("SubTimer {} stopped. {}", v("timer_stopped", MDC.get("timer_name_sub")), kv("total_time", ChronometerConverter.getTimerValue(MDC.get("timer_start_sub"))) );			
		}
		if (MDC.get("timer_start") != null) {
			logger.debug("Timer {} stopped. {}", v("timer_stopped", MDC.get("timer_name")), kv("total_time", ChronometerConverter.getTimerValue(MDC.get("timer_start"))) );			
		}
		MDC.remove("timer_name");
		MDC.remove("timer_start");
		MDC.remove("timer_name_sub");
		MDC.remove("timer_start_sub");
	}

	public static void stopSubTimer() {
		if (MDC.get("timer_start_sub") != null) {
			logger.debug("SubTimer {} stopped. {}", v("timer_stopped", MDC.get("timer_name_sub")), kv("total_time", ChronometerConverter.getTimerValue(MDC.get("timer_start_sub"))) );			
		}
		MDC.remove("timer_name_sub");
		MDC.remove("timer_start_sub");
	}
}
