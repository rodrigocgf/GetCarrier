package br.com.supportcomm;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;

public class AppServletContextListener implements ServletContextListener, ApplicationListener<ApplicationEnvironmentPreparedEvent> {
	//public static Logger logger = LoggerFactory.getLogger(AppServletContextListener.class);
	
    @Override
    public void contextDestroyed(ServletContextEvent event) {
		GetCarrier.logger.info("CONTEXT DESTROYED");
    }
 
    @Override
    public void contextInitialized(ServletContextEvent event) {
    	GetCarrier.logger.info("CONTEXT STARTED");
    }

	@Override
	public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
		GetCarrier.logger.info("APPLICATION EnvironmentPreparedEvent");
	}
    
}
