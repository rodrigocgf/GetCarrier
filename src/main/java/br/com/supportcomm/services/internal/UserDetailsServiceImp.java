package br.com.supportcomm.services.internal;

import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.supportcomm.jparepositories.UserLoginRepoImpl;
import io.micrometer.core.annotation.Timed;

@Service
public class UserDetailsServiceImp implements UserDetailsService {
	public static Logger logger = LoggerFactory.getLogger(UserDetailsServiceImp.class);
			
    //@Autowired
	
    private UserLoginRepoImpl userLoginRepoImpl;
    
 
    @Override
    @Timed("sc.userlogin.fetch")
    public UserDetails loadUserByUsername(String username) {
    	
    	Query query = (Query) userLoginRepoImpl.findByUserName(username);
        //Optional<UserLogin> user = userRepository.findById(username);
    	
        //if (!user.isPresent()) {
    	if ( query.getResultList().size() > 0 ) {
        	logger.debug("loadUserByUsername " + username + " NOT FOUND");
            throw new UsernameNotFoundException(username);
        }
    
        try 
        {
        	UserBuilder builder = org.springframework.security.core.userdetails.User.withUsername(username);
        	return builder.build();
        	/*
	        UserLogin userLogin = user.get();
	        UserBuilder builder = org.springframework.security.core.userdetails.User.withUsername(username);
	        builder.password(userLogin.getPassword());
	        builder.roles(userLogin.getRole());
	        builder.disabled(userLogin.getEnabled() != Boolean.TRUE);
	        return builder.build();
	        */
        }
        catch (Exception e) {
        	logger.error("Error building UserDetails", e);
            throw new InternalAuthenticationServiceException("Error building UserDetails", e);
        }
    }
    
    @PostConstruct
    public void init() {
    	logger.info("UserPrincipalService Contructed");
    }

    @PreDestroy
    public void destroy() {
    	logger.info("UserPrincipalService will be destroyed");
    }
}
