package br.com.supportcomm.repositories;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.models.db.Historico;

@Transactional
public interface HistoricoRepository extends CrudRepository<Historico, Long > {
	List<Historico> RetornoOrdernado();	
}
