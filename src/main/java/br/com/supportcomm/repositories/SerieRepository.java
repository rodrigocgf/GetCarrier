package br.com.supportcomm.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;

@Transactional
public interface SerieRepository extends CrudRepository<Serie, Long > {
	List<Serie> procuraPor_Area_Especial_Serie(@Param("areaid") Area areaid ,@Param("especial") int especial ,@Param("serie") int serie);
	List<Serie> buscaPorPais(@Param("pais") Pais pais);
	List<Serie> selectAll();
}
