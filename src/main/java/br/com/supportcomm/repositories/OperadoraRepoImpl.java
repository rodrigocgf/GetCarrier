package br.com.supportcomm.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.supportcomm.business.Msisdn;
import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Pais;

@Repository
public class OperadoraRepoImpl implements IOperadora {

	public static Logger logger = LoggerFactory.getLogger(OperadoraRepoImpl.class);
	
	@Autowired
	private OperadoraRepository repo;
	
	@Override
	@Transactional
	public 
	List<Operadora> procuraPor_Pais_Nome(@Param("paisid") Pais paisid, @Param("cnpj") String cnpj) {
		List<Operadora> query = repo.procuraPor_Pais_Nome(paisid, cnpj);
		return query;
	}

	@Override
	@Transactional
	public
	List<Operadora> procuraPor_Pais_Spid(@Param("paisid") Pais paisid, @Param("spid") short spid) {
		logger.info(String.format("SELECT o FROM Operadora o WHERE o.paisid = %d AND o.spid = %d", paisid.getPaisId(), spid));
		
		try {
		List<Operadora> query = repo.procuraPor_Pais_Spid(paisid, spid);
		return query;
		}
		catch(Exception ex) {
			logger.info(String.format("procuraPor_Pais_Spid error : %s", ex.toString()));
			return null;
		}
	}
	
	@Override
	@Transactional
	public
	List<Operadora> procuraPor_Pais(@Param("paisid") Pais paisid) {
		
		logger.info(String.format("SELECT o FROM Operadora o WHERE o.paisid = %d ORDER BY o.spid", paisid.getPaisId() ) );
		
		List<Operadora> query = repo.procuraPor_Pais(paisid);
		return query;
	}
	
	
	@Transactional
	public 
	Operadora selectOperadoraById(@Param("operadoraid") Long operadoraid) {
		logger.info(String.format("SELECT a FROM Operadora a WHERE a.operadoraid = %d", operadoraid));
		Operadora oper = repo.selectOperadoraById(operadoraid);
		return oper;
	}
	
}
