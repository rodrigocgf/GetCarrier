package br.com.supportcomm.repositories;

import java.util.List;

import org.springframework.data.repository.query.Param;

import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Pais;

public interface IOperadora {
	List<Operadora> procuraPor_Pais_Nome(@Param("pais") Pais pais, @Param("cnpj") String cnpj);
	
	List<Operadora> procuraPor_Pais_Spid(@Param("pais") Pais pais, @Param("spid") short spid);
	
	List<Operadora> procuraPor_Pais(@Param("pais") Pais pais);
	
	//List<Operadora> getOperadoras();
}
