package br.com.supportcomm.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Pais;

@Transactional
public interface OperadoraRepository extends CrudRepository<Operadora, Long > {
	List<Operadora> procuraPor_Pais_Nome(@Param("pais") Pais pais, @Param("cnpj") String cnpj);
	List<Operadora> procuraPor_Pais_Spid(@Param("paisid") Pais paisid, @Param("spid") short spid);
	List<Operadora> procuraPor_Pais(@Param("pais") Pais pais);
	Operadora selectOperadoraById(@Param("operadoraid") Long operadoraid);
}

