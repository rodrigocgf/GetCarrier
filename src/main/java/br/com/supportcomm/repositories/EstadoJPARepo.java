package br.com.supportcomm.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import br.com.supportcomm.models.db.Estado;

public interface EstadoJPARepo extends JpaRepository<Estado, Long >
{

}
