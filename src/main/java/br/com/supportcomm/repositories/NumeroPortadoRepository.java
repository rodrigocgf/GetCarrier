package br.com.supportcomm.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.NumeroPortado;

public interface NumeroPortadoRepository extends CrudRepository<NumeroPortado, Long > {
	List<NumeroPortado> procuraPor_Area_Numero(@Param("areaid") Area areaid, @Param("numero") Long numero);
	List<NumeroPortado> procuraPor_Numero(@Param("numero") Long numero);
	void delete_StatusUpdate_1();
	void updateStatusUpdate();	
	void deletePor_NumeroPortadoId(@Param("id") Long id);	
}
