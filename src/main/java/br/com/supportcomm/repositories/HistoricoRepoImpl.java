package br.com.supportcomm.repositories;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.supportcomm.business.Msisdn;
import br.com.supportcomm.models.db.Historico;

@Repository
public class HistoricoRepoImpl implements IHistorico{
	
	public static Logger logger = LoggerFactory.getLogger(HistoricoRepoImpl.class);
	
	private static final Date Date = null;
	
	@Autowired
	private HistoricoRepository historicoRepo;
	
	
	@Override
	@Transactional
	public
	List<Historico> RetornoOrdernado() {
		logger.info("SELECT h FROM Historico h where h.arquivo like 'BDT%' or h.arquivo like 'CPL%' ORDER BY h.dataDeReferencia DESC");
		List<Historico> query = historicoRepo.RetornoOrdernado();
		return query;
	}	
	

	@Override
	@Transactional
	public void update(	Timestamp dataDeProcessamento, Timestamp dataDeReferencia, int cargaFull,
							int numRegProc, String arquivo, int version) {
		
		Historico hist = new Historico();
		hist.setHistoricoId(0L);
		hist.setDataDeProcessamento(dataDeProcessamento);
		hist.setDataDeReferencia(dataDeReferencia);
		hist.setCargaFull(cargaFull);
		hist.setNumeroDeRegistrosProcessados(numRegProc);
		hist.setArquivo(arquivo);
		hist.setVersion(version);
		
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\r\n");
		sb.append("=======================================================").append("\r\n");
		sb.append("         INSERT INTO Historico ").append("\r\n");
		sb.append("         (      ").append("\r\n");
		sb.append("             arquivo, ").append("\r\n");
		sb.append("             datadeprocessamento, ").append("\r\n");
		sb.append("             datadereferencia, ").append("\r\n");
		sb.append("             numeroderegistrosprocessados ").append("\r\n");
		sb.append("         ) values ( ").append("\r\n");
		sb.append("             ").append(arquivo).append(" ,\r\n");		
		sb.append("             ").append(dataDeProcessamento.toString()).append(" ,\r\n");		
		sb.append("             ").append(dataDeReferencia.toString()).append(" ,\r\n");		
		sb.append("             ").append(String.valueOf(numRegProc)).append(" \r\n");
		sb.append("         )").append(" \r\n");
		sb.append("=======================================================").append("\r\n");
		
		logger.info(sb.toString());
		
		try {
			historicoRepo.save(hist);
		}
		catch( Exception ex) {
			logger.info(String.format("Excepction : %s", ex.toString()));
		}
	}
	
	@Override
	@Transactional
	public void insert(Historico historico) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("\r\n");
		sb.append("=======================================================").append("\r\n");
		sb.append("         INSERT INTO Historico ").append("\r\n");
		sb.append("         (      ").append("\r\n");
		sb.append("             arquivo, ").append("\r\n");
		sb.append("             datadeprocessamento, ").append("\r\n");
		sb.append("             datadereferencia, ").append("\r\n");
		sb.append("             numeroderegistrosprocessados ").append("\r\n");
		sb.append("         ) values ( ").append("\r\n");
		sb.append("             ").append(historico.getArquivo()).append(" ,\r\n");		
		sb.append("             ").append(historico.getDataDeProcessamento().toString()).append(" ,\r\n");		
		sb.append("             ").append(historico.getDataDeReferencia().toString()).append(" ,\r\n");		
		sb.append("             ").append(String.valueOf(historico.getNumeroDeRegistrosProcessados())).append(" \r\n");
		sb.append("         )").append(" \r\n");
		sb.append("=======================================================").append("\r\n");
		
		logger.info(sb.toString());
		
		try {
			historicoRepo.save(historico);
		}
		catch( Exception ex) {
			logger.info(String.format("Excepction : %s", ex.toString()));			
		}
	}
	
	@Override
	@Transactional
	public void update(Historico historico) {
		StringBuilder sb = new StringBuilder();		
		
		sb.append("\r\n");
		sb.append("=======================================================").append("\r\n");
		sb.append("         UPDATE Historico h set ").append("\r\n");
		sb.append("             h.arquivo = ");
		sb.append(historico.getArquivo()).append(",\r\n");
		sb.append("             h.datadeprocessamento = ");
		sb.append(historico.getDataDeProcessamento().toString()).append(",\r\n");
		sb.append("             h.datadereferencia = ");
		sb.append(historico.getDataDeReferencia().toString()).append(",\r\n");
		sb.append("             h.numeroderegistrosprocessados = ");
		sb.append(String.valueOf(historico.getNumeroDeRegistrosProcessados())).append(",\r\n");
		sb.append("=======================================================").append("\r\n");
		logger.info(sb.toString());
		
		try {
			historicoRepo.save(historico);
		}
		catch( Exception ex) {
			logger.info(String.format("Excepction : %s", ex.toString()));			
		}
	}
}
