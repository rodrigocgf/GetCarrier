package br.com.supportcomm.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.supportcomm.models.db.Estado;

@Repository
public class EstadoRepoImpl implements IEstado {
	
	@Autowired
	private EstadoRepository repo;
	
	@Override
	@Transactional
	public 	List<Estado> selectNomeSigla(){
		List<Estado> estados = (List<Estado>)repo.selectNomeSigla();
		return estados;
	}
}
