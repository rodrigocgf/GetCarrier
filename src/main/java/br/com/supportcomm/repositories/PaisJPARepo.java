package br.com.supportcomm.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.supportcomm.models.db.Pais;

public interface PaisJPARepo extends JpaRepository<Pais, Integer >
{

}
