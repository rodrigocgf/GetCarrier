package br.com.supportcomm.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.supportcomm.business.Msisdn;
import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Pais;

@Repository
public class AreaRepoImpl implements IArea {

	public static Logger logger = LoggerFactory.getLogger(AreaRepoImpl.class);
	
	@Autowired
	private AreaRepository repo;
	
	@Override
	@Transactional
	public
	Area procuraPor_Pais_DDD(@Param("pais") Pais pais, @Param("ddd") int ddd) {
		List<Area> query = (List<Area>)repo.procuraPor_Pais_DDD(pais, ddd);
		if ( query != null && query.size() > 0 )
			return query.get(0);
		else
			return null;
	}
	
	@Override
	@Transactional
	public
	List<Area> selectAll() {
		List<Area> query = (List<Area>)repo.selectAll();
		return query;
	}
	
	@Override
	@Transactional
	public
	Area       selectAreaById(@Param("areaid") Long areaid) {
		logger.info(String.format("SELECT a FROM Area a WHERE a.areaid = %d", areaid));
		Area area = repo.selectAreaById(areaid);
		return area;
	}
	
}
