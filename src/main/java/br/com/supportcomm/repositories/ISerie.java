package br.com.supportcomm.repositories;

import java.util.List;

import org.springframework.data.repository.query.Param;

import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;

public interface ISerie {
	List<Serie> procuraPor_Area_Especial_Serie(@Param("area") Area area ,@Param("especial") int especial ,@Param("serie") int serie);
	List<Serie> buscaPorPais(@Param("pais") Pais pais);
	List<Serie> selectAll();
	void update(Serie serie);
}
