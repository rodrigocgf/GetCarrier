package br.com.supportcomm.repositories;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.data.repository.query.Param;

import br.com.supportcomm.models.db.Historico;

public interface IHistorico {
	List<Historico> RetornoOrdernado();
	void update(Timestamp dateDeProcessamento, Timestamp dateDeReferencia, int cargaFull, int numRegProc , String arquivo, int version);	
	void update(Historico historico);
	void insert(Historico historico);
}
