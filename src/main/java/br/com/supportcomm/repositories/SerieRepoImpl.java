package br.com.supportcomm.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.supportcomm.business.Msisdn;
import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;

@Repository
public class SerieRepoImpl implements ISerie {
	
	public static Logger logger = LoggerFactory.getLogger(SerieRepoImpl.class);
	
	@Autowired
	private SerieRepository repo;

	
	@Override
	@Transactional
	public
	List<Serie> procuraPor_Area_Especial_Serie(@Param("areaid") Area areaid ,@Param("especial") int especial ,@Param("serie") int serie) {
		logger.info(String.format("SELECT s FROM Serie s WHERE s.areaid = %d AND s.especial = %d AND s.serie = %d", 
				areaid.getAreaid() , especial, serie));
		List<Serie> query = repo.procuraPor_Area_Especial_Serie(areaid, especial, serie);
		if ( query.size() == 0) 
			logger.info("NO SERIE FOUND");
		return query;
	}
	
	@Override
	@Transactional
	public
	List<Serie> buscaPorPais(@Param("pais") Pais pais) {
		logger.info(String.format("SELECT s FROM Serie s where s.areaid.paisid = %d",pais.getPaisId()) );
		
		List<Serie> query = repo.buscaPorPais(pais);
		if ( query.size() == 0) 
			logger.info("NO SERIE FOUND");
		return query;
	}
	
	@Override
	@Transactional
	public
	List<Serie> selectAll() {
		List<Serie> query = repo.selectAll();
		return query;
	}
	
	@Override
	@Transactional
	public void update(Serie serie) {
		repo.save(serie);
	}
}
