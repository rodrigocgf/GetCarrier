package br.com.supportcomm.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.cache.CacheNames;
import br.com.supportcomm.models.db.UserLogin;
import io.micrometer.core.annotation.Timed;

@Transactional
public interface UserLoginRepository extends CrudRepository<UserLogin, Long > {
	List<UserLogin> findByUserName(@Param("username") String username);
	
}

/*
@Repository
public interface UserLoginRepository extends JpaRepository<UserLogin, String>{
	@SuppressWarnings("unchecked")
	@Timed("sc.userlogin.save")
	UserLogin save(UserLogin entity);

	@Override
	@Timed("sc.userlogin.findById")
	@Cacheable(cacheNames=CacheNames.NAME_CACHE_USER_LOGIN)
	Optional<UserLogin> findById(String id);
	
}
*/