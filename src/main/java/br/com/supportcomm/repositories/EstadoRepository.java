package br.com.supportcomm.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.models.db.Estado;

@Transactional
public interface EstadoRepository extends CrudRepository<Estado, Long > {
	
	List<Estado> selectNomeSigla();

}