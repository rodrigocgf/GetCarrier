package br.com.supportcomm.repositories;

import java.util.List;

import br.com.supportcomm.models.db.Estado;

public interface IEstado {	
	List<Estado> selectNomeSigla(); 
}
