package br.com.supportcomm.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.supportcomm.models.db.UserLogin;

@Repository
public class UserLoginRepoImpl implements IUserLogin {
	
	@Autowired
	private UserLoginRepository repo;
	
	@Override
	@Transactional
	public List<UserLogin> findByUserName(@Param("username") String username) {
		List<UserLogin> query = repo.findByUserName(username);
		return query;
	}
}
