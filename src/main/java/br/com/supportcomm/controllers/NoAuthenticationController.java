package br.com.supportcomm.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.supportcomm.jparepositories.PaisRepoImpl;

@RestController
public class NoAuthenticationController {
	private static Logger logger = LoggerFactory.getLogger(NoAuthenticationController.class);

	@Autowired
	public PaisRepoImpl  paisRepoImpl;
	
	@RequestMapping(value="noauthentication")
	@ResponseBody
    public String hello(@RequestParam String name) {
    	//String result = "Hello " + name + " from NoAuthentication";
    	//logger.info("NoAuthentication called by " + name);
    	logger.info("NoAuthentication called.");
    	String result = "Hello !";
    	return result; 
    }
}
