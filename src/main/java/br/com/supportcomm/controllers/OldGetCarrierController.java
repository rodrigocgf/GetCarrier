package br.com.supportcomm.controllers;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.supportcomm.GetCarrier;
import br.com.supportcomm.business.MsisdnExt;
import br.com.supportcomm.config.AppConfig;
import br.com.supportcomm.exception.CodigoRetornoEnum;
import br.com.supportcomm.exception.PortabilidadeException;
import br.com.supportcomm.log.LogExtension;
import br.com.supportcomm.util.ArquivoAbr;
import br.com.supportcomm.util.ArquivoAnatel;
import br.com.supportcomm.util.Ferramentas;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//@EnableAutoConfiguration // DEPLOY TO TOMCAT
//@RestController
public class OldGetCarrierController {
	public static Logger logger = LoggerFactory.getLogger(GetCarrier.class);
	
	
	private int statusDaCarga = 0;
	
	@Autowired
	MsisdnExt msisdnExt;
	
	//@Autowired
	//ArquivoAnatel anatel;
	
	@Autowired
	ArquivoAbr arqBr;
	
	@Autowired 
	private AppConfig appConfig;
	
	public OldGetCarrierController() {		
				
	}	
	
	// DEBUG : 
	//	@RequestMapping("getcarrier")
	// 	@ResponseBody
	
	// DEPLOY TO TOMCAT :
	//	@RequestMapping(method = RequestMethod.GET)	
	//	@GetMapping(path="/getcarrier")	
	//	@ResponseBody
	
	public void execute(	@RequestParam(value="operacao", defaultValue="buscar") String operacao,
									@RequestParam(value="formato", defaultValue="") String formato,
									@RequestParam(value="interface", defaultValue="") String interf,
									@RequestParam(value="msisdn", defaultValue="") String msisdn,																		
									HttpServletRequest request ,
									HttpServletResponse response)
	{	
		
		logger.info("------------------------------------------------------------");
		
		LogExtension.clearMsisdn();
		LogExtension.setMsisdn(msisdn);
		logger.info(String.format("/getcarrier?operacao=%s&formato=%s&msisdn=%s&interface=%s", 
					operacao, formato, msisdn,interf));
		
		if ( operacao.equals("buscar")) {
			if ( !checkRequestParameters_Buscar(request , response))
				return ;
			
			comando(msisdn, formato, request , response);
		}  else if ( operacao.equals("carga")) {			
			ShowParameters_Carga();
			
			carga(msisdn, formato, request , response);
		} 
		
		logger.info("------------------------------------------------------------");
	}
	
	private boolean checkRequestParameters_Buscar(HttpServletRequest request , HttpServletResponse response) {
		boolean bRet = true;
		
		if (request.getParameter("msisdn")  == null) {
			try {
				geraPaginaDeErro(	response, HttpServletResponse.SC_BAD_REQUEST, 
									new PortabilidadeException(CodigoRetornoEnum.FALHA, "Msisdn nao especificado."));
			} catch (ServletException | IOException e1) {
				e1.printStackTrace();
			}
			bRet = false;
		}
		
		if (request.getParameter("operacao")  == null) {
			try {
				geraPaginaDeErro(	response, HttpServletResponse.SC_BAD_REQUEST, 
									new PortabilidadeException(CodigoRetornoEnum.FALHA, "Operacao nao especificada."));
			} catch (ServletException | IOException e1) {
				e1.printStackTrace();
			}
			bRet = false;
		}
		if ( !(request.getParameter("operacao").equalsIgnoreCase("buscar") == true ||				 
				 request.getParameter("operacao").equalsIgnoreCase("carga") == true
				)
		) {
			try {
				geraPaginaDeErro(	response, HttpServletResponse.SC_BAD_REQUEST, 
									new PortabilidadeException(CodigoRetornoEnum.FALHA, "Operacao " + request.getParameter("operacao") + " desconhecida."));
			} catch (ServletException | IOException e1) {
				e1.printStackTrace();
			}
			bRet = false;
		}
		
		if (request.getParameter("formato")  == null) {
			try {
				geraPaginaDeErro(	response, HttpServletResponse.SC_BAD_REQUEST, 
									new PortabilidadeException(CodigoRetornoEnum.FALHA, "Formato nao especificado."));
			} catch (ServletException | IOException e1) {
				e1.printStackTrace();
			}
			bRet = false;
		}
		
		if ( !(request.getParameter("formato").equalsIgnoreCase("html") == true ||
			 request.getParameter("formato").equalsIgnoreCase("xml") == true ||
			 request.getParameter("formato").equalsIgnoreCase("text") == true
			)
		) {
			try {
				geraPaginaDeErro(	response, HttpServletResponse.SC_BAD_REQUEST, 
									new PortabilidadeException(CodigoRetornoEnum.FALHA, "Formato " + request.getParameter("formato") + " desconhecido."));
			} catch (ServletException | IOException e1) {
				e1.printStackTrace();
			}
			bRet = false;
		}
		
		return bRet;
	}
	
	private void carga(	String msisdn, String formato ,  
			HttpServletRequest request , HttpServletResponse response) {
		logger.info("STARTING CHARGE!");
		if (statusDaCarga != 0) {
			return;
		}
		statusDaCarga = 1;
		try {
			String diretorioEntradaDeCarga = appConfig.getDiretorioEntradaDeCarga();
			String diretorioSaidaDeCarga = appConfig.getDiretorioSaidaDeCarga();
			String diretorioIgnorados = appConfig.getDiretorioIgnorados();
			String diretorioRejeitados = appConfig.getDiretorioRejeitados();
			
			Path pathEntrada = Paths.get(diretorioEntradaDeCarga);
			Path pathSaida = Paths.get(diretorioSaidaDeCarga);
			Path pathIgnorados = Paths.get(diretorioIgnorados);
			Path pathRejeitados = Paths.get(diretorioRejeitados);
			
			
			File dirIn = pathEntrada.toFile();
			File dirOut = pathSaida.toFile();
			File dirIgnorados = pathIgnorados.toFile();
			File dirRejeitados = pathRejeitados.toFile();
			
			
			if (!dirIn.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioEntradaDeCarga);
				return;
			}
			
			if (!dirOut.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioSaidaDeCarga);
				return;
			}
			
			if (!dirIgnorados.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioIgnorados);
				return;
			}
			
			if (!dirRejeitados.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioRejeitados);
				return;
			}
			
			logger.info("STARTING PROCESS!");		

			// O PROCESSAMENTO ANATEL FOI DESCONTINUADO !
			// A Anatel não está mais disponibilizando os arquivos CE_M* das centrais móveis.
			// Via site, eles apenas disponibilizam arquivos do tipo CE_F* das centrais fixas.
			//anatel.processa(dirIn, dirOut);
			
			//ArquivoAbr arqBr = new ArquivoAbr();
			
			arqBr.processa(dirIn, dirOut, dirIgnorados, dirRejeitados );
		}
		catch(Exception ex  ) {
			logger.debug("Exception : " + ex.toString());
		}
		finally {
			statusDaCarga = 0;
		}
	}
	
	
	
	private void comando(	String msisdn, String formato , HttpServletRequest request , HttpServletResponse response)
	{
		StringBuilder sb = new StringBuilder();
		PrintWriter out = null;
		String strResponse;

		
		long tempo = System.currentTimeMillis();
		try
		{				
			msisdnExt.setMsisdn(msisdn);
			msisdnExt.start();
			
			if(msisdn.startsWith("52")) {
				msisdnExt.setDdi(new Short("52"));
				msisdnExt.setNomeDaOperadora("TelCel Mexico");
				msisdnExt.setNomeDaOperadoraNormalizado("Telcel");
			} 
			
			sb.append("MSIDN(").append(msisdn).append(") esta na operadora(").append(msisdnExt.getNomeDaOperadora());
			sb.append("), formato(").append(formato).append("), ip(").append(request.getRemoteAddr()).append(")");
			logger.debug(sb.toString());			
			
			tempo = System.currentTimeMillis() - tempo;
			strResponse = mountResponse(formato , tempo , response , msisdnExt);
			
			out = response.getWriter();
			out.print(strResponse);
		}		
		catch (Exception e) {
			logger.debug(String.format("Erro na função comando : %s", e.toString()));
			PortabilidadeException pex = new PortabilidadeException(CodigoRetornoEnum.FALHA, e.getMessage());
			try {
				geraPaginaDeErro(response, HttpServletResponse.SC_BAD_REQUEST, pex);
			} catch (ServletException | IOException e1) {
				e1.printStackTrace();
			}			
		}
		finally {
			if (out != null) {
				out.close();
			}
		}		
	}
	
	private String mountResponse(String formato , long tempo, HttpServletResponse response, MsisdnExt msisdnExt) {
		StringBuffer sb = new StringBuffer();
		
		if (formato.equals("html")) {
			response.setContentType("text/html;charset=UTF-8");
			sb.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2//EN\">\n");
			sb.append("<html>\n");
			sb.append("<head>\n");
			sb.append("<title>Portabilidade</title>\n");
			sb.append("</head>\n");
			sb.append("<body>\n");
			sb.append("<table>\n");
			sb.append("<tbody>\n");
		} else if (formato.equals("xml")) {
			response.setContentType("text/xml;charset=UTF-8");
			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			sb.append("<Portabilidade>\n");
		} else {
			response.setContentType("text/plain;charset=UTF-8");
		}
		formataLinha(sb, "CodigoRetorno", CodigoRetornoEnum.OK.toString(), formato);
		formataLinha(sb, "MSISDN", msisdnExt.getMsisdn(), formato);
		formataLinha(sb, "DDI", msisdnExt.getDdi(), formato);
		formataLinha(sb, "DDD", msisdnExt.getDdd(), formato);
		formataLinha(sb, "Telefone", msisdnExt.getNumero(), formato);
		formataLinha(sb, "Pais", msisdnExt.getNomeDoPais(), formato);
		formataLinha(sb, "Estado", msisdnExt.getNomeDoEstado(), formato);
		formataLinha(sb, "SiglaDoEstado", msisdnExt.getSiglaDoEstado(), formato);
		formataLinha(sb, "Operadora", msisdnExt.getNomeDaOperadora(), formato);
		formataLinha(sb, "OperadoraNomeNormalizado", msisdnExt.getNomeDaOperadoraNormalizado(), formato);
		formataLinha(sb, "CodigoDaOperadora", msisdnExt.getCodigoDaOperadora(), formato);
		formataLinha(sb, "Precisao", msisdnExt.getPrecisao(), formato);
		formataLinha(sb, "Portado", msisdnExt.getPortado(), formato);
		formataLinha(sb, "TempoPesquisaMsec", tempo, formato);

		if (formato.equals("html")) {
			sb.append("</tbody>");
			sb.append("</table>");
			sb.append("</body>\n");
			sb.append("</html>\n");
		} else if (formato.equals("xml")) {
			sb.append("</Portabilidade>\n");
		}
		
		return sb.toString();
		
	}
	
	private void formataLinha(StringBuffer sb, String titulo, long valor, String formato) {
		formataLinha(sb, titulo, Long.toString(valor), formato);
	}

	private void formataLinha(StringBuffer sb, String titulo, String valor, String formato) {
		if (formato.equals("html")) {
			sb.append("<tr><td><b>");
			sb.append(Ferramentas.converteAcentosHtml(Ferramentas.converteEspeciaisHtml(titulo)));
			sb.append("</b></td><td>");
			sb.append(Ferramentas.converteAcentosHtml(Ferramentas.converteEspeciaisHtml(valor)));
			sb.append("</td>\n");
		} else if (formato.equals("xml")) {
			String rasTitulo = titulo.replaceAll(" ", "");
			sb.append("<");
			sb.append(rasTitulo);
			sb.append(">");
			if (valor.matches(".*[<>&].*")) {
				sb.append("<![CDATA[");
				sb.append(valor);
				sb.append("]]>");
			} else {
				sb.append(valor);
			}
			sb.append("</");
			sb.append(rasTitulo);
			sb.append(">\n");
		} else {
			sb.append(titulo);
			sb.append("=");
			sb.append(valor);
			sb.append("\n");
		}
	}

	private void geraPaginaDeErro(HttpServletResponse response, int retorno, PortabilidadeException e, String formato) throws ServletException, IOException {
		StringBuffer sb = new StringBuffer();
		if (formato.equals("html")) {
			response.setContentType("text/html;charset=UTF-8");
			sb.append("<html>\n");
			sb.append("<head>\n");
			sb.append("	<title>Erro</title>\n");
			sb.append("</head>\n");
			sb.append("<body>\n");
			sb.append("<H1>Mensagem</H1>\n");
			sb.append("<H2>");
			sb.append(e.getMessage());
			sb.append("</H2>\n");
			sb.append("</body>\n");
			sb.append("</html>\n");
		} else if (formato.equals("xml")) {
			response.setContentType("text/xml;charset=UTF-8");
			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			sb.append("<Portabilidade>\n");
			formataLinha(sb, "CodigoRetorno", e.getCodigoRetorno().toString(), formato);
			formataLinha(sb, "MensagemErro", e.getMessage(), formato);
			sb.append("</Portabilidade>\n");
		} else {
			response.setContentType("text/plain;charset=UTF-8");
			formataLinha(sb, "CodigoRetorno", e.getCodigoRetorno().toString(), formato);
			formataLinha(sb, "MensagemErro", e.getMessage(), formato);
		}
		PrintWriter out = response.getWriter();
		response.setStatus(retorno);
		try {
			out.print(sb.toString());
		} finally {
			out.close();
		}
	}

	private void geraPaginaDeErro(HttpServletResponse response, int retorno, PortabilidadeException e) throws ServletException, IOException {
		geraPaginaDeErro(response, retorno, e, "html");
	}
	
	private void ShowParameters_Carga() {
		String dirEntrada = "";
		String dirSaida = "";
		String dirIgnorados = "";
		
		try {
			dirEntrada = appConfig.getDiretorioEntradaDeCarga();
			dirSaida = appConfig.getDiretorioSaidaDeCarga();
			dirIgnorados = appConfig.getDiretorioIgnorados();
		} catch (Exception ex) {
			logger.debug("GetCarrierController exception : " + ex.toString());
		}
		logger.debug("======================================");
		logger.debug("                                      ");
		logger.debug("  PARAMETROS PARA CARGA DE ARQUIVOS   ");
		logger.debug("Diretorio de Entrada   : " + dirEntrada);
		logger.debug("Diretorio de Saida     : " + dirSaida);
		logger.debug("Diretorio de Ignorados : " + dirIgnorados);
		logger.debug("                                      ");
		logger.debug("======================================");
	}
	
}
