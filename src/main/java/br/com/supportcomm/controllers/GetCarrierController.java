package br.com.supportcomm.controllers;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.persistence.Cacheable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.supportcomm.GetCarrier;
import br.com.supportcomm.business.InfoMsisdn;
import br.com.supportcomm.business.MsisdnExt;
import br.com.supportcomm.business.ProcessFileAbr;
import br.com.supportcomm.business.ProcessFileAnatel;
import br.com.supportcomm.business.ProcessRequest;
import br.com.supportcomm.config.AppConfig;
import br.com.supportcomm.exception.CodigoRetornoEnum;
import br.com.supportcomm.exception.PortabilidadeException;
import br.com.supportcomm.log.LogExtension;
import br.com.supportcomm.util.Ferramentas;

//@EnableAutoConfiguration // DEPLOY TO TOMCAT
@RestController
public class GetCarrierController {

	private int statusCarga = 0;
	
	public static Logger logger = LoggerFactory.getLogger(GetCarrier.class);
	
	
	@Autowired
	ProcessRequest processRequest;
	
	
	@Autowired
	ProcessFileAbr processFileAbr;
	

	
	@Autowired
	ProcessFileAnatel processFileAnatel;
	
	@Autowired 
	private AppConfig appConfig;
	
	// DEBUG : 
	//	@RequestMapping("getcarrier")
	// 	@ResponseBody
	
	// DEPLOY TO TOMCAT :
	//	@RequestMapping(method = RequestMethod.GET)	
	//	@GetMapping(path="/getcarrier")	
	//	@ResponseBody	
	
	@RequestMapping("getcarrierJunit")
	@ResponseBody
	public void executeJunit(	@RequestParam(value="operacao", defaultValue="buscar") String operacao,
			@RequestParam(value="formato", defaultValue="") String formato,
			@RequestParam(value="interface", defaultValue="") String interf,
			@RequestParam(value="msisdn", defaultValue="") String msisdn,																		
			HttpServletRequest request ,
			HttpServletResponse response) {

		PrintWriter out = null;
		String strResponse = "";
		logger.info("------------------------------------------------------------");
		
		LogExtension.clearMsisdn();
		LogExtension.setMsisdn(msisdn);
		logger.info(String.format("/getcarrier?operacao=%s&formato=%s&msisdn=%s&interface=%s", 
		operacao, formato, msisdn,interf));
		
		
		if ( !checkRequest_Operacao(request, response ) )
			return;
		
		if ( operacao.equals("buscar")) {
			if ( !checkRequestParameters_Buscar(request , response))
				return;
		
			strResponse = buscar(msisdn,formato);
		
			if (formato.equals("html")) {
				response.setContentType("text/html;charset=UTF-8");
			} else if (formato.equals("xml")) {
				response.setContentType("text/xml;charset=UTF-8");
			} else {
				response.setContentType("text/plain;charset=UTF-8");			
			}
		
			try {
				out = response.getWriter();
			} catch (IOException e) {
				e.printStackTrace();
			}
			out.print(strResponse);
		
		} else if ( operacao.equals("carga")) {
			ShowParameters_Carga();
		
			carga(response);
		} else if ( operacao.equals("carga_anatel")) {
			ShowParameters_Carga();
		
			carga_anatel();
		}
		
		logger.info("------------------------------------------------------------");
		
	}
	
	@RequestMapping(method = RequestMethod.GET)	
	@GetMapping(path="/getcarrier")	
	@ResponseBody
	public void execute(	@RequestParam(value="operacao", defaultValue="buscar") String operacao,
									@RequestParam(value="formato", defaultValue="") String formato,
									@RequestParam(value="interface", defaultValue="") String interf,
									@RequestParam(value="msisdn", defaultValue="") String msisdn,																		
									HttpServletRequest request ,
									HttpServletResponse response) {
		
		PrintWriter out = null;
		String strResponse = "";
		logger.info("------------------------------------------------------------");
		
		LogExtension.clearMsisdn();
		LogExtension.setMsisdn(msisdn);
		logger.info(String.format("/getcarrier?operacao=%s&formato=%s&msisdn=%s&interface=%s", 
					operacao, formato, msisdn,interf));
		
		
		if ( !checkRequest_Operacao(request, response ) )
			return;
		
		if ( operacao.equals("buscar")) {
			
			if ( statusCarga == 1) {
				
				geraPaginaDeErro(response, HttpServletResponse.SC_SERVICE_UNAVAILABLE, new PortabilidadeException(CodigoRetornoEnum.FALHA, "A base de dados esta sendo carregada"));
				
				return;
			}
			
			if ( !checkRequestParameters_Buscar(request , response))
				return;

			strResponse = buscar(msisdn,formato);
			
			if (formato.equals("html")) {
				response.setContentType("text/html;charset=UTF-8");
			} else if (formato.equals("xml")) {
				response.setContentType("text/xml;charset=UTF-8");
			} else {
				response.setContentType("text/plain;charset=UTF-8");			
			}
			
			try {
				out = response.getWriter();
			} catch (IOException e) {
				e.printStackTrace();
			}
			out.print(strResponse);
		
		} else if ( operacao.equals("carga")) {
			ShowParameters_Carga();
			
			carga(response);
		} else if ( operacao.equals("carga_delta")) {
			ShowParameters_Carga();
			
			cargaDelta();
		} else if ( operacao.equals("carga_anatel")) {
			ShowParameters_Carga();
			
			carga_anatel();
		}
		
		logger.info("------------------------------------------------------------");
		
	}
	

	@RequestMapping("Controlador")
	@ResponseBody
	public void executeControlador(	@RequestParam(value="operacao", defaultValue="buscar") String operacao,
									@RequestParam(value="formato", defaultValue="") String formato,
									@RequestParam(value="interface", defaultValue="") String interf,
									@RequestParam(value="msisdn", defaultValue="") String msisdn,																		
									HttpServletRequest request ,
									HttpServletResponse response) {
		
		PrintWriter out = null;
		String strResponse = "";
		logger.info("------------------------------------------------------------");
		
		LogExtension.clearMsisdn();
		LogExtension.setMsisdn(msisdn);
		logger.info(String.format("/Controlador?operacao=%s&formato=%s&msisdn=%s&interface=%s", 
					operacao, formato, msisdn,interf));
		
		
		if ( !checkRequest_Operacao(request, response ) )
			return;
		
		if ( operacao.equals("buscar")) {
			if ( !checkRequestParameters_Buscar(request , response))
				return;

			strResponse = buscar(msisdn,formato);
			
			if (formato.equals("html")) {
				response.setContentType("text/html;charset=UTF-8");
			} else if (formato.equals("xml")) {
				response.setContentType("text/xml;charset=UTF-8");
			} else {
				response.setContentType("text/plain;charset=UTF-8");			
			}
			
			try {
				out = response.getWriter();
			} catch (IOException e) {
				e.printStackTrace();
			}
			out.print(strResponse);
		
		} else if ( operacao.equals("carga")) {
			ShowParameters_Carga();
			
			carga(response);
		} else if ( operacao.equals("carga_anatel")) {
			ShowParameters_Carga();
			
			carga_anatel();
		}
		
		logger.info("------------------------------------------------------------");
		
	}
	
	private String buscar( String msisdn, String formato ) {
		
		String strResponse;
		long tempo = System.currentTimeMillis();
		
		InfoMsisdn infoMsisdn = null;
		infoMsisdn = processRequest.execute(msisdn);
		if ( infoMsisdn == null )
			logger.info(String.format("NO OPERADORA FOUND for msidn %s",msisdn));
		
		tempo = System.currentTimeMillis() - tempo;
		strResponse = mountResponse(formato , tempo , infoMsisdn, msisdn );
		
		return strResponse;
	}
	
	private String mountResponse(String formato , long tempo, InfoMsisdn infoMsisdn, String msisdn) {
		StringBuffer sb = new StringBuffer();
		
		if (formato.equals("html")) {
			
			sb.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2//EN\">\n");
			sb.append("<html>\n");
			sb.append("<head>\n");
			sb.append("<title>Portabilidade</title>\n");
			sb.append("</head>\n");
			sb.append("<body>\n");
			sb.append("<table>\n");
			sb.append("<tbody>\n");
		} else if (formato.equals("xml")) {			
			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			sb.append("<Portabilidade>\n");
		} 
		if ( infoMsisdn != null ) {
			formataLinha(sb, "CodigoRetorno", CodigoRetornoEnum.OK.toString(), formato);
			formataLinha(sb, "MSISDN", infoMsisdn.getMsisdn(), formato);
			formataLinha(sb, "DDI", infoMsisdn.getDdi(), formato);
			formataLinha(sb, "DDD", infoMsisdn.getDdd(), formato);
			formataLinha(sb, "Telefone", infoMsisdn.getNumero(), formato);
			formataLinha(sb, "Pais", infoMsisdn.getNomeDoPais(), formato);
			formataLinha(sb, "Estado", infoMsisdn.getNomeDoEstado(), formato);
			formataLinha(sb, "SiglaDoEstado", infoMsisdn.getSiglaDoEstado(), formato);
			formataLinha(sb, "Operadora", infoMsisdn.getNomeDaOperadora(), formato);
			formataLinha(sb, "OperadoraNomeNormalizado", infoMsisdn.getNomeDaOperadoraNormalizado(), formato);
			formataLinha(sb, "CodigoDaOperadora", infoMsisdn.getCodigoDaOperadora(), formato);
			formataLinha(sb, "Precisao", infoMsisdn.getPrecisao(), formato);
			formataLinha(sb, "Portado", infoMsisdn.getPortado(), formato);
			formataLinha(sb, "TempoPesquisaMsec", tempo, formato);
		} else {			
			sb.append("<H1>Mensagem</H1>");
			sb.append("<H2>Nao foi encontrado operadora para o MSISDN ").append(msisdn).append("</H2>");
		}

		if (formato.equals("html")) {
			sb.append("</tbody>");
			sb.append("</table>");
			sb.append("</body>\n");
			sb.append("</html>\n");
		} else if (formato.equals("xml")) {
			sb.append("</Portabilidade>\n");
		}
		
		return sb.toString();
		
	}
	
	private boolean checkRequest_Operacao(HttpServletRequest request , HttpServletResponse response) {
		
		if ( request.getParameter("operacao")  == null) {
			
			geraPaginaDeErro(	response, HttpServletResponse.SC_BAD_REQUEST, 
								new PortabilidadeException(CodigoRetornoEnum.FALHA, "Operacao nao especificada."));
			
			return false;
		}
		
		
		
		if ( !(request.getParameter("operacao").equalsIgnoreCase("buscar")  ||				 
				 request.getParameter("operacao").equalsIgnoreCase("carga") ||
				 request.getParameter("operacao").equalsIgnoreCase("carga_delta") ||
				 request.getParameter("operacao").equalsIgnoreCase("carga_anatel") ||
				 request.getParameter("operacao").equalsIgnoreCase("mock")
				)
		) {
			
			geraPaginaDeErro(	response, HttpServletResponse.SC_BAD_REQUEST,
								new PortabilidadeException(CodigoRetornoEnum.FALHA, "Operacao " + request.getParameter("operacao") + " desconhecida."));
			
			return false;
		}
		
		return true;		
	}
	
	private boolean checkRequestParameters_Buscar(HttpServletRequest request , HttpServletResponse response) {		
		
		
		if (request.getParameter("msisdn")  == null) {
			
			geraPaginaDeErro(	response, HttpServletResponse.SC_BAD_REQUEST, 
								new PortabilidadeException(CodigoRetornoEnum.FALHA, "Msisdn nao especificado."));
			
			return false;
		}
		
		if (request.getParameter("formato")  == null) {
			
			geraPaginaDeErro(	response, HttpServletResponse.SC_BAD_REQUEST, 
								new PortabilidadeException(CodigoRetornoEnum.FALHA, "Formato nao especificado."));
			
			return false;
		}
		
		
		
		if ( !(request.getParameter("formato").equalsIgnoreCase("html") == true ||
			 request.getParameter("formato").equalsIgnoreCase("xml") == true ||
			 request.getParameter("formato").equalsIgnoreCase("text") == true
			)
		) {
			
			geraPaginaDeErro(	response, HttpServletResponse.SC_BAD_REQUEST, 
								new PortabilidadeException(CodigoRetornoEnum.FALHA, "Formato " + request.getParameter("formato") + " desconhecido."));
			
			return false;
		}
		
		return true;
	}
	
	private void geraPaginaDeErro(HttpServletResponse response, int retorno, PortabilidadeException e, String formato) {
		StringBuffer sb = new StringBuffer();
		if (formato.equals("html")) {
			response.setContentType("text/html;charset=UTF-8");
			sb.append("<html>\n");
			sb.append("<head>\n");
			sb.append("	<title>Erro</title>\n");
			sb.append("</head>\n");
			sb.append("<body>\n");
			sb.append("<H1>Mensagem</H1>\n");
			sb.append("<H2>");
			sb.append(e.getMessage());
			sb.append("</H2>\n");
			sb.append("</body>\n");
			sb.append("</html>\n");
		} else if (formato.equals("xml")) {
			response.setContentType("text/xml;charset=UTF-8");
			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			sb.append("<Portabilidade>\n");
			formataLinha(sb, "CodigoRetorno", e.getCodigoRetorno().toString(), formato);
			formataLinha(sb, "MensagemErro", e.getMessage(), formato);
			sb.append("</Portabilidade>\n");
		} else {
			response.setContentType("text/plain;charset=UTF-8");
			formataLinha(sb, "CodigoRetorno", e.getCodigoRetorno().toString(), formato);
			formataLinha(sb, "MensagemErro", e.getMessage(), formato);
		}
		PrintWriter out;
		try {
			out = response.getWriter();
		
			response.setStatus(retorno);
			
			out.print(sb.toString());
			out.close();
			 
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

	private void geraPaginaDeErro(HttpServletResponse response, int retorno, PortabilidadeException e)  {
		geraPaginaDeErro(response, retorno, e, "html");
	}
	
	private void formataLinha(StringBuffer sb, String titulo, long valor, String formato) {
		formataLinha(sb, titulo, Long.toString(valor), formato);
	}

	private void formataLinha(StringBuffer sb, String titulo, String valor, String formato) {
		if (formato.equals("html")) {
			sb.append("<tr><td><b>");
			sb.append(Ferramentas.converteAcentosHtml(Ferramentas.converteEspeciaisHtml(titulo)));
			sb.append("</b></td><td>");
			sb.append(Ferramentas.converteAcentosHtml(Ferramentas.converteEspeciaisHtml(valor)));
			sb.append("</td>\n");
		} else if (formato.equals("xml")) {
			String rasTitulo = titulo.replaceAll(" ", "");
			sb.append("<");
			sb.append(rasTitulo);
			sb.append(">");
			if (valor.matches(".*[<>&].*")) {
				sb.append("<![CDATA[");
				sb.append(valor);
				sb.append("]]>");
			} else {
				sb.append(valor);
			}
			sb.append("</");
			sb.append(rasTitulo);
			sb.append(">\n");
		} else {
			sb.append(titulo);
			sb.append("=");
			sb.append(valor);
			sb.append("\n");
		}
	}
	
	private void ShowParameters_Carga() {
		String dirEntrada = "";
		String dirSaida = "";
		String dirIgnorados = "";
		
		try {
			dirEntrada = appConfig.getDiretorioEntradaDeCarga();
			dirSaida = appConfig.getDiretorioSaidaDeCarga();
			dirIgnorados = appConfig.getDiretorioIgnorados();
		} catch (Exception ex) {
			logger.debug("GetCarrierController exception : " + ex.toString());
		}
		logger.debug("======================================");
		logger.debug("                                      ");
		logger.debug("  PARAMETROS PARA CARGA DE ARQUIVOS   ");
		logger.debug("Diretorio de Entrada   : " + dirEntrada);
		logger.debug("Diretorio de Saida     : " + dirSaida);
		logger.debug("Diretorio de Ignorados : " + dirIgnorados);
		logger.debug("                                      ");
		logger.debug("======================================");
	}
	
	
	private void carga(HttpServletResponse response) {
		logger.debug("======================================");
		logger.info("STARTING LOAD FROM FILES...");
		
		if ( statusCarga == 1) {
			
			geraPaginaDeErro(response, HttpServletResponse.SC_SERVICE_UNAVAILABLE, new PortabilidadeException(CodigoRetornoEnum.FALHA, "Já há uma carga ABR FULL em andamento."));
			
			return;
		}
		
		statusCarga = 1;
		

		try {
			String diretorioEntradaDeCarga = appConfig.getDiretorioEntradaDeCarga();
			String diretorioSaidaDeCarga = appConfig.getDiretorioSaidaDeCarga();
			String diretorioIgnorados = appConfig.getDiretorioIgnorados();
			String diretorioRejeitados = appConfig.getDiretorioRejeitados();
			
			Path pathEntrada = Paths.get(diretorioEntradaDeCarga);
			Path pathSaida = Paths.get(diretorioSaidaDeCarga);
			Path pathIgnorados = Paths.get(diretorioIgnorados);
			Path pathRejeitados = Paths.get(diretorioRejeitados);
			
			
			File dirIn = pathEntrada.toFile();
			File dirOut = pathSaida.toFile();
			File dirIgnorados = pathIgnorados.toFile();
			File dirRejeitados = pathRejeitados.toFile();
			
			
			if (!dirIn.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioEntradaDeCarga);
				statusCarga = 0;
				return;
			}
			
			if (!dirOut.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioSaidaDeCarga);
				statusCarga = 0;
				return;
			}
			
			if (!dirIgnorados.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioIgnorados);
				statusCarga = 0;
				return;
			}
			
			if (!dirRejeitados.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioRejeitados);
				statusCarga = 0;
				return;
			}		
			
			processFileAbr.process(dirIn, dirOut, dirIgnorados, dirRejeitados);		
			
			

			logger.info("LOAD FROM FILES EXECUTED.");
			logger.debug("======================================");
		}
		catch(Exception ex  ) {
			logger.debug("Exception : " + ex.toString());
			statusCarga = 0;
		}
		
		statusCarga = 0;

	}
	
	private void cargaDelta() {
		logger.debug("======================================");
		logger.info("STARTING LOAD FROM FILES...");
		

		try {
			String diretorioEntradaDeCarga = appConfig.getDiretorioEntradaDeCarga();
			String diretorioSaidaDeCarga = appConfig.getDiretorioSaidaDeCarga();
			String diretorioIgnorados = appConfig.getDiretorioIgnorados();
			String diretorioRejeitados = appConfig.getDiretorioRejeitados();
			
			Path pathEntrada = Paths.get(diretorioEntradaDeCarga);
			Path pathSaida = Paths.get(diretorioSaidaDeCarga);
			Path pathIgnorados = Paths.get(diretorioIgnorados);
			Path pathRejeitados = Paths.get(diretorioRejeitados);
			
			
			File dirIn = pathEntrada.toFile();
			File dirOut = pathSaida.toFile();
			File dirIgnorados = pathIgnorados.toFile();
			File dirRejeitados = pathRejeitados.toFile();
			
			
			if (!dirIn.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioEntradaDeCarga);
				return;
			}
			
			if (!dirOut.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioSaidaDeCarga);
				return;
			}
			
			if (!dirIgnorados.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioIgnorados);
				return;
			}
			
			if (!dirRejeitados.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioRejeitados);
				return;
			}		
			
			processFileAbr.processDelta(dirIn, dirOut, dirIgnorados, dirRejeitados);		
			
			

			logger.info("LOAD FROM FILES EXECUTED.");
			logger.debug("======================================");
		}
		catch(Exception ex  ) {
			logger.debug("Exception : " + ex.toString());
		}

	}
	
	private void carga_anatel() {
		logger.debug("======================================");
		logger.info("STARTING LOAD FROM FILES...");
		

		try {
			String diretorioEntradaDeCarga = appConfig.getDiretorioEntradaDeCarga();
			String diretorioSaidaDeCarga = appConfig.getDiretorioSaidaDeCarga();
			String diretorioIgnorados = appConfig.getDiretorioIgnorados();
			String diretorioRejeitados = appConfig.getDiretorioRejeitados();
			
			Path pathEntrada = Paths.get(diretorioEntradaDeCarga);
			Path pathSaida = Paths.get(diretorioSaidaDeCarga);
			Path pathIgnorados = Paths.get(diretorioIgnorados);
			Path pathRejeitados = Paths.get(diretorioRejeitados);
			
			
			File dirIn = pathEntrada.toFile();
			File dirOut = pathSaida.toFile();
			File dirIgnorados = pathIgnorados.toFile();
			File dirRejeitados = pathRejeitados.toFile();
			
			
			if (!dirIn.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioEntradaDeCarga);
				return;
			}
			
			if (!dirOut.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioSaidaDeCarga);
				return;
			}
			
			if (!dirIgnorados.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioIgnorados);
				return;
			}
			
			if (!dirRejeitados.isDirectory()) {
				logger.debug("Nao foi encontrado o diretorio " + diretorioRejeitados);
				return;
			}		
			
				
			processFileAnatel.process(dirIn, dirOut);
			

			logger.info("LOAD FROM FILES EXECUTED.");
			logger.debug("======================================");
		}
		catch(Exception ex  ) {
			logger.info("Exception : " + ex.toString());
		}

	}
	
}
