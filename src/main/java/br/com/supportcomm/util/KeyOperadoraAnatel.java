package br.com.supportcomm.util;


import br.com.supportcomm.models.db.Pais;

public class KeyOperadoraAnatel {
	
	public KeyOperadoraAnatel() {
		
	}
	
	public KeyOperadoraAnatel(Pais pais, String cnpj) {
		this.pais = pais;
		this.cnpj = cnpj;
	}
	
	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}
	

	@Override
	public String toString() {
		return "KeyOperadoraAnatel [cnpj=" + cnpj + ", pais=" + pais + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
		result = prime * result + ((pais == null) ? 0 : pais.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KeyOperadoraAnatel other = (KeyOperadoraAnatel) obj;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (pais == null) {
			if (other.pais != null)
				return false;
		} else if (!pais.equals(other.pais))
			return false;
		return true;
	}


	String cnpj;

	Pais pais;

}
