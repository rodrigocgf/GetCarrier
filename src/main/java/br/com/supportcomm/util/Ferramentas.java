package br.com.supportcomm.util;

import java.util.Enumeration;
import java.util.Hashtable;

public class Ferramentas {
	private static Hashtable<String, String> acentos = null;
	private static Hashtable<String, String> especiais = null;

	private Ferramentas() {
	}

	public static String converteAcentosHtml(StringBuffer in) {
		return (converteAcentosHtml(in.toString()));
	}

	public static synchronized String converteAcentosHtml(String in) {
		Enumeration<String> keys;
		String ras;
		String acento;
		String acentoHtml;

		if (acentos == null) {
			acentos = new Hashtable<String, String>();

			acentos.put("Ã�", "&Aacute;");
			// acentos.put("Ãˆ", "&Egrave;");
			acentos.put("Ã´", "&ocirc;");
			acentos.put("Ã‡", "&Ccedil;");
			acentos.put("Ã¡", "&aacute;");
			// acentos.put("Ã¨", "&egrave;");
			// acentos.put("Ã’", "&Ograve;");
			acentos.put("Ã§", "&ccedil;");
			acentos.put("Ã‚", "&Acirc;");
			acentos.put("Ã‹", "&Euml;");
			// acentos.put("Ã²", "&ograve;");
			acentos.put("Ã¢", "&acirc;");
			acentos.put("Ã«", "&euml;");
			// acentos.put("Ã˜", "&Oslash;");
			acentos.put("Ã‘", "&Ntilde;");
			acentos.put("Ã€", "&Agrave;");
			// acentos.put("Ã�", "&ETH;");
			// acentos.put("Ã¸", "&oslash;");
			acentos.put("Ã±", "&ntilde;");
			acentos.put("Ã ", "&agrave;");
			// acentos.put("Ã°", "&eth;");
			acentos.put("Ã•", "&Otilde;");
			// acentos.put("Ã…", "&Aring;");
			acentos.put("Ãµ", "&otilde;");
			// acentos.put("Ã�", "&Yacute;");
			// acentos.put("Ã¥", "&aring;");
			acentos.put("Ã�", "&Iacute;");
			acentos.put("Ã–", "&Ouml;");
			// acentos.put("Ã½", "&yacute;");
			acentos.put("Ãƒ", "&Atilde;");
			acentos.put("Ã­", "&iacute;");
			acentos.put("Ã¶", "&ouml;");
			acentos.put("Ã£", "&atilde;");
			acentos.put("ÃŽ", "&Icirc;");
			// acentos.put("Ã„", "&Auml;");
			acentos.put("Ã®", "&icirc;");
			acentos.put("Ãš", "&Uacute;");
			acentos.put("Ã¤", "&auml;");
			acentos.put("ÃŒ", "&Igrave;");
			acentos.put("Ãº", "&uacute;");
			// acentos.put("Ã†", "&AElig;");
			acentos.put("Ã¬", "&igrave;");
			acentos.put("Ã›", "&Ucirc;");
			// acentos.put("Ã¦", "&aelig;");
			acentos.put("Ã�", "&Iuml;");
			acentos.put("Ã»", "&ucirc;");
			acentos.put("Ã¯", "&iuml;");
			acentos.put("Ã™", "&Ugrave;");
			acentos.put("Â®", "&reg;");
			acentos.put("Ã‰", "&Eacute;");
			// acentos.put("Ã¹", "&ugrave;");
			// acentos.put("Â©", "&copy;");
			acentos.put("Ã©", "&eacute;");
			acentos.put("Ã“", "&Oacute;");
			acentos.put("Ãœ", "&Uuml;");
			// acentos.put("Ãž", "&THORN;");
			acentos.put("ÃŠ", "&Ecirc;");
			acentos.put("Ã³", "&oacute;");
			acentos.put("Ã¼", "&uuml;");
			// acentos.put("Ã¾", "&thorn;");
			acentos.put("Ãª", "&ecirc;");
			acentos.put("Ã”", "&Ocirc;");
			// acentos.put("ÃŸ", "&szlig;");
		}

		keys = acentos.keys();
		ras = in;

		while (keys.hasMoreElements()) {
			acento = keys.nextElement();
			acentoHtml = acentos.get(acento);
			ras = ras.replaceAll(acento, acentoHtml);
		}

		return (ras);
	}

	public static String converteEspeciaisHtml(StringBuffer in) {
		return (converteEspeciaisHtml(in.toString()));
	}

	public static synchronized String converteEspeciaisHtml(String in) {
		Enumeration<String> keys;
		String ras;
		String especial;
		String especialHtml;

		if (especiais == null) {
			especiais = new Hashtable<String, String>();

			especiais.put("\"", "&quot;");
			especiais.put("<", "&lt;");
			especiais.put(">", "&gt;");
			especiais.put("&", "&amp;");
		}

		keys = especiais.keys();
		ras = in;

		while (keys.hasMoreElements()) {
			especial = keys.nextElement();
			especialHtml = especiais.get(especial);
			ras = ras.replaceAll(especial, especialHtml);
		}

		return (ras);
	}
}

