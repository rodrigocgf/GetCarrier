package br.com.supportcomm.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.supportcomm.business.MsisdnExt;
//import br.com.supportcomm.crudrepositories.PaisRepositoryFormer;
import br.com.supportcomm.exception.PortabilidadeException;
import br.com.supportcomm.jparepositories.AreaRepoImpl;
import br.com.supportcomm.jparepositories.HistoricoRepoImpl;
import br.com.supportcomm.jparepositories.NumeroPortadoRepoImpl;
import br.com.supportcomm.jparepositories.OperadoraRepoImpl;
import br.com.supportcomm.jparepositories.PaisRepoImpl;
import br.com.supportcomm.jparepositories.SerieRepoImpl;
import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Historico;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;
import br.com.supportcomm.models.db.TelefoniaEnum;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
@Deprecated
public class ArquivoAbr {
	private static final Logger logger = LoggerFactory.getLogger(ArquivoAbr.class);
	
	private File dirIn;
	private File dirOut;
	private File dirIgnorados;
	private File dirRejeitados;
	private File fileRejeitados;
	private BufferedWriter bufWout;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	
	
	@Autowired
	private HistoricoRepoImpl historicoRepoImpl;
	
	@Autowired
	private PaisRepoImpl paisRepoImpl;
	
	
	@Autowired
	private SerieRepoImpl serieRepoImpl;
	
	@Autowired
	private OperadoraRepoImpl operadoraRepoImpl;
	
	@Autowired
	private AreaRepoImpl areaRepoImpl;
	
	@Autowired
	private NumeroPortadoRepoImpl numeroPortadoRepoImpl;
	
	@Autowired
	private FiltroDeArquivosAbrFull filtroAbrFull;
	
	@Autowired
	private FiltroDeArquivosAbrDelta filtroAbrDelta;
	
	@Autowired
	private ProcessAbrLineBlock vLine;
	
	@Autowired
	MsisdnExt msisdnExt;
	
	private MessageDigest digest;
	private String[] firstLine;

	public void processa( File dirIn, File dirOut, File dirIgnorados, File dirRejeitados)  {
		logger.info("======== PROCESSAMENTO ABR ========");
		
		this.dirIn = dirIn;
		this.dirOut = dirOut;
		this.dirIgnorados = dirIgnorados;
		this.dirRejeitados = dirRejeitados;
		
		fileRejeitados = new File( this.dirRejeitados, "LinhasRejeitadas.txt");
		
		
		try {
			if(!fileRejeitados.exists()){
				fileRejeitados.createNewFile();
	    	}
			bufWout = new BufferedWriter(new FileWriter(fileRejeitados,true));

			processaLoteAbr(filtroAbrFull,true);
						
			processaLoteAbr(filtroAbrDelta,false);
			
			bufWout.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private void processaLoteAbr(FileFilter filtro, boolean full)  {
		
		if ( filtro instanceof  FiltroDeArquivosAbrFull) {
			logger.info("  PROCESSA ARQUIVO ABR FULL ");
		} else if( filtro instanceof  FiltroDeArquivosAbrDelta) { 
			logger.info("  PROCESSA ARQUIVO ABR DELTA ");
		}
		
		File[] lista;
		String[] nomes;
		
		lista = dirIn.listFiles(filtro);
		boolean execute = true;
		if (lista == null || lista.length == 0) {
			return;
		}
		nomes = new String[lista.length];
		for (int ctr = 0; ctr < lista.length; ctr++) {
			nomes[ctr] = lista[ctr].getName();
		}
		
		Arrays.sort(nomes);
		
		for (int ctr = 0; ctr < nomes.length; ctr++) {
			try {				
				logger.info("processaArquivoAbr(" + nomes[ctr] + ")");				
				execute = processaArquivoAbr(nomes[ctr], true);
				if(execute) {
					processaArquivoAbr(nomes[ctr], false);
				}
			} catch (Exception ex) {
				logger.error(ex.toString());
				break;
			} 
		}
	}

	@SuppressWarnings("unchecked")
	private boolean processaArquivoAbr( String arquivoStr, boolean check) throws Exception {
		logger.debug("[processaArquivoAbr]");
		try {
			digest = MessageDigest.getInstance("MD5");
			
			if (arquivoStr.toLowerCase().startsWith("cpl")) {
				digest.reset();
				return cargaFullAbr(arquivoStr, check);
			} else {
				List<Historico> lista;
				lista = historicoRepoImpl.RetornoOrdernado();
				
				if (lista == null || lista.size() == 0) {
					logger.info("Foi tentado carregar uma arquivo delta sem carga full anterior.");
					return false;
				}
				Historico hist = (Historico) lista.get(0);
				digest.reset();
				return cargaDeltaAbr(arquivoStr, check, hist);
			}
		} catch (NoSuchAlgorithmException e) {
			logger.error("ALGORITHM MD5 NOT FOUNDED", e);
		} catch (FileNotFoundException e) {
			logger.error("FILE [" + arquivoStr + "] NOT FOUND!", e);
		} catch (IOException e) {
			logger.error("ERROR ON FILE MANIPULATION", e);
		}
		return true;
	}

	private boolean cargaFullAbr(String arquivoStr, boolean check) throws PortabilidadeException, IOException {
		logger.info("STARTING FULL | FILE: " + arquivoStr);
		
		boolean result = true;
		BufferedReader arquivo = new BufferedReader(new FileReader(new File(dirIn, arquivoStr)));
		
		if (!check) {
			logger.warn("CHANGING THE STATUS OF ALL NUMBERS");
			msisdnExt.setAllMsisdnForUpdate();
			logger.warn("UPDATE COMPLETE");
		}
		
		result = processFileAbrFull(arquivoStr, check, arquivo);
		arquivo.close();
		
		if(!check) {
			try {
				logger.info("INSERT INTO HISTORICO");
				
				historicoRepoImpl.update(	new Timestamp(sdf.parse(firstLine[0]).getTime()), 
											new Timestamp(new Date().getTime()),
											1,
											Integer.parseInt(firstLine[1]), 
											arquivoStr,
											1);				
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	private boolean processFileAbrFull( String arquivoStr, boolean check, BufferedReader arquivo) {
		boolean resultChecksum = true;
		Pais pais;
		int numeroLinhas = 0;
		List<String> lineList = new ArrayList<String>();
		AtomicInteger counter = new AtomicInteger(0);
		String checksum = "";
		int errors = 0;
		HashMap<String, Serie> seriesList = null;
		HashMap<String, Area> areaList = null;
		HashMap<Short, Operadora> opList = null;
		
		try {
			pais = paisRepoImpl.procuraPor_Nome("BRASIL").get(0);
			
			firstLine = arquivo.readLine().split(";");
			numeroLinhas = Integer.parseInt(firstLine[1]);
			logger.info("PROCESSING FILE" + arquivoStr + " WITH[" + firstLine[1] + "] MODE[FULL] | TEST[" + check + "]");			
			
			
			if(!check) {
				logger.info("LOADING SERIES ...");			
				seriesList = getListSeries();
				
				logger.info("LOADING AREAS ...");
				areaList = getListArea();
				
				logger.info("LOADING OPERADORAS ...");
				opList = getOperadoraList(pais);				
				
			}
			
			if(seriesList != null || check) {
				String line;
				int i=1;
				
				while((line = arquivo.readLine() )!=null) {
					
					if(i == (numeroLinhas - 1)) {
						
						checksum = line;
						
						vLine.processLine(counter, lineList, pais, seriesList, areaList, opList, check);
						counter.incrementAndGet();
						lineList = new ArrayList<String>();
						logger.info(">>>>>>>>>>>>>>>> Agendamentos concluidos");
					} else {
						 lineList.add(line);
					}
					
					if(numeroLinhas != (i-1)) {
						line += "\n";
						digest.update((line).getBytes());
					}
					if(lineList.size() == 1000) {
						vLine.processLine(counter, lineList, pais, seriesList, areaList, opList, check);
						counter.incrementAndGet();
						lineList = new ArrayList<String>();
						logger.info(">>>>>>>>>>>>>>>> 1000 agendados");
					}
					
					errors = 0;
					
					if (counter.intValue() > 9) {
		                while(counter.intValue() > 8 && errors < 1000) {
		                	try {
		                		Thread.sleep(500);
		                	} catch (InterruptedException e) {
								errors++;
		            			logger.error("THREAD SLEEP ERROR: " + e.getMessage());
		            		}
		                }
		                if (counter.intValue() > 8) {
		                	logger.error("FINAL THREAD SLEEP ERROR");
		                	break;
		                }
		                logger.info(">>>>>>>>>>>>>>>> Continuando agendamento");
		            }
					i++;
				}
				
			
				
				while(counter.intValue() > 0 && errors < 1000) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						errors++;
						logger.error("THREAD SLEEP ERROR" + e.getMessage());
					}
	            }
				
				logger.info("TERMINATED");
				if(check) {
					logger.info("EXECUTING CHECKSUM");
					resultChecksum = executeChecksum(checksum, arquivoStr);
				} else {
					logger.info("REMOVING NOT UPDATED");
					removeNotUpdated();
					logger.info("MOVING FILE [" + arquivoStr + "]");
					new File(dirIn, arquivoStr).renameTo(new File(dirOut, arquivoStr));
				}
			} else {
				logger.error("NO SERIE FOUND");
			}
		} catch (IOException e) {
			logger.error("ERROR ON READ LINE FROM FILE [" + arquivoStr + "]", e);
			return false;
		} 
		return resultChecksum;
	}
	
	private HashMap<String, Serie> getListSeries() {
		HashMap<String, Serie> serieMap = new HashMap<String, Serie>();
	
		List<Serie> series = serieRepoImpl.selectAll();

		for ( Serie serie : series) {
			serieMap.put(serie.getSerieid().toString(), serie);
		}		
		 
		return serieMap;
	}
	
	private HashMap<Short, Operadora> getOperadoraList(Pais pais) {
		HashMap<Short, Operadora> result = new HashMap<Short, Operadora>();
		
		
		List<Operadora> operadoras = operadoraRepoImpl.procuraPor_Pais(pais.getPaisId());
		for ( Operadora operadora : operadoras ) {
			result.put(operadora.getSpid(), operadora);
		}
		
		return result;
	}

	private HashMap<String, Area> getListArea() {
		HashMap<String, Area> areaMap = new HashMap<String, Area>();
		
		List<Area> areas = areaRepoImpl.selectAll();
		
		for ( Area area : areas) {
			areaMap.put(String.valueOf(area.getDdd()), area);
		}
		
		return areaMap;
	}
	
	private void removeNotUpdated() {
		numeroPortadoRepoImpl.delete_StatusUpdate_1();
	}

	private boolean executeChecksum(String checksum, String arquivoStr) {
		
		byte[] md5sum;
		md5sum = digest.digest();
		BigInteger bigInt = new BigInteger(1, md5sum);
		String output = bigInt.toString(16);
		while (output.length() < 32) {
			output = "0" + output;
		}
		if (!output.equals(checksum)) {
			logger.info("WRONG CHECKSUM ON FILE[" + arquivoStr + "] READ[" + checksum + "] CALCULATED[" + output + "]");
			return true;
		} else {
			logger.info("CHECKSUM OK ON FILE[" + arquivoStr + "] STARTING ATUALIZATION");
			return true;
		}
	}
	
		
	@SuppressWarnings({"resource", "unchecked" })
	private boolean cargaDeltaAbr(String arquivoStr, boolean check, Historico hist) throws Exception {		
		int numeroDeLinhas = 0;
		String checksum = "";
		int tipoDeLinha;
		short spid;
		Pais brasil;
		String linha;
		Date dataDaGeracao = new Date();
		
		long numeroDoBilhete;
		int acao;
		String tn;
		
		HashMap<String, Serie> seriesList = new HashMap<String, Serie>();
				
		logger.info("STARTING DELTA | FILE: " + arquivoStr);		

		try {
			brasil = paisRepoImpl.procuraPor_Nome("BRASIL").get(0);
		} catch (NoResultException nre) {
			throw new NoResultException("Nao foi encontrado o BRASIL");
		}

		List<Serie> series;
		try {
			series = serieRepoImpl.buscaPorPais(brasil.getPaisId());
		} catch (Exception nre) {
			throw new Exception("Nao foi encontrado o BRASIL");
		}

		for (Serie aSerie : series) {
			String key = aSerie.getEspecial() + ":" + aSerie.getSerie();
			seriesList.put(key, aSerie);
		}
		
		BufferedReader arquivo = new BufferedReader(new FileReader(new File(dirIn, arquivoStr)));	
		
		try {
			long tempo1 = System.currentTimeMillis();
			long tempo2;

			
			for (int numeroDaLinha = 1; (linha = arquivo.readLine()) != null; numeroDaLinha++) {
				
				String[] campos = linha.split(";");
				
				if (check) {
					logger.info("Lido " + campos.length + " campos de |" + linha + "|");
				}
				
				
				
				if (numeroDaLinha == 1) {
					
					linha += "\n";
					digest.update((linha).getBytes());
					
					if (campos.length != 2) {
						throw new ServletException("A primeira linha esta fora de formato em " + arquivoStr + ", numero invalido de campos");
					}
					try {
						dataDaGeracao = sdf.parse(campos[0]);
					} catch (ParseException ex) {
						throw new ServletException("A primeira linha esta fora de formato em " + arquivoStr + ", data fora de formato");
					}
					
					try {
						numeroDeLinhas = Integer.parseInt(campos[1]);
					} catch (NumberFormatException nfe) {
						throw new ServletException("A primeira linha esta fora de formato em " + arquivoStr + ", numero de linhas fora de formato");
					}
					
					if (numeroDaLinha < 0) {
						throw new ServletException("A primeira linha esta fora de formato em " + arquivoStr + ", numero de linhas negativo");
					}					
					
					logger.info("----------------------------------------------------------------------------------------------------------------");
					logger.info("Processando " + arquivoStr + " com " + numeroDeLinhas + " (modo=Delta), teste=" + check + ", gerado em " + dataDaGeracao.toString());
					logger.info("----------------------------------------------------------------------------------------------------------------");
					
					if (check == false && dataDaGeracao.getTime() <= hist.getDataDeReferencia().getTime()) {
						
						logger.info("Ignorando o antigo " + arquivoStr + " DatadaGeracao: " + dataDaGeracao.getTime() + " DataDeReferencia..: " + hist.getDataDeReferencia().toString());
						logger.info("Movendo o arquivo " + arquivoStr + " para o diretorio de arquivos IGNORADOS...");
						
						if (arquivo != null) {
							arquivo.close();
						}
						new File(dirIn, arquivoStr).renameTo(new File(dirIgnorados, arquivoStr));
						logger.info("Arquivo " + arquivoStr + " movido.");
						return false;
					}
					continue;
				}
				
				
				
				if (numeroDaLinha % 200 == 0) {
					tempo2 = System.currentTimeMillis();
					logger.info("Analisando a linha " + numeroDaLinha + " de " + numeroDeLinhas + " em " + (tempo2 - tempo1) + " mS");
					tempo1 = tempo2;
				}
				
				if (numeroDaLinha < numeroDeLinhas) {
					
					numeroDoBilhete = Long.parseLong(campos[0]);
					acao = Integer.parseInt(campos[1]);
					tn = campos[2];
					
					campos = linha.split(";");
					linha += "\n";
					
					digest.update((linha).getBytes());
					if (campos.length < 3) {
						logger.warn("A linha numero " + numeroDaLinha + " esta fora de formato em " + arquivoStr + ", linha muito curta");
						continue;
					}
					
					if (acao != 0 && acao != 1) {
						logger.warn("INVALID ACTION SKIP LINE [" + numeroDaLinha + "]");
						continue;
					}
					
					if (tn.length() >= 12) {
						logger.warn("TN OUT OF FORMAT [" + tn + "]");
						continue;
					}
					
					if (acao == 0) {
						if (campos.length < 6) {
							logger.warn("SKIP LINE [" + numeroDaLinha + " OUT OF FORMAT");
							continue;
						}
						try {
							tipoDeLinha = Integer.parseInt(campos[3]);
						} catch (NumberFormatException nfe) {
							tipoDeLinha = 0;
						}
						
						if (tipoDeLinha != 0 && tipoDeLinha != 2) {
							logger.warn("SKIP CNG [" + numeroDaLinha + "]");
							continue;
						}
						try {
							spid = Short.parseShort(campos[5]);
						} catch (NumberFormatException nfe) {
							spid = 0;
						}
						if (spid < 0) {
							logger.warn("A linha numero " + numeroDaLinha + " esta fora de formato em " + arquivoStr + ", tipo SPID invalido");
							continue;
						}
					} else {
						tipoDeLinha = 0;
						spid = 0;
					}
					
					if (!check) {						
						
						if (acao == 0) {
							try {
								msisdnExt.setMsisdn(55 + tn);
								msisdnExt.start();
								
								msisdnExt.Execute("55" + tn, spid, numeroDoBilhete, seriesList);
								
							} catch (Exception ex) {
								logger.debug("Ignorando o MSISDN: " + tn);
								gravaArquivoRejeitado(linha);
							}
						} else {
							try {
								msisdnExt.apagarMsisdn("55" + tn);
							} catch (Exception e) {
								//logger.warn("Apagar MSISDN => Ignorando o MSISDN: " + tn);
							}
						}						
						
					} 
				} else {
					if (numeroDaLinha == numeroDeLinhas) {
						checksum = linha;
					}
				}
			} // for {
			
			if (!check) {				
				historicoRepoImpl.update(	new Timestamp(new Date().getTime()),
											new Timestamp(dataDaGeracao.getTime()),
											0,
											new Integer(numeroDeLinhas - 2), 
											arquivoStr,
											1);
				
			}
		} catch (ServletException se) {
			throw se;
		} catch (IOException ex) {
			throw new ServletException("Erro durante leitura do arquivo " + arquivoStr + ", " + ex.getMessage(), ex);
		} finally {
			try {				
				if (arquivo != null) {
					arquivo.close();
				}
			} catch (Exception e1) {
			}
		}
		
		if (check) {
			return executeChecksum(checksum, arquivoStr);
		} else {
			logger.info("Movendo o arquivo " + arquivoStr + " para o diretorio de saida.");
			new File(dirIn, arquivoStr).renameTo(new File(dirOut, arquivoStr));
			return true;
		}
	}

	private void gravaArquivoRejeitado(String linha) {
		
		try {						
			bufWout.write(linha);
		} catch (IOException ex) {
			logger.info("Erro gravando arquivo de rejeitado: " + linha);
		}
	}
}
