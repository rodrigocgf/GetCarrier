package br.com.supportcomm.util;

import br.com.supportcomm.models.db.Operadora;

public class ValueOperadoraAnatel {
	
	public ValueOperadoraAnatel() {
		
	}
	
	public ValueOperadoraAnatel(String nomeOperadora , String cnpj, String nomeOperadoraNormalizado , String ddd) {
		this.nomeOperadora = nomeOperadora;
		this.cnpj = cnpj;
		this.nomeOperadoraNormalizado = nomeOperadoraNormalizado;
		this.ddd  = ddd;
	}

	public String getNomeOperadora() {
		return nomeOperadora;
	}

	public void setNomeOperadora(String nomeOperadora) {
		this.nomeOperadora = nomeOperadora;
	}

	public String getNomeOperadoraNormalizado() {
		return nomeOperadoraNormalizado;
	}

	public void setNomeOperadoraNormalizado(String nomeOperadoraNormalizado) {
		this.nomeOperadoraNormalizado = nomeOperadoraNormalizado;
	}
	
	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}
	
	public Operadora getOperadora() {
		return operadora;
	}

	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}


	String nomeOperadora;
	
	String nomeOperadoraNormalizado;
	
	String cnpj;


	String ddd;
	
	Operadora operadora;

	
	
}
