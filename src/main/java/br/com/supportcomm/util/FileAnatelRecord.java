package br.com.supportcomm.util;

import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.TelefoniaEnum;

public class FileAnatelRecord {
	
	public Operadora getOperadora() {
		return operadora;
	}

	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}
	
	public String getStrOperadora() {
		return strOperadora;
	}

	public void setStrOperadora(String strOperadora) {
		this.strOperadora = strOperadora;
	}
	
	public String getStrOperadoraNormalizada() {
		return strOperadoraNormalizada;
	}

	public void setStrOperadoraNormalizada(String strOperadoraNormalizada) {
		this.strOperadoraNormalizada = strOperadoraNormalizada;
	}
	
	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public int getPrefixo() {
		return prefixo;
	}

	public void setPrefixo(int prefixo) {
		this.prefixo = prefixo;
	}

	public int getPrefixo_start() {
		return prefixo_start;
	}

	public void setPrefixo_start(int prefixo_start) {
		this.prefixo_start = prefixo_start;
	}

	public int getPrefixo_end() {
		return prefixo_end;
	}

	public void setPrefixo_end(int prefixo_end) {
		this.prefixo_end = prefixo_end;
	}
	
	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	public TelefoniaEnum getTelefonia() {
		return telefonia;
	}

	public void setTelefonia(TelefoniaEnum telefonia) {
		this.telefonia = telefonia;
	}

	Operadora operadora;
	
	String strOperadora;
	
	String strOperadoraNormalizada;

	String ddd;

	Area area;
	
	String cnpj;

	int prefixo;
	
	int prefixo_start;
	
	int prefixo_end;
	
	TelefoniaEnum telefonia;

	
}
