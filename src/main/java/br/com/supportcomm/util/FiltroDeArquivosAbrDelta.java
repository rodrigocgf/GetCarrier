package br.com.supportcomm.util;

import java.io.File;
import java.io.FileFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
//import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Component
public class FiltroDeArquivosAbrDelta implements FileFilter
{
	private static final Logger logger = LoggerFactory.getLogger (FiltroDeArquivosAbrDelta.class);

	@Override
	public boolean accept (File pathname)
	{
		if (! pathname.isFile ())
		{
			return (false);
		}

		String nome;

		nome = pathname.getName ().toLowerCase ();

		if (nome.startsWith ("bdt") && nome.endsWith (".txt"))
		{
			if (! pathname.canRead ())
			{
				logger.warn ("Saltando " + nome + " por falta de permissao");
				return (false);
			}
			
			logger.info ("Processando " + nome + " como Arquivo ABR DELTA.");
			
			return (true);
		}

		
		return (false);
	}
}

