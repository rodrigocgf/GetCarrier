package br.com.supportcomm.util;


/*
 * 
 * Março de 2020 : O ARQUIVO ANATEL E OBTIDO ATRAVES DO SITE : 
 * 
 *       1 - https://sistemas.anatel.gov.br/sapn/
 *       2 - ARQUIVOS
 *       3 - ARQUIVOS SMP
 *       4 - ARQUIVO SMP GERAL
 *       
 */


import java.io.File;
import java.io.FileFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FiltroDeArquivosAnatel implements FileFilter{

  private static final Logger logger = LoggerFactory.getLogger (FiltroDeArquivosAnatel.class);

	@Override
	public boolean accept (File pathname)
	{
		if (! pathname.isFile ())
		{
			return (false);
		}

		String nome;

		nome = pathname.getName ().toUpperCase();

		if ( nome.startsWith ("FAIXA_SMP_") && nome.endsWith (".TXT") )
		//if ( nome.startsWith ("FAKE_") && nome.endsWith (".TXT") )
		{
			if (! pathname.canRead ())
			{
				logger.warn ("Saltando " + nome + " por falta de permissao");
				return (false);
			}

			logger.info ("Processando " + nome + " como Arquivo ANATEL.");
			return (true);
		}

		
		return (false);
	}

}

