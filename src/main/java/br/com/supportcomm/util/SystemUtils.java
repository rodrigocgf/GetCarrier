package br.com.supportcomm.util;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class SystemUtils {
	public static Logger logger = LoggerFactory.getLogger(SystemUtils.class);
			
	public long getTick() {
		return System.currentTimeMillis();
	}
	
	public static String getPropertyOrEnvironment(String name, String defaultValue) {
		String ret = System.getProperty(name);
		if (ret == null) {
			ret = System.getenv(name);
		}
		return ret == null ? defaultValue: ret;
	}

	public static String getSpringConfigLocator(String configName, String defaultConfigFile) {
    	String config = getPropertyOrEnvironment(configName, defaultConfigFile);
    	if (!config.trim().isEmpty()) {
	    	File configFile = new File(config.trim());
	    	if (configFile.exists()) {
	    		return "classpath:application.properties,file:" + config;
	    	}
	    	else {
	    		System.out.println("CONFIG MAIN: file " + config + " doesn't exist. Using internal");
	    	}
    	}
    	else {
    		System.out.println("CONFIG MAIN: no " + configName + ". Using internal");
    	}
		return null;
	}

	public static boolean fileExists(String fileName) {
    	File file = new File(fileName);
    	return file.exists();
	}

	public static Properties loadFile(String fileName) {
		Properties properties = new Properties();
		try (FileReader file = new FileReader(fileName)){
			properties.load(file);
			return properties;
		} catch (IOException e) {
			logger.error("Error readin properties " + fileName, e);
			return null;
		}
	}

	public static Properties loadResource(String resourceName) {
		Properties properties = new Properties();
		try (InputStream is = SystemUtils.class.getClassLoader().getResourceAsStream(resourceName)){
			properties.load(is);
			return properties;
		} catch (IOException e) {
			logger.error("Error readin properties " + resourceName, e);
			return null;
		}
	}
}
