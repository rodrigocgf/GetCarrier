package br.com.supportcomm.util;

import java.io.File;
import java.io.FileFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.apache.log4j.Logger;

public class FiltroDeArquivosAnatelNovo implements FileFilter{

  private static final Logger logger = LoggerFactory.getLogger (FiltroDeArquivosAnatelNovo.class);

	@Override
	public boolean accept (File pathname)
	{
		if (! pathname.isFile ())
		{
			return (false);
		}

		String nome;

		nome = pathname.getName ().toLowerCase ();

		if ( ( nome.startsWith ("CE_") || nome.startsWith ("ce_") ) && nome.endsWith (".txt"))
		{
			if (! pathname.canRead ())
			{
				logger.warn ("Saltando " + nome + " por falta de permissao");
				return (false);
			}

			return (true);
		}

		logger.info ("Saltando " + nome + ", nome do arquivo fora de formato");
		return (false);
	}

}

