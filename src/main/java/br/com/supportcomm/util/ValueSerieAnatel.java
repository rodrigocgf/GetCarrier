package br.com.supportcomm.util;

import br.com.supportcomm.models.db.TelefoniaEnum;

public class ValueSerieAnatel {
	
	public ValueSerieAnatel() {
		
	}
	
	public ValueSerieAnatel(String prefixo_start , String prefixo_end, TelefoniaEnum telefonia) {
		this.prefixo_start = prefixo_start;
		this.prefixo_end = prefixo_end;
		this.telefonia = telefonia;
	}
	
	public String getPrefixo_start() {
		return prefixo_start;
	}

	public void setPrefixo_start(String prefixo_start) {
		this.prefixo_start = prefixo_start;
	}

	public String getPrefixo_end() {
		return prefixo_end;
	}

	public void setPrefixo_end(String prefixo_end) {
		this.prefixo_end = prefixo_end;
	}
	
	public TelefoniaEnum getTelefonia() {
		return telefonia;
	}

	public void setTelefonia(TelefoniaEnum telefonia) {
		this.telefonia = telefonia;
	}

	@Override
	public String toString() {
		return "ValueAnatel [prefixo_start=" + prefixo_start + ", prefixo_end=" + prefixo_end + "]";
	}

	String prefixo_start;
	
	String prefixo_end;
	
	TelefoniaEnum telefonia;

	

}
