package br.com.supportcomm.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.supportcomm.jparepositories.NumeroPortadoRepoImpl;
import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.NumeroPortado;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;
import br.com.supportcomm.models.db.TelefoniaEnum;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.apache.log4j.Logger;

@Service
public class ProcessAbrLineBlock {
	Logger log = LoggerFactory.getLogger(ProcessAbrLineBlock.class);

	@Autowired
	NumeroPortadoRepoImpl numeroPortadoRepoImpl;
	
	public void processLine(	AtomicInteger counter, List<String> lineList, Pais pais, 
								HashMap<String, Serie> seriesList, HashMap<String, Area> areaList, 
								HashMap<Short, Operadora> opList, boolean test) {
		long initTime = System.nanoTime();
		try {
			
			if(areaList != null && opList != null) {
				
				log.info("STARTING EXECUTION FOR [" + Thread.currentThread().getName() + "] size List to Process: " + lineList.size());
				
				for(String line : lineList) {
					String[] campos=null;
					if (line!=null) {
						campos = line.split(";");
					}
					else {
						log.error("linha nula");
					}
					if(campos != null) {
						if(!campos[1].equals("0")) {
							ignoreLine(line,test);
						}
						if(campos[5].equals("0")) {
							ignoreLine(line, test);
						}
						if(!test) {
							Operadora op = opList.get(Short.parseShort(campos[5]));
							String ddd = campos[2].substring(0, pais.getTamanhoDoDdd());
							Long tn = Long.parseLong(campos[2].substring(pais.getTamanhoDoDdd()));
							Area a = areaList.get(ddd);
							TelefoniaEnum telefonia = getTelefonia(ddd, tn, seriesList);
							if(a != null) {
								NumeroPortado portado = findNumeroPortadoList(a, tn);
								if(op != null) {
									if(portado != null) {
										portado.setDataDeModificacao(new Date());
										portado.setAreaid(a.getAreaid());
										portado.setNumeroDeModificacoes(portado.getNumeroDeModificacoes() + 1);
										portado.setNumerodobilhete(Long.parseLong(campos[0]));
										portado.setOperadoraid(op.getOperadoraId());
										portado.setStatusupdate(0);
										portado.setTelefonia(telefonia);
										
										updateNumeroPortado(portado);
										
										log.info("ATUALIZADO Operadora:" + op.getNomeNormalizado() +" msisdn: "+portado.getNumero());
									} else {
										NumeroPortado nPortado = new NumeroPortado();
										nPortado.setAreaid(a.getAreaid());
										nPortado.setNumero(tn);
										nPortado.setDataDeInclusao(new Date());
										nPortado.setDataDeModificacao(new Date());
										nPortado.setNumeroDeModificacoes(0L);
										nPortado.setNumerodobilhete(Long.parseLong(campos[0]));
										nPortado.setOperadoraid(op.getOperadoraId());
										nPortado.setStatusupdate(0);
										nPortado.setTelefonia(telefonia);
										
										insertNumeroPortado(nPortado);
										
										log.info("ADICIONADO Operadora:" + op.getNomeNormalizado() +" msisdn: "+nPortado.getNumero());

									} 
								} else {
									ignoreLine(line, test);
								}
							} else {
								ignoreLine(line, test);
							}
						}
					}
				}	
			}
			counter.decrementAndGet();
		} finally {
			if(!test) {				
				log.info("COMMITING | EXECUTION TIME [" + (System.nanoTime() - initTime) + "]");
			}			
		}
	}
		
	private TelefoniaEnum getTelefonia(String ddd, Long tn, HashMap<String, Serie> seriesList) {
		String prefix = "";
		if(tn.toString().length() == 9) {
			prefix = tn.toString().substring(0, 5);
		} else if (tn.toString().length() == 8) {
			prefix = tn.toString().substring(0, 4);
		} else {
			return TelefoniaEnum.UNDEFINED;
		}
		TelefoniaEnum telefonia = TelefoniaEnum.UNDEFINED;
		if(seriesList.containsKey(ddd + prefix)) {
			telefonia = seriesList.get(ddd + prefix).getTelefonia();
		}
		return telefonia;
	}

	private void insertNumeroPortado( NumeroPortado portado) {
		
		numeroPortadoRepoImpl.Update(portado);
		
		/*
		PreparedStatement pstmt = null;		
		try {
			pstmt = conn.prepareStatement("INSERT INTO NUMEROPORTADO(NUMEROPORTADOID, DATADEINCLUSAO, DATADEMODIFICACAO, NUMERODEMODIFICACOES, " +
					"NUMERODOBILHETE, NUMERO, VERSION, OPERADORAID, AREAID, STATUSUPDATE, TELEFONIA) " +
					"VALUES (NUMEROPORTADO_SEQ.NEXTVAL, SYSDATE, SYSDATE, ?, ?, ?, ?, ?, ?, ?, ?)");
			pstmt.setLong(1, portado.getNumeroDeModificacoes());
			pstmt.setLong(2, portado.getNumerodobilhete());
			pstmt.setLong(3, portado.getNumero());
			pstmt.setInt(4, portado.getVersion());
			pstmt.setLong(5, portado.getOperadora().getOperadoraId());
			pstmt.setLong(6, portado.getArea().getAreaid());
			pstmt.setInt(7, portado.getStatus_Update());		
			pstmt.setInt(8, portado.getTelefonia().getValue());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			log.error("ERROR ON INSERT NEW NUMBER [" + portado.getNumero() + "]", e);
		} finally {
			ConnectionPooler.closeAll(pstmt, null);
		}
		*/
	}

	private void updateNumeroPortado( NumeroPortado portado) {
		
		numeroPortadoRepoImpl.Update(portado);
		/*
		PreparedStatement pstmt = null;		
		try {
			pstmt = conn.prepareStatement("UPDATE NUMEROPORTADO SET DATADEMODIFICACAO = SYSDATE, NUMERODEMODIFICACOES = ?, " +
					"NUMERODOBILHETE = ?, NUMERO = ?, VERSION = ?, OPERADORAID = ?, AREAID = ?, STATUSUPDATE = ?, TELEFONIA = ? WHERE NUMEROPORTADOID = ?");
			pstmt.setLong(1, portado.getNumeroDeModificacoes());
			pstmt.setLong(2, portado.getNumerodobilhete());
			pstmt.setLong(3, portado.getNumero());
			pstmt.setInt(4, portado.getVersion());
			pstmt.setLong(5, portado.getOperadora().getOperadoraId());
			pstmt.setLong(6, portado.getArea().getAreaid());
			pstmt.setInt(7, portado.getStatus_Update());		
			pstmt.setInt(8, portado.getTelefonia().getValue());
			pstmt.setLong(9, portado.getNumeroPortadoId());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			log.error("ERROR ON UPDATE NUMBER [ "+ portado + "]", e);
		} finally {
			ConnectionPooler.closeAll(pstmt, null);
		}
		*/
	}

	private NumeroPortado findNumeroPortadoList( Area area, Long tn) {
		
		NumeroPortado portado = null;
		
		portado = numeroPortadoRepoImpl.procuraPor_Area_Numero(area.getAreaid(), tn);
		
		return portado;
		
		/*
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = conn.prepareStatement("SELECT NUMEROPORTADOID, NUMERODEMODIFICACOES FROM NUMEROPORTADO WHERE AREAID = ? AND NUMERO = ?");
			pstmt.setLong(1, area.getAreaid());
			pstmt.setLong(2, tn);
			rs = pstmt.executeQuery();
			if(rs != null && rs.next()) {
				portado = new NumeroPortado();
				portado.setArea(area);
				portado.setNumero(tn);
				portado.setNumeroPortadoId(rs.getLong("NUMEROPORTADOID"));
				portado.setNumeroDeModificacoes(rs.getLong("NUMERODEMODIFICACOES"));
			}
		} catch (SQLException e) {
			log.info("ERROR ON FIND NUMEROPORTADO [" + tn + "]", e);
			return null;
		} catch (NullPointerException e) {
			log.info("ERROR ON FIND NUMEROPORTADO [" + tn + "]", e);
		} finally {
			ConnectionPooler.closeAll(pstmt, rs);
		}
		return portado;
		*/
	}

	public void  ignoreLine(String line, boolean test) {
		if(test) {
			log.info("IGNORING LINE");
			log.info(line);
		}
	}
}
