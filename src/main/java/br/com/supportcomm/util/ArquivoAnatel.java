package br.com.supportcomm.util;

import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Historico;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;
import br.com.supportcomm.models.db.TelefoniaEnum;
import br.com.supportcomm.repositories.AreaRepoImpl;
import br.com.supportcomm.repositories.HistoricoRepoImpl;
import br.com.supportcomm.repositories.OperadoraRepoImpl;
import br.com.supportcomm.repositories.PaisRepoImpl;
import br.com.supportcomm.repositories.SerieRepoImpl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.ServletException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import org.apache.log4j.Logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class ArquivoAnatel {
	private static final Logger logger = LoggerFactory.getLogger(ArquivoAnatel.class);
	private EntityManagerFactory factory;
	private File dirIn;
	private File dirOut;
	private HashMap<String, Long> areas = new HashMap<String, Long>();
	private HashMap<String, Long> operadoras = new HashMap<String, Long>();
	
	@Autowired
	private PaisRepoImpl paisRepoImpl;

	@Autowired
	private SerieRepoImpl serieRepoImpl;
	
	@Autowired
	private OperadoraRepoImpl operadoraRepoImpl;
	
	@Autowired
	private AreaRepoImpl areaRepoImpl;
	
	@Autowired
	private HistoricoRepoImpl histRepoImpl;
	
	public ArquivoAnatel() {
		
	}
	

	public void processa(File dirIn, File dirOut) {
		logger.info("======== PROCESSAMENTO ANATEL ========");
		File[] lista;
		String[] nomes;
		
		this.dirIn = dirIn;
		this.dirOut = dirOut;
		
		lista = dirIn.listFiles();
		if (lista == null || lista.length == 0) {
			return;
		}
		
		nomes = new String[lista.length];
		
		for (int ctr = 0; ctr < lista.length; ctr++) {
			nomes[ctr] = lista[ctr].getName();
		}
		
		Arrays.sort(nomes);
		for (int ctr = 0; ctr < nomes.length; ctr++) {
			try {				
				processaArquivoAnatel(nomes[ctr]);
			} catch (ServletException ex) {
				logger.error("Saltando o arquivo " + nomes[ctr] + ", " + ex.getMessage(), ex);
			} 
			
		}
	}
	
	@SuppressWarnings("unchecked")
	private void processaArquivoAnatel(String arquivoStr) throws ServletException {
		
		logger.debug("[processaArquivoAnatel]");
		
		int count = 0;
		String linha = "";
		String ddd = "";
		String prefixo = "";
		String oper = "";		
		Pais pais;
		Query query;
		Serie serie;
		BufferedReader arquivo;
		Date dataDaGeracao = new Date();
		EntityTransaction et = null;
		Historico hist = null;
		List<Serie> series;
		Map<String, Serie> seriesMap = new HashMap<String, Serie>();
		List<Pais> paises;

		
		if(arquivoStr.startsWith("CE") || arquivoStr.startsWith("ce")) {			
			
			try {
				paises = paisRepoImpl.procuraPor_Nome("BRASIL");
				pais = paises.get(0);				
				
			} catch (NoResultException nre) {
				throw new ServletException("Nao foi encontrado o BRASIL");
			}
			
			logger.info("Recuperando series do pais: " + pais);
			
			try {
				series = serieRepoImpl.buscaPorPais(pais);
			
			} catch (Exception nre) {
				throw new ServletException("Nao foi encontrado o BRASIL");
			}
			
			for (Serie aSerie : series) {
				String key = aSerie.getEspecial() + ":" + aSerie.getSerie();
				seriesMap.put(key, aSerie);
			}
			
			logger.info("Recuperadas series: " + series.size());
			
			
			try {
				arquivo = new BufferedReader(new InputStreamReader(new FileInputStream(new File(dirIn, arquivoStr)), "UTF-8"));
												
				while((linha = arquivo.readLine()) != null) {
					
					if(linha.length() > 36) {
						
						ddd = "";
						prefixo = "";
						oper = "";
						
						if (arquivoStr.startsWith("CE_M")) {
							
							ddd = linha.substring(0, 2).trim();
							prefixo = linha.substring(2, 7).trim();
							oper = linha.substring(7, 37).trim();
							
							if(oper.contains("Ç") ) {
								oper = oper.replace("Ç", "C");
							} 
							
							if (oper.contains("ç")) {
								oper = oper.replace("ç", "c");
							}
							
							String key = "1:" + ddd + prefixo;
							serie = seriesMap.get(key);
							
							TelefoniaEnum telefonia = null;
							if (arquivoStr.toLowerCase().startsWith("ce_f")) {
								telefonia = TelefoniaEnum.STFC;
							} else if (arquivoStr.toLowerCase().startsWith("ce_m")) {
								telefonia = TelefoniaEnum.SMP;
							} else {
								telefonia = TelefoniaEnum.UNDEFINED;
							}
							
							Operadora operadora = procuraOperadora(oper, pais, true);
							if(operadora != null) {
								if (serie != null) {
									logger.info(" Atualizado a serie " + ddd + prefixo);
									serie.setOperadora(operadora);
									serie.setTelefonia(telefonia);
									
									serieRepoImpl.update(serie);
									
								} else {
									logger.info("Inserindo a serie " + ddd + " " + prefixo + " para " + operadora);
									serie = new Serie();
									serie.setArea(procuraArea(ddd, pais));
									serie.setEspecial(new Short((short) (1)));
									serie.setOperadora(operadora);
									serie.setSerie(new Integer(ddd + prefixo));
									serie.setTelefonia(telefonia);
									
									serieRepoImpl.update(serie);
									
									seriesMap.put(key, serie);
								}
							}
							count++;
							if((count % 1000) == 0) {
								et.commit();
								et.begin();
							}
						} // if  {
						
					}
				} // while {
				
				hist = new Historico();
				hist.setArquivo(arquivoStr);
				hist.setDataDeReferencia(new Timestamp(dataDaGeracao.getTime()));
				hist.setDataDeProcessamento(new Timestamp(new Date().getTime()));
				hist.setCargaFull(1);
				hist.setNumeroDeRegistrosProcessados(count);
				
				histRepoImpl.update(hist);
				
				logger.debug("Movendo o arquivo " + arquivoStr + " para " + dirOut);
				new File(dirIn, arquivoStr).renameTo(new File(dirOut, arquivoStr));
			} catch (FileNotFoundException ex) {
				throw new ServletException("Nao foi encontrado o arquivo " + arquivoStr, ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}

	private Area procuraArea( String ddd, Pais pais) throws IOException {
		Area areaRas;
		StringBuffer sb = new StringBuffer();
		sb.append(pais.getPaisId().toString());
		sb.append("_");
		sb.append(ddd);
		
		String key = sb.toString();
		if (areas.containsKey(key)) {
			Long id = areas.get(key);
			if (id.longValue() < 0) {
				return (null);
			}
			return areaRepoImpl.selectAreaById(id);		
		}		
		
		areaRas = areaRepoImpl.procuraPor_Pais_DDD(pais, new Integer(ddd.trim()));
		if ( areaRas != null )
			areas.put(key, areaRas.getAreaid());
		
		return (areaRas);
	}

	private Operadora procuraOperadora( String oper, Pais pais, boolean retry) {
		Operadora operadora = null;
		
		List<Operadora> _operadoras = operadoraRepoImpl.procuraPor_Pais_Nome(pais, oper);
		if ( _operadoras != null ) {
			operadora = _operadoras.get(0);
		
			operadoras.put(oper, operadora.getOperadoraId());
		}
		
		return (operadora);		
		
	}
}
