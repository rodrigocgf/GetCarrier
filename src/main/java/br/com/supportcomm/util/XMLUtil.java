package br.com.supportcomm.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class XMLUtil {
	private static Logger logger = LoggerFactory.getLogger(XMLUtil.class);

	public static <T> String getXML(T elem) throws IOException {
		XmlMapper xmlMapper = new XmlMapper();

		String xml = xmlMapper.writeValueAsString(elem);
		logger.trace("Converteu " + elem + " to XML: " + xml);
		return xml;
	}

	public static <T> T getObject(Class<T> cl, String strXml) throws IOException {
		XmlMapper xmlMapper = new XmlMapper();
		T obj = xmlMapper.readValue(strXml, cl);
		logger.trace("Converteu XML: " + strXml + " to " + obj);
		return obj;
	}
}
