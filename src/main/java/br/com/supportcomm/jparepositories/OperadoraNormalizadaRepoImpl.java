package br.com.supportcomm.jparepositories;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import br.com.supportcomm.interfaces.IOperadoraNormalizada;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.OperadoraNormalizada;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import br.com.supportcomm.crudrepositories.OperadoraNormalizadaRepository;
import br.com.supportcomm.jparepositories.OperadoraNormalizadaRepository;


@Service
public class OperadoraNormalizadaRepoImpl implements IOperadoraNormalizada {
	
	public static Logger logger = LoggerFactory.getLogger(OperadoraNormalizadaRepoImpl.class);
	
	@Autowired
	private OperadoraNormalizadaRepository repo;

	@Override
	@Transactional
	public List<OperadoraNormalizada> selectAll() {
		logger.info(String.format("SELECT o FROM OperadoraNormalizada o"));
		List<OperadoraNormalizada> query = repo.selectAll();
		return query;
		
	}

}
