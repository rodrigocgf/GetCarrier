package br.com.supportcomm.jparepositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import br.com.supportcomm.interfaces.IUserLogin;
import br.com.supportcomm.models.db.UserLogin;

//import br.com.supportcomm.crudrepositories.UserLoginRepository;
import br.com.supportcomm.jparepositories.UserLoginRepository;


@Service
public class UserLoginRepoImpl implements IUserLogin {
	
	@Autowired
	private UserLoginRepository repo;
	
	@Override
	@Transactional
	public List<UserLogin> findByUserName(@Param("username") String username) {
		List<UserLogin> query = repo.findByUserName(username);
		return query;
	}
}
