package br.com.supportcomm.jparepositories;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.models.db.UserLogin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.io.Serializable;

@Repository
//@Transactional(value="getcarrierTransactionManager")
public interface UserLoginRepository extends JpaRepository<UserLogin, Serializable> {
	@Query("SELECT u FROM UserLogin u WHERE u.username = :username")
	List<UserLogin> findByUserName(@Param("username") String username);
}
