package br.com.supportcomm.jparepositories;

import java.util.List;

import javax.persistence.Cacheable;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.apache.log4j.Logger;

import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import br.com.supportcomm.business.Msisdn;

import br.com.supportcomm.interfaces.INumeroPortado;
import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.NumeroPortado;
import br.com.supportcomm.models.db.Operadora;

//import br.com.supportcomm.crudrepositories.NumeroPortadoRepository;
import br.com.supportcomm.jparepositories.NumeroPortadoRepository;

@Service
public class NumeroPortadoRepoImpl implements INumeroPortado {

	public static Logger logger = LoggerFactory.getLogger(NumeroPortadoRepoImpl.class);
	
	@Autowired
	private NumeroPortadoRepository repo;
	
	@Override
	public 
	NumeroPortado procuraPor_Area_Numero(@Param("long") Long areaid, @Param("numero") Long numero)
	{
		logger.info(String.format("SELECT np FROM NumeroPortado np WHERE np.areaid = %d AND np.numero = %d",areaid , numero) );
		NumeroPortado np = new NumeroPortado();
		try {
			List<NumeroPortado> query = (List<NumeroPortado>)repo.procuraPor_Area_Numero(areaid, numero);
			if ( query == null ) {
				logger.info("No RECORDS Found.");
				return null;
			}
			if ( query.size() == 0 ) {
				logger.info("No RECORDS Found.");
				return null;
			}
			logger.info("RECORD Found .");
			np = (NumeroPortado)query.get(0);
			
			
			return np;			
		}
		catch(InvalidDataAccessResourceUsageException ex ) {
			logger.debug(String.format("[procuraOperadora] Exception : %s", ex.toString() ) );
			logger.debug(String.format("[procuraOperadora] Stack Trace : %s", ex.fillInStackTrace() ) );
			return (null);
		}
		catch(SQLGrammarException ex) {
			logger.debug("[procuraOperadora] Exception : %s", ex.fillInStackTrace());
			return (null);
		}
	
	}
		
	@Override
	public 
	NumeroPortado procuraPor_Numero(@Param("numero") Long numero)
	{
		try {
			List<NumeroPortado> query = (List<NumeroPortado>)repo.procuraPor_Numero(numero);
			return query.get(0);			
		}
		catch(InvalidDataAccessResourceUsageException ex ) {
			logger.debug(String.format("[procuraOperadora] Exception : %s", ex.toString() ) );
			logger.debug(String.format("[procuraOperadora] Stack Trace : %s", ex.fillInStackTrace() ) );
			return (null);
		}
		catch(SQLGrammarException ex) {
			logger.debug("[procuraOperadora] Exception : %s", ex.fillInStackTrace());
			return (null);
		}
	
	}

	@Override
	@Modifying
	public
	void delete_StatusUpdate_1() {
		logger.info("DELETE FROM NumeroPortado np WHERE np.statusupdate = 1" );
		repo.delete_StatusUpdate_1();
	}
		
	@Override
	@Modifying
	public void Update(NumeroPortado numeroPortado) {				
		
		try {
			logger.info(String.format( 
					"\r\n=======================================================\r\n"
					+ "         UPDATE NumeroPortado np \r\n"
					+ "         SET np.operadoraid = %d, \r\n"
					+ "             np.numero = %d , \r\n"
					+ "             np.datademodificadao = %s , \r\n"
					+ "             np.numerodemodificacoes = %d, \r\n"
					+ "             np.numerodobilhete = %d \r\n"
					+ "\r\n=======================================================\r\n",
					numeroPortado.getOperadoraid(),
					numeroPortado.getNumero(),
					numeroPortado.getDataDeModificacao().toString(),
					numeroPortado.getNumeroDeModificacoes(),
					numeroPortado.getNumerodobilhete()));
			repo.save(numeroPortado);
		} catch(Exception ex ) {
			logger.info(String.format("Exception : %s"),ex.toString());
		}
		
	}
	
	@Override
	@Modifying
	public void Insert(NumeroPortado numeroPortado) {
		try {
			
			logger.info(String.format( 
					"\r\n=======================================================\r\n"
					+ "          INSERT INTO NumeroPortado \r\n"
					+ "          ( \r\n"
					+ "              operadoraid, \r\n"
					+ "              numero   , \r\n"
					+ "              datademodificacao , \r\n"
					+ "              datadeinclusao , \r\n"
					+ "              numerodemodificacoes , \r\n"
					+ "              numerodobilhete \r\n"
					+ "          ) values ( \r\n"
					+ "             %d , \r\n"
					+ "             %d , \r\n"
					+ "             %s , \r\n"
					+ "             %s , \r\n"
					+ "             %d , \r\n"
					+ "             %d   \r\n"
					+ "          ) \r\n "
					+ "\r\n=======================================================\r\n",
					numeroPortado.getOperadoraid(),
					numeroPortado.getNumero(),
					numeroPortado.getDataDeModificacao().toString(),
					numeroPortado.getDataDeInclusao().toString(),
					numeroPortado.getNumeroDeModificacoes(),
					numeroPortado.getNumerodobilhete()));
			
			repo.save(numeroPortado);
		} catch(Exception ex ) {
			logger.info(String.format("Exception : %s"),ex.toString());
		}
	}
	
	
	@Override
	@Modifying
	public void updateStatusUpdate() {
		logger.info("UPDATE NumeroPortado np SET np.statusupdate = 1 WHERE np.statusupdate=0");
		repo.updateStatusUpdate();
	}
	
	@Override
	@Modifying
	public
	void DeletePor_NumeroPortadoId(@Param("id") Long id ) {
		logger.info(String.format("DELETE FROM NumeroPortado np WHERE np.numeroportadoid = %d", id) );
		
		repo.DeletePor_NumeroPortadoId(id);
	}
	
	@Override	
	@Modifying
	public
	void Delete(NumeroPortado numeroPortado) {
		
		try {
			logger.info(String.format( 
					"\r\n=======================================================\r\n"
					+ "         DELETE FROM NumeroPortado np \r\n"
					+ "         WHERE  np.operadoraid = %d AND \r\n"
					+ "                np.numero = %d AND \r\n"
					+ "                np.datademodificadao = %s AND \r\n"
					+ "                np.numerodemodificacoes = %d AND \r\n"
					+ "                np.numerodobilhete = %d \r\n"
					+ "\r\n=======================================================\r\n",
					numeroPortado.getOperadoraid(),
					numeroPortado.getNumero(),
					numeroPortado.getDataDeModificacao().toString(),
					numeroPortado.getNumeroDeModificacoes(),
					numeroPortado.getNumerodobilhete()));
			
			repo.delete(numeroPortado);
		} catch(Exception ex ) {
			logger.info(String.format("Exception : %s"),ex.toString());
		}
		
	}
	
}
