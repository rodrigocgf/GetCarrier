package br.com.supportcomm.jparepositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import br.com.supportcomm.interfaces.IEstado;
import br.com.supportcomm.models.db.Estado;

//import br.com.supportcomm.crudrepositories.EstadoRepository;
import br.com.supportcomm.jparepositories.EstadoRepository;

@Service
public class EstadoRepoImpl implements IEstado {
	
	@Autowired
	private EstadoRepository repo;
	
	@Override
	public 	List<Estado> selectNomeSigla(){
		List<Estado> estados = (List<Estado>)repo.selectNomeSigla();
		return estados;
	}
	
	@Override
	public Estado searchById(@Param("estadoid") long estadoid)
	{
		 Estado estado = repo.searchById(estadoid);
		 return estado;
		
	}
}
