package br.com.supportcomm.jparepositories;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import br.com.supportcomm.business.Msisdn;
import br.com.supportcomm.interfaces.IArea;
import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Pais;

//import br.com.supportcomm.crudrepositories.AreaRepository;
import br.com.supportcomm.jparepositories.AreaRepository;


@Service
public class AreaRepoImpl implements IArea {

	public static Logger logger = LoggerFactory.getLogger(AreaRepoImpl.class);
	
	@Autowired
	private AreaRepository repo;
	
	@Override
	public
	Area procuraPor_Pais_DDD(@Param("paisid") long paisid, @Param("ddd") int ddd) {
		
		logger.info(String.format("SELECT a FROM Area a WHERE a.paisid = %d AND a.ddd = %d", paisid , ddd) );
		Area query = repo.procuraPor_Pais_DDD(paisid, ddd);
		if ( query != null  )
			return query;
		else
			return null;
	}
	
	@Override
	public
	List<Area> selectAll() {
		List<Area> query = (List<Area>)repo.selectAll();
		return query;
	}
	
	@Override
	public
	Area       selectAreaById(@Param("areaid") Long areaid) {
		logger.info(String.format("SELECT a FROM Area a WHERE a.areaid = %d", areaid));
		Area area = repo.selectAreaById(areaid);
		return area;
	}
	
}
