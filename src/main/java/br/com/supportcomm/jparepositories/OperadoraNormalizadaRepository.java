package br.com.supportcomm.jparepositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.models.db.OperadoraNormalizada;

@Repository
//@Transactional(value="getcarrierTransactionManager")
public interface OperadoraNormalizadaRepository extends JpaRepository<OperadoraNormalizada, Serializable> {
	
	@Query("SELECT o FROM OperadoraNormalizada o")
	public List<OperadoraNormalizada> selectAll();
}
