package br.com.supportcomm.jparepositories;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import java.io.Serializable;

@Repository
//@Transactional(value="getcarrierTransactionManager")
public interface SerieRepository extends JpaRepository<Serie, Serializable> {
	
	@Query("SELECT s FROM Serie s WHERE s.areaid = :areaid AND s.especial = :especial AND s.serie = :serie")
	List<Serie> procuraPor_Area_Especial_Serie(	@Param("areaid") Long areaid ,
												@Param("especial") int especial ,
												@Param("serie") int serie);
	
	@Query("SELECT s FROM Serie s WHERE s.areaid = :areaid AND s.especial = :especial AND s.operadoraid = :operadoraid AND s.serie = :serie")
	List<Serie> procuraPor_Area_Especial_Operadora_Serie(	@Param("areaid") Long areaid ,
															@Param("especial") int especial , 
															@Param("operadoraid") Long operadoraid , 
															@Param("serie") int serie);
	
	@Query("SELECT s FROM Serie s WHERE s.areaid = :areaid AND s.especial = :especial AND s.serie = :serie AND s.serie_start <= :sufix AND s.serie_end >= :sufix")
	List<Serie> procuraPor_Area_Especial_Serie_Sufixo(			@Param("areaid") Long areaid ,
																@Param("especial") int especial ,
																@Param("serie") int serie, 
																@Param("sufix") int sufix);

	@Query("SELECT s FROM Serie s WHERE s.areaid = :areaid AND s.operadoraid = :operadoraid AND s.especial = :especial AND s.serie = :serie AND s.serie_start = :serie_start AND s.serie_end = :serie_end")
	Serie procuraPor_Area_Operadora_Especial_Serie_Start_End(	@Param("areaid") Long areaid , 
																@Param("operadoraid") Long operadoraid, 
																@Param("especial") int especial , 
																@Param("serie") int serie ,
																@Param("serie_start") int serie_start , 
																@Param("serie_end") int serie_end);
	
	@Query("SELECT s FROM Serie s WHERE s.areaid = :areaid AND s.operadoraid = :operadoraid AND s.especial = :especial AND s.serie = :serie AND s.serie_start >= :serie_start AND s.serie_end <= :serie_end")
	List<Serie> procuraPor_Area_Operadora_Especial_Serie_Between_Start_End(	@Param("areaid") Long areaid , 
																			@Param("operadoraid") Long operadoraid, 
																			@Param("especial") int especial , 
																			@Param("serie") int serie ,
																			@Param("serie_start") int serie_start , 
																			@Param("serie_end") int serie_end);

	@Modifying
	@Query("DELETE FROM Serie s WHERE s.areaid = :areaid AND s.operadoraid = :operadoraid AND s.especial = :especial AND s.serie = :serie AND s.serie_start >= :serie_start AND s.serie_end < :serie_end")
	void deleteBy_Area_Operadora_Especial_Serie_Between_Start_End(	@Param("areaid") Long areaid , 
																	@Param("operadoraid") Long operadoraid, 
																	@Param("especial") int especial , 
																	@Param("serie") int serie ,
																	@Param("serie_start") int serie_start , 
																	@Param("serie_end") int serie_end);

	@Modifying
	@Query("DELETE FROM Serie s WHERE s.areaid = :areaid AND s.operadoraid = :operadoraid AND s.especial = :especial AND s.serie = :serie AND s.serie_start = :serie_start AND s.serie_end = :serie_end")
	void deleteBy_Area_Operadora_Especial_Serie_Start_End(	@Param("areaid") Long areaid , 
															@Param("operadoraid") Long operadoraid, 
															@Param("especial") int especial , 
															@Param("serie") int serie ,
															@Param("serie_start") int serie_start , 
															@Param("serie_end") int serie_end);
	@Modifying
	@Query("DELETE FROM Serie s WHERE s.serieid = :serieid")
	void deleteBy_Serieid( @Param("serieid") Long serieid);
	
	

	@Query("SELECT s FROM Serie s INNER JOIN Area a ON s.areaid = a.areaid where a.paisid = :paisid")
	List<Serie> buscaPorPais(@Param("paisid") Long paisid);
	
	@Query("SELECT s FROM Serie s")
	List<Serie> selectAll();

}
