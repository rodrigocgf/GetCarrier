package br.com.supportcomm.jparepositories;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import br.com.supportcomm.business.Msisdn;

import br.com.supportcomm.interfaces.ISerie;
import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;

//import br.com.supportcomm.crudrepositories.SerieRepository;
import br.com.supportcomm.jparepositories.SerieRepository;

@Service
public class SerieRepoImpl implements ISerie {
	
	public static Logger logger = LoggerFactory.getLogger(SerieRepoImpl.class);
	
	@Autowired
	private SerieRepository repo;

	
	@Override
	@Modifying
	public
	List<Serie> procuraPor_Area_Especial_Serie(	@Param("areaid") Long areaid ,
												@Param("especial") int especial ,
												@Param("serie") int serie) {
		List<Serie> query = null;
		try {
			logger.info(String.format("SELECT s FROM Serie s WHERE s.areaid = %d AND s.especial = %d AND s.serie = %d", 
					areaid , especial, serie));
			query = repo.procuraPor_Area_Especial_Serie(areaid, especial, serie);
			if ( query.size() == 0) 
				logger.info("NO SERIE FOUND");
		} catch(Exception ex) {
			logger.info("Exception : " + ex.toString());
		}
		return query;
	}
	
	@Override
	@Modifying
	public
	List<Serie> procuraPor_Area_Especial_Operadora_Serie(	@Param("areaid") Long areaid ,
															@Param("especial") int especial , 
															@Param("operadoraid") Long operadoraid, 
															@Param("serie") int serie) {
		List<Serie> query = null;
		try {
			logger.info(String.format("SELECT s FROM Serie s WHERE s.areaid = %d AND s.especial = %d AND s.serie = %d", 
					areaid , especial, serie));
			query = repo.procuraPor_Area_Especial_Operadora_Serie(areaid, especial, operadoraid, serie);
			if ( query.size() == 0) 
				logger.info("NO SERIE FOUND");
		} catch( Exception ex ) {
			logger.info("Exception : " + ex.toString());
		}
		return query;
	}
	
	@Override
	public
	List<Serie> procuraPor_Area_Especial_Serie_Sufixo(	@Param("areaid") Long areaid ,
														@Param("especial") int especial ,
														@Param("serie") int serie , 
														@Param("sufix") int sufix) {
		List<Serie> query = null;
		try {
			logger.info(String.format("SELECT s FROM Serie s WHERE s.areaid = %d AND s.especial = %d AND s.serie = %d AND s.serie_start <= %d AND s.serie_end >= %d", 
					areaid , especial, serie,sufix, sufix));
			query = repo.procuraPor_Area_Especial_Serie_Sufixo(areaid, especial, serie, sufix);
			if ( query.size() == 0) 
				logger.info("NO SERIE FOUND");
		}
		catch( Exception ex ) {
			logger.info("Exception : " + ex.toString());
		}
		return query;
	}
	
	@Override
	@Modifying
	public
	Serie procuraPor_Area_Operadora_Especial_Serie_Start_End(	@Param("areaid") Long areaid , 
																@Param("operadoraid") Long operadoraid, 
																@Param("especial") int especial , 
																@Param("serie") int serie ,
																@Param("serie_start") int serie_start , 
																@Param("serie_end") int serie_end ) {
		Serie serieFromDb = null;
		try {
			String query = "SELECT s FROM Serie s WHERE s.areaid = %d AND " +
						   "s.operadora = %d AND s.especial = %d AND " +
						   "s.serie = %d AND s.serie_start = %d AND s.serie_end = %d";
			logger.info(String.format(query, areaid, operadoraid , especial , serie , serie_start, serie_end ) );
			
			serieFromDb = repo.procuraPor_Area_Operadora_Especial_Serie_Start_End(areaid, operadoraid, especial, serie, serie_start, serie_end);
		} catch( Exception ex ) {
			logger.info("Exception : " + ex.toString());
		}
				
		return serieFromDb;
	}
	
	
	@Override
	@Modifying
	public
	List<Serie> procuraPor_Area_Operadora_Especial_Serie_Between_Start_End(	@Param("areaid") Long areaid , 
																			@Param("operadoraid") Long operadoraid, 
																			@Param("especial") int especial , 
																			@Param("serie") int serie ,
																			@Param("serie_start") int serie_start , 
																			@Param("serie_end") int serie_end ) {
		List<Serie> lSeries = null;
		try {
			String query = "SELECT s FROM Serie s WHERE s.areaid = %d AND " +
						   "s.operadora = %d AND s.especial = %d AND " +
						   "s.serie = %d AND s.serie_start >= %d AND s.serie_end <= %d";
			logger.info(String.format(query, areaid, operadoraid , especial , serie , serie_start, serie_end ) );
			
			lSeries  = repo.procuraPor_Area_Operadora_Especial_Serie_Between_Start_End(areaid, operadoraid, especial, serie, serie_start, serie_end);
		} catch( Exception ex ) {
			logger.info("Exception : " + ex.toString());
		}
		
		return lSeries;
	}
	
	@Override
	@Modifying
	public
	void insert_Area_Operadora_Especial_Serie_Start_End(	@Param("areaid") Long area , 
															@Param("operadoraid") Long operadora, 
															@Param("especial") int especial , 
															@Param("serie") int serie ,
															@Param("serie_start") int serie_start , 
															@Param("serie_end") int serie_end) {
		
		try {
			String query = "INSERT INTO Serie (areaid, operadora, especial, serie, serie_start, serie_end) " +
			               " VALUES (%d, %d, %d, %d, %d, %d)";
			
			logger.info(String.format(query, area, operadora, especial, serie, serie_start, serie_end));
			
			Serie lSerie = new Serie();
			lSerie.setAreaid(area);
			lSerie.setOperadoraid(operadora);
			lSerie.setEspecial(especial);
			lSerie.setSerie(serie);
			lSerie.setSerieStart(serie_start);
			lSerie.setSerieEnd(serie_end);
			
			repo.save(lSerie);
		} catch ( Exception ex)  {
			logger.info(String.format("Exception : %s" , ex.toString()));
		}
	}
	
	@Override
	@Modifying
	public
	void insert(Serie serie) {
		try {
			String query = "INSERT INTO Serie (areaid, operadora, especial, serie, serie_start, serie_end) " +
		               " VALUES (%d, %d, %d, %d, %d, %d)";
		
			logger.info(String.format(	query, serie.getAreaid(), serie.getOperadoraid(), 
										serie.getEspecial(), serie.getSerie(), serie.getSerieStart(), serie.getSerieEnd()));
		
			repo.save(serie);
		} catch ( Exception ex)  {
			logger.info(String.format("Exception : %s" , ex.toString()));
		}
		
		
	}
	
	@Override
	@Modifying
	public
	void deleteBy_Area_Operadora_Especial_Serie_Between_Start_End(	@Param("areaid") Long areaid , 
																	@Param("operadoraid") Long operadoraid, 
																	@Param("especial") int especial , 
																	@Param("serie") int serie ,
																	@Param("serie_start") int serie_start , 
																	@Param("serie_end") int serie_end ) {
		try {
			String query = "DELETE FROM Serie s WHERE s.areaid = %d AND " +
						   "s.operadoraid = %d AND s.especial = %d AND " +
						   "s.serie = %d AND s.serie_start >= %d AND s.serie_end < %d";
			logger.info(String.format(query, areaid, operadoraid , especial , serie , serie_start, serie_end ) );
			
			repo.deleteBy_Area_Operadora_Especial_Serie_Between_Start_End(areaid, operadoraid, especial, serie, serie_start, serie_end);
		} catch ( Exception ex)  {
			logger.info(String.format("Exception : %s" , ex.toString()));
		}
		
	}
	
	@Override
	@Modifying
	public
	void deleteBy_Area_Operadora_Especial_Serie_Start_End(	@Param("areaid") Long areaid , 
															@Param("operadoraid") Long operadoraid, 
															@Param("especial") int especial , 
															@Param("serie") int serie ,
															@Param("serie_start") int serie_start , 
															@Param("serie_end") int serie_end ) {
		try {
			String query = "DELETE FROM Serie s WHERE s.areaid = %d AND " +
						   "s.operadoraid = %d AND s.especial = %d AND " +
						   "s.serie = %d AND s.serie_start = %d AND s.serie_end = %d";
			logger.info(String.format(query, areaid, operadoraid , especial , serie , serie_start, serie_end ) );
			
			repo.deleteBy_Area_Operadora_Especial_Serie_Start_End(areaid, operadoraid, especial, serie, serie_start, serie_end);
		} catch ( Exception ex)  {
			logger.info(String.format("Exception : %s" , ex.toString()));
		}
		
	}
	
	@Override
	@Modifying
	public
	void deleteBy_Serieid( @Param("serieid") Long serieid) {
		try {
			String query = "DELETE FROM Serie s where s.serieid = %d";
			logger.info(String.format(query, serieid));
			
			repo.deleteById(serieid);
		} catch ( Exception ex)  {
			logger.info(String.format("Exception : %s" , ex.toString()));
		}

	}
	
	@Override
	public
	List<Serie> buscaPorPais(@Param("paisid") Long paisid) {
		List<Serie> query = null;
		try {
			logger.info(String.format("SELECT s FROM Serie s INNER JOIN Area a ON s.areaid = a.areaid where a.paisid = %d",paisid) );
			
			query = repo.buscaPorPais(paisid);
			if ( query.size() == 0) 
				logger.info("NO SERIE FOUND");
		}
		catch ( Exception ex)  {
			logger.info(String.format("Exception : %s" , ex.toString()));
		}
		return query;
	}
	
	@Override
	public
	List<Serie> selectAll() {
		List<Serie> query = repo.selectAll();
		return query;
	}
	
	@Override
	@Modifying
	public void update(@Param("serie") Serie serie) {
		repo.save(serie);
	}
}
