package br.com.supportcomm.jparepositories;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Pais;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.io.Serializable;

@Repository
//@Transactional(value="getcarrierTransactionManager")
public interface OperadoraRepository extends JpaRepository<Operadora, Serializable>{
	
	@Query("SELECT o FROM Operadora o WHERE o.paisid = :paisid AND o.nome = :nome")
	List<Operadora> procuraPor_Pais_Nome(@Param("paisid") Long paisid, @Param("nome") String nome);
	
	@Query("SELECT o FROM Operadora o WHERE o.paisid = :paisid AND o.cnpj = :cnpj")
	List<Operadora> procuraPor_Pais_Cnpj(@Param("paisid") Long paisid, @Param("cnpj") String cnpj);
	
	@Query("SELECT o FROM Operadora o WHERE o.paisid = :paisid AND o.spid = :spid")
	List<Operadora> procuraPor_Pais_Spid(@Param("paisid") Long paisid, @Param("spid") short spid);
	
	@Query("SELECT o FROM Operadora o WHERE o.paisid = :paisid ORDER BY o.spid")
	List<Operadora> procuraPor_Pais(@Param("paisid") Long paisid);
	
	@Query("SELECT a FROM Operadora a WHERE a.operadoraid = :operadoraid")
	Operadora selectOperadoraById(@Param("operadoraid") Long operadoraid);
}
