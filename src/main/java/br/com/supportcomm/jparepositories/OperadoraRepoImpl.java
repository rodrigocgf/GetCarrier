package br.com.supportcomm.jparepositories;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import br.com.supportcomm.business.Msisdn;

import br.com.supportcomm.interfaces.IOperadora;
import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Pais;

//import br.com.supportcomm.crudrepositories.OperadoraRepository;
import br.com.supportcomm.jparepositories.OperadoraRepository;

@Service
public class OperadoraRepoImpl implements IOperadora {

	public static Logger logger = LoggerFactory.getLogger(OperadoraRepoImpl.class);
	
	@Autowired
	private OperadoraRepository repo;
	
	@Override
	@Transactional
	public 
	List<Operadora> procuraPor_Pais_Nome(@Param("paisid") Long paisid, @Param("nome") String nome) {
		logger.info(String.format("SELECT o FROM Operadora o WHERE o.paisid = %d AND o.nome = '%s'", paisid  , nome));
		List<Operadora> query = repo.procuraPor_Pais_Nome(paisid, nome);
		return query;
	}
	
	@Override
	@Transactional
	public 
	List<Operadora> procuraPor_Pais_Cnpj(@Param("paisid") Long paisid, @Param("cnpj") String cnpj) {
		logger.info(String.format("SELECT o FROM Operadora o WHERE o.paisid = %d AND o.cnpj = '%s'", paisid  , cnpj));
		List<Operadora> query = repo.procuraPor_Pais_Cnpj(paisid, cnpj);
		return query;
	}

	@Override
	@Transactional
	public
	List<Operadora> procuraPor_Pais_Spid(@Param("paisid") Long paisid, @Param("spid") short spid) {
		logger.info(String.format("SELECT o FROM Operadora o WHERE o.paisid = %d AND o.spid = %d", paisid, spid));
		
		try {
		List<Operadora> query = repo.procuraPor_Pais_Spid(paisid, spid);
		return query;
		}
		catch(Exception ex) {
			logger.info(String.format("procuraPor_Pais_Spid error : %s", ex.toString()));
			return null;
		}
	}
	
	@Override
	@Transactional
	public
	List<Operadora> procuraPor_Pais(@Param("paisid") Long paisid) {
		
		logger.info(String.format("SELECT o FROM Operadora o WHERE o.paisid = %d ORDER BY o.spid", paisid ) );
		
		List<Operadora> query = repo.procuraPor_Pais(paisid);
		return query;
	}
	
	
	@Transactional
	public 
	Operadora selectOperadoraById(@Param("operadoraid") Long operadoraid) {
		logger.info(String.format("SELECT a FROM Operadora a WHERE a.operadoraid = %d", operadoraid));
		Operadora oper = repo.selectOperadoraById(operadoraid);
		return oper;
	}
	
	@Override
	@Transactional
	public void update(Operadora operadora) {
		logger.info(String.format( 
				"\r\n=======================================================\r\n"
				+ "         UPDATE Operadora op \r\n"
				+ "         SET op.nome = %s, \r\n"
				+ "             op.cnpj = %s, \r\n"
				+ "             op.nomenormalizado = %s, \r\n"
				+ "             op.codigo = %d, \r\n"
				+ "             op.spid = %d \r\n"
				+ "\r\n=======================================================\r\n",
				operadora.getNome(),
				operadora.getCNPJ(),
				operadora.getNomeNormalizado(),
				operadora.getCodigo(),
				operadora.getSpid()));
		repo.save(operadora);
	}
	
}
