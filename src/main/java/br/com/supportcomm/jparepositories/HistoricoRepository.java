package br.com.supportcomm.jparepositories;

import java.util.List;
import java.util.Optional;
import java.io.Serializable;
import java.sql.Timestamp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.models.db.Historico;

@Repository
//@Transactional(value="getcarrierTransactionManager")
public interface HistoricoRepository extends JpaRepository<Historico, Serializable> {
	
	@Query("SELECT h.historicoid from Historico h where h.arquivo = :arquivo and h.processado = 0")
	long RetornaHistoricoIdPorArquivo(@Param("arquivo") String arquivo);
	
	@Query("SELECT h FROM Historico h where h.arquivo = :arquivo and h.processado = 0 and TRUNC(DATE_PART('day',now() - h.dataDeProcessamento )) > 1")
	List<Historico>  ProcuraPorProcessadoAcima24horas(@Param("arquivo") String arquivo);
	
	@Query("SELECT h FROM Historico h where h.arquivo = :arquivo")
	List<Historico>  ProcuraPorArquivo(@Param("arquivo") String arquivo);
	
	@Query("SELECT h FROM Historico h where h.arquivo like 'BDT%' or h.arquivo like 'CPL%' ORDER BY h.dataDeReferencia DESC")
	List<Historico> RetornoOrdernado();	
	
	@Modifying
	@Query("UPDATE Historico h SET h.dataDeProcessamento = :dataDeProcessamento, h.numeroDeRegistrosProcessados = :numeroDeRegistrosProcessados, h.processado = 1 WHERE h.processado = 0 and h.arquivo = :arquivo")
	void UpdatePorAquivoNaoProcessado(@Param("dataDeProcessamento") Timestamp dataDeProcessamento , @Param("numeroDeRegistrosProcessados") int numeroDeRegistrosProcessados , @Param("arquivo") String arquivo);
	
	@Query("SELECT h FROM Historico h where h.historicoid = :historicoid")
	List<Historico> searchHistoricoById(@Param("historicoid") Long historicoid);
}
