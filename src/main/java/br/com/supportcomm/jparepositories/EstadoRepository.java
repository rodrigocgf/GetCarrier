package br.com.supportcomm.jparepositories;

import java.util.List;
import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.models.db.Estado;

@Repository
//@Transactional(value="getcarrierTransactionManager")
public interface EstadoRepository extends JpaRepository<Estado, Serializable> {
	
	@Query("SELECT e FROM Estado e")
	List<Estado> selectNomeSigla();
	
	@Query("SELECT e FROM Estado e where e.id = :estadoid")
	Estado searchById(@Param("estadoid") long estadoid);
}
