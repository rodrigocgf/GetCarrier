package br.com.supportcomm.jparepositories;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.models.db.Pais;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.io.Serializable;

@Repository
//@Transactional(value="getcarrierTransactionManager")
public interface PaisRepository extends JpaRepository<Pais, Serializable> {
	
	@Query("SELECT p FROM Pais p WHERE p.nome = :nome")
	List<Pais> procuraPor_Nome(@Param("nome") String nome);
	
	@Query("SELECT p FROM Pais p WHERE p.ddi = :ddi ORDER BY p.prioridade DESC")
	List<Pais> procuraPor_DDI(@Param("ddi") Short ddi);
	
	@Query("SELECT p FROM Pais p WHERE p.paisid = :paisid")
	Pais 	   selectPaisById(@Param("paisid") Long paisid);
	
}
