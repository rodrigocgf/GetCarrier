package br.com.supportcomm.jparepositories;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import br.com.supportcomm.interfaces.IHistorico;
import br.com.supportcomm.models.db.Historico;

//import br.com.supportcomm.crudrepositories.HistoricoRepository;
import br.com.supportcomm.jparepositories.HistoricoRepository;

@Service
public class HistoricoRepoImpl implements IHistorico{
	
	public static Logger logger = LoggerFactory.getLogger(HistoricoRepoImpl.class);
	
	private static final Date Date = null;
	
	@Autowired
	private HistoricoRepository historicoRepo;
	
	@Override
	public
	long RetornaHistoricoIdPorArquivo(@Param("arquivo") String arquivo) {
		try {
			long id = historicoRepo.RetornaHistoricoIdPorArquivo(arquivo);
			return id;
		}
		catch(Exception ex) {
			logger.info(String.format("Exception : %s", ex.toString()));
			return 0L;
		}
	}
	
	@Override
	@Transactional
	public
	List<Historico>  ProcuraPorArquivo(@Param("arquivo") String arquivo) {
		
		try {
			logger.info(String.format("SELECT h FROM Historico h where h.arquivo = %s", arquivo));
			List<Historico> query = historicoRepo.ProcuraPorArquivo(arquivo);
			return query;
		}
		catch(Exception ex) {
			logger.info(String.format("Exception : %s", ex.toString()));
			return null;
		}
	}
	
	@Override
	@Modifying
	@Query("UPDATE Historico h SET h.dataDeProcessamento = ?1, h.numeroDeRegistrosProcessados = ?2, h.processado = 1 WHERE h.processado = 0 and h.arquivo = ?3")
	public
	void UpdatePorAquivoNaoProcessado(	@Param("dataDeProcessamento") Timestamp dataDeProcessamento,
										@Param("numeroDeRegistrosProcessados") int numeroDeRegistrosProcessados , 
										@Param("arquivo") String arquivo) {
		try {
			logger.info(String.format("UPDATE historico h SET h.dataDeProcessamento = %s, h.numeroDeRegistrosProcessados = %d, h.processado = 1 WHERE h.processado = 0 and arquivo = %s",
					dataDeProcessamento.toString(), numeroDeRegistrosProcessados, arquivo));
			historicoRepo.UpdatePorAquivoNaoProcessado(dataDeProcessamento, numeroDeRegistrosProcessados, arquivo);
		
		}
		catch(Exception ex) {
			logger.info(String.format("Exception : %s", ex.toString()));
		}
		
	}
	
	@Override
	public
	List<Historico>  ProcuraPorProcessadoAcima24horas(@Param("arquivo") String arquivo) {
		try {
			logger.info(String.format("SELECT h FROM Historico h where h.arquivo = %s and h.processado = 0 and TRUNC(DATE_PART('day',now() - h.dataDeProcessamento )) > 1",arquivo));
			List<Historico> query = historicoRepo.ProcuraPorProcessadoAcima24horas(arquivo);
			return query;
		}
		catch(Exception ex) {
			logger.info(String.format("Exception : %s", ex.toString()));
			return null;
		}
	}
	
	@Override
	public
	List<Historico> RetornoOrdernado() {
		logger.info("SELECT h FROM Historico h where h.arquivo like 'BDT%' or h.arquivo like 'CPL%' ORDER BY h.dataDeReferencia DESC");
		List<Historico> query = historicoRepo.RetornoOrdernado();
		return query;
	}	
	
	@Override
	@Transactional
	public List<Historico> searchHistoricoById(@Param("historicoid") Long historicoid) {
		List<Historico> listHist;
		
		listHist = historicoRepo.searchHistoricoById(historicoid);
		return listHist;
	}
	

	@Override
	@Transactional
	public void update(	Timestamp dataDeProcessamento, Timestamp dataDeReferencia, int cargaFull,
							int numRegProc, String arquivo, int version) {
		
		Historico hist = new Historico();
		hist.setHistoricoId(0L);
		hist.setDataDeProcessamento(dataDeProcessamento);
		hist.setDataDeReferencia(dataDeReferencia);
		hist.setCargaFull(cargaFull);
		hist.setNumeroDeRegistrosProcessados(numRegProc);
		hist.setArquivo(arquivo);
		hist.setVersion(version);
		
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\r\n");
		sb.append("=======================================================").append("\r\n");
		sb.append("         INSERT INTO Historico ").append("\r\n");
		sb.append("         (      ").append("\r\n");
		sb.append("             arquivo, ").append("\r\n");
		sb.append("             datadeprocessamento, ").append("\r\n");
		sb.append("             datadereferencia, ").append("\r\n");
		sb.append("             numeroderegistrosprocessados ").append("\r\n");
		sb.append("         ) values ( ").append("\r\n");
		sb.append("             ").append(arquivo).append(" ,\r\n");		
		sb.append("             ").append(dataDeProcessamento.toString()).append(" ,\r\n");		
		sb.append("             ").append(dataDeReferencia.toString()).append(" ,\r\n");		
		sb.append("             ").append(String.valueOf(numRegProc)).append(" \r\n");
		sb.append("         )").append(" \r\n");
		sb.append("=======================================================").append("\r\n");
		
		logger.info(sb.toString());
		
		try {
			historicoRepo.save(hist);
		}
		catch( Exception ex) {
			logger.info(String.format("Excepction : %s", ex.toString()));
		}
	}
	
	@Override
	public void insert(Historico historico) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("\r\n");
		sb.append("=======================================================").append("\r\n");
		sb.append("         INSERT INTO Historico ").append("\r\n");
		sb.append("         (      ").append("\r\n");
		sb.append("             arquivo, ").append("\r\n");
		sb.append("             datadeprocessamento, ").append("\r\n");
		sb.append("             datadereferencia, ").append("\r\n");
		sb.append("             numeroderegistrosprocessados ").append("\r\n");
		sb.append("         ) values ( ").append("\r\n");
		sb.append("             ").append(historico.getArquivo()).append(" ,\r\n");		
		sb.append("             ").append(historico.getDataDeProcessamento().toString()).append(" ,\r\n");		
		sb.append("             ").append(historico.getDataDeReferencia().toString()).append(" ,\r\n");		
		sb.append("             ").append(String.valueOf(historico.getNumeroDeRegistrosProcessados())).append(" \r\n");
		sb.append("         )").append(" \r\n");
		sb.append("=======================================================").append("\r\n");
		
		logger.info(sb.toString());
		
		try {
			historicoRepo.save(historico);
		}
		catch( Exception ex) {
			logger.info(String.format("Excepction : %s", ex.toString()));			
		}
	}
	
	@Override
	public void update(Historico historico) {
		StringBuilder sb = new StringBuilder();		
		
		sb.append("\r\n");
		sb.append("=======================================================").append("\r\n");
		sb.append("         UPDATE Historico h set ").append("\r\n");
		sb.append("             h.arquivo = ");
		sb.append(historico.getArquivo()).append(",\r\n");
		sb.append("             h.datadeprocessamento = ");
		sb.append(historico.getDataDeProcessamento().toString()).append(",\r\n");
		sb.append("             h.datadereferencia = ");
		sb.append(historico.getDataDeReferencia().toString()).append(",\r\n");
		sb.append("             h.numeroderegistrosprocessados = ");
		sb.append(String.valueOf(historico.getNumeroDeRegistrosProcessados())).append(",\r\n");
		sb.append("             h.processado = ");
		sb.append(String.valueOf(historico.getProcessado())).append("\r\n");
		sb.append("=======================================================").append("\r\n");
		logger.info(sb.toString());
		
		try {
			historicoRepo.save(historico);
		}
		catch( Exception ex) {
			logger.info(String.format("Excepction : %s", ex.toString()));			
		}
	}
}
