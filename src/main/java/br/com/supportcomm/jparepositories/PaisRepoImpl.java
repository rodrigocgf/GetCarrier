package br.com.supportcomm.jparepositories;

import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import br.com.supportcomm.business.Msisdn;
import br.com.supportcomm.interfaces.IPais;
import br.com.supportcomm.models.db.Pais;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import br.com.supportcomm.crudrepositories.PaisRepository;
import br.com.supportcomm.jparepositories.PaisRepository;

@Service
public class PaisRepoImpl implements IPais {
	
	public static Logger logger = LoggerFactory.getLogger(PaisRepoImpl.class);
	
	@Autowired	
	private PaisRepository repo;	
	
	
	@Override
	@Transactional
	public
	List<Pais> procuraPor_Nome(@Param("nome") String nome) {
		logger.info(String.format("SELECT p FROM Pais p WHERE p.nome = '%s'", nome));
		List<Pais> query = repo.procuraPor_Nome(nome);
		return query;
	}
	
	@Override
	@Transactional
	public
	List<Pais> procuraPor_DDI(@Param("ddi") Short ddi) {
		logger.info("procuraPor_DDI");
		logger.info(String.format("SELECT p FROM Pais p WHERE p.ddi = %d ORDER BY p.prioridade DESC", ddi ) );		
		List<Pais> query = (List<Pais>) repo.procuraPor_DDI(ddi);		
		return query;
	}
	
	@Override
	@Transactional
	public
	Pais selectPaisById(@Param("paisid") Long paisid) {
		logger.info(String.format("SELECT p FROM Pais p WHERE p.paisid = %d", paisid));		
		return repo.selectPaisById(paisid);
	}
	
}
