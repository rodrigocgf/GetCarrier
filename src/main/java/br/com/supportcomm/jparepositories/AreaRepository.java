package br.com.supportcomm.jparepositories;

import java.io.Serializable;
import java.util.List;

import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Pais;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
//@Transactional(value="getcarrierTransactionManager")
public interface AreaRepository extends JpaRepository<Area, Serializable> {

	@Query("SELECT a FROM Area a WHERE a.paisid = :paisid AND a.ddd = :ddd")
	Area procuraPor_Pais_DDD(@Param("paisid") long paisid, @Param("ddd")int ddd);
	
	@Query("SELECT a FROM Area a")
	List<Area> selectAll();
	
	@Query("SELECT a FROM Area a WHERE a.areaid = :areaid")
	Area       selectAreaById(@Param("areaid") Long areaid); 

}
