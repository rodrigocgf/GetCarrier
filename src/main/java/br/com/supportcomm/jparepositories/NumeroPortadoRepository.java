package br.com.supportcomm.jparepositories;

import java.util.List;
import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.NumeroPortado;


@Repository
//@Transactional(value="getcarrierTransactionManager")
public interface NumeroPortadoRepository extends JpaRepository<NumeroPortado, Serializable> {
	
	@Query("SELECT np FROM NumeroPortado np WHERE np.areaid = :areaid AND np.numero = :numero")
	List<NumeroPortado> procuraPor_Area_Numero(@Param("areaid") Long areaid, @Param("numero") Long numero);
	
	@Query("SELECT np FROM NumeroPortado np WHERE np.numero = :numero")
	List<NumeroPortado> procuraPor_Numero(@Param("numero") Long numero);
	
	@Modifying
	@Query("DELETE FROM NumeroPortado np WHERE np.statusupdate = 1")
	void delete_StatusUpdate_1();
	
	@Modifying
	@Query("UPDATE NumeroPortado np SET np.statusupdate = 1 WHERE np.statusupdate=0")
	void updateStatusUpdate();	
	
	@Modifying
	@Query("DELETE FROM NumeroPortado np WHERE np.numeroportadoid = :id")
	void DeletePor_NumeroPortadoId(@Param("id") Long id);
	
}
