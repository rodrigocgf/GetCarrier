package br.com.supportcomm.crudrepositories;

import java.util.List;

import javax.persistence.Query;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.models.db.Pais;

/*@Transactional
public interface PaisRepositoryFormer extends CrudRepository<Pais, Long > {
	List<Pais> procuraPor_Nome(@Param("nome") String nome);
	List<Pais> procuraPor_DDI(@Param("ddi") Short ddi);
	Pais 	   selectPaisById(@Param("paisid") Long paisid);
}
*/