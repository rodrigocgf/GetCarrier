package br.com.supportcomm.crudrepositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;
import br.com.supportcomm.models.db.Operadora;

/*@Transactional
public interface SerieRepositoryFormer extends CrudRepository<Serie, Long > {
	List<Serie> procuraPor_Area_Especial_Serie(@Param("areaid") Area areaid ,@Param("especial") int especial ,@Param("serie") int serie);
	
	List<Serie> procuraPor_Area_Especial_Operadora_Serie(	@Param("areaid") Area areaid ,@Param("especial") int especial , 
															@Param("operadoraid") Operadora operadoraid , @Param("serie") int serie);
	
	List<Serie> procuraPor_Area_Especial_Serie_Sufixo(			@Param("areaid") Area areaid ,@Param("especial") int especial ,
																@Param("serie") int serie, @Param("sufix") int sufix);
	
	Serie procuraPor_Area_Operadora_Especial_Serie_Start_End(	@Param("areaid") Area areaid , @Param("operadoraid") Operadora operadoraid, 
																@Param("especial") int especial , @Param("serie") int serie ,
																@Param("serie_start") int serie_start , @Param("serie_end") int serie_end);
	
	List<Serie> procuraPor_Area_Operadora_Especial_Serie_Between_Start_End(	@Param("areaid") Area areaid , @Param("operadoraid") Operadora operadoraid, 
																			@Param("especial") int especial , @Param("serie") int serie ,
																			@Param("serie_start") int serie_start , @Param("serie_end") int serie_end);
	
	void deleteBy_Area_Operadora_Especial_Serie_Between_Start_End(	@Param("areaid") Area areaid , @Param("operadoraid") Operadora operadoraid, 
																	@Param("especial") int especial , @Param("serie") int serie ,
																	@Param("serie_start") int serie_start , @Param("serie_end") int serie_end);
	
	void deleteBy_Area_Operadora_Especial_Serie_Start_End(	@Param("areaid") Area areaid , @Param("operadoraid") Operadora operadoraid, 
															@Param("especial") int especial , @Param("serie") int serie ,
															@Param("serie_start") int serie_start , @Param("serie_end") int serie_end);
	
	void deleteBy_Serieid( @Param("serieid") long serieid);
	
	List<Serie> buscaPorPais(@Param("pais") Pais pais);
	List<Serie> selectAll();
	
}*/
