package br.com.supportcomm.crudrepositories;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.supportcomm.models.db.Historico;

/*@Transactional
public interface HistoricoRepositoryFormer extends CrudRepository<Historico, Long > {
	List<Historico>  ProcuraPorProcessadoAcima24horas(@Param("arquivo") String arquivo);
	List<Historico>  ProcuraPorArquivo(@Param("arquivo") String arquivo);
	long RetornaHistoricoIdPorArquivo(@Param("arquivo") String arquivo);
	List<Historico> RetornoOrdernado();	
	void UpdatePorAquivoNaoProcessado(@Param("dataDeProcessamento") Timestamp dataDeProcessamento , @Param("numeroDeRegistrosProcessados") int numeroDeRegistrosProcessados , @Param("arquivo") String arquivo);
	Optional<Historico> findById(@Param("historicoid") Long historicoId);
}
*/