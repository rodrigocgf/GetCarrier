package br.com.supportcomm.config;

import java.util.Arrays;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.stereotype.Component;

@Component
public class AppConfig {
	public Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
	public static final String APP_NAME = "getcarrier";
	
	public static final String CONFIG_EXTERNAL_FILE = "application.properties";
	public static final String NAME_EXTERNAL_FILE = "/var/supportcomm/getcarrier/application.properties";

	public static final String CONFIG_LOG_JSON = "LogJson";
	
	@Autowired
    private Environment env;	
	
	
	@Value("${sc.basedir}")
	private String basedir;
	
	@Value("${sc.carga.DiretorioEntradaDeCarga}")
	private String diretorioEntradaDeCarga;
	
	@Value("${sc.carga.DiretorioSaidaDeCarga}")
	private String diretorioSaidaDeCarga;
	
	@Value("${sc.carga.DiretorioIgnorados}")
	private String diretorioIgnorados;
	
	@Value("${sc.carga.DiretorioRejeitados}")
	private String diretorioRejeitados;
	
	@Value("${sc.http.timeout_connection:1000}")
	private Integer connectTimeout;

	@Value("${sc.http.timeout_socket:10000}")
	private Integer readTimeout;

	@Value("${sc.http.conn-manager.timeout:1000}")
	private Integer httpPoolTimeout;

	@Value("${sc.http.conn-manager.maxPerRoute:#{null}}")
	private Integer httpMaxPerRoute;

	@Value("${sc.http.conn-manager.maxConnections:#{null}}")
	private Integer httpMaxConnections;

	@Value("${sc.http.conn-manager.secondsKeepAlive:#{null}}")
	private Integer httpSecondsKeepAlive;

	@Value("${sc.http.proxyHost:#{null}}")
	private String httpProxyHost;

	@Value("${sc.http.proxyPort:3128}")
	private int httpProxyPort;
	
	@Value("${sc.http.no-ssl-validation:false}")
	private boolean noSslValidation;

	@Value("${sc.admim.username:#{null}}")
	private String adminUsername;

	@Value("${sc.admim.password:#{null}}")
	private String adminPassword;

	@Value("${management.endpoints.web.base-path:/actuator}")
	private String actuatorPath;
		
/*	@Value("${sc.oracle.datasource.driver-class-name}")
	private String dsOracleDriverClassName;
	
	@Value("${sc.oracle.datasource.url}")
	private String dsOracleURL;
	
	@Value("${sc.oracle.datasource.username}")
	private String dsOracleUser;
	
	@Value("${sc.oracle.datasource.password}")
	private String dsOraclePwd;
	
	
	public String getOracleDriverClassName() {
		return dsOracleDriverClassName;
	}
	public String getOracleDsURL() {
		return dsOracleURL;
	}
	public String getOracleUser() {
		return dsOracleUser;
	}
	public String getOraclePwd() {
		return dsOraclePwd;
	}*/
	
	
	public String getbaseDir() {
		return basedir;
	}

	public String getDiretorioEntradaDeCarga() {
		return diretorioEntradaDeCarga;
	}
	
	public String getDiretorioSaidaDeCarga() {
		return diretorioSaidaDeCarga;
	}

	public String getDiretorioIgnorados() {
		return diretorioIgnorados;
	}
	
	public String getDiretorioRejeitados() {
		return diretorioRejeitados;
	}
	
	public Integer getConnectTimeout() {
		return connectTimeout;
	}

	public Integer getReadTimeout() {
		return readTimeout;
	}

	public Integer getHttpMaxPerRoute() {
		return httpMaxPerRoute;
	}

	public Integer getHttpMaxConnections() {
		return httpMaxConnections;
	}

	public String getHttpProxyHost() {
		return httpProxyHost;
	}

	public int getHttpProxyPort() {
		return httpProxyPort;
	}

	public Integer getHttpSecondsKeepAlive() {
		return httpSecondsKeepAlive;
	}

	public Integer getHttpPoolTimeout() {
		return httpPoolTimeout;
	}

	public boolean isNoSslValidation() {
		return noSslValidation;
	}

	public String getActuatorPath() {
		return actuatorPath;
	}

	public String getAdminUsername() {
		return adminUsername;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public String getProperty(String name) {
		return env.getProperty(name);
	}

	public void logConfig() {
		String config = toString().replaceAll(", adminPassword=[^ ]+", ", adminPassword=X");
		logger.info(config);
	}

	
	
	
	@Override
	public String toString() {
		return "AppConfig [connectTimeout=" + connectTimeout + ", readTimeout="
				+ readTimeout + ", httpPoolTimeout=" + httpPoolTimeout + ", httpMaxPerRoute=" + httpMaxPerRoute
				+ ", httpMaxConnections=" + httpMaxConnections + ", httpSecondsKeepAlive=" + httpSecondsKeepAlive
				+ ", httpProxyHost=" + httpProxyHost + ", httpProxyPort=" + httpProxyPort + ", noSslValidation="
				+ noSslValidation + ", adminUsername=" + adminUsername + ", adminPassword=" + adminPassword
				+ ", diretorioEntradaDeCarga=" + diretorioEntradaDeCarga +  ", diretorioSaidaDeCarga" + diretorioSaidaDeCarga 
				+ ", diretorioIgnorados=" + diretorioIgnorados 
				+ ", actuatorPath=" + actuatorPath + "]";
	}
	
	public void logAllProperties() {
        logger.info("====== Environment and configuration ======");
        logger.info("Active profiles: {}", Arrays.toString(env.getActiveProfiles()));
        final MutablePropertySources sources = ((AbstractEnvironment) env).getPropertySources();
        StreamSupport.stream(sources.spliterator(), false)
                .filter(ps -> ps instanceof EnumerablePropertySource)
                .map(ps -> ((EnumerablePropertySource<?>) ps).getPropertyNames())
                .flatMap(Arrays::stream)
                .distinct()
                .filter(prop -> !(prop.contains("credentials") || prop.contains("password") || prop.contains("access_token")))
                .forEach(prop -> logger.info("{}: {}", prop, env.getProperty(prop)));
        logger.info("===========================================");
    }

	public static void printSystemInfo() {
		System.out.println("java.vendor=" + System.getProperty("java.vendor"));
		System.out.println("java.version=" + System.getProperty("java.version"));
		System.out.println("os.arch=" + System.getProperty("os.arch"));
		System.out.println("os.name=" + System.getProperty("os.name"));
		System.out.println("os.version=" + System.getProperty("os.version"));
		System.out.println("availableProcessors=" + Runtime.getRuntime().availableProcessors());
		System.out.println("maxMemory=" + Runtime.getRuntime().maxMemory() / 1024/1024 + " MB");
		System.out.println("totalMemory=" + Runtime.getRuntime().totalMemory() / 1024/1024 + " MB");
		System.out.println("freeMemory=" + Runtime.getRuntime().freeMemory() / 1024/1024 + " MB");
	}	


}
