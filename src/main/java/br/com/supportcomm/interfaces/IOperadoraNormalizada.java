package br.com.supportcomm.interfaces;

import java.util.List;

import org.springframework.data.repository.query.Param;

import br.com.supportcomm.models.db.OperadoraNormalizada;

public interface IOperadoraNormalizada {
	List<OperadoraNormalizada> selectAll();
}
