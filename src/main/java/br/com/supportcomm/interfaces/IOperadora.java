package br.com.supportcomm.interfaces;

import java.util.List;

import org.springframework.data.repository.query.Param;

import br.com.supportcomm.models.db.Operadora;
import br.com.supportcomm.models.db.Pais;

public interface IOperadora {
	List<Operadora> procuraPor_Pais_Nome(@Param("paisid") Long paisid, @Param("nome") String nome);
	
	List<Operadora> procuraPor_Pais_Cnpj(@Param("paisid") Long paisid, @Param("cnpj") String cnpj);
	
	List<Operadora> procuraPor_Pais_Spid(@Param("paisid") Long paisid, @Param("spid") short spid);
	
	List<Operadora> procuraPor_Pais(@Param("paisid") Long paisid);
	
	void update(Operadora operadora);
	
}
