package br.com.supportcomm.interfaces;

import java.util.List;

import javax.persistence.Query;

import org.springframework.data.repository.query.Param;

import br.com.supportcomm.models.db.Pais;

public interface IPais {
	List<Pais> procuraPor_Nome(@Param("nome") String nome);
	List<Pais> procuraPor_DDI(@Param("ddi") Short ddi);
	Pais 	   selectPaisById(@Param("paisid") Long paisid);
}
