package br.com.supportcomm.interfaces;

import java.util.List;

import org.springframework.data.repository.query.Param;

import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.NumeroPortado;

public interface INumeroPortado {
	NumeroPortado procuraPor_Area_Numero(@Param("areaid") Long areaid, @Param("numero") Long numero);
	NumeroPortado procuraPor_Numero(@Param("numero") Long numero);
	void delete_StatusUpdate_1();
	void Update(NumeroPortado numeroPortado);
	void Insert(NumeroPortado numeroPortado);
	void updateStatusUpdate();	
	void DeletePor_NumeroPortadoId(@Param("id") Long id);
	void Delete(NumeroPortado numeroPortado);
}
