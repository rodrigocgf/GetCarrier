package br.com.supportcomm.interfaces;

import java.util.List;

import org.springframework.data.repository.query.Param;

import br.com.supportcomm.models.db.Estado;

public interface IEstado {	
	List<Estado> selectNomeSigla();
	Estado searchById(@Param("estadoid") long estadoid);
}
