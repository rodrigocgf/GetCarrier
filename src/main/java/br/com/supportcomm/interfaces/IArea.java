package br.com.supportcomm.interfaces;

import java.util.List;

import org.springframework.data.repository.query.Param;
import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Pais;

public interface IArea {
	Area procuraPor_Pais_DDD(@Param("paisid") long paisid, @Param("ddd") int ddd);
	List<Area> selectAll();
	Area       selectAreaById(@Param("areaid") Long areaid);
}
