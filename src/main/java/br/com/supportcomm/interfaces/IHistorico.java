package br.com.supportcomm.interfaces;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

import br.com.supportcomm.models.db.Historico;

public interface IHistorico {
	List<Historico>  ProcuraPorProcessadoAcima24horas(@Param("arquivo") String arquivo);
	List<Historico>  ProcuraPorArquivo(@Param("arquivo") String arquivo);
	long RetornaHistoricoIdPorArquivo(@Param("arquivo") String arquivo);
	List<Historico> RetornoOrdernado();
	void update(Timestamp dateDeProcessamento, Timestamp dateDeReferencia, int cargaFull, int numRegProc , String arquivo, int version);	
	void update(Historico historico);
	void insert(Historico historico);
	void UpdatePorAquivoNaoProcessado(Timestamp dataDeProcessamento, int numeroDeRegistrosProcessados, String arquivo);
	List<Historico> searchHistoricoById(@Param("historicoid") Long historicoid);
}
