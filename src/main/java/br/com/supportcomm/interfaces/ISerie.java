package br.com.supportcomm.interfaces;

import java.util.List;

import org.springframework.data.repository.query.Param;

import br.com.supportcomm.models.db.Area;
import br.com.supportcomm.models.db.Pais;
import br.com.supportcomm.models.db.Serie;
import br.com.supportcomm.models.db.Operadora;

public interface ISerie {
	List<Serie> procuraPor_Area_Especial_Serie(	@Param("areaid") Long areaid ,
												@Param("especial") int especial ,
												@Param("serie") int serie);
	
	List<Serie> procuraPor_Area_Especial_Operadora_Serie(		@Param("area") Long area ,
																@Param("especial") int especial , 
																@Param("operadoraid") Long operadoraid , 
																@Param("serie") int serie);
	
	List<Serie> procuraPor_Area_Especial_Serie_Sufixo(			@Param("areaid") Long areaid ,
																@Param("especial") int especial ,
																@Param("serie") int serie, 
																@Param("sufix") int sufix);
	
	Serie procuraPor_Area_Operadora_Especial_Serie_Start_End(	@Param("areaid") Long areaid , 
																@Param("operadoraid") Long operadoraid, 
																@Param("especial") int especial , 
																@Param("serie") int serie ,
																@Param("serie_start") int serie_start , 
																@Param("serie_end") int serie_end);
	
	List<Serie> procuraPor_Area_Operadora_Especial_Serie_Between_Start_End(	@Param("areaid") Long areaid , 
																			@Param("operadoraid") Long operadoraid, 
																			@Param("especial") int especial , 
																			@Param("serie") int serie ,
																			@Param("serie_start") int serie_start , 
																			@Param("serie_end") int serie_end );
																	
	void deleteBy_Area_Operadora_Especial_Serie_Between_Start_End(	@Param("areaid") Long areaid , 
																	@Param("operadoraid") Long operadoraid, 
																	@Param("especial") int especial , 
																	@Param("serie") int serie ,
																	@Param("serie_start") int serie_start , 
																	@Param("serie_end") int serie_end);
	
	void deleteBy_Area_Operadora_Especial_Serie_Start_End(	@Param("areaid") Long areaid , 
															@Param("operadoraid") Long operadoraid, 
															@Param("especial") int especial , 
															@Param("serie") int serie ,
															@Param("serie_start") int serie_start , 
															@Param("serie_end") int serie_end);
	
	void deleteBy_Serieid( @Param("serieid") Long serieid);
	
	void insert_Area_Operadora_Especial_Serie_Start_End(	@Param("areaid") Long areaid , 
															@Param("operadoraid") Long operadoraid, 
															@Param("especial") int especial , 
															@Param("serie") int serie ,
															@Param("serie_start") int serie_start , 
															@Param("serie_end") int serie_end);
	
	void insert(Serie serie);
	
	List<Serie> buscaPorPais(@Param("paisid") Long paisid);
	
	List<Serie> selectAll();
	void update(Serie serie);
}
