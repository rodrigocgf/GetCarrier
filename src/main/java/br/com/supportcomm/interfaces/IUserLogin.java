package br.com.supportcomm.interfaces;

import java.util.List;

import org.springframework.data.repository.query.Param;

import br.com.supportcomm.models.db.UserLogin;

public interface IUserLogin {
	List<UserLogin> findByUserName(@Param("username") String username);
}
