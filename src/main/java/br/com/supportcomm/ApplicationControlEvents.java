package br.com.supportcomm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import br.com.supportcomm.cache.CacheGlobal;
import br.com.supportcomm.config.AppConfig;
//import br.com.supportcomm.crudrepositories.UserLoginRepositoryFormer;
import br.com.supportcomm.models.db.UserLogin;

@Component
public class ApplicationControlEvents {
	public static Logger logger = LoggerFactory.getLogger(ApplicationControlEvents.class);
	//@Autowired private UserLoginRepository userRepository;
	//@Autowired private PasswordEncoder encoder;
	@Autowired private AppConfig appConfig;
	//@Autowired private CacheGlobal cacheGlobal;
	
	@EventListener(ApplicationReadyEvent.class)
	public void onApplicationReadyEvent() {
		//cacheGlobal.createCaches();
		String adminUser = appConfig.getAdminUsername();
		if (StringUtils.hasText(adminUser)) {
			String adminPass = appConfig.getAdminPassword();
			if (!StringUtils.hasText(adminPass)) {
				GetCarrier.logger.debug("Cannot create admin user: " + adminUser + " No password");
			}
			else 
			{
				
				logger.debug("Creating admin user: " + adminUser);
				/*
				UserLogin user = new UserLogin();
				user.setUsername(adminUser);
				user.setPassword(encoder.encode(adminPass));
				user.setRole("ADMIN");
				user.setEnabled(true);
				userRepository.save(user);
				*/
				
			}
		}
		GetCarrier.logger.info("APPLICATION READY");
		appConfig.logAllProperties();
		appConfig.logConfig();
	}

	@EventListener(ApplicationStartedEvent.class)
	public void onApplicationStartedEvent() {
		GetCarrier.logger.info("APPLICATION STARTED");
	}
	
	@EventListener
	public void onContextRefreshedEvent(ContextRefreshedEvent cre) {
		GetCarrier.logger.info("CONTEXT REFRESHED");
	}
    
}
