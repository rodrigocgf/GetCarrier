package br.com.supportcomm.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.supportcomm.business.Msisdn;
import br.com.supportcomm.exception.CodigoRetornoEnum;


public class PortabilidadeException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public static Logger logger = LoggerFactory.getLogger(Msisdn.class);
	
	private CodigoRetornoEnum codigoRetorno;
	private String mensagem;
	
	public PortabilidadeException(CodigoRetornoEnum codigoRetorno, String mensagem) {
		super();
		this.codigoRetorno = codigoRetorno;
		this.mensagem = mensagem;
		
		logger.info(mensagem);
	}

	public CodigoRetornoEnum getCodigoRetorno() {
		return codigoRetorno;
	}

	public String getMessage() {
		return mensagem;
	}
	
}
