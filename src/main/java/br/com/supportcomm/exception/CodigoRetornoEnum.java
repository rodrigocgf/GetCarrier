package br.com.supportcomm.exception;

public enum CodigoRetornoEnum {
	OK,
	MSISDN_INVALIDO,
	OPERADORA_NAO_ENCONTRADA,
	FALHA;
}
