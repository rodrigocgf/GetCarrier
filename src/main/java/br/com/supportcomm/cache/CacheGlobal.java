package br.com.supportcomm.cache;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import javax.cache.Cache;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.CreatedExpiryPolicy;

import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheEventListenerConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.event.EventType;
import org.ehcache.jsr107.Eh107Configuration;
import org.ehcache.jsr107.EhcacheCachingProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.cache.CacheMetricsRegistrar;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

import br.com.supportcomm.models.db.UserLogin;

@Component
public class CacheGlobal {
	/*
	private static final Logger logger = LoggerFactory.getLogger(CacheGlobal.class);
	
	@Autowired private javax.cache.CacheManager cacheManager;
	@Autowired private CacheManager cacheManagerSpring;
	@Autowired private CacheMetricsRegistrar cacheMetricsRegistrar;
	
	public Cache<String, UserLogin> getCacheUsers() {
		return cacheManager.getCache(CacheEnum.CACHE_USER_LOGIN.getCacheId(), String.class, UserLogin.class);
	}

	public void createCaches() {
		for (CacheEnum ch : CacheEnum.values()) {
			if (cacheManager.getCache(ch.getCacheId(), ch.getKeyClass(), ch.getValueClass()) == null) {
				createCache(ch, ch.getKeyClass(), ch.getValueClass());
				cacheMetricsRegistrar.bindCacheToRegistry(cacheManagerSpring.getCache(ch.getCacheId()));
			}
			else {
				logger.debug("Cache " + ch.getCacheId() + " already exists!");
			}
		}
	}
	
	private <K,V> Cache<K,V> createCache(CacheEnum cache, Class<K> classKey, Class<V> classElem) {
		if (cacheManager.getCachingProvider() instanceof EhcacheCachingProvider) {
			CacheConfigurationBuilder<K,V> cacheConfigurationBuilder = CacheConfigurationBuilder.newCacheConfigurationBuilder(classKey, classElem, ResourcePoolsBuilder.heap(cache.getSize()));
			if (cache.getSecondsExpiry() != null) {
				cacheConfigurationBuilder = cacheConfigurationBuilder.withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(cache.getSecondsExpiry().longValue())));
			}
			CacheEventListenerConfigurationBuilder cacheEventListenerConfiguration = CacheEventListenerConfigurationBuilder
			           .newEventListenerConfiguration(new CustomCacheEventLogger(), EventType.CREATED, EventType.UPDATED, EventType.EVICTED, EventType.EXPIRED, EventType.REMOVED)
			           .unordered().asynchronous(); 
			cacheConfigurationBuilder.withService(cacheEventListenerConfiguration);
			logger.debug("Will create EhCache " + cache);
			return cacheManager.createCache(cache.getCacheId(), Eh107Configuration.fromEhcacheCacheConfiguration(cacheConfigurationBuilder.build()));
		}
		else {
			MutableConfiguration<K, V> configuration = new MutableConfiguration<>();
			configuration = configuration.setTypes(classKey, classElem).setStoreByValue(false);
			if (cache.getSecondsExpiry() != null) {
				configuration = configuration.setExpiryPolicyFactory(CreatedExpiryPolicy.factoryOf(new javax.cache.expiry.Duration(TimeUnit.SECONDS, cache.getSecondsExpiry().longValue())));
			}
			return cacheManager.createCache(cache.getCacheId(), configuration);
		}
	}
	*/
}
