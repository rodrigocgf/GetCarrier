package br.com.supportcomm.cache;

import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.logstash.logback.argument.StructuredArguments.kv;
import static net.logstash.logback.argument.StructuredArguments.v;

public class CustomCacheEventLogger implements CacheEventListener<Object, Object> {
 
    private static final Logger logger = LoggerFactory.getLogger(CustomCacheEventLogger.class);
 
	@Override
	public void onEvent(CacheEvent<?, ?> event) {
        logger.info("Cache {}, {},  {}-->{}", kv("event", event.getType()),
        		kv("key",event.getKey()), v("oldValue",event.getOldValue()), v("newValue",event.getNewValue()));
	}
}