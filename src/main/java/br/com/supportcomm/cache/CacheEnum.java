package br.com.supportcomm.cache;

import br.com.supportcomm.models.db.UserLogin;

public enum CacheEnum {
	CACHE_USER_LOGIN("Cache do login dos usuários", CacheNames.NAME_CACHE_USER_LOGIN, String.class, UserLogin.class, Integer.MAX_VALUE, 60, true),
	;

	private final String description;
	private final String cacheId;
	private final Class<?> keyClass;
	private final Class<?> valueClass;
	private final int size;
	private final Integer secondsExpiry;
	private final boolean canBeCleaned;
	
	private CacheEnum(String description, String cacheId, Class<?> keyClass, Class<?> valueClass, int size, Integer secondsExpiry, boolean canBeCleaned) {
		this.description = description;
		this.cacheId = cacheId;
		this.keyClass = keyClass;
		this.valueClass = valueClass;
		this.size = size;
		this.secondsExpiry = secondsExpiry;
		this.canBeCleaned = canBeCleaned;
	}

    public String getDescription() {
		return description;
	}
	public String getCacheId() {
		return cacheId;
	}

	public Class<?> getKeyClass() {
		return keyClass;
	}

	public Class<?> getValueClass() {
		return valueClass;
	}

	public int getSize() {
		return size;
	}

	public Integer getSecondsExpiry() {
		return secondsExpiry;
	}

	public boolean isCanBeCleaned() {
		return canBeCleaned;
	}
}
