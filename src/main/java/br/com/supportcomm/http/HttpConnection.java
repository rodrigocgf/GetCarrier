package br.com.supportcomm.http;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import br.com.supportcomm.util.XMLUtil;

@Component
public class HttpConnection {
	public Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired RestTemplate restTemplate;
	@Autowired HttpInterceptor httpInterceptor; 
	
	static public final int HTTP_INTERNAL_ERROR = 599;
	
	@FunctionalInterface
	private interface SupplierResponseEntity<T> {
	    public ResponseEntity<T> get() throws RestClientException, URISyntaxException;
	}	
	
	private ResponseEntity<String> generateResult (SupplierResponseEntity<String> supplier) {
		ResponseEntity<String> ret = null;
		try {
			ret = supplier.get();
		} catch (HttpClientErrorException | HttpServerErrorException  e ) {
			logger.error("Error in URI: {}", e.getMessage());
			ret = ResponseEntity.status(e.getRawStatusCode()).body(Objects.toString(e.getResponseBodyAsString(), e.getMessage()));
		} catch (RestClientException | URISyntaxException e) {
			logger.error("Error in URI: " + e, e);
			ret = ResponseEntity.status(HTTP_INTERNAL_ERROR).body(e.toString());
		}
		if (ret != null && ret.getBody() != null) {
			HttpRequestInfo reqInfo = httpInterceptor.getHttpRequestLog();
			if (reqInfo != null) {
				reqInfo.setResponseBuffer(ret.getBody());
			}
		}
		return ret;
	}

	public ResponseEntity<String> sendXML(String url, String xmlString) {		
	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_XML);
	    logger.debug("Vai criar request Xml={}", xmlString);
	    HttpEntity<String> request = new HttpEntity<String>(xmlString, headers);
	    logger.debug("Vai chamar URL={}", url);
		return generateResult(() -> restTemplate.postForEntity(new URI(url), request, String.class));
	}
	
	public <T> ResponseEntity<String> sendInXML(String url, T objToSend) throws IOException {
		return sendXML(url, XMLUtil.getXML(objToSend));
	}
	
	public ResponseEntity<String> sendGet(String url, Map<String,?> uriVariables) {
		if (uriVariables != null) {
			Function<Object,String> urlEncoder = s -> {
				try {
					return URLEncoder.encode(Objects.toString(s, ""), StandardCharsets.UTF_8.toString());
				} catch (UnsupportedEncodingException e) {
					logger.error("Error encoding " + s, e);
					return Objects.toString(s, "");
				}
			};
			boolean hasQuestion = url.indexOf('?') >= 0; 
			url = url + (hasQuestion? "" : "?") + uriVariables.entrySet().stream().map(e -> e.getKey() + "=" + urlEncoder.apply(e.getValue())).collect(Collectors.joining("&")); 
		}
		logger.debug("Vai chamar URL={}", url);
		// Usando a classe URI, pois se passar a String direto, ele ignora tudo depois do caracter # (sem encodar) ou reencoda o %23
		// Além disso, ele pode mudar a ordem dos parâmetros
		String urlFinal = url;
		return generateResult(() -> restTemplate.getForEntity(new URI(urlFinal), String.class));
	}
	
}
