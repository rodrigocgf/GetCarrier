package br.com.supportcomm.http;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

@Component
public class HttpInterceptor implements ClientHttpRequestInterceptor {
	public static Logger logger = LoggerFactory.getLogger(HttpInterceptor.class);
	
	public HttpInterceptor() {
		logger.debug("*****HttpInterceptor Criado: {}", this);
	}
	
	private final static ThreadLocal<HttpRequestInfo> interceptors = new ThreadLocal<>();  
	
	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
		logger.debug("intercept interceptors=" + interceptors + " Thread=" + Thread.currentThread());
		long tickIni = System.currentTimeMillis();
		ClientHttpResponse response = execution.execute(request, body);
		interceptors.set(new HttpRequestInfo(request, body, response, System.currentTimeMillis() - tickIni));
		return response;
	}
	
	public HttpRequestInfo getHttpRequestLog() {
		return interceptors.get();
	}
	
	public HttpRequestInfo getAndClearHttpRequestLog() {
		logger.debug("getAndClearHttpRequestLog: interceptors: {} Thread: {}", interceptors, Thread.currentThread());
		HttpRequestInfo ret = interceptors.get();
		logger.debug("getAndClearHttpRequestLog: ret={}", ret);
		interceptors.set(null);
		return ret;
	}
	
	public void clearHttpRequestLog() {
		interceptors.set(null);
	}
}
