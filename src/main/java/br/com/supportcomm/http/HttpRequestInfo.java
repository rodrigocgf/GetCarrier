package br.com.supportcomm.http;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpResponse;

public class HttpRequestInfo {
	public static Logger logger = LoggerFactory.getLogger(HttpRequestInfo.class);
	private final String requestLog;
	private final HttpRequest request;
	private final byte[] body;
	private final ClientHttpResponse response;
	private final long milliseconds;
	private final int httpStatusCode;
	private String responseBuffer;
	
	public HttpRequestInfo(HttpRequest request, byte[] body, ClientHttpResponse response, long milliseconds) {
		super();
		this.request = request;
		this.body = body;
		this.response = response;
		this.milliseconds = milliseconds;
		int httpStatusCode = 0;
		try {
			httpStatusCode = response.getRawStatusCode();
		}
		catch (IOException e) {
		}
		this.httpStatusCode = httpStatusCode;
		String log = null;
		if (request != null) {
			log = request.getMethodValue() + " " + request.getURI().toString();
			if (body != null) {
				log = log + " body=" + new String(body);
			}
		}
		this.requestLog = log;
		logger.debug("HttpRequestLog Criado");
	}

	public HttpRequestInfo(String requestLog, int httpStatusCode, String responseBuffer, long milliseconds) {
		super();
		this.request = null;
		this.body = null;
		this.requestLog = requestLog;
		this.response = null;
		this.milliseconds = milliseconds;
		this.httpStatusCode = httpStatusCode;
		this.responseBuffer = responseBuffer;
		logger.debug("HttpRequestLog Criado");
	}

	public HttpRequest getRequest() {
		return request;
	}

	public byte[] getBody() {
		return body;
	}

	public ClientHttpResponse getResponse() {
		return response;
	}

	public long getMilliseconds() {
		return milliseconds;
	}
	
	public String getRequestLog() {
		return this.requestLog;
	}

	public int getHttpStatusCode() {
		return httpStatusCode;
	}

	@Override
	public String toString() {
		return "HttpRequestInfo [requestLog=" + requestLog + ", response=" + response
				+ ", milliseconds=" + milliseconds + ", httpStatusCode=" + httpStatusCode + ", responseBuffer="
				+ responseBuffer + "]";
	}

	public String getResponseBuffer() {
		return responseBuffer;
	}

	public void setResponseBuffer(String responseBuffer) {
		this.responseBuffer = responseBuffer;
	}
	
}
