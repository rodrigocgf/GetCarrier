CREATE TABLE IF NOT EXISTS config
(
  name character varying NOT NULL,
  value character varying,
  CONSTRAINT config_pkey PRIMARY KEY (name)
);

CREATE TABLE IF NOT EXISTS user_login
(
  username character varying(50) NOT NULL,
  password character varying(65) NOT NULL,
  enabled boolean NOT NULL,
  role varchar(15) NOT NULL,
  CONSTRAINT user_login_pkey PRIMARY KEY (username)
);
