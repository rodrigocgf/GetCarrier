#!/usr/bin/env python

import sys
import threading
from controller import Controller as Controller
import time
import os

def main(pAbrFullFileName):    

	sCurDir = os.getcwd()
	LogsDir = os.path.join( os.getcwd() , "logs" )
	inputFileDir = os.path.join( os.getcwd(), "cargaEntrada" )
	#sys.stdout.write("Logs directory : " + LogsDir )
	#sys.stdout.write(" ")
	
	thrMain = threading.Thread(target = LoopMain, args=(LogsDir , inputFileDir , pAbrFullFileName, ))
	thrMain.start()	
	
def LoopMain(pLogsDir, pInputFileDir , pAbrFullFileName):
	strInput = ""
	strChoice = ""	
	sys.stdout.write("***************************************************************************\r\n")
	sys.stdout.write("\tLogs directory : " + pLogsDir + "\r\n")
	sys.stdout.write("\tInput file directory : " + pInputFileDir + "\r\n")
	sys.stdout.write("***************************************************************************\r\n")
	
	print("Going to parse " + pAbrFullFileName)	
	
	controller = Controller(pLogsDir, pInputFileDir , pAbrFullFileName)
	#controller.LoadMaps()
	#controller.ParseFile()
	
	while strInput != "quit":
		print("Enter 'quit' to exit : ")
		strInput = sys.stdin.readline()
		strInput = strInput.lstrip().rstrip()		
		
		#if ( str(strInput) == "quit" ):
		#	controller.Stop()
	
	print("****** STOPPED *******")

if __name__ == "__main__":
   sys.exit(main(sys.argv[1]) or 0)