#!/usr/bin/env python

import time
import json
import sys
import websocket
import threading
import queue
import logger
import os
import datetime
import psycopg2
import copy
from area import Area as Area
from pais import Pais as Pais
from operadora import Operadora as Operadora
from numeroportado import NumeroPortado as NumeroPortado

class Controller(object):
	
	def __init__(self, osLogsDir, pInputFileDir , filename):	
		self.OsLogsDir = str(osLogsDir)
		self.filename = filename
		self.inputFileDir = os.path.join(pInputFileDir , filename )
		
		self.log = logger.Logger(self.OsLogsDir, "Controller")		
		self.log.propagate = False
		
		# PROD
		self.conn = psycopg2.connect(host="172.16.0.22",database="db_Portability", user="root", password="S@Ssg8ar?p")
		
		# HOMOL
		#self.conn = psycopg2.connect(host="192.168.3.108",database="db_Portability", user="postgres", password="postgres")
		
		self.cur = self.conn.cursor()
		
		self.notInAreaMapCount = 0
		self.notInAreaList = []
		self.notInCarrierMapCount = 0
		self.notInCarrierList = []
		self.insertedInTable = 0
		
		
		self.linesList = []
		self.seriesList = []
		self.areasList = []
		self.carrierList = []
		self.numeroPortadoList = []
		
		self.areaMap = {}
		self.operadoraMap = {}
		self.spid2operadoraidMap = {}
		
		self.pais = Pais()
		
		self.cur.execute("""SELECT p.paisid, p.version, p.nome, p.ddi, p.prioridade, p.tamanhoDoDdd , p.usaPortabilidade FROM Pais p WHERE p.nome = 'BRASIL'""")
		records = self.cur.fetchall()
		
		for row in records:
			print("row : " + str(row))
			self.pais.paisid = row[0]
			self.pais.version = row[1]
			self.pais.nome = row[2]
			self.pais.ddi = row[3]
			self.pais.prioridade = row[4]
			self.pais.tamanhoDoDdd = row[5]
			self.pais.usaPortabilidade = row[6]
			
		print("Pais id : " + str(self.pais.paisid) )
		
		self.LoadMaps()
		self.ParseFile()
		
	def LoadSeries(self):
		self.cur.execute("""SELECT s FROM Serie s""")
		self.log.info("SELECT s FROM Serie s")
		print("SELECT s FROM Serie s")
		
		
		_count = 0
		for record in self.cur:
			self.seriesList.append( record )
			_count = _count + 1
			#print("serie : " + str(record) )
			
		self.log.info(str(_count) + " series carregadas da tabela serie.")
		print(str(_count) + " series carregadas da tabela serie.")	
		
	
	def LoadAreas(self):
		self.cur.execute("""SELECT a.areaid , a.ddd , a.estadoid, a.paisid FROM Area a""")
		self.log.info("SELECT a.areaid , a.ddd , a.estadoid, a.paisid FROM Area a")
		print("SELECT a.areaid , a.ddd , a.estadoid, a.paisid FROM Area a")
		
		records = self.cur.fetchall()
		
		_count = 0
		for row in records:
			area = Area()
			
			area.areaid = row[0]
			area.ddd = row[1]
			area.estadoid = row[2]
			area.paisid = row[3]
			
			self.areaMap[area.ddd] = area
			_count = _count + 1
	
		self.log.info(str(_count) + " areas carregadas da tabela area.")
		print(str(_count) + " areas carregadas da tabela area.")
	
	def LoadOperadorasMap(self):
		ss = "SELECT o.operadoraid , o.codigo, o.spid, o.nome , o.cnpj, o.version, o.paisid , o.nomenormalizado  FROM "\
		"Operadora o WHERE o.paisid = {:15} ORDER BY o.spid".format(self.pais.paisid)
		self.cur.execute(ss)
		
		self.log.info(ss)
		print(ss)
		
		records = self.cur.fetchall()
		_count = 0
		
		for row in records:
			operadora = Operadora()
			
			operadora.operadoraid = row[0]
			operadora.codigo = row[1]
			operadora.spid = row[2]
			operadora.nome = row[3]
			operadora.cnpj = row[4]
			operadora.version = row[5]
			operadora.paisid = row[6]
			operadora.nomenormalizado = row[7]
			
			self.carrierList.append(operadora)
			
			self.operadoraMap[operadora.operadoraid] = operadora 
			
			if operadora.spid not in self.spid2operadoraidMap.keys():
				#print("spid -> operadoraid : " + str(operadora.spid) + " -> " + str(operadora.operadoraid) )
				#self.log.info("spid -> operadoraid : " + str(operadora.spid) + " -> " + str(operadora.operadoraid) )
				self.spid2operadoraidMap[operadora.spid] = operadora.operadoraid
			
			_count = _count + 1
			
		self.log.info(str(_count) + " operadoras carregadas da tabela operadora.")
		print(str(_count) + " operadoras carregadas da tabela operadora.")
		
	def LoadMaps(self):
		self.log.info("LOADING SERIES ...");
		print("LOADING SERIES ...");	
		self.LoadSeries()
		
		self.log.info("LOADING AREAS ...");
		print("LOADING AREAS ...");
		self.LoadAreas()
		
		self.log.info("LOADING OPERADORAS ...");
		print("LOADING OPERADORAS ...");
		self.LoadOperadorasMap()
	
		
	def ParseFile(self):
		self.log.info("========================================================")
		self.log.info("ParseFile()")
		self.log.info(" ")
		
		print("========================================================")
		print("ParseFile()")
		print(" ")
		
		iNumeroDeRegistros = 0
		self.initTime = datetime.datetime.now()
		
		print('Parsing file ' + self.inputFileDir)
		self.log.info('Parsing file ' + self.inputFileDir)
		
		with open(self.inputFileDir, 'r') as file_input:
			line = " "   
			line = file_input.readline().strip()
			pos1 = line.find(';')
			
			dataDaGeracao = line[:pos1]
			numeroDeRegistros = line[pos1+1:]
			iNumeroDeRegistros = int(numeroDeRegistros)
			count = 2
			print("numero de registros : " + str(numeroDeRegistros ))
			while line:
				line = file_input.readline().strip()
				#print("line : " + line )				
				if ( count < iNumeroDeRegistros ):
					if len(line) > 0:
						self.linesList.append(line)
				
				#time.sleep(0.001)		
				count = count + 1
				
				if ( count%10000 == 0 ):
					print("Numero do Registro : " + str(count) )
					self.log.info("Numero do Registro : " + str(count) )
					
				if ( count == iNumeroDeRegistros + 1 ):
					self.log.info("Total de Registros : " + str(numeroDeRegistros) )
					self.log.info("checksum : " + line)

		self.log.info(self.inputFileDir + ' file parsed.')
		
		self.UpdataDbFromLinesList()
		self.InsertHistorico(iNumeroDeRegistros)
		
		self.endTime = datetime.datetime.now()
		
		print("Total time elapsed : " + str(self.endTime - self.initTime) )
		print("     Records not in Area Map    : " + str(self.notInAreaMapCount) )
		print("     Records not in Carrier Map : " + str(self.notInCarrierMapCount) )
		print("     Records inserted at Table  : " + str(self.insertedInTable) )
		
		self.log.info("Total time elapsed : " + str(self.endTime - self.initTime) )
		self.log.info("     Records not in Area Map    : " + str(self.notInAreaMapCount) )
		self.log.info("     Records not in Carrier Map : " + str(self.notInCarrierMapCount) )
		self.log.info("     Records inserted at Table  : " + str(self.insertedInTable) )
		
		print("     DDD's nao encontrados : ")
		self.log.info("     DDD's nao encontrados : ")
		for strDdd in self.notInAreaList:
			print("        " + strDdd )
			self.log.info("        " + strDdd )
		
		print("     Operadoras's nao encontradas : ")
		self.log.info("     Operadoras's nao encontradas : ")
		for iCarrierNumber in self.notInCarrierList:
			print("        " + str(iCarrierNumber) )		
			self.log.info("        " + str(iCarrierNumber) )
		
		self.cur.close()
		self.conn.close()
	
	def GetOperadoraId(self , spid):
		operadoraid = 0
		if spid in self.spid2operadoraidMap.keys():
			operadoraid = self.spid2operadoraidMap[spid]
		return int(operadoraid)
		
	
	"""
	#DATA DA GERAÇAO; NUMERO DE LINHAS
	20200330180119;32426187
	
	#           ;      ;DDD + NUMERO DO TELEFONE ;      ;     ;SPID     ;
	69          ;0     ;27992555674              ;0     ;0    ;0341     ;427     ;20080904000008;20080904000000
	
	"""
		
		
	def UpdataDbFromLinesList(self):
		self.log.info("========================================================")
		self.log.info("UpdataDbFromLinesList()")
		self.log.info(" ")
		
		#for key, value in self.areaMap.items() :
		#	print(" key ,  value : " + str(key) + " , " + str(value) )
				
		#for key, value in self.spid2operadoraidMap.items() :
		#	print(" spid ->  operadoraid : " + str(key) + " , " + str(value) )
	
		count = 1
		for line in self.linesList:
			#print("linesList line : " + str(line))	
			numeroPortado = NumeroPortado()
		
			fields = line.split(';')
			
			ticketNumber = float(fields[0]);
			ddd = str(fields[2][0: self.pais.tamanhoDoDdd])
			telephoneNumber = float( fields[2][ self.pais.tamanhoDoDdd  : ]  )
			
			spid =  fields[6] 
			if self.RepresentsInt(spid) == False:
				print("spid : " + str(spid) + " is not an integer")
				continue

			operadoraid = self.GetOperadoraId(int(spid))
		

			#print("line : " + line)
			#print("spid : " + str(spid) )
			#print("operadoraid : " + str(operadoraid) )
	
			numeroPortado.numerodobilhete = ticketNumber
			numeroPortado.ddd = ddd
			numeroPortado.numero = telephoneNumber
			numeroPortado.operadoraid = operadoraid
			numeroPortado.version = 6
			
			
			if (int(ddd) in self.areaMap.keys()) and (operadoraid in self.operadoraMap.keys()):
				
				numeroPortado.area = self.areaMap[int(ddd)]
				numeroPortado.operadora = self.operadoraMap[operadoraid]
				
				self.InsertNumberoPortado(numeroPortado)
				
				self.insertedInTable = self.insertedInTable + 1
			else:
				if int(ddd) not in self.areaMap.keys():
					if ddd not in self.notInAreaList:
						self.notInAreaList.append(ddd)
						
					self.notInAreaMapCount = self.notInAreaMapCount + 1
					
				if int(spid) not in self.operadoraMap.keys():
					if spid not in self.notInCarrierList:
						self.notInCarrierList.append(spid)
						
					self.notInCarrierMapCount = self.notInCarrierMapCount + 1
			
			if ( count % 10000 == 0 ):
				self.log.info("line number : " + str(count) )
				print("line number : " + str(count) )
			
			count = count + 1
	
	def InsertNumberoPortado(self , p_numerPortado):
		
		ss = "insert into NumeroPortado (datadeinclusao, datademodificacao, numerodemodificacoes, numerodobilhete, numero , version , operadoraid, areaid , statusupdate , telefonia ) " \
			 " values ( '{0}' , '{1}', {2} , {3} , {4}, {5}, {6}, {7}, {8}, {9} )".format( 
			 p_numerPortado.datadeinclusao,
			 p_numerPortado.datademodificacao , 
			 p_numerPortado.numerodemodificacoes , 
			 p_numerPortado.numerodobilhete ,
			 p_numerPortado.numero , 
			 p_numerPortado.version , 
			 p_numerPortado.operadoraid , 
			 p_numerPortado.area.areaid ,
			 p_numerPortado.statusupdate , 
			 p_numerPortado.telefonia )
		
		#print(ss)
		
		self.cur.execute(ss)
		self.conn.commit()
	
	def InsertHistorico(self, p_numRegistros):
		self.log.info("========================================================")
		self.log.info("InsertHistorico()")
		self.log.info(" ")
		
		ss = "insert into Historico( datadeprocessamento, datadereferencia, cargafull, numeroderegistrosprocessados, arquivo, version )" \
		     " values ( '{0}' , '{1}', {2} , {3}, '{4}', {5} )".format(
			 datetime.datetime.now(),
			 datetime.datetime.now(),
			 1,
			 p_numRegistros,
			 self.filename,
			 1
			 )
		
		self.cur.execute(ss)
		self.conn.commit()
	def RepresentsInt(self , s):
		try: 
			int(s)
			return True
		except ValueError:
			return False	
