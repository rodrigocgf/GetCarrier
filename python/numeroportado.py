import datetime
from operadora import Operadora as Operadora
from area import Area as Area


class NumeroPortado(object):

	def __init__(self):
		self.numeroportadoid = 0.0
		self.datadeinclusao = datetime.datetime.now()
		self.datademodificacao  = datetime.datetime.now()
		self.numerodemodificacoes = 0
		self.numerodobilhete = 0
		self.numero = 0
		self.version = 0
		self.carrierNumber = 0
		self.operadoraid = 0
		self.areaid = 0
		self.statusupdate = 0
		self.telefonia = 2
		